//
//  ViewFullImageVC.m
//  Petlox
//
//  Created by Sumit Sharma on 13/10/15.
//  Copyright (c) 2015 Konstant Infosolutions Pvt Ltd. All rights reserved.
//

#import "ViewFullImageVC.h"
#import "UIImage+GIF.h"


@interface ViewFullImageVC ()
{
    
}
- (IBAction)onCloseAction:(id)sender;
- (IBAction)onSelectAction:(id)sender;
@end

@implementation ViewFullImageVC


-  (void)viewDidLoad
{
    [super viewDidLoad];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        UIImage *animatedImage = [UIImage sd_animatedGIFWithData:self.gifImageData];
        UIImageView *imageView = [self.view viewWithTag:201];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [imageView setImage:animatedImage];
        });
    });
    
    [[UIApplication sharedApplication]setStatusBarHidden:NO withAnimation:UIStatusBarAnimationFade];
    [[UIApplication sharedApplication]setStatusBarStyle:UIStatusBarStyleLightContent];
}

- (IBAction)onCloseAction:(id)sender {
    [(UIActivityIndicatorView *)[self.view viewWithTag:100] stopAnimating];
    _gifImageData = nil;
    UIImageView *imageView = [self.view viewWithTag:201];
    imageView.image = nil;
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)onSelectAction:(id)sender {
    
    [(UIActivityIndicatorView *)[self.view viewWithTag:100] stopAnimating];
    [NSUSERDEFAULTS setObject:_gifImageData forKey:@"selectedGif"];
    
//    [self dismissViewControllerAnimated:YES completion:nil];
    
    [[NSNotificationCenter defaultCenter]postNotificationName:@"kGoToPostScreen" object:nil];

}
@end
