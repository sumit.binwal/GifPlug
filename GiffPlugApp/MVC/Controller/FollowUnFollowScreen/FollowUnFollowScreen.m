//
//  FollowUnFollowScreen.m
//  GiffPlugApp
//
//  Created by Sumit Sharma on 29/01/16.
//  Copyright © 2016 Kshitij Godara. All rights reserved.
//

#import "FollowUnFollowScreen.h"
#import "FollowUnfollowCustomeCell.h"
#import "LoginVC.h"
#import "OtherUserProfileVC.h"
#import "GuidedContentScreen.h"
#import "ResponsiveLabel.h"
#import "WebViewVC.h"
#import "AppDataManager.h"
@interface FollowUnFollowScreen ()<UITableViewDataSource,UITableViewDelegate,UIGestureRecognizerDelegate,CustomTableViewCellDelegate>
{
    IBOutlet UITableView *tblVwFollwers;
    __weak IBOutlet UILabel *lblErrormsg;
//    NSMutableArray *arrSelectedIndexPath;
    UITapGestureRecognizer *tapGesture;
    
    float scaleX;
    AppDataManager *dataManager;
}
@property (strong, nonatomic) NSMutableArray *expandedIndexPaths;
@end

@implementation FollowUnFollowScreen
@synthesize arrFollowersData;
- (void)viewDidLoad {
    [super viewDidLoad];
    
    dataManager = [AppDataManager sharedInstance];
    
    scaleX =[UIScreen mainScreen].bounds.size.width/320;
    
    tapGesture=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(labelBtnClicked)];
    tapGesture.delegate=self;
    
//    [tblVwFollwers registerNib:[UINib nibWithNibName:NSStringFromClass([FollowUnfollowCustomeCell class]) bundle:nil] forCellReuseIdentifier:@"FollowUnfollowCustomeCell"];
    
    NSArray *viewControllers = self.navigationController.viewControllers;
    
    UIViewController *rootViewController = (UIViewController *)[viewControllers objectAtIndex:0];
    
    NSLog(@"%@",rootViewController);
    
//    arrSelectedIndexPath = [[NSMutableArray alloc]init];
    
    if ([rootViewController isKindOfClass:[LoginVC class]]) {
        if([CommonFunction reachabiltyCheck])
        {
            [CommonFunction showActivityIndicatorWithText:@""];
            [self getFollowersListAPI];
        }
        else
        {
            [CommonFunction alertTitle:@"" withMessage:@"Check your network connection."];
        }
    }
    
    // Do any additional setup after loading the view from its nib.
}
-(void)labelBtnClicked
{
    NSLog(@"fsdkjlfhsda");
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[UIApplication sharedApplication]setStatusBarHidden:NO withAnimation:UIStatusBarAnimationFade];
    [[UIApplication sharedApplication]setStatusBarStyle:UIStatusBarStyleLightContent];
    [tblVwFollwers reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableView Delegate Method
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrFollowersData.count;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
  //  float multiplierHeight = [UIScreen mainScreen].bounds.size.height/568.0f;
//    return 70.0f *multiplierHeight;
        return 70.0f;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier=@"FollowUnfollowCustomeCell";
    
    FollowUnfollowCustomeCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (!cell)
    {
        cell = [[NSBundle mainBundle]loadNibNamed:NSStringFromClass([FollowUnfollowCustomeCell class]) owner:self options:nil][0];
        cell.delegate=self;
    }
    
    
    for (UIGestureRecognizer *gesture in cell.imageViewProfile.gestureRecognizers)
    {
        [cell.imageViewProfile removeGestureRecognizer:gesture];
    }
    
    UITapGestureRecognizer *tappGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(profileBtnCLicked:)];
    tappGesture.numberOfTapsRequired=1;
    [cell.imageViewProfile addGestureRecognizer:tappGesture];
    cell.imageViewProfile.tag = indexPath.row;
    
    cell.btnFollowUnfollow.tag=indexPath.row;
    [cell.btnFollowUnfollow addTarget:self action:@selector(plusMinusBtnCLicked:) forControlEvents:UIControlEventTouchUpInside];
    
//    cell.imgProfileImg.tag=indexPath.row;
//    [cell.imgProfileImg addTarget:self action:@selector(profileBtnCLicked:) forControlEvents:UIControlEventTouchUpInside];
    
    if (arrFollowersData.count>0) {
        lblErrormsg.text=@"";
        NSString *strAvtarimg=[NSString stringWithFormat:@"%@",[[arrFollowersData objectAtIndex:indexPath.row]valueForKey:@"avatar_image"]];
        
        if (strAvtarimg.length>1) {
            
//            __weak typeof(cell) weakCell = cell;
            [cell.imageViewProfile sd_setImageWithURL:[NSURL URLWithString:strAvtarimg] placeholderImage:[UIImage imageNamed:@"default.jpg"] options:0 completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                if (error)
                {
                    cell.imageViewProfile.contentMode=UIViewContentModeScaleToFill;
                }
                else
                {
                    if ([[strAvtarimg pathExtension]isEqualToString:@"gif"])
                    {
                        cell.imageViewProfile.contentMode=UIViewContentModeScaleAspectFill;
                    }
                    else
                    {
                        cell.imageViewProfile.contentMode=UIViewContentModeScaleToFill;
                    }
                }
            }];
//            [cell.imgProfileImg setImage:nil forState:UIControlStateNormal];
//            [cell.imgProfileImg sd_setImageWithURL:[NSURL URLWithString:strAvtarimg] forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"default.jpg"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
//                if (error) {
//                    NSLog(@"eror loading gif profile pic");
//                }
//                [weakCell setNeedsLayout];
////                NSData *data = UIImageJPEGRepresentation(image, 1.0f);
////                [cell.imgProfileImg setImage:[UIImage imageWithData:data] forState:UIControlStateNormal];
//            }];
        }
        else
        {
//            [cell.imgProfileImg setImage:[UIImage imageNamed:@"default.jpg"] forState:UIControlStateNormal];
            [cell.imageViewProfile setImage:[UIImage imageNamed:@"default.jpg"]];
        }
        
        if ([[dataManager.followersDataArray valueForKey:@"user_id"]containsObject:arrFollowersData[indexPath.row][@"id"]])
        {
            NSInteger index = [[dataManager.followersDataArray valueForKey:@"user_id"]indexOfObject:arrFollowersData[indexPath.row][@"id"] ];
            BOOL requestAcess = [dataManager.followersDataArray[index][@"request_status"]boolValue];
            if (requestAcess) {
                [cell.btnFollowUnfollow setImage:[UIImage imageNamed:@"FUFS_MinusIcon"] forState:UIControlStateNormal];
                cell.btnFollowUnfollow.hidden = NO;
                cell.labelRequested.hidden = YES;
            }
            else
            {
                cell.btnFollowUnfollow.hidden = YES;
                cell.labelRequested.hidden = NO;
            }
        }
        else
        {
            BOOL isFollowing = [arrFollowersData [indexPath.row][@"is_following"]boolValue];
            if (isFollowing)
            {
                BOOL isRequestAccepted = [arrFollowersData[indexPath.row][@"request_status"] boolValue];
                if (isRequestAccepted) {
                    [cell.btnFollowUnfollow setImage:[UIImage imageNamed:@"FUFS_MinusIcon"] forState:UIControlStateNormal];
                    cell.btnFollowUnfollow.hidden = NO;
                    cell.labelRequested.hidden = YES;
                }
                else
                {
                    cell.btnFollowUnfollow.hidden = YES;
                    cell.labelRequested.hidden = NO;
                }
            }
            else
            {
                cell.btnFollowUnfollow.hidden = NO;
                cell.labelRequested.hidden = YES;
                [cell.btnFollowUnfollow setImage:[UIImage imageNamed:@"FUFS_PlusIcon"] forState:UIControlStateNormal];
            }
        }
        
        
        if ([[dataManager.followersDataArray valueForKey:@"user_id"] containsObject:[[arrFollowersData objectAtIndex:indexPath.row] objectForKey:@"id"]])
        {
            [cell.btnFollowUnfollow setImage:[UIImage imageNamed:@"FUFS_MinusIcon"] forState:UIControlStateNormal];
        }
        else
        {
            [cell.btnFollowUnfollow setImage:[UIImage imageNamed:@"FUFS_PlusIcon"] forState:UIControlStateNormal];
        }
        
        cell.lblUsername.text=[[arrFollowersData objectAtIndex:indexPath.row]valueForKey:@"username"];
        cell.lblUserBio.text=[NSString stringWithFormat:@"%@",[[arrFollowersData objectAtIndex:indexPath.row]valueForKey:@"bio"]];
        [cell configureText:cell.lblUserBio.text forExpandedState:[self.expandedIndexPaths containsObject:indexPath]];
    }else
    {
        lblErrormsg.text=@"No User Found.";
    }
    
    return cell;
    
}


/*
- (void)tableView:(UITableView *)tableView willDisplayCell:(FollowUnfollowCustomeCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    cell.btnFollowUnfollow.tag=indexPath.row;
    [cell.btnFollowUnfollow addTarget:self action:@selector(plusMinusBtnCLicked:) forControlEvents:UIControlEventTouchUpInside];
    
    cell.imgProfileImg.tag=indexPath.row;
    [cell.imgProfileImg addTarget:self action:@selector(profileBtnCLicked:) forControlEvents:UIControlEventTouchUpInside];
    
    if (arrFollowersData.count>0) {
        lblErrormsg.text=@"";
        NSString *strAvtarimg=[NSString stringWithFormat:@"%@",[[arrFollowersData objectAtIndex:indexPath.row]valueForKey:@"avatar_image"]];
        
        if (strAvtarimg.length>1) {
            
            [cell.imgProfileImg sd_setImageWithURL:[NSURL URLWithString:strAvtarimg] forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"default.jpg"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                if (error) {
                    NSLog(@"eror loading gif profile pic");
                }
                [cell.imgProfileImg setImage:[UIImage imageNamed:@"LaunchImage"] forState:UIControlStateNormal];
            }];
        }
        else
        {
            [cell.imgProfileImg setImage:[UIImage imageNamed:@"default.jpg"] forState:UIControlStateNormal];
        }
        
        
        if ([[dataManager.followersDataArray valueForKey:@"user_id"]containsObject:arrFollowersData[indexPath.row][@"id"]])
        {
            NSInteger index = [[dataManager.followersDataArray valueForKey:@"user_id"]indexOfObject:arrFollowersData[indexPath.row][@"id"] ];
            BOOL requestAcess = [dataManager.followersDataArray[index][@"request_status"]boolValue];
            if (requestAcess) {
                [cell.btnFollowUnfollow setImage:[UIImage imageNamed:@"FUFS_MinusIcon"] forState:UIControlStateNormal];
                cell.btnFollowUnfollow.hidden = NO;
                cell.labelRequested.hidden = YES;
            }
            else
            {
                cell.btnFollowUnfollow.hidden = YES;
                cell.labelRequested.hidden = NO;
            }
        }
        else
        {
            BOOL isFollowing = [arrFollowersData [indexPath.row][@"is_following"]boolValue];
            if (isFollowing)
            {
                BOOL isRequestAccepted = [arrFollowersData[indexPath.row][@"request_status"] boolValue];
                if (isRequestAccepted) {
                    [cell.btnFollowUnfollow setImage:[UIImage imageNamed:@"FUFS_MinusIcon"] forState:UIControlStateNormal];
                    cell.btnFollowUnfollow.hidden = NO;
                    cell.labelRequested.hidden = YES;
                }
                else
                {
                    cell.btnFollowUnfollow.hidden = YES;
                    cell.labelRequested.hidden = NO;
                }
            }
            else
            {
                cell.btnFollowUnfollow.hidden = NO;
                cell.labelRequested.hidden = YES;
                [cell.btnFollowUnfollow setImage:[UIImage imageNamed:@"FUFS_PlusIcon"] forState:UIControlStateNormal];
            }
        }
        
        
        if ([[dataManager.followersDataArray valueForKey:@"user_id"] containsObject:[[arrFollowersData objectAtIndex:indexPath.row] objectForKey:@"id"]])
        {
            [cell.btnFollowUnfollow setImage:[UIImage imageNamed:@"FUFS_MinusIcon"] forState:UIControlStateNormal];
        }
        else
        {
            [cell.btnFollowUnfollow setImage:[UIImage imageNamed:@"FUFS_PlusIcon"] forState:UIControlStateNormal];
        }
        
        cell.lblUsername.text=[[arrFollowersData objectAtIndex:indexPath.row]valueForKey:@"username"];
        cell.lblUserBio.text=[NSString stringWithFormat:@"%@",[[arrFollowersData objectAtIndex:indexPath.row]valueForKey:@"bio"]];
        [cell configureText:cell.lblUserBio.text forExpandedState:[self.expandedIndexPaths containsObject:indexPath]];
    }else
    {
        lblErrormsg.text=@"No User Found.";
    }
}*/


- (IBAction)gifPlugBtnClicked:(id)sender {
    
    if ([CommonFunction reachabiltyCheck]) {
//        [CommonFunction showActivityIndicatorWithText:@""];
//        [self postFollowersListAPI];
        [dataManager.followersDataArray removeAllObjects];
        [CommonFunction changeRootViewController:YES];
    }
    else
    {
        [CommonFunction alertTitle:@"" withMessage:@"Please check your network connection"];
    }
}
#pragma mark - IBACtionButtonMethods
-(IBAction)profileBtnCLicked:(UITapGestureRecognizer *)sender
{
    OtherUserProfileVC *oupvc=[[OtherUserProfileVC alloc]initWithNibName:@"OtherUserProfileVC" bundle:nil];
    oupvc.hidesBottomBarWhenPushed=YES;
    oupvc.strUserName=arrFollowersData [sender.view.tag][@"username"];
    //    [self presentViewController:oupvc animated:YES completion:nil];
    [self.navigationController pushViewController:oupvc animated:YES];
}

-(IBAction)plusMinusBtnCLicked:(UIButton *)sender
{
    
    FollowUnfollowCustomeCell *tableViewCell = (FollowUnfollowCustomeCell *)sender.superview.superview;

    tableViewCell.btnFollowUnfollow.hidden = YES;
    tableViewCell.loader.hidden = NO;
    [tableViewCell.loader startAnimating];
    
    NSMutableDictionary *dataDict=[[NSMutableDictionary alloc]initWithObjectsAndKeys:[[arrFollowersData objectAtIndex:sender.tag]valueForKey:@"id"],@"user_id",[[arrFollowersData objectAtIndex:sender.tag]valueForKey:@"account_type"],@"account_type", nil];
    
    if ([[dataManager.followersDataArray valueForKey:@"user_id"] containsObject:[[arrFollowersData objectAtIndex:sender.tag] objectForKey:@"id"]])
    {
        [self unFollowUserApiAtIndex:sender.tag WithObject:dataDict];
    }
    else
    {
        [self postFollowersListAPIAtIndex:sender.tag WithObject:dataDict];
    }
}


#pragma mark - GetFollowersList API
-(void)getFollowersListAPI
{
    
    NSString *url = [NSString stringWithFormat:@"users/findByCategory"];
    //http://192.168.0.22:8353/v1/users/findByCategory
    
    NSMutableDictionary *header =[[NSMutableDictionary alloc]initWithObjectsAndKeys:@"application/json",@"Content-Type",[NSUSERDEFAULTS valueForKey:kUSERTOKEN],@"User-Token", nil];
    
    NSMutableDictionary *param=[[NSMutableDictionary alloc]initWithObjectsAndKeys:[NSUSERDEFAULTS objectForKey:kFOLLOWINGCATEGORY],@"categories", nil];
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypePost withHttpHeaders:header withServiceName:url withParameters:param withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"responce dict %@",responseDict);
        
        [CommonFunction removeActivityIndicator];
        if(responseDict==Nil)        {
            [CommonFunction alertTitle:@"" withMessage:@"Server Error"];
        }
        else if([operation.response statusCode]==200)
        {
            arrFollowersData=[responseDict objectForKey:@"users"];
            if (!(arrFollowersData.count>0)) {
                lblErrormsg.text=@"No User Found.";
            }
            [tblVwFollwers reloadData];
            for (NSInteger i=0; i<arrFollowersData.count; i++)
            {
                if ([[dataManager.followersDataArray valueForKey:@"user_id"]containsObject:arrFollowersData[i][@"id"]])
                {
                    NSInteger index = [[dataManager.followersDataArray valueForKey:@"user_id"]indexOfObject:arrFollowersData[i][@"id"]];
                    BOOL requestAcess = [arrFollowersData[i][@"request_status"] boolValue];
                    [dataManager.followersDataArray[index] setValue:[NSNumber numberWithBool:requestAcess] forKey:@"request_status"];
                }
            }
        }
        else
        {
            [CommonFunction alertTitle:@"" withMessage:[responseDict objectForKey:@"msg"]];
        }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          if([operation.response statusCode]  == 400 ){
                                              NSLog(@"impo response %@",operation.response);
                                              
                                              [CommonFunction removeActivityIndicator];
                                              [CommonFunction alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                              
                                              
                                          }
                                          else{
                                              
                                              if (wbServiceCount==1) {
                                                  wbServiceCount=0;
                                                  [CommonFunction showActivityIndicatorWithText:@""];
                                                  //      [self loginAPI];
                                              }
                                              else
                                              {
                                                  [CommonFunction removeActivityIndicator];
                                                  [CommonFunction alertTitle:@"" withMessage:@"Network Error..."];
                                              }
                                              
                                          }
                                          
                                      }];
}

-(void)postFollowersListAPIAtIndex:(NSInteger)index WithObject:(NSDictionary *)dataDict
{
    NSString *url = [NSString stringWithFormat:@"users/following"];
    //http://192.168.0.22:8353/v1/users/following
    
    NSMutableDictionary *header =[[NSMutableDictionary alloc]initWithObjectsAndKeys:@"application/json",@"Content-Type",[NSUSERDEFAULTS valueForKey:kUSERTOKEN],@"User-Token", nil];
    
    NSMutableDictionary *param=[[NSMutableDictionary alloc]initWithObjectsAndKeys:@[dataDict],@"following", nil];
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypePost withHttpHeaders:header withServiceName:url withParameters:param withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        FollowUnfollowCustomeCell *tableViewCell = (FollowUnfollowCustomeCell *)[tblVwFollwers cellForRowAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0]];
        
        tableViewCell.btnFollowUnfollow.hidden = NO;
        tableViewCell.loader.hidden = YES;
        [tableViewCell.loader stopAnimating];
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"responce dict %@",responseDict);
        
        [CommonFunction removeActivityIndicator];
        if(responseDict==Nil)        {
            [CommonFunction alertTitle:@"" withMessage:@"Server Error"];
        }
        else if([operation.response statusCode]==200)
        {
            [NSUSERDEFAULTS setObject:@"1" forKey:kISFOLLOWINGSUBMITTED];
            [NSUSERDEFAULTS synchronize];
            
            
            NSMutableDictionary *newDict = [NSMutableDictionary dictionaryWithDictionary:dataDict];
            if ([arrFollowersData[index][@"account_type"] isEqualToString:@"private"])
            {
                [newDict setValue:[NSNumber numberWithBool:NO] forKey:@"request_status"];
            }
            else
            {
                [newDict setValue:[NSNumber numberWithBool:YES] forKey:@"request_status"];
            }
            if (dataManager.followersDataArray) {
                if ([[dataManager.followersDataArray valueForKey:@"user_id"]containsObject:newDict[@"user_id"]])
                {
                    NSInteger index = [[dataManager.followersDataArray valueForKey:@"user_id"]indexOfObject:newDict[@"user_id"]];
                    [dataManager.followersDataArray removeObjectAtIndex:index];
                }
                [dataManager.followersDataArray addObject:newDict];
            }
            [tblVwFollwers reloadData];
            NSLog(@"************* FOLLOW *************");
        }
        else
        {
            [CommonFunction alertTitle:@"" withMessage:[responseDict objectForKey:@"msg"]];
        }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          
                                          FollowUnfollowCustomeCell *tableViewCell = (FollowUnfollowCustomeCell *)[tblVwFollwers cellForRowAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0]];
                                          tableViewCell.btnFollowUnfollow.hidden = NO;
                                          tableViewCell.loader.hidden = YES;
                                          [tableViewCell.loader stopAnimating];
                                          
                                          if([operation.response statusCode]  == 400 ){
                                              NSLog(@"impo response %@",operation.response);
                                              
                                              [CommonFunction removeActivityIndicator];
                                              [CommonFunction alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                              
                                              
                                          }
                                          else{
                                              
                                              if (wbServiceCount==1) {
                                                  wbServiceCount=0;
                                                  [CommonFunction showActivityIndicatorWithText:@""];
                                                  //      [self loginAPI];
                                              }
                                              else
                                              {
                                                  [CommonFunction removeActivityIndicator];
                                                  [CommonFunction alertTitle:@"" withMessage:@"Network Error..."];
                                              }
                                              
                                          }
                                          
                                      }];
}


- (void)unFollowUserApiAtIndex:(NSInteger)index WithObject:(NSDictionary *)dataDict
{
    NSString *url = [NSString stringWithFormat:@"users/following/%@",dataDict[@"user_id"]];
    //http://192.168.0.22:8353/v1//users/following/{userId}
    
    NSMutableDictionary *header =[[NSMutableDictionary alloc]initWithObjectsAndKeys:@"application/json",@"Content-Type",[NSUSERDEFAULTS valueForKey:kUSERTOKEN],@"User-Token", nil];
    
    //    NSMutableDictionary *param=[[NSMutableDictionary alloc]initWithObjectsAndKeys:strUserId,@"userId", nil];
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypeDelete withHttpHeaders:header withServiceName:url withParameters:nil withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        FollowUnfollowCustomeCell *tableViewCell = (FollowUnfollowCustomeCell *)[tblVwFollwers cellForRowAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0]];
        
        tableViewCell.btnFollowUnfollow.hidden = NO;
        tableViewCell.loader.hidden = YES;
        [tableViewCell.loader stopAnimating];
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"responce dict %@",responseDict);
        
        [CommonFunction removeActivityIndicator];
        [CommonFunction removeActivityIndicatorWithWait];
        if(responseDict==Nil)        {
            [CommonFunction alertTitle:@"" withMessage:@"Server Error"];
            
        }
        else if([operation.response statusCode]==200)
        {
            NSMutableDictionary *newDict = [NSMutableDictionary dictionaryWithDictionary:dataDict];
            
            [newDict setValue:[NSNumber numberWithBool:NO] forKey:@"request_status"];
            
            if (dataManager.followersDataArray) {
                if ([[dataManager.followersDataArray valueForKey:@"user_id"]containsObject:newDict[@"user_id"]])
                {
                    NSInteger index = [[dataManager.followersDataArray valueForKey:@"user_id"]indexOfObject:newDict[@"user_id"]];
                    [dataManager.followersDataArray removeObjectAtIndex:index];
                }
            }
            
            if (dataManager.followersDataArray.count==0)
            {
                [NSUSERDEFAULTS setObject:@"0" forKey:kISFOLLOWINGSUBMITTED];
                [NSUSERDEFAULTS synchronize];
            }
            [tblVwFollwers reloadData];
            
            NSLog(@"************* UNFOLLOW *************");
        }
        else
        {
            [CommonFunction alertTitle:@"" withMessage:[responseDict objectForKey:@"msg"]];
        }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          
                                          FollowUnfollowCustomeCell *tableViewCell = (FollowUnfollowCustomeCell *)[tblVwFollwers cellForRowAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0]];
                                          tableViewCell.btnFollowUnfollow.hidden = NO;
                                          tableViewCell.loader.hidden = YES;
                                          [tableViewCell.loader stopAnimating];
                                          
                                          if([operation.response statusCode]  == 400 ){
                                              NSLog(@"impo response %@",operation.response);
                                              
                                              [CommonFunction removeActivityIndicator];
                                              [CommonFunction alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                          }
                                          else{
                                              
                                              if (wbServiceCount==1) {
                                                  wbServiceCount=0;
                                                  [CommonFunction showActivityIndicatorWithText:@""];
                                                  //      [self loginAPI];
                                              }
                                              else
                                              {
                                                  [CommonFunction removeActivityIndicator];
                                                  [CommonFunction alertTitle:@"" withMessage:@"Network Error..."];
                                              }
                                              
                                          }
                                          
                                      }];
}


- (void)customTableViewCell:(FollowUnfollowCustomeCell *)cell didTapOnURL:(NSString *)urlString {

    WebViewVC *wvc=[[WebViewVC alloc]init];
    if (![urlString containsString:@"http"])
    {
        urlString = [NSString stringWithFormat:@"http://%@",urlString];
    }
    
        wvc.strWbUrl=urlString;
        [self.navigationController pushViewController:wvc animated:YES];
}

- (void)showAlertWithMessage:(NSString *)message {
    UIAlertController *controller = [UIAlertController alertControllerWithTitle:@"Message" message:message preferredStyle:UIAlertControllerStyleAlert];
    [self presentViewController:controller animated:YES completion:nil];
    UIAlertAction *action = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        [controller dismissViewControllerAnimated:YES completion:nil];
    }];
    [controller addAction:action];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
