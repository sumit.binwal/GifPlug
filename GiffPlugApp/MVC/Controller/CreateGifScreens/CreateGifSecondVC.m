//
//  CreateGifSecondVC.m
//  GiffPlugApp
//
//  Created by Kshitij Godara on 11/01/16.
//  Copyright © 2016 Kshitij Godara. All rights reserved.
//



#import "CreateGifSecondVC.h"
#import "FilteredCollectionCell.h"
#import <objc/runtime.h>

@interface CreateGifSecondVC ()
{

//    NSMutableArray *filterNameArray;
    NSMutableArray *selectedFilterArr;
    NSMutableArray *filteredImageArray;
    NSMutableArray *filteredThumbnailArr;
    
    
    NSMutableArray *normalGiffArray;
    
    BOOL isBrightness;
    BOOL isSpeed;
    BOOL isfilterApplied;
    float animationDuration;
    
    float giffBrightness;
    
    
    
    NSMutableArray *brightenedGiff;
    
    BOOL disableFilterButtons;
    
    CGFloat delayDuration;
}
@property (strong, nonatomic)CIFilter *vigentaFilter;
@property (strong, nonatomic)CIFilter *photoFilter;
@property (strong, nonatomic)CIFilter *sepiaFilter;
@property (strong, nonatomic)CIFilter *brightnessFilter;
@property (strong, nonatomic) IBOutlet UICollectionReusableView *collectionHeaderView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *progressBarHeightConstraint;
@property (weak, nonatomic) IBOutlet UIProgressView *progressBar;

@end

@implementation CreateGifSecondVC


#pragma mark - View Life Cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //Set Navigation Properties
    
    disableFilterButtons = NO;
    [self setNavigationProperties];
    
    [self initiateFilters];
    
    
    [_bsSlider setHidden:true];
    [_bsSlider setThumbImage:[UIImage imageNamed:@"brightnessIcon"] forState:UIControlStateNormal];
    [_sliderSpeed setThumbImage:[UIImage imageNamed:@"speedIcon"] forState:UIControlStateNormal];
    
    //Default value is going to be == 1.0
    isfilterApplied = false;
    
    delayDuration = 0.1;
    
    animationDuration = _giffPartsImgArray.count*delayDuration;
    giffBrightness = _bsSlider.value;
    
//    normalGiffArray = [_giffPartsImgArray copy];
    
    
    filteredThumbnailArr = [[NSMutableArray alloc] init];
    
    selectedFilterArr = [[NSMutableArray alloc] init];
    
//    filterNameArray = [[NSMutableArray alloc] init];
    
    filteredImageArray = [[NSMutableArray alloc]init];
    filteredImageArray = [_giffPartsImgArray mutableCopy];
    
    
    _progressBarHeightConstraint.constant = 4.0f*(SCREEN_WIDTH/320);
    
    if ([NSUSERDEFAULTS objectForKey:@"progressValue"]) {
        float progresValue = [[NSUSERDEFAULTS objectForKey:@"progressValue"] floatValue];
        
        _progressBar.progress = progresValue;
    }
    else
    {
        _progressBar.progress = 0;
    }
    [_filterCollectionView registerClass:[UICollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"HeaderView"];
    
    [_filterCollectionView registerClass:[UICollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"FooterView"];
    
    [_filterCollectionView registerClass:[FilteredCollectionCell class] forCellWithReuseIdentifier:@"filterCustomCell"];
    //Populate Bottom Filter
    
    {
  
        
        for (int i =0;i<4;i++)
        {
            switch (i) {
                case 0:
                {
//                    [filterNameArray addObject:@"NORMAL"];
                    [selectedFilterArr addObject:[NSNumber numberWithInt:1]];
                    [filteredThumbnailArr addObject:@{@"image":_thumbnailSampleImg, @"name":@"NORMAL"}];
//                    [filteredThumbnailArr addObject:[_thumbnailSampleImg copy]];
                    [_filterCollectionView reloadData];
                   // [_filterCollectionView insertItemsAtIndexPaths:[NSArray arrayWithObjects:[NSIndexPath indexPathForItem:0 inSection:0], nil]];
                }
                    break;
                case 1:
                {
                  
                    [self applySepiaFilter:_thumbnailSampleImg order:@1 completeion:^(UIImage *img, NSNumber *orderNumber) {
//                        [filterNameArray addObject:@"SEPIA"];
                        [selectedFilterArr addObject:[NSNumber numberWithInt:0]];
//                        [filteredThumbnailArr addObject:img];
                        [filteredThumbnailArr addObject:@{@"image":img, @"name":@"PRIME"}];
                        [_filterCollectionView reloadData];
                    }];
                }
                    break;
                case 2:
                {
                 
                    [self applyPhotoEffectFilter:_thumbnailSampleImg order:@1 completeion:^(UIImage *img, NSNumber *orderNumber) {
//                        [filterNameArray addObject:@"PHOTO"];
                        [selectedFilterArr addObject:[NSNumber numberWithInt:0]];
//                        [filteredThumbnailArr addObject:img];
                        [filteredThumbnailArr addObject:@{@"image":img, @"name":@"FIFTY"}];
                        [_filterCollectionView reloadData];
                    }];
                }
                    break;
                case 3:
                {
                    [self applyVignetteFilter:_thumbnailSampleImg order:@1 completeion:^(UIImage *img, NSNumber *orderNumber) {
//                        [filterNameArray addObject:@"VIGNETTE"];
                        [selectedFilterArr addObject:[NSNumber numberWithInt:0]];
//                        [filteredThumbnailArr addObject:img];
                        [filteredThumbnailArr addObject:@{@"image":img, @"name":@"V-80"}];
                        [_filterCollectionView reloadData];
                    }];
                }
                    break;
                    
                default:
                    break;
            }
        }
    
    }
    
    

    
    
    [self animateGiffParts:nil];
    
    // Do any additional setup after loading the view.
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    self.view.userInteractionEnabled=NO;
    [self animateGiffParts:nil];
}

- (void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    self.view.userInteractionEnabled=YES;
    if (isBrightness)
    {
        [self animateGiffParts:brightenedGiff];
    }
    else
    {
        [self animateGiffParts:filteredImageArray];
    }
}

- (void)touchesCancelled:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    self.view.userInteractionEnabled=YES;
    if (isBrightness)
    {
        [self animateGiffParts:brightenedGiff];
    }
    else
    {
        [self animateGiffParts:filteredImageArray];
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[UIApplication sharedApplication]setStatusBarHidden:YES withAnimation:UIStatusBarAnimationFade];
    [(UIButton *)[self.view viewWithTag:500] setEnabled:YES];
    if (globalCIContext == nil)
    {
        EAGLContext *glContext = [[EAGLContext alloc]initWithAPI:kEAGLRenderingAPIOpenGLES3];
        if (glContext == nil)
        {
            glContext = [[EAGLContext alloc]initWithAPI:kEAGLRenderingAPIOpenGLES2];
        }
        NSDictionary *options = @{ kCIContextWorkingColorSpace : [NSNull null] };
        globalCIContext = [CIContext contextWithEAGLContext:glContext options:options];
    }
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    globalCIContext = nil;
}

//-(void)viewDidDisappear:(BOOL)animated
//{
//    
//    [super viewDidDisappear:animated];
//    
//    [_giffImageView stopAnimating];
//    filteredImageArray = nil;
//    brightenedGiff = nil;
//    _giffPartsImgArray = nil;
//    selectedFilterArr = nil;
//    filteredThumbnailArr = nil;
//    filterNameArray = nil;
//    
//}

- (void)dealloc
{
    self.giffImageView.image = nil;
}

#pragma mark - Navigation Properties

-(void)setNavigationProperties
{
    
    [CommonFunction hideNavigationBarFromController:self];
    
}

- (void)initiateFilters
{
    if (globalCIContext == nil)
    {
        EAGLContext *glContext = [[EAGLContext alloc]initWithAPI:kEAGLRenderingAPIOpenGLES3];
        if (glContext == nil)
        {
            glContext = [[EAGLContext alloc]initWithAPI:kEAGLRenderingAPIOpenGLES2];
        }
        NSDictionary *options = @{ kCIContextWorkingColorSpace : [NSNull null] };
        globalCIContext = [CIContext contextWithEAGLContext:glContext options:options];
    }
    self.sepiaFilter = [CIFilter filterWithName:@"CIColorControls"];
    [self.sepiaFilter setDefaults];
    [self.sepiaFilter setValue:@(1.7) forKey:kCIInputSaturationKey];
    [self.sepiaFilter setValue:@(1.2) forKey:kCIInputContrastKey];
    
    self.brightnessFilter = [CIFilter filterWithName:@"CIColorControls"];
    [self.brightnessFilter setDefaults];
    
    self.photoFilter = [CIFilter filterWithName:@"CIPhotoEffectTonal"];
    [self.photoFilter setDefaults];
    
    self.vigentaFilter = [CIFilter filterWithName:@"CIColorControls"];
    [self.vigentaFilter setDefaults];
    [self.vigentaFilter setValue:@(0.76) forKey:kCIInputSaturationKey];
    
    CGFloat t = -.37 * .13;
    CGFloat newB = 0.13*t;
    [self.vigentaFilter setValue:@(newB) forKey:kCIInputBrightnessKey];

}

#pragma mark - hide status bar

- (BOOL)prefersStatusBarHidden
{
    
    return YES;
}

#pragma mark - Memory Managment

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



#pragma mark - Navigation Actions

- (IBAction)backBtnPressed:(id)sender
{
    
    [self.navigationController popViewControllerAnimated:YES];
    
}

- (IBAction)nextBtnPressed:(id)sender
{
    [(UIButton *)[self.view viewWithTag:500] setEnabled:NO];
    
    if (self.isOpenForProfilePurpose)
    {
        if (isBrightness)
        {
            [[NSNotificationCenter defaultCenter]postNotificationName:@"gifSelected" object:@{@"image":brightenedGiff, @"duration":[NSNumber numberWithFloat:animationDuration]}];
        }
        else
        {
            [[NSNotificationCenter defaultCenter]postNotificationName:@"gifSelected" object:@{@"image":filteredImageArray, @"duration":[NSNumber numberWithFloat:animationDuration]}];
        }
        [self.navigationController dismissViewControllerAnimated:YES completion:nil];
    }
    else{
        PostGiffScreen *pgs = [[PostGiffScreen alloc] initWithNibName:@"PostGiffScreen" bundle:nil];
        
        if (isBrightness)
        {
            pgs.giffImagePartsArray = [brightenedGiff copy];
        }
        else
        {
            pgs.giffImagePartsArray = [filteredImageArray copy];
        }
        
        pgs.animationDuration = animationDuration;
        
        [self.navigationController pushViewController:pgs animated:YES];
    }
}

#pragma mark - Filter Collection View


-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    
    return filteredThumbnailArr.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"filterCustomCell";

   FilteredCollectionCell *cell =  [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];

    cell.filterLabel.text = filteredThumbnailArr[indexPath.row][@"name"];
    [cell.filteredImageView setImage:filteredThumbnailArr[indexPath.row][@"image"]];
    cell.filteredImageView.layer.cornerRadius = 5; // this value vary as per your desire
    cell.filteredImageView.clipsToBounds = YES;
    
    [CommonFunction setRoundedView:cell.smallIndicatorView toDiameter:cell.smallIndicatorView.frame.size.width];
    if ([[selectedFilterArr objectAtIndex:indexPath.row] integerValue]==1) {
        
        
        cell.filterLabel.textColor = [UIColor whiteColor];
        
        [cell.bottomIndicatorBar setHidden:false];
        [cell.smallIndicatorView setHidden:false];
    }
    else
    {
        cell.filterLabel.textColor = [UIColor colorWithRed:1.0 green:33.0/255.0 blue:87/255.0 alpha:1.0];
        
        [cell.bottomIndicatorBar setHidden:true];
        [cell.smallIndicatorView setHidden:true];
        
    }

    

    
    return cell;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
   
    
    UICollectionReusableView *reusableview = nil;
    
    if (kind == UICollectionElementKindSectionHeader)
    {
        UICollectionReusableView *headerView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"HeaderView" forIndexPath:indexPath];
      
        [headerView setFrame:CGRectMake(0, 0,(35*SCREEN_WIDTH)/375,(106*SCREEN_HEIGHT)/667)];

        
        reusableview = headerView;
    }
    
    if (kind == UICollectionElementKindSectionFooter) {
        UICollectionReusableView *footerview = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"FooterView" forIndexPath:indexPath];
        [footerview setFrame:CGRectMake(0, 0,(35*SCREEN_WIDTH)/375,(106*SCREEN_HEIGHT)/667)];
        reusableview = footerview;
    }
    
    return reusableview;
    
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return (21*SCREEN_WIDTH)/375;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return (21*SCREEN_WIDTH)/375;
}

// Layout: Set Edges
- (UIEdgeInsets)collectionView:
(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    // return UIEdgeInsetsMake(0,8,0,8);  // top, left, bottom, right
    return UIEdgeInsetsMake(0,0,0,0);  // top, left, bottom, right
}


- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout*)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
  
    return CGSizeMake((60*SCREEN_WIDTH)/375,(106*SCREEN_HEIGHT)/667);
}


#pragma mark - Delegate Methods

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    for (int i =0;i<selectedFilterArr.count;i++) {
        
        if (i==indexPath.row)
        {
            [selectedFilterArr replaceObjectAtIndex:i withObject:[NSNumber numberWithInt:1]];
            
        }
        else
        {
            [selectedFilterArr replaceObjectAtIndex:i withObject:[NSNumber numberWithInt:0]];
        }
        
    }
    if (disableFilterButtons)
    {
        return;
    }
    isBrightness = NO;
    disableFilterButtons = YES;
    [self processFilterOnGiff:filteredThumbnailArr[indexPath.row][@"name"]];
    [collectionView reloadData];
}

#pragma mark - Animated Images

-(void)animateGiffParts:(NSArray *)imageArray
{
    NSMutableArray *animatedArray = [NSMutableArray array];
    
    if (!imageArray)
    {
        for (NSDictionary *dict in _giffPartsImgArray)
        {
            UIImage *image = (UIImage *)dict[@"image"];
            [animatedArray addObject:image];
        }
    }
    else
    {
        imageArray = [imageArray sortedArrayUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"order" ascending:YES]]];
        for (NSDictionary *dict in imageArray)
        {
            UIImage *image = (UIImage *)dict[@"image"];
            [animatedArray addObject:image];
        }
    }
    
    UIImage *animatedImage = [UIImage animatedImageWithImages:animatedArray duration:animationDuration];
    [self.giffImageView setImage:animatedImage];

    
//    NSMutableArray *newArray = [NSMutableArray array];
//    if (!imageArray)
//    {
//        for (NSDictionary *dict in _giffPartsImgArray)
//        {
//            UIImage *image = (UIImage *)dict[@"image"];
//            [newArray addObject:(id)image.CGImage];
//        }
//    }
//    else
//    {
//        imageArray = [imageArray sortedArrayUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"order" ascending:YES]]];
//        for (NSDictionary *dict in imageArray)
//        {
//            UIImage *image = (UIImage *)dict[@"image"];
//            [newArray addObject:(id)image.CGImage];
//        }
//    }
//    
//    [self.giffImageView.layer removeAnimationForKey:@"contents"];
//    CAKeyframeAnimation *animation = [CAKeyframeAnimation animationWithKeyPath:@"contents"];
//    animation.calculationMode = kCAAnimationDiscrete;
//    animation.duration = animationDuration;
//    animation.values = newArray;
//    animation.repeatCount = HUGE_VALF;
//    animation.removedOnCompletion = NO;
//    animation.fillMode = kCAFillModeForwards;
//    [self.giffImageView.layer addAnimation:animation forKey:@"contents"];
//    @synchronized(_giffImageView)
//    {
//        if (imageArray==nil)
//        {
//            _giffImageView.animationImages = (NSArray *)_giffPartsImgArray;
//        }
//        else
//        {
//            _giffImageView.animationImages = imageArray;
//        }
//        _giffImageView.animationDuration = animationDuration;
//        _giffImageView.animationRepeatCount = 0;
//        [_giffImageView startAnimating];
//    }
}


#pragma mark - Document Directory

-(void)getImagesFromDocumentDirectory
{
    
}

#pragma mark - Apply Filters

-(void)applySepiaFilter:(UIImage *)originalImage order:(NSNumber *)orderNumber completeion:(void(^)(UIImage* img,NSNumber *orderNumber)) completion
{
//    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        @autoreleasepool {
            NSNumber *order = orderNumber;
//            UIImage *compressedImage = nil;
            CIImage *image = [[CIImage alloc]initWithImage:originalImage];
            [self.sepiaFilter setValue:image forKey:@"inputImage"];
//            [self.sepiaFilter setValue:[NSNumber numberWithFloat:1.0f] forKey:@"inputIntensity"];
            CIImage *outputImage = [self.sepiaFilter outputImage];
            
            CIFilter *newFilter = [CIFilter filterWithName:@"CIColorControls"];
            [newFilter setValue:outputImage forKey:@"inputImage"];
//            [newFilter setValue:@(1.7) forKey:kCIInputSaturationKey];
            [newFilter setValue:@(1.2) forKey:kCIInputContrastKey];
//            [newFilter setValue:@(0.08) forKey:kCIInputBrightnessKey];
            
            outputImage = newFilter.outputImage;
            
            [self.brightnessFilter setValue:outputImage forKey:@"inputImage"];
            [self.brightnessFilter setValue:[NSNumber numberWithFloat:giffBrightness] forKey:kCIInputBrightnessKey];
            
            CIImage *finalOutputImage = [self.brightnessFilter outputImage];
            
            CGImageRef imgRef=[globalCIContext createCGImage:finalOutputImage fromRect:finalOutputImage.extent];
            UIImage *filteredImage = [UIImage imageWithCGImage:imgRef];
//            NSData *data = UIImageJPEGRepresentation(filteredImage, 0.1);
//            compressedImage = [UIImage imageWithData:data];
            CFRelease(imgRef);
            
//            dispatch_async(dispatch_get_main_queue(), ^{
                completion(filteredImage,order);
//            });
        }
//    });
}

-(void)applyPhotoEffectFilter:(UIImage *)originalImage order:(NSNumber *)orderNumber completeion:(void(^)(UIImage* img,NSNumber *orderNumber)) completion
{
//    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
    
        @autoreleasepool {
            NSNumber *order = orderNumber;
//            UIImage *compressedImage = nil;
            CIImage *image = [[CIImage alloc]initWithImage:originalImage];
            [self.photoFilter setValue:image forKey:@"inputImage"];
            CIImage *outputImage = [self.photoFilter outputImage];
            
            [self.brightnessFilter setValue:outputImage forKey:@"inputImage"];
            [self.brightnessFilter setValue:[NSNumber numberWithFloat:giffBrightness] forKey:kCIInputBrightnessKey];
            
            CIImage *finalOutputImage = [self.brightnessFilter outputImage];
            
            CGImageRef imgRef=[globalCIContext createCGImage:finalOutputImage fromRect:finalOutputImage.extent];
            UIImage *filteredImage = [UIImage imageWithCGImage:imgRef];
//            NSData *data = UIImageJPEGRepresentation(filteredImage, 0.1);
//            compressedImage = [UIImage imageWithData:data];
            CFRelease(imgRef);
            
//            dispatch_async(dispatch_get_main_queue(), ^{
                completion(filteredImage,order);
//            });
        }
//    });
}

-(void)applyVignetteFilter:(UIImage *)originalImage order:(NSNumber *)orderNumber completeion:(void(^)(UIImage* img,NSNumber *orderNumber)) completion
{
//    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        @autoreleasepool {
            NSNumber *order = orderNumber;
            CIImage *image = [[CIImage alloc]initWithImage:originalImage];
            [self.vigentaFilter setValue:image forKey:@"inputImage"];

            CIImage *outputImage = [self.vigentaFilter outputImage];
            
            CIFilter *filterEV = [CIFilter filterWithName:@"CINoiseReduction"];
            [filterEV setDefaults];
            [filterEV setValue:@(0.02+0.02) forKey:@"inputNoiseLevel"];
            [filterEV setValue:@(3) forKey:@"inputSharpness"];
            [filterEV setValue:outputImage forKey:@"inputImage"];
//            [filterEV setValue:@(1.197) forKey:kCIInputEVKey];
            outputImage = [filterEV outputImage];
            
            [self.brightnessFilter setValue:outputImage forKey:@"inputImage"];
            [self.brightnessFilter setValue:[NSNumber numberWithFloat:giffBrightness] forKey:kCIInputBrightnessKey];
            
            CIImage *finalOutputImage = [self.brightnessFilter outputImage];
            
            CGImageRef imgRef=[globalCIContext createCGImage:finalOutputImage fromRect:finalOutputImage.extent];
            UIImage *filteredImage = [UIImage imageWithCGImage:imgRef];
//            NSData *data = UIImageJPEGRepresentation(filteredImage, 0.1);
//            compressedImage = [UIImage imageWithData:data];
            CFRelease(imgRef);
            
//            dispatch_async(dispatch_get_main_queue(), ^{
                completion(filteredImage,order);
//            });
        }
//    });
}

-(void)applyColorControl:(UIImage *)originalImage order:(NSNumber *)orderNumber withBrightness:(float)brightness completeion:(void(^)(UIImage* img,NSNumber *orderNumber)) completion
{
//    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        @autoreleasepool {
            NSNumber *order = orderNumber;
//            UIImage *compressedImage = nil;
            CIImage *image = [[CIImage alloc]initWithImage:originalImage];
            [self.brightnessFilter setValue:image forKey:@"inputImage"];
            [self.brightnessFilter setValue:[NSNumber numberWithFloat:brightness] forKey:kCIInputBrightnessKey];
            
            CIImage *finalOutputImage = [self.brightnessFilter outputImage];
            
            CGImageRef imgRef=[globalCIContext createCGImage:finalOutputImage fromRect:finalOutputImage.extent];
            UIImage *filteredImage = [UIImage imageWithCGImage:imgRef];
//            NSData *data = UIImageJPEGRepresentation(filteredImage, 0.1);
//            compressedImage = [UIImage imageWithData:data];
            CFRelease(imgRef);

//            dispatch_async(dispatch_get_main_queue(), ^{
                    completion(filteredImage,order);
//            });
        }
//    });
}


#pragma mark - Process Filter on Giff Parts

-(void)processFilterOnGiff:(NSString *)filterName
{
    brightenedGiff = nil;
    if ([filterName isEqualToString:@"NORMAL"]) {
        isfilterApplied = false;
        
        if (filteredImageArray)
        {
            [filteredImageArray removeAllObjects];
        }
        
        for (NSDictionary *dict in _giffPartsImgArray) {
            [self applyColorControl:dict[@"image"] order:dict[@"order"] withBrightness:giffBrightness completeion:^(UIImage *img, NSNumber *orderNumber) {
                [filteredImageArray addObject:@{@"image":img, @"order":orderNumber}];
                if (filteredImageArray.count>=_giffPartsImgArray.count)
                {
                    disableFilterButtons = NO;
                    [self animateGiffParts:filteredImageArray];
                }
            }];
        }
    }
    else if ([filterName isEqualToString:@"PRIME"]) {

        isfilterApplied = true;
        
        if (filteredImageArray)
        {
            [filteredImageArray removeAllObjects];
        }
        
        for (NSDictionary *dict in _giffPartsImgArray) {
            [self applySepiaFilter:dict[@"image"] order:dict[@"order"] completeion:^(UIImage *img, NSNumber *orderNumber) {
                [filteredImageArray addObject:@{@"image":img, @"order":orderNumber}];
                if (filteredImageArray.count>=_giffPartsImgArray.count)
                {
                    disableFilterButtons = NO;
                    [self animateGiffParts:filteredImageArray];
                }
            }];
        }
    }
    else if ([filterName isEqualToString:@"FIFTY"]) {
        
        //Photo Effect
        isfilterApplied = true;
        if (filteredImageArray)
        {
            [filteredImageArray removeAllObjects];
        }
        
        for (NSDictionary *dict in _giffPartsImgArray) {
            [self applyPhotoEffectFilter:dict[@"image"] order:dict[@"order"] completeion:^(UIImage *img, NSNumber *orderNumber) {
                [filteredImageArray addObject:@{@"image":img, @"order":orderNumber}];
                if (filteredImageArray.count>=_giffPartsImgArray.count)
                {
                    disableFilterButtons = NO;
                    [self animateGiffParts:filteredImageArray];
                }
            }];
        }
    }
    else if ([filterName isEqualToString:@"V-80"]) {
        
        //Vigentta
        
        isfilterApplied = true;
        
        if (filteredImageArray)
        {
            [filteredImageArray removeAllObjects];
        }
        
        for (NSDictionary *dict in _giffPartsImgArray) {
            [self applyVignetteFilter:dict[@"image"] order:dict[@"order"] completeion:^(UIImage *img, NSNumber *orderNumber) {
                [filteredImageArray addObject:@{@"image":img, @"order":orderNumber}];
                if (filteredImageArray.count>=_giffPartsImgArray.count)
                {
                    disableFilterButtons = NO;
                    [self animateGiffParts:filteredImageArray];
                }
            }];
        }
    }
}


#pragma mark - Slider Values

- (IBAction)sliderValueChanged:(id)sender
{
    NSLog(@"_bs slider value %f",_bsSlider.value);
    
    isBrightness = YES;
    
    giffBrightness = _bsSlider.value;
    
    if (_bsSlider.value==0) {
        
        [self animateGiffParts:filteredImageArray];
    }
    else
    {
        if (brightenedGiff)
        {
            [brightenedGiff removeAllObjects],brightenedGiff=nil;
        }
        brightenedGiff = [[NSMutableArray alloc] init];
        
//        if (isfilterApplied) {
        
            for (NSDictionary *dict in filteredImageArray) {
                [self applyColorControl:dict[@"image"] order:dict[@"order"] withBrightness:giffBrightness completeion:^(UIImage *img, NSNumber *orderNumber) {
                    [brightenedGiff addObject:@{@"image":img, @"order":orderNumber}];
                    if (brightenedGiff.count>=filteredImageArray.count)
                    {
//                        [filteredImageArray removeAllObjects];
//                        [filteredImageArray addObjectsFromArray:brightenedGiff];
//                        [brightenedGiff removeAllObjects];
                        [self animateGiffParts:brightenedGiff];
                    }
                }];
            }
//        }
//        else
//        {
//            for (NSDictionary *dict in _giffPartsImgArray) {
//                [self applyColorControl:dict[@"image"] order:dict[@"order"] withBrightness:giffBrightness completeion:^(UIImage *img, NSNumber *orderNumber) {
//                    [brightenedGiff addObject:@{@"image":img, @"order":orderNumber}];
//                    if (brightenedGiff.count>=_giffPartsImgArray.count)
//                    {
//                        [self animateGiffParts:brightenedGiff];
//                    }
//                }];
//            }
//        }
    }
}

- (IBAction)sliderTouchInside:(id)sender
{
    
}

#pragma mark -
#pragma mark Speed Slider Value
- (IBAction)onSpeedSliderValueChanged:(id)sender {
    
    UISlider *slider = (UISlider*)sender;
    delayDuration = [slider value];
    NSLog(@"Printing delay: %f",delayDuration);
    
    CGFloat reverseDelay = (slider.minimumValue+slider.maximumValue) - delayDuration;
    
    animationDuration = reverseDelay * _giffPartsImgArray.count;
    
    if (isBrightness)
    {
        [self animateGiffParts:brightenedGiff];
    }
    else
    {
        [self animateGiffParts:filteredImageArray];
    }
}

#pragma mark -
#pragma mark On Brightness Button Action
- (IBAction)brightnessBtnPressed:(id)sender
{
    
    if (!_sliderSpeed.isHidden)
    {
        _sliderSpeed.hidden = YES;
        [_speedBtn setBackgroundImage:[UIImage imageNamed:@"speedIcon"] forState:UIControlStateNormal];
    }
    
    if (!_bsSlider.isHidden)
    {
        _bsSlider.hidden = YES;
        [_brightnessBtn setBackgroundImage:[UIImage imageNamed:@"brightnessIcon"] forState:UIControlStateNormal];
        return;
    }
    
    _bsSlider.hidden = NO;
    [_brightnessBtn setBackgroundImage:[UIImage imageNamed:@"brightnessIconActive"] forState:UIControlStateNormal];
    _bsSlider.value = giffBrightness;
    
//    isSpeed = false;
//    [_speedBtn setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
//
//    if (isBrightness)
//    {
//        //.5x 1x 1.5x 2x 3x 4x
//        
//        [_brightnessBtn setBackgroundImage:[UIImage imageNamed:@"brightnessIcon"] forState:UIControlStateNormal];
//       // [_brightnessBtn setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
//        [_bsSlider setHidden:true];
//        isBrightness = false;
//    }
//    else
//    {
//         //[_brightnessBtn setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
//        [_speedBtn setBackgroundImage:[UIImage imageNamed:@"speedIcon"] forState:UIControlStateNormal];
//        [_brightnessBtn setBackgroundImage:[UIImage imageNamed:@"brightnessIconActive"] forState:UIControlStateNormal];
//        [_bsSlider setThumbImage:[UIImage imageNamed:@"brightnessIcon"] forState:UIControlStateNormal];
//        
//        _bsSlider.continuous = NO;
//        
//        [_bsSlider setMinimumValue:-25];
//        [_bsSlider setMaximumValue:25];
//        
//        [_bsSlider setValue:giffBrightness];
//        
//      
//        
//        
//        
//        [_bsSlider setHidden:false];
//        isBrightness = true;
//    }
}
-(void)saveImage:(NSData *)imageData withImageName:(NSString*)imageName
{
    
    NSError *error;
    NSFileManager *fileManager = [NSFileManager defaultManager];//create instance of NSFileManager
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES); //create an array and store result of our search for the documents directory in it
    
    NSString *documentsDirectory = [paths objectAtIndex:0]; //create NSString object, that holds our exact path to the documents directory
    
    
    
    NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"/%@",kAPPFOLDERNAME]];
    
    NSString *folderPath = [dataPath stringByAppendingPathComponent:[NSString stringWithFormat:@"/%@",kGIFFPARTSFOLDER]];
    if (![[NSFileManager defaultManager] fileExistsAtPath:folderPath])
    {
        [[NSFileManager defaultManager] createDirectoryAtPath:dataPath withIntermediateDirectories:NO attributes:nil error:&error];
        
    }
    
    
    
    [[NSFileManager defaultManager] createDirectoryAtPath:folderPath withIntermediateDirectories:NO attributes:nil error:&error]; //Create folder
    
    
    NSString *fullPath = [folderPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpeg", imageName]]; //add our image to the path
    
    [fileManager createFileAtPath:fullPath contents:imageData attributes:nil]; //finally save the path (image)
    
}

#pragma mark - Speed Processesing

- (IBAction)speedBtnPressed:(id)sender
{

//   [CommonFunction alertTitle:@"" withMessage:@"This will be in next release."];
    if (!_bsSlider.isHidden)
    {
        _bsSlider.hidden = YES;
        [_brightnessBtn setBackgroundImage:[UIImage imageNamed:@"brightnessIcon"] forState:UIControlStateNormal];
    }
    
    if (!_sliderSpeed.isHidden)
    {
        _sliderSpeed.hidden = YES;
        [_speedBtn setBackgroundImage:[UIImage imageNamed:@"speedIcon"] forState:UIControlStateNormal];
        return;
    }
    
    _sliderSpeed.hidden = NO;
    [_speedBtn setBackgroundImage:[UIImage imageNamed:@"speedIconActive"] forState:UIControlStateNormal];
    _sliderSpeed.value = delayDuration;
}


@end

//@interface UISlider (adjust)
//
//- (CGRect)cat_thumbRectForBounds:(CGRect)bounds trackRect:(CGRect)rect value:(float)value;
//
//
//@end
//
//@implementation UISlider (adjust)
//
//+ (void)load
//{
//    static dispatch_once_t once_token;
//    dispatch_once(&once_token,  ^{
//        SEL oldThumbRect = @selector(thumbRectForBounds:trackRect:value:);
//        SEL newThumbRect = @selector(cat_thumbRectForBounds:trackRect:value:);
//        Method originalMethod = class_getInstanceMethod(self, oldThumbRect);
//        Method extendedMethod = class_getInstanceMethod(self, newThumbRect);
//        method_exchangeImplementations(originalMethod, extendedMethod);
//    });
//}
//
//- (CGRect)cat_thumbRectForBounds:(CGRect)bounds trackRect:(CGRect)rect value:(float)value
//{
//    if (self.tag == 202)
//    {
//        return CGRectOffset([self cat_thumbRectForBounds:bounds trackRect:rect value:value], 30*value, 0);
//    }
//    if (value<0.2)
//    {
//        return CGRectOffset([self cat_thumbRectForBounds:bounds trackRect:rect value:value], -10, 0);
//    }
//    return CGRectOffset([self cat_thumbRectForBounds:bounds trackRect:rect value:value], 10, 0);
//
////    return CGRectInset([self cat_thumbRectForBounds:bounds trackRect:rect value:value], -30, 0);
//}
//
//@end


