//
//  LikeVC.m
//  GiffPlugApp
//
//  Created by Sumit Sharma on 09/03/16.
//  Copyright © 2016 Kshitij Godara. All rights reserved.
//

#import "ReplugVC.h"
#import "FollowUnfollowCustomeCell.h"
#import "OtherUserProfileVC.h"
#import "AppDataManager.h"
@interface ReplugVC ()
{
    IBOutlet UITableView *tblVw;
    NSMutableArray *arrFollowersData;
    NSMutableArray *expandedIndexPaths;
    NSMutableArray *arrSelectedIndexPath;
    IBOutlet UILabel *lblLikeCount;
    IBOutlet UILabel *lblErrorMsg;
    
    NSInteger currentPage, totalPage;
    BOOL isPageRefreshing;
    AppDataManager *dataManager;
}
@end

@implementation ReplugVC
@synthesize strPostId,strLikeCount;
- (void)viewDidLoad {
    [super viewDidLoad];
    
    dataManager = [AppDataManager sharedInstance];
    
    arrFollowersData=[[NSMutableArray alloc]init];
    
    arrSelectedIndexPath=[[NSMutableArray alloc]init];
    lblLikeCount.text=strLikeCount;
    
    
    currentPage =1;
    // Do any additional setup after loading the view from its nib.
    
//    [tblVw registerNib:[UINib nibWithNibName:NSStringFromClass([FollowUnfollowCustomeCell class]) bundle:nil] forCellReuseIdentifier:@"FollowUnfollowCustomeCell"];
    
//    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
//    refreshControl.attributedTitle = [[NSAttributedString alloc] initWithString:@"Please Wait..."]; //to give the attributedTitle
//    [refreshControl addTarget:self action:@selector(refresh:) forControlEvents:UIControlEventValueChanged];
//    [tblVw addSubview:refreshControl];
    
}


- (void)refresh:(UIRefreshControl *)refreshControl
{
    if ([CommonFunction reachabiltyCheck]) {
        currentPage = 1;
        totalPage = 0;
        [self getPostLikes];
    }
    else
    {
        [CommonFunction alertTitle:@"" withMessage:@"Please check your network connection"];
    }
//    [refreshControl endRefreshing];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillAppear:(BOOL)animated
{
    [self.navigationController.navigationBar setHidden:YES];
    
    if ([CommonFunction reachabiltyCheck]) {
        currentPage = 1;
        totalPage = 0;
        [self getPostLikes];
    }
    else
    {
        [CommonFunction alertTitle:@"" withMessage:@"Please check your network connection"];
    }
    
    [super viewWillAppear:animated];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
//    [APPDELEGATE.appTabBarController.tabBar setHidden:NO];
}
#pragma mark - IBAction Button Methods
- (IBAction)backBtnClicked:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
-(IBAction)profileBtnCLicked:(UITapGestureRecognizer *)sender
{
    NSString *loggedInUser = [NSUSERDEFAULTS objectForKey:kUSERID];
    if ([loggedInUser isEqualToString:arrFollowersData [sender.view.tag][@"username"]])
    {
        
    }
    else
    {
        OtherUserProfileVC *oupvc=[[OtherUserProfileVC alloc]initWithNibName:@"OtherUserProfileVC" bundle:nil];
        oupvc.hidesBottomBarWhenPushed=YES;
        oupvc.strUserName=arrFollowersData [sender.view.tag][@"username"];
        [self.navigationController pushViewController:oupvc animated:YES];
    }
}

-(IBAction)plusMinusBtnCLicked:(UIButton *)sender
{
    FollowUnfollowCustomeCell *tableViewCell = (FollowUnfollowCustomeCell *)sender.superview.superview;
    
    tableViewCell.btnFollowUnfollow.hidden = YES;
    tableViewCell.loader.hidden = NO;
    [tableViewCell.loader startAnimating];
    
    NSMutableDictionary *dataDict=[[NSMutableDictionary alloc]initWithObjectsAndKeys:[[arrFollowersData objectAtIndex:sender.tag]valueForKey:@"user_id"],@"user_id",[[arrFollowersData objectAtIndex:sender.tag]valueForKey:@"account_type"],@"account_type", nil];
    
    if ([[arrFollowersData[sender.tag][@"is_following"]stringValue] isEqualToString:@"0"]) {
        [self postFollowersListAPIAtIndex:sender.tag WithObject:dataDict];
    }
    else
    {
        [self unFollowUserApiAtIndex:sender.tag WithObject:dataDict];
    }
}

-(void)postFollowersListAPIAtIndex:(NSInteger)index WithObject:(NSDictionary *)dataDict
{
    [CommonFunction showActivityIndicatorWithText:@""];
    NSString *url = [NSString stringWithFormat:@"users/following"];
    //http://192.168.0.22:8353/v1/users/following
    
    NSMutableDictionary *header =[[NSMutableDictionary alloc]initWithObjectsAndKeys:@"application/json",@"Content-Type",[NSUSERDEFAULTS valueForKey:kUSERTOKEN],@"User-Token", nil];
    
    NSMutableDictionary *param=[[NSMutableDictionary alloc]initWithObjectsAndKeys:@[dataDict],@"following", nil];
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypePost withHttpHeaders:header withServiceName:url withParameters:param withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        FollowUnfollowCustomeCell *tableViewCell = (FollowUnfollowCustomeCell *)[tblVw cellForRowAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0]];
        
        tableViewCell.btnFollowUnfollow.hidden = NO;
        tableViewCell.loader.hidden = YES;
        [tableViewCell.loader stopAnimating];
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"responce dict %@",responseDict);
        
        [CommonFunction removeActivityIndicator];
        if(responseDict==Nil)        {
            [CommonFunction alertTitle:@"" withMessage:@"Server Error"];
        }
        else if([operation.response statusCode]==200)
        {
            NSMutableDictionary *newDict = [NSMutableDictionary dictionaryWithDictionary:dataDict];
            [arrFollowersData[index] setValue:@1 forKey:@"is_following"];
            if ([arrFollowersData[index][@"account_type"] isEqualToString:@"private"])
            {
                [newDict setValue:[NSNumber numberWithBool:NO] forKey:@"request_status"];
                [arrFollowersData[index] setValue:[NSNumber numberWithBool:NO] forKey:@"request_status"];
            }
            else
            {
                [newDict setValue:[NSNumber numberWithBool:YES] forKey:@"request_status"];
                [arrFollowersData[index] setValue:[NSNumber numberWithBool:YES] forKey:@"request_status"];
            }
            if (dataManager.followersDataArray) {
                if ([[dataManager.followersDataArray valueForKey:@"user_id"]containsObject:newDict[@"user_id"]])
                {
                    NSInteger index = [[dataManager.followersDataArray valueForKey:@"user_id"]indexOfObject:newDict[@"user_id"]];
                    [dataManager.followersDataArray removeObjectAtIndex:index];
                }
                [dataManager.followersDataArray addObject:newDict];
            }
        }
        else
        {
            [CommonFunction alertTitle:@"" withMessage:[responseDict objectForKey:@"msg"]];
        }
        [tblVw reloadData];
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          [CommonFunction removeActivityIndicator];

                                          FollowUnfollowCustomeCell *tableViewCell = (FollowUnfollowCustomeCell *)[tblVw cellForRowAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0]];
                                          tableViewCell.btnFollowUnfollow.hidden = NO;
                                          tableViewCell.loader.hidden = YES;
                                          [tableViewCell.loader stopAnimating];
                                          
                                          if([operation.response statusCode]  == 400 ){
                                              [CommonFunction alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                          }
                                          else{
                                              
                                              if (wbServiceCount==1) {
                                                  wbServiceCount=0;
                                              }
                                              else
                                              {
                                                  [CommonFunction alertTitle:@"" withMessage:@"Network Error..."];
                                              }
                                          }
                                          [tblVw reloadData];
                                      }];
}


- (void)unFollowUserApiAtIndex:(NSInteger)index WithObject:(NSDictionary *)dataDict
{
    [CommonFunction showActivityIndicatorWithText:@""];
    NSString *url = [NSString stringWithFormat:@"users/following/%@",dataDict[@"user_id"]];
    //http://192.168.0.22:8353/v1//users/following/{userId}
    
    NSMutableDictionary *header =[[NSMutableDictionary alloc]initWithObjectsAndKeys:@"application/json",@"Content-Type",[NSUSERDEFAULTS valueForKey:kUSERTOKEN],@"User-Token", nil];
    
    //    NSMutableDictionary *param=[[NSMutableDictionary alloc]initWithObjectsAndKeys:strUserId,@"userId", nil];
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypeDelete withHttpHeaders:header withServiceName:url withParameters:nil withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        FollowUnfollowCustomeCell *tableViewCell = (FollowUnfollowCustomeCell *)[tblVw cellForRowAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0]];
        
        tableViewCell.btnFollowUnfollow.hidden = NO;
        tableViewCell.loader.hidden = YES;
        [tableViewCell.loader stopAnimating];
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"responce dict %@",responseDict);
        
        [CommonFunction removeActivityIndicator];
        if(responseDict==Nil)        {
            [CommonFunction alertTitle:@"" withMessage:@"Server Error"];
            
        }
        else if([operation.response statusCode]==200)
        {
            NSMutableDictionary *newDict = [NSMutableDictionary dictionaryWithDictionary:dataDict];
            [arrFollowersData[index] setValue:@0 forKey:@"is_following"];
            
            [newDict setValue:[NSNumber numberWithBool:NO] forKey:@"request_status"];
            [arrFollowersData[index] setValue:[NSNumber numberWithBool:NO] forKey:@"request_status"];
            
            if (dataManager.followersDataArray) {
                if ([[dataManager.followersDataArray valueForKey:@"user_id"]containsObject:newDict[@"user_id"]])
                {
                    NSInteger index = [[dataManager.followersDataArray valueForKey:@"user_id"]indexOfObject:newDict[@"user_id"]];
                    [dataManager.followersDataArray removeObjectAtIndex:index];
                }
            }
        }
        else
        {
            [CommonFunction alertTitle:@"" withMessage:[responseDict objectForKey:@"msg"]];
        }
        [tblVw reloadData];
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          [CommonFunction removeActivityIndicator];

                                          FollowUnfollowCustomeCell *tableViewCell = (FollowUnfollowCustomeCell *)[tblVw cellForRowAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0]];
                                          tableViewCell.btnFollowUnfollow.hidden = NO;
                                          tableViewCell.loader.hidden = YES;
                                          [tableViewCell.loader stopAnimating];
                                          
                                          if([operation.response statusCode]  == 400 ){
                                              [CommonFunction alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                          }
                                          else{
                                              
                                              if (wbServiceCount==1) {
                                                  wbServiceCount=0;
                                              }
                                              else
                                              {
                                                  [CommonFunction alertTitle:@"" withMessage:@"Network Error..."];
                                              }
                                          }
                                          [tblVw reloadData];
                                      }];
}



#pragma mark - UITableView Delegate Method
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
        return arrFollowersData.count;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{

    return 70.0f*SCREEN_YScale;
    
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier=@"FollowUnfollowCustomeCell";
    
    FollowUnfollowCustomeCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (!cell)
    {
        cell = [[NSBundle mainBundle]loadNibNamed:NSStringFromClass([FollowUnfollowCustomeCell class]) owner:self options:nil][0];
    }
    
    return cell;
    
}



- (void)tableView:(UITableView *)tableView willDisplayCell:(FollowUnfollowCustomeCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    for (UIGestureRecognizer *gesture in cell.imageViewProfile.gestureRecognizers)
    {
        [cell.imageViewProfile removeGestureRecognizer:gesture];
    }
    
    UITapGestureRecognizer *tappGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(profileBtnCLicked:)];
    tappGesture.numberOfTapsRequired=1;
    [cell.imageViewProfile addGestureRecognizer:tappGesture];
    cell.imageViewProfile.tag = indexPath.row;
    
    cell.btnFollowUnfollow.tag=indexPath.row;
    [cell.btnFollowUnfollow addTarget:self action:@selector(plusMinusBtnCLicked:) forControlEvents:UIControlEventTouchUpInside];
    
//    cell.imgProfileImg.tag=indexPath.row;
//    [cell.imgProfileImg addTarget:self action:@selector(profileBtnCLicked:) forControlEvents:UIControlEventTouchUpInside];
    
    if (arrFollowersData.count>0) {
        //lblErrormsg.text=@"";
        NSString *strAvtarimg=[NSString stringWithFormat:@"%@",[[arrFollowersData objectAtIndex:indexPath.row]valueForKey:@"avatar_image"]];
        
        if (strAvtarimg.length>1) {
            
            [cell.imageViewProfile sd_setImageWithURL:[NSURL URLWithString:strAvtarimg] placeholderImage:[UIImage imageNamed:@"default.jpg"] options:0 completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                if (error)
                {
                    cell.imageViewProfile.contentMode=UIViewContentModeScaleToFill;
                }
                else
                {
                    if ([[strAvtarimg pathExtension]isEqualToString:@"gif"])
                    {
                        cell.imageViewProfile.contentMode=UIViewContentModeScaleAspectFill;
                    }
                    else
                    {
                        cell.imageViewProfile.contentMode=UIViewContentModeScaleToFill;
                    }
                }
            }];
            
//            [cell.imgProfileImg sd_setImageWithURL:[NSURL URLWithString:strAvtarimg] forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"default.jpg"]];
        }
        else
        {
            [cell.imageViewProfile setImage:[UIImage imageNamed:@"default.jpg"]];
//            [cell.imgProfileImg setImage:[UIImage imageNamed:@"default.jpg"] forState:UIControlStateNormal];
        }
        
        if ([[arrFollowersData [indexPath.row][@"is_my_profile"]stringValue]isEqualToString:@"1"]) {
            [cell.btnFollowUnfollow setHidden:YES];
            cell.labelRequested.hidden = YES;
        }
        else
        {
            if (arrFollowersData[indexPath.row][@"showDetail"])
            {
                if ([[dataManager.followersDataArray valueForKey:@"user_id"]containsObject:arrFollowersData[indexPath.row][@"user_id"]])
                {
                    NSInteger index = [[dataManager.followersDataArray valueForKey:@"user_id"]indexOfObject:arrFollowersData[indexPath.row][@"user_id"] ];
                    BOOL requestAcess = [dataManager.followersDataArray[index][@"request_status"]boolValue];
                    if (requestAcess) {
                        [cell.btnFollowUnfollow setImage:[UIImage imageNamed:@"FUFS_MinusIcon"] forState:UIControlStateNormal];
                        cell.btnFollowUnfollow.hidden = NO;
                        cell.labelRequested.hidden = YES;
                    }
                    else
                    {
                        cell.btnFollowUnfollow.hidden = YES;
                        cell.labelRequested.hidden = NO;
                    }
                }
                else
                {
                    BOOL isFollowing = [arrFollowersData [indexPath.row][@"is_following"]boolValue];
                    if (isFollowing)
                    {
                        BOOL isRequestAccepted = [arrFollowersData[indexPath.row][@"request_status"] boolValue];
                        if (isRequestAccepted) {
                            [cell.btnFollowUnfollow setImage:[UIImage imageNamed:@"FUFS_MinusIcon"] forState:UIControlStateNormal];
                            cell.btnFollowUnfollow.hidden = NO;
                            cell.labelRequested.hidden = YES;
                        }
                        else
                        {
                            cell.btnFollowUnfollow.hidden = YES;
                            cell.labelRequested.hidden = NO;
                        }
                    }
                    else
                    {
                        cell.btnFollowUnfollow.hidden = NO;
                        cell.labelRequested.hidden = YES;
                        [cell.btnFollowUnfollow setImage:[UIImage imageNamed:@"FUFS_PlusIcon"] forState:UIControlStateNormal];
                    }
                }
            }
            else
            {
                cell.btnFollowUnfollow.hidden = YES;
                cell.labelRequested.hidden = YES;
                cell.loader.hidden = YES;
            }
        }
        
        
        cell.lblUsername.text=[[arrFollowersData objectAtIndex:indexPath.row]valueForKey:@"username"];
        cell.lblUserBio.text=[NSString stringWithFormat:@"%@",[[arrFollowersData objectAtIndex:indexPath.row]valueForKey:@"bio"]];
        [cell configureText:cell.lblUserBio.text forExpandedState:[expandedIndexPaths containsObject:indexPath]];
    }
}

-(void)getPostLikes
{
    NSString *url = [NSString stringWithFormat:@"posts/%@/replugs/%d",strPostId,currentPage];
    //http://192.168.0.22:8353/v1/users/posts/{postId}/replugs/{page}
    
    NSMutableDictionary *header =[[NSMutableDictionary alloc]initWithObjectsAndKeys:@"application/json",@"Content-Type",[NSUSERDEFAULTS valueForKey:kUSERTOKEN],@"User-Token", nil];
    
    //    NSMutableDictionary *param=[[NSMutableDictionary alloc]initWithObjectsAndKeys:[CommonFunction trimSpaceInString:txtUsername.text],@"username",[CommonFunction trimSpaceInString:txtEmail.text],@"email",[CommonFunction trimSpaceInString:txtPassword.text],@"password",@"ios",@"device_type",pushDeviceToken,@"device_token", nil];
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    
    __weak typeof(self) weakSelf = self;
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypeGet withHttpHeaders:header withServiceName:url withParameters:nil withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"--responce dict %@",responseDict);
        
        [CommonFunction removeActivityIndicator];
        if(responseDict==Nil)        {
            [CommonFunction alertTitle:@"" withMessage:@"Server Error"];
            if (currentPage>1)
            {
                currentPage--;
            }
            isPageRefreshing = NO;
        }
        else if([operation.response statusCode]==200)
        {
            if ([responseDict[@"users"] isKindOfClass:[NSArray class]])
            {
                NSArray *arrListing = responseDict[@"users"];
                
                if (arrListing.count>0)
                {
                    lblErrorMsg.text=@"";
                    if (currentPage == 1)
                    {
                        if (arrFollowersData.count > 0)
                        {
                            [arrFollowersData removeAllObjects];
                        }
                        [arrFollowersData addObjectsFromArray:[arrListing mutableCopy]];
                    }
                    else
                    {
                        [arrFollowersData addObjectsFromArray:[arrListing mutableCopy]];
                    }
                    totalPage=[[responseDict objectForKey:@"total_pages"] integerValue];
                    [tblVw reloadData];
                    
                    
                    for (NSInteger i=0; i<arrFollowersData.count; i++)
                    {
                        if ([[dataManager.followersDataArray valueForKey:@"user_id"]containsObject:arrFollowersData[i][@"user_id"]])
                        {
                            NSInteger index = [[dataManager.followersDataArray valueForKey:@"user_id"]indexOfObject:arrFollowersData[i][@"user_id"]];
                            BOOL requestAcess = [arrFollowersData[i][@"request_status"] boolValue];
                            [dataManager.followersDataArray[index] setValue:[NSNumber numberWithBool:requestAcess] forKey:@"request_status"];
                            
                            BOOL isFolowing = [arrFollowersData[i][@"is_following"] boolValue];
                            if (!isFolowing)
                            {
                                [dataManager.followersDataArray removeObjectAtIndex:index];
                            }
                        }
                        NSInteger someIndex = [[arrFollowersData valueForKey:@"username"]indexOfObject:arrFollowersData[i][@"username"]];
                        [arrFollowersData[someIndex] setValue:@YES forKey:@"showDetail"];
                    }
                }
                else
                {
                    lblErrorMsg.text=@"No Replugs Found.";
                }
            }
            else
            {
                lblErrorMsg.text=@"No Replugs Found.";
            }
            isPageRefreshing = NO;
        }
        else
        {
            if (currentPage>1)
            {
                currentPage--;
            }
            isPageRefreshing = NO;
            [CommonFunction alertTitle:@"" withMessage:[responseDict objectForKey:@"msg"]];
        }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          
                                          if (currentPage>1)
                                          {
                                              currentPage--;
                                          }
                                          isPageRefreshing = NO;
                                          
                                          if([operation.response statusCode]  == 400 ){
                                              NSLog(@"impo response %@",operation.response);
                                              
                                              [CommonFunction removeActivityIndicator];
                                              [CommonFunction alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                          }
                                          else{
                                              
                                              if (wbServiceCount==1) {
                                                  wbServiceCount=0;
                                                  [CommonFunction showActivityIndicatorWithText:@""];
                                                  [weakSelf getPostLikes];
                                              }
                                              else
                                              {
                                                  [CommonFunction removeActivityIndicator];
                                                  [CommonFunction alertTitle:@"" withMessage:@"Network Error..."];
                                              }
                                          }
                                      }];
}


#pragma mark - UIScrollViewDelegate Method

-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (([tblVw contentOffset].y + tblVw.frame.size.height) >= [tblVw contentSize].height)
    {
        NSLog(@"scrolled to bottom");
        if (!isPageRefreshing) {
            
            if(totalPage <= currentPage)
            {
                return;
            }
            else
            {
                currentPage++;
            }
            isPageRefreshing = YES;
            [self getPostLikes];
        }
    }
}


@end
