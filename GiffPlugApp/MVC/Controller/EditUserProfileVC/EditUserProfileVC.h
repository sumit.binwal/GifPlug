//
//  EditUserProfileVC.h
//  GiffPlugApp
//
//  Created by Sumit Sharma on 06/02/16.
//  Copyright © 2016 Kshitij Godara. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EditUserProfileVC : UIViewController
@property(nonatomic,strong)NSMutableDictionary *dictData;
@end
