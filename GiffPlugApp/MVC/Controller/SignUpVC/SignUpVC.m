//
//  SignUpVC.m
//  GiffPlugApp
//
//  Created by Sumit Sharma on 14/01/16.
//  Copyright © 2016 Kshitij Godara. All rights reserved.
//
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import "GuidedContentScreen.h"
#import "SignUpVC.h"
#import "LoginVC.h"
#import "FollowUnFollowScreen.h"

@interface SignUpVC ()<UITextFieldDelegate,UIGestureRecognizerDelegate,UITextFieldDelegate>
{
    IBOutlet UIScrollView *scrllVw;
    IBOutlet UITextField *txtUsername;
    IBOutlet UITextField *txtEmail;
    IBOutlet UITextField *txtPassword;
    __weak IBOutlet UIView *vwBgVideo;
        CMTime vdoCurrentTime;
    
    NSArray *arrayCurseWords;
}
@property (strong,nonatomic)id timeObserver;
@property(strong,nonatomic)AVAssetImageGenerator *imageGenerator;
@property (strong, nonatomic) IBOutlet UIImageView *containerThumbnailView;
@property(strong,nonatomic)AVPlayer *avPlayer;
@property(strong,nonatomic)AVPlayerItem *avPlayerItem;
@property(strong,nonatomic)AVAsset *avAsset;
@property(strong,nonatomic)AVPlayerLayer *avPlayerLayer;
@property (weak, nonatomic) IBOutlet UIButton *getStartedButton;
@end

@implementation SignUpVC


#pragma mark - ViewController Lyf Cycle Method
- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUpView];

    arrayCurseWords = @[@"shit", @"piss", @"fuck", @"cunt", @"motherfucker", @"cocksucker", @"tit"];
    // Do any additional setup after loading the view from its nib.
}


-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    
    [[UIApplication sharedApplication]setStatusBarHidden:NO withAnimation:UIStatusBarAnimationFade];
    [[UIApplication sharedApplication]setStatusBarStyle:UIStatusBarStyleLightContent];
    
    if (self.avPlayer)
    {
        [self.avPlayer play];
    }
    else
    {
        [self playVideo];
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appEnteredForeground) name:UIApplicationWillEnterForegroundNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appEnteredBackground) name:UIApplicationDidEnterBackgroundNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(playerItemDidReachEnd:)
                                                 name:AVPlayerItemDidPlayToEndTimeNotification
                                               object:[self.avPlayer currentItem]];

}
-(void)viewWillDisappear:(BOOL)animated
{
//        [self.avPlayer removeTimeObserver:self.timeObserver];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationWillEnterForegroundNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidEnterBackgroundNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:AVPlayerItemDidPlayToEndTimeNotification object:nil];
    [self.avPlayer pause];
    [super viewWillDisappear:animated];
}


#pragma mark - View Setup Method
-(void)setUpView
{
    //Hide Navigation Bar

    
    [txtEmail setValue:[UIColor colorWithRed:255.0f/255.0f green:255.0f/255.0f blue:255.0f/255.0f alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
    [txtPassword setValue:[UIColor colorWithRed:255.0f/255.0f green:255.0f/255.0f blue:255.0f/255.0f alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
    [txtUsername setValue:[UIColor colorWithRed:255.0f/255.0f green:255.0f/255.0f blue:255.0f/255.0f alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
    [scrllVw setContentOffset:CGPointMake(0.0f, 20.0f)];
    
    [scrllVw setContentInset:UIEdgeInsetsMake(-20,0,0,0)];

    
    UITapGestureRecognizer *tapGesture=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(singleTapBtnClicked)];
    tapGesture.numberOfTapsRequired=1;
    tapGesture.delegate=self;
    [self.view addGestureRecognizer:tapGesture];
    
    wbServiceCount=1;
    
}

- (void)locateThumbnail:(CMTime)time
{
    __weak __typeof(self)weakSelf = self;
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^(void) {
        CGImageRef imageRef = [weakSelf.imageGenerator copyCGImageAtTime:time actualTime:NULL error:NULL];
        UIImage *image = [UIImage imageWithCGImage:imageRef];
        CGImageRelease(imageRef);
        __strong __typeof(weakSelf)strongSelf = weakSelf;
        dispatch_async(dispatch_get_main_queue(), ^(void) {
            strongSelf.containerThumbnailView.image = image;
        });
    });
}
#pragma  mark - Video Play Background Method
-(void)playVideo
{
    NSString *filepath1 = [[NSBundle mainBundle] pathForResource:@"newlogin" ofType:@"mov"];
    NSURL *fileURL1 = [NSURL fileURLWithPath:filepath1];
    
    self.avPlayer = [AVPlayer playerWithURL:fileURL1]; //
    
    self.avPlayerLayer = [AVPlayerLayer layer];
    
    [self.avPlayerLayer setPlayer:self.avPlayer];
    [self.avPlayerLayer setFrame:[UIScreen mainScreen].bounds];
    //[layer setBackgroundColor:[UIColor redColor].CGColor];
    [self.avPlayerLayer setVideoGravity:AVLayerVideoGravityResizeAspectFill];
    
    [vwBgVideo.layer addSublayer:self.avPlayerLayer];
    [self.avPlayer play];
    
    self.avPlayer.actionAtItemEnd = AVPlayerActionAtItemEndNone;
//    CMTime bgInterval = CMTimeMake(1, 10);
//    __weak __typeof(self)weakSelf = self;
//    self.timeObserver = [_avPlayer addPeriodicTimeObserverForInterval:bgInterval
//                                                                queue:dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0)
//                                                           usingBlock:^(CMTime time)
//                         {
//                             [weakSelf locateThumbnail:time];
//                         }];
    
    
}

#pragma mark - avPlayer Notification Method
-(void) appEnteredForeground {
    // Application Goes In Forground and play video
    __weak typeof(self) weakSelf = self;
    [self.avPlayer seekToTime:vdoCurrentTime toleranceBefore:kCMTimeZero toleranceAfter:kCMTimeZero completionHandler:^(BOOL finished) {
        [weakSelf.avPlayer play];
    }];
}

-(void) appEnteredBackground {
    // Application Goes In Background
    [self.avPlayer pause];
    NSLog(@"%@",self.avPlayer.currentItem);
    AVPlayerItem *currentItem = self.avPlayer.currentItem;
    
    vdoCurrentTime = currentItem.currentTime;
}



- (void)playerItemDidReachEnd:(NSNotification *)notification {
    AVPlayerItem *p = [notification object];
    [p seekToTime:kCMTimeZero];
}


-(void)singleTapBtnClicked
{
    [self.view endEditing:YES];
    [scrllVw setScrollEnabled:NO];
    [scrllVw setContentOffset:CGPointZero];
}


-(BOOL)isTextFieldValidate
{
    if (![CommonFunction isValueNotEmpty:txtUsername.text]) {

        [CommonFunction showAlertWithTitle:@"GifPlug" message:@"Please enter username." onViewController:self useAsDelegate:NO dismissBlock:nil];
        return NO;
    }
    else if ([CommonFunction isString:txtUsername.text containsCuserWord:arrayCurseWords]) {
        
        [CommonFunction showAlertWithTitle:@"GifPlug" message:@"We found username inappropriate." onViewController:self useAsDelegate:NO dismissBlock:nil];
        return NO;
    }
    else if (![CommonFunction isValueNotEmpty:txtEmail.text]) {
        
                [CommonFunction showAlertWithTitle:@"GifPlug" message:@"Please enter email." onViewController:self useAsDelegate:NO dismissBlock:nil];
        return NO;
    }
    else if (![CommonFunction IsValidEmail:txtEmail.text])
    {
        [CommonFunction showAlertWithTitle:@"GifPlug" message:@"Please enter a valid email." onViewController:self useAsDelegate:NO dismissBlock:nil];

        return NO;
    }
    else if (![CommonFunction isValueNotEmpty:txtPassword.text])
    {
        [CommonFunction showAlertWithTitle:@"GifPlug" message:@"Please enter password." onViewController:self useAsDelegate:NO dismissBlock:nil];
        return NO;
    }
    else if (txtPassword.text.length<6)
    {
        [CommonFunction showAlertWithTitle:@"GifPlug" message:@"Please enter minimum 6 digit password." onViewController:self useAsDelegate:NO dismissBlock:nil];

        return NO;
    }
    else
    {
        return YES;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - IBAction Button Methods
- (IBAction)backBtnClicked:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)getStartedBtnClicked:(id)sender
{
    [self singleTapBtnClicked];
    if ([CommonFunction reachabiltyCheck]) {
        if ([self isTextFieldValidate]) {
            if ([CommonFunction reachabiltyCheck]) {
                [UIApplication sharedApplication].networkActivityIndicatorVisible=YES;
                [sender setUserInteractionEnabled:NO];
                [self signUpAPI];
            }
        }
    }
    else
    {
        [CommonFunction alertTitle:@"" withMessage:@"Please check your network connection."];
    }
//    GuidedContentScreen *gcs=[[GuidedContentScreen alloc] initWithNibName:@"GuidedContentScreen" bundle:nil];
//  [self.navigationController pushViewController:gcs animated:YES];
}
- (IBAction)facebookBtnClicked:(id)sender
{
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    
    [login logOut];
    __weak typeof(self) weakSelf = self;
    [login
     logInWithReadPermissions: @[@"public_profile", @"email", @"user_friends"]
     fromViewController:weakSelf
     handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
         [UIApplication sharedApplication].networkActivityIndicatorVisible=YES;

         //             NSLog(@"Logged in :%@",resu);
         
         if (result.token)
         {
             [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:@{@"fields": @"about, first_name, last_name, email"}] startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id res, NSError *error)
              {
                  if (!result || result.isCancelled) {
                      [UIApplication sharedApplication].networkActivityIndicatorVisible=NO;
                      
                  }
                  else if (!error) {
                      NSLog(@"%@",res);
                      
                      [weakSelf fbLoginAPI:res];
                      NSLog(@"the EMail = %@", [res objectForKey:@"username"]);
                  }
                  else
                  {
                      NSLog(@"%@", [error localizedDescription]);
                  }}];
         }
     }];
    

}

#pragma mark - UIScrollView Methods
-(void)scrollViewToCenterOfScreen:(UITextField *)textField
{
    [scrllVw setScrollEnabled:YES];
//    float difference;
//    if (scrllVw.contentSize.height == 600)
//        difference = 10.0f;
//    else
//        difference = 10.0f;
    CGFloat viewCenterY = textField.center.y;
    NSLog(@"center.y -- %f",textField.center.y);
    CGRect applicationFrame = [[UIScreen mainScreen] applicationFrame];
    
    CGFloat avaliableHeight = applicationFrame.size.height - 2.0f;
    NSLog(@"avaliableHeight---%f",applicationFrame.size.height);
    
    CGFloat y = viewCenterY - avaliableHeight / 2.0f;
    if (y < 0)
        y = 0;
    NSLog(@"%f",y);
    [scrllVw setContentOffset:CGPointMake(0, y) animated:YES];
    [scrllVw setContentSize:CGSizeMake(320.0f, 630.0f)];
}

#pragma mark - UITextField Delegate Methods
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField==txtUsername) {
        [txtEmail becomeFirstResponder];
    }
    else if (txtEmail==textField)
    {
        [txtPassword becomeFirstResponder];
    }
    else if (txtPassword==textField)
    {
        [self.view endEditing:YES];
        [scrllVw setScrollEnabled:NO];
        [scrllVw setContentOffset:CGPointZero];
    }
    return YES;
}
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    [self scrollViewToCenterOfScreen:textField];
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    [textField resignFirstResponder];
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if(textField == txtUsername )
    {
        if (textField.text.length >= 20 && range.length == 0)
            return NO;
        if ([string isEqualToString:@" "])
        {
            return NO;
        }
    }
   else if(textField == txtPassword )
    {
        if (textField.text.length >= 14 && range.length == 0)
            return NO;
        if ([string isEqualToString:@" "])
        {
            return NO;
        }
    }
 else if (textField==txtEmail || textField==txtUsername)
 {
    return [textField textInputMode] != nil;
 }
    return YES;
}


#pragma mark - WebService API
-(void)signUpAPI
{
    NSString *url = [NSString stringWithFormat:@"users"];
    //http://192.168.0.22:8353/v1/users
    
    NSMutableDictionary *header =[[NSMutableDictionary alloc]initWithObjectsAndKeys:@"application/json",@"Content-Type", nil];
    
    NSMutableDictionary *param=[[NSMutableDictionary alloc]initWithObjectsAndKeys:[CommonFunction trimSpaceInString:txtUsername.text],@"username",[CommonFunction trimSpaceInString:txtEmail.text],@"email",[CommonFunction trimSpaceInString:txtPassword.text],@"password",@"ios",@"device_type",appDeviceToken,@"device_token", nil];
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    
    __weak typeof(self) weakSelf = self;
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypePost withHttpHeaders:header withServiceName:url withParameters:param withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"responce dict %@",responseDict);
        
        [UIApplication sharedApplication].networkActivityIndicatorVisible=NO;

        if(responseDict==Nil)        {
            [CommonFunction alertTitle:@"" withMessage:@"Server Error"];
            [self.getStartedButton setUserInteractionEnabled:YES];
        }
        else if([operation.response statusCode]==200)
        {
            [NSUSERDEFAULTS setObject:[responseDict valueForKey:@"token"] forKey:kUSERTOKEN];
            [NSUSERDEFAULTS setObject:[responseDict valueForKey:@"username"] forKey:kUSERNAME];
            [NSUSERDEFAULTS setObject:[responseDict valueForKey:@"userId"] forKey:kUSER_SID];
            [NSUSERDEFAULTS setObject:[CommonFunction trimSpaceInString:txtUsername.text] forKey:kUSERID];
            [NSUSERDEFAULTS synchronize];
            
            [APPDELEGATE.communication setUpCommunicationStream];
            
            GuidedContentScreen *gcs=[[GuidedContentScreen alloc] initWithNibName:@"GuidedContentScreen" bundle:nil];
            [weakSelf.navigationController pushViewController:gcs animated:YES];
        }
        else
        {
            [self.getStartedButton setUserInteractionEnabled:YES];
            [CommonFunction showAlertWithTitle:@"GifPlug" message:[responseDict objectForKey:@"msg"] onViewController:weakSelf useAsDelegate:NO dismissBlock:nil];
        }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          if([operation.response statusCode]  == 400 ){
                                              [self.getStartedButton setUserInteractionEnabled:YES];
                                              NSLog(@"impo response %@",operation.response);
                                              
                                              [UIApplication sharedApplication].networkActivityIndicatorVisible=NO;

                                                          [CommonFunction showAlertWithTitle:@"GifPlug" message:[operation.responseObject objectForKey:@"response"] onViewController:weakSelf useAsDelegate:NO dismissBlock:nil];
                                          }
                                          else{
                                              
                                              if (wbServiceCount==1) {
                                                  wbServiceCount=0;
                                                  [UIApplication sharedApplication].networkActivityIndicatorVisible=YES;

                                                  [weakSelf signUpAPI];
                                              }
                                              else
                                              {
                                                  [UIApplication sharedApplication].networkActivityIndicatorVisible=NO;

                                                  [CommonFunction alertTitle:@"" withMessage:@"Network Error..."];
                                                  
                                              }
                                              
                                          }
                                          
                                      }];

}

-(void)fbLoginAPI:(NSMutableDictionary *)dataDict
{
    
    NSString *url = [NSString stringWithFormat:@"users/socialSignup"];
    //http://192.168.0.22:8353/v1/users/socialSignup
    NSString *strEmail=[dataDict valueForKey:@"email"];
    if (strEmail.length<1) {
        strEmail=@"";
    }
    NSString *strUsername=[NSString stringWithFormat:@"%@_%@",[dataDict valueForKey:@"first_name"],[dataDict valueForKey:@"last_name"]];
    
    NSMutableDictionary *header =[[NSMutableDictionary alloc]initWithObjectsAndKeys:@"application/json",@"Content-Type", nil];
    
    NSMutableDictionary *param=[[NSMutableDictionary alloc]initWithObjectsAndKeys:strUsername,@"username",[dataDict valueForKey:@"id"],@"social_id",[dataDict valueForKey:@"first_name"],@"name",@"facebook",@"social_type",@"ios",@"device_type",appDeviceToken,@"device_token",strEmail,@"email", nil];
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    __weak typeof(self) weakSelf = self;
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypePost withHttpHeaders:header withServiceName:url withParameters:param withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"responce dict %@",responseDict);
        
        [UIApplication sharedApplication].networkActivityIndicatorVisible=NO;

        if(responseDict==Nil)        {
            [CommonFunction alertTitle:@"" withMessage:@"Server Error"];
            
        }
        else if([operation.response statusCode]==200)
            
        {
            //[CommonFunction alertTitle:@"" withMessage:[responseDict objectForKey:@"msg"]];
            
            [NSUSERDEFAULTS setObject:[responseDict valueForKey:@"username"] forKey:kUSERNAME];
            [NSUSERDEFAULTS setObject:[responseDict valueForKey:@"token"] forKey:kUSERTOKEN];
            [NSUSERDEFAULTS setObject:[responseDict objectForKey:@"categories"] forKey:kFOLLOWINGCATEGORY];
            [NSUSERDEFAULTS setObject:[responseDict valueForKey:@"userId"] forKey:kUSER_SID];
            [NSUSERDEFAULTS setObject:[[responseDict valueForKey:@"hasCategory"] stringValue] forKey:kISCATEGORYSUBMITTED];
            [NSUSERDEFAULTS setObject:[[responseDict valueForKey:@"hasFollowing"] stringValue] forKey:kISFOLLOWINGSUBMITTED];
            
            
            [NSUSERDEFAULTS synchronize];
            
            [APPDELEGATE.communication setUpCommunicationStream];

            
            if ([[NSUSERDEFAULTS valueForKey:kISCATEGORYSUBMITTED] isEqualToString:@"0"]) {
                GuidedContentScreen *gcS=[[GuidedContentScreen alloc]initWithNibName:@"GuidedContentScreen" bundle:nil];
                [weakSelf.navigationController pushViewController:gcS animated:YES];
            }
            else if ([[NSUSERDEFAULTS valueForKey:kISFOLLOWINGSUBMITTED] isEqualToString:@"0"])
            {
                FollowUnFollowScreen *fufs=[[FollowUnFollowScreen alloc]initWithNibName:@"FollowUnFollowScreen" bundle:nil];
                NSLog(@"%@",[NSUSERDEFAULTS objectForKey:kFOLLOWINGCATEGORY]);
                [weakSelf.navigationController pushViewController:fufs animated:YES];
            }
            else
            {
                [CommonFunction changeRootViewController:YES];
                [[UITabBar appearance] setTintColor:[UIColor colorWithRed:255.0f/255.0f green:33.0f/255.0f blue:87.0f/255.0f alpha:1]];
            }

        }
        else
        {
            if([[responseDict objectForKey:@"msg"]isEqualToString:@"EMAIL_ALREADY_EXIST"])
            {
                UIAlertController *alert=[UIAlertController alertControllerWithTitle:[NSString stringWithFormat:@"Your Facebook email address, %@ , already has an account with GifPlug",strEmail ]message:@"" preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *newAccount=[UIAlertAction actionWithTitle:@"New Account" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
                                    }];
                UIAlertAction *lgin=[UIAlertAction actionWithTitle:@"Log in" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {

                    [weakSelf.navigationController popViewControllerAnimated:YES];
                }];
                [alert addAction:newAccount];
                [alert addAction:lgin];
                [weakSelf presentViewController:alert animated:YES completion:nil];
            }
            else
            {
                [CommonFunction alertTitle:@"" withMessage:[responseDict objectForKey:@"msg"]];
            }

        }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          if([operation.response statusCode]  == 400 ){
                                              NSLog(@"impo response %@",operation.response);
                                              
                                              [UIApplication sharedApplication].networkActivityIndicatorVisible=NO;

                                              [CommonFunction alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                              
                                              
                                          }
                                          else{
                                              
                                              if (wbServiceCount==1) {
                                                  wbServiceCount=0;
                                                  [UIApplication sharedApplication].networkActivityIndicatorVisible=YES;

//                                                  [self loginAPI];
                                              }
                                              else
                                              {
                                                  [UIApplication sharedApplication].networkActivityIndicatorVisible=NO;

                                                  [CommonFunction alertTitle:@"" withMessage:@"Network Error..."];
                                              }
                                              
                                          }
                                          
                                      }];
}

@end
