//
//  SinglePostVC.m
//  GiffPlugApp
//
//  Created by Santosh on 07/05/16.
//  Copyright © 2016 Kshitij Godara. All rights reserved.
//

#import "SinglePostVC.h"
#import "FLAnimatedImageView.h"
#import "HomePostedGifCustomeCell.h"
#import "AppDataManager.h"
#import "LikeVC.h"
#import "CommentVC.h"
#import "ReplugVC.h"
#import "OtherUserProfileVC.h"
#import "CategoryDetailVC.h"
#import "PhotoPickerController.h"
#import <AssetsLibrary/AssetsLibrary.h>

@interface SinglePostVC ()<UITableViewDelegate,UITableViewDataSource,UserHandleDelegate>
{
    NSMutableArray *arrayPost;
    AppDataManager *appDataManager;
    UIRefreshControl *refreshPullDown;
}
@property (weak, nonatomic) IBOutlet UILabel *labelUsername;

@property (weak, nonatomic) IBOutlet FLAnimatedImageView *gifImageView;

@property (weak, nonatomic) IBOutlet UIProgressView *gifProgressBar;
@property (weak, nonatomic) IBOutlet UIImageView *imgLike;

@property (weak, nonatomic) IBOutlet UIButton *moreButton;

@property (weak, nonatomic) IBOutlet UILabel *replugCount;

@property (weak, nonatomic) IBOutlet UILabel *commentCount;

@property (weak, nonatomic) IBOutlet UILabel *likeCount;

@property (weak, nonatomic) IBOutlet UILabel *captionLabel;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *captionHeightConstraint;
@property (weak, nonatomic) IBOutlet UIButton *buttonProfileImage;

@property (weak, nonatomic) IBOutlet UITableView *tableViewPost;
- (IBAction)onBackButtonAction:(id)sender;

- (IBAction)onMoreButtonAction:(id)sender;

- (IBAction)onReplugButtonAction:(id)sender;

- (IBAction)onCommentButtonAction:(id)sender;

- (IBAction)onLikeButtonAction:(id)sender;
- (IBAction)onProfileImageButtonAction:(id)sender;
@end

@implementation SinglePostVC


-(void)dealloc
{
    [[AppWebHandler sharedInstance]removeDelegate:self];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [[AppWebHandler sharedInstance]addDelegate:self];
    appDataManager = [AppDataManager sharedInstance];
    
    arrayPost = [[NSMutableArray alloc]init];
    [self.tableViewPost registerNib:[UINib nibWithNibName:NSStringFromClass([HomePostedGifCustomeCell class]) bundle:nil] forCellReuseIdentifier:@"homeFeedCell"];
    
    CGFloat fontSizeUsername = self.labelUsername.font.pointSize;
    
    [self.labelUsername setFont:[UIFont fontWithName:self.labelUsername.font.fontName size:fontSizeUsername*SCREEN_XScale]];
    
    self.buttonProfileImage.layer.cornerRadius=self.buttonProfileImage.frame.size.width*SCREEN_XScale/2;
    self.buttonProfileImage.clipsToBounds=YES;
    
    refreshPullDown = [[UIRefreshControl alloc]initWithFrame:CGRectMake(0, 0, self.tableViewPost.frame.size.width, 50)];
    refreshPullDown.backgroundColor=self.view.backgroundColor;
    refreshPullDown.tintColor=[UIColor whiteColor];
    [refreshPullDown addTarget:self action:@selector(onRefreshPullDown) forControlEvents:UIControlEventValueChanged];
    [self.tableViewPost addSubview:refreshPullDown];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self getSinglePost];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)onRefreshPullDown
{
    if ([CommonFunction reachabiltyCheck]) {
        [self getSinglePost];
    }
    else
    {
        [CommonFunction alertTitle:@"" withMessage:@"Please check your network connection."];
        [refreshPullDown endRefreshing];
    }
}


#pragma mark -
#pragma mark Get Post Api
- (void)getSinglePost
{
    [CommonFunction showActivityIndicatorWithText:@""];
    NSString *url = [NSString stringWithFormat:@"posts/%@",self.postID];
    //http://192.168.0.22:8353/v1/posts/list/{page}
    
    NSMutableDictionary *header =[[NSMutableDictionary alloc]initWithObjectsAndKeys:@"application/json",@"Content-Type",[NSUSERDEFAULTS valueForKey:kUSERTOKEN],@"User-Token", nil];
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypeGet withHttpHeaders:header withServiceName:url withParameters:nil withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        if (refreshPullDown.isRefreshing)
        {
            AudioServicesPlaySystemSound(1109);
        }
        [refreshPullDown endRefreshing];
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"--responce dict %@",responseDict);
        
        [CommonFunction removeActivityIndicator];
        
        if(responseDict==Nil)        {
            [CommonFunction alertTitle:@"" withMessage:@"Server Error"];
        }
        else if([operation.response statusCode]==200)
        {
            if (responseDict[@"post"] && [responseDict[@"post"] isKindOfClass:[NSDictionary class]])
            {
                [arrayPost removeAllObjects];
                [arrayPost addObject:responseDict[@"post"]];
                [self.tableViewPost reloadData];
                self.labelUsername.text = responseDict[@"post"][@"username"];
                
                UITapGestureRecognizer *nameTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(usernameClicked:)];
                nameTap.numberOfTapsRequired=1;
                self.labelUsername.userInteractionEnabled = YES;
                [self.labelUsername addGestureRecognizer:nameTap];
                self.labelUsername.tag = 0;
                
                NSURL *imgUrl=[NSURL URLWithString:responseDict[@"post"][@"avatar_image"]];
                
                [self.buttonProfileImage sd_setImageWithURL:imgUrl forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"default.jpg"] options:0 completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                    if (error)
                    {
                        self.buttonProfileImage.contentMode=UIViewContentModeScaleToFill;
                        self.buttonProfileImage.imageView.contentMode=UIViewContentModeScaleToFill;
                    }
                    else
                    {
                        if ([[imgUrl pathExtension]isEqualToString:@"gif"])
                        {
                            self.buttonProfileImage.contentMode=UIViewContentModeScaleAspectFill;
                            self.buttonProfileImage.imageView.contentMode=UIViewContentModeScaleAspectFill;
                        }
                        else
                        {
                            self.buttonProfileImage.contentMode=UIViewContentModeScaleToFill;
                            self.buttonProfileImage.imageView.contentMode=UIViewContentModeScaleToFill;
                        }
                    }
                }];
            }
        }
        else
        {
            [CommonFunction alertTitle:@"" withMessage:[responseDict objectForKey:@"msg"]];
        }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          
                                          [refreshPullDown endRefreshing];
                                          
                                          [CommonFunction removeActivityIndicator];
                                          
                                          if([operation.response statusCode]  == 400 ){
                                              NSLog(@"impo response %@",operation.response);
                                              
                                              [CommonFunction alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                          }
                                          else{
                                              
                                              if (wbServiceCount==1) {
                                                  wbServiceCount=0;
                                              }
                                              else
                                              {
                                                  [CommonFunction alertTitle:@"" withMessage:@"Network Error..."];
                                              }
                                          }
                                      }];
}

#pragma mark -
#pragma mark On Back Button Action
- (IBAction)onBackButtonAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark -
#pragma mark On Share Button Action
- (IBAction)onMoreButtonAction:(UIButton*)sender {
    //    NSString *string = @"ddd";
    NSURL *URL = [NSURL URLWithString:arrayPost[sender.tag][@"watermark_image"]];
    //    [sender setImage:[UIImage imageNamed:@"PG_menu_Red"] forState:UIControlStateNormal];
    
    //    [moreButton setImage:[UIImage imageNamed:@"PG_menu"] forState:UIControlStateNormal];
    
    //    UIImage *img=[UIImage imageNamed:@"GCS_bgImg"];
    
    UIActivityViewController *activityViewController =
    [[UIActivityViewController alloc] initWithActivityItems:@[URL]
                                      applicationActivities:nil];
    
    NSArray *excludedActivities = @[
                                    UIActivityTypePostToWeibo,
                                    UIActivityTypePrint,
                                    UIActivityTypeAssignToContact,
                                    UIActivityTypeAirDrop
                                    ];
    activityViewController.excludedActivityTypes = excludedActivities;
    
    // Present the controller
    [self presentViewController:activityViewController animated:YES completion:nil];
    
}

#pragma mark -
#pragma mark On Replug Button Action
- (IBAction)onReplugButtonAction:(UIButton*)sender {
    ReplugVC *rpvc=[[ReplugVC alloc]init];
    rpvc.strPostId=arrayPost[sender.tag][@"post_id"];
    rpvc.strLikeCount=[NSString stringWithFormat:@"%@",arrayPost[sender.tag][@"replugged_count"]];
    rpvc.hidesBottomBarWhenPushed=YES;
    [self.navigationController pushViewController:rpvc animated:YES];
}

#pragma mark -
#pragma mark On Comment Button Action
- (IBAction)onCommentButtonAction:(UIButton*)sender {
    CommentVC *lvc=[[CommentVC alloc]init];
    
    lvc.strPostId=arrayPost[sender.tag][@"post_id"];
    lvc.strLikeCount=[NSString stringWithFormat:@"%@",arrayPost[sender.tag][@"comments_count"]];
    
    lvc.hidesBottomBarWhenPushed=YES;
    [self.navigationController pushViewController:lvc animated:YES];
}

#pragma mark -
#pragma mark On Like Button Action
- (IBAction)onLikeButtonAction:(UIButton*)sender {
    LikeVC *lvc=[[LikeVC alloc]init];
    lvc.strPostId=arrayPost[sender.tag][@"post_id"];
    lvc.strLikeCount=[NSString stringWithFormat:@"%@",arrayPost[sender.tag][@"likes_count"]];
    lvc.hidesBottomBarWhenPushed=YES;
    [self.navigationController pushViewController:lvc animated:YES];
}

#pragma mark -
#pragma mark On Profile Button Action
- (IBAction)onProfileImageButtonAction:(UIButton*)sender {
    
    NSString *loggedInUser = [NSUSERDEFAULTS objectForKey:kUSERID];
    if ([loggedInUser isEqualToString:arrayPost [sender.tag][@"username"]])
    {
        [APPDELEGATE goToUserProfile];
    }
    else
    {
        OtherUserProfileVC *oupvc=[[OtherUserProfileVC alloc]initWithNibName:@"OtherUserProfileVC" bundle:nil];
        oupvc.hidesBottomBarWhenPushed=YES;
        oupvc.strUserName=arrayPost [sender.tag][@"username"];
        [self.navigationController pushViewController:oupvc animated:YES];
    }
}

#pragma mark -
#pragma mark UITableView Datasource And Delegates
- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 461*SCREEN_YScale;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 461*SCREEN_YScale;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrayPost.count;
}

- (HomePostedGifCustomeCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    HomePostedGifCustomeCell *cell = [tableView dequeueReusableCellWithIdentifier:@"homeFeedCell"];
    cell.delegate=nil;
    cell.delegate = self;
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(HomePostedGifCustomeCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    
    
    [cell.btnLike addTarget:self action:@selector(onLikeButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [cell.btnComment addTarget:self action:@selector(onCommentButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [cell.btnReplug addTarget:self action:@selector(onReplugButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [cell.btnMoreClicked addTarget:self action:@selector(onMoreButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    cell.btnLike.tag=indexPath.row;
    cell.btnComment.tag=indexPath.row;
    cell.btnReplug.tag=indexPath.row;
    cell.imgVwGif.tag = indexPath.row;
    cell.btnMoreClicked.tag = indexPath.row;
    
    for (NSInteger i=0; i<cell.imgVwGif.gestureRecognizers.count; i++)
    {
        UITapGestureRecognizer *imgLikeTap = cell.imgVwGif.gestureRecognizers[i];
        [cell.imgVwGif removeGestureRecognizer:imgLikeTap];
    }
    UITapGestureRecognizer *imgLikeTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onDoubleTapLikeAction:)];
    imgLikeTap.numberOfTapsRequired=2;
    [cell.imgVwGif addGestureRecognizer:imgLikeTap];
    
    UILongPressGestureRecognizer *imgsavepress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(onSaveGifAction:)];
    [cell.imgVwGif addGestureRecognizer:imgsavepress];
    
    
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.lineSpacing = 5.0f;
    paragraphStyle.lineBreakMode = NSLineBreakByTruncatingTail;
    //    paragraphStyle.maximumLineHeight = 5.0f;
    
    NSString *string = [NSString stringWithFormat:@"%@",arrayPost[indexPath.row][@"caption"]];
    NSDictionary *attributtes = @{
                                  NSParagraphStyleAttributeName : paragraphStyle,
                                  NSForegroundColorAttributeName : [UIColor whiteColor],
                                  NSFontAttributeName : [UIFont fontWithName:cell.lblCaption.font.fontName size:14.0f]
                                  };
    cell.lblCaption.attributedText = [[NSAttributedString alloc] initWithString:string
                                                                     attributes:attributtes];
    
    
    // cell.lblCaption.text=[NSString stringWithFormat:@"%@",arrFeedData[indexPath.row][@"caption"]];
    
    
    
    if ([[arrayPost[indexPath.row][@"is_like"]stringValue] isEqualToString:@"0"]) {
        [cell.imgLikeUnlikeImg setImage:[UIImage imageNamed:@"HF_likeUnfillIcon"]];
        cell.imgLikeUnlikeImg.tag=0;
    }
    else
    {
        [cell.imgLikeUnlikeImg setImage:[UIImage imageNamed:@"PG_LikeBtn"]];
        cell.imgLikeUnlikeImg.tag=1;
    }
    
    [[arrayPost[indexPath.row][@"is_comment"]stringValue] isEqualToString:@"0"]
    ?[cell.imgCommentFillUnFill setImage:[UIImage imageNamed:@"PG_Comment"]]
    :[cell.imgCommentFillUnFill setImage:[UIImage imageNamed:@"PG_CommentSelected"]];
    
    
    cell.lblCommentCount.text=[NSString stringWithFormat:@"%@",[[arrayPost objectAtIndex:indexPath.row]objectForKey:@"comments_count"]];
    cell.lblLikeCount.text=[NSString stringWithFormat:@"%@",[[arrayPost objectAtIndex:indexPath.row]objectForKey:@"likes_count"]];
    cell.lblReplugCount.text=[NSString stringWithFormat:@"%@",[[arrayPost objectAtIndex:indexPath.row]objectForKey:@"replugged_count"]];
    
    cell.tag = indexPath.row;
    
    NSString *imgGifUrl=arrayPost[indexPath.row][@"post_image"];
    
    NSURL *url1 = [NSURL URLWithString:imgGifUrl];
    
    NSNumber *progress = [[AppWebHandler sharedInstance].dictProgressTask objectForKey:[url1 absoluteString]];
    //    NSNumber *progress = [self.progressBuffer objectForKey:[url1 absoluteString]];
    if (!progress) progress = @(0.0);
    
    [cell setProgress:[progress floatValue]];
    
    if ([progress intValue]==1) {
        
        [cell.progresView setHidden:true];
        [cell.imgVwGif setHidden:false];
    }
    else
    {
        [cell.progresView setHidden:false];
        [cell.imgVwGif setHidden:true];
    }
    
    if (![[AppWebHandler sharedInstance].dictProgressTask objectForKey:[url1 absoluteString]]) {
        
        // Download Giff with URL Item
        [self downloadGifFromUrl:url1];
    }
    else
    {
        // Update Progress Buffer
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
            NSURL *localURL = [self URLForGiffWithName:[imgGifUrl lastPathComponent]];
            
            if ([[NSFileManager defaultManager] fileExistsAtPath:[localURL path]]) {
                
                NSData *animatedImageData = [[NSFileManager defaultManager] contentsAtPath:localURL.path];
                FLAnimatedImage *animatedImage = [FLAnimatedImage animatedImageWithGIFData:animatedImageData generateThumbnail:NO];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    [cell.imgVwGif setAnimatedImage:nil];
                    [cell.imgVwGif setAnimatedImage:animatedImage];
                });
            }
        });
    }
    [cell handleCaptionBar];
}

#pragma mark -
#pragma mark Download Gif
- (void)downloadGifFromUrl:(NSURL *)gifUrl {
    
    [[AppWebHandler sharedInstance]initializeBackgroundSession];
    if (gifUrl) {
        
        [[AppWebHandler sharedInstance]downloadTaskForUrl:gifUrl];
        
        // Schedule Download Task
        //        [[self.session downloadTaskWithURL:gifUrl] resume];
        
        // Update Progress Buffer
        [[AppWebHandler sharedInstance].dictProgressTask setObject:@(0.0) forKey:[gifUrl absoluteString]];
        //        [self.progressBuffer setObject:@(0.0) forKey:[gifUrl absoluteString]];
    }
}

#pragma mark -
#pragma mark Gif Name And Directory
- (NSURL *)URLForGiffWithName:(NSString *)name {
    if (!name) return nil;
    return [self.gifDirectory URLByAppendingPathComponent:name];
}

- (NSURL *)gifDirectory {
    return [AppWebHandler sharedInstance].tempDirectory;
}

#pragma mark -
#pragma mark Cell For Task
- (HomePostedGifCustomeCell *)cellForForDownloadTask:(NSURLSessionDownloadTask *)downloadTask {
    // Helpers
    
    HomePostedGifCustomeCell *cell = nil;
    NSURL *URL = [[downloadTask originalRequest] URL];
    
    for (NSInteger i =0; i<arrayPost.count; i++)
    {
        BOOL flag = NO;
        NSDictionary *dict = arrayPost[i];
        
        NSString *strImgUrl=[dict valueForKey:@"post_image"];
        NSURL *mainURL=[NSURL URLWithString:[NSString stringWithFormat:@"%@",strImgUrl]];
        //   NSURL *feedItemURL = [self urlForFeedItem:feedItem];
        
        __weak typeof(self) weakSelf = self;
        @try {
            if ([URL isEqual:mainURL]) {
                NSUInteger index = [arrayPost indexOfObject:dict];
                NSIndexPath *indexpath = [NSIndexPath indexPathForRow:index inSection:0];
                if ([[weakSelf.tableViewPost indexPathsForVisibleRows]containsObject:indexpath]) {
                    cell = (HomePostedGifCustomeCell *)[weakSelf.tableViewPost cellForRowAtIndexPath:indexpath];
                    flag = YES;
                }
            }
        } @catch (NSException *exception) {
            [weakSelf.tableViewPost reloadData];
        } @finally {
            if (flag) {
                break;
            }
        }
    }
    
    return cell;
}

#pragma mark -
#pragma mark NSURLSessionDownloadTask Delegates
- (void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didResumeAtOffset:(int64_t)fileOffset expectedTotalBytes:(int64_t)expectedTotalBytes {
    NSLog(@"%s", __PRETTY_FUNCTION__);
}


- (void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didWriteData:(int64_t)bytesWritten totalBytesWritten:(int64_t)totalBytesWritten totalBytesExpectedToWrite:(int64_t)totalBytesExpectedToWrite {
    // Calculate Progress
    double progress = (double)totalBytesWritten / (double)totalBytesExpectedToWrite;
    
    // Update Progress Buffer
    NSURL *URL = [[downloadTask originalRequest] URL];
    [[AppWebHandler sharedInstance].dictProgressTask setObject:@(progress) forKey:[URL absoluteString]];
    //    [self.progressBuffer setObject:@(progress) forKey:[URL absoluteString]];
    
    // Update Table View Cell
    HomePostedGifCustomeCell *cell = [self cellForForDownloadTask:downloadTask];
    __weak typeof(cell) weakCell =cell;
    dispatch_async(dispatch_get_main_queue(), ^{
        [weakCell setProgress:progress];
    });
}


- (void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didFinishDownloadingToURL:(NSURL *)location {
    // Write File to Disk
    
    // NSData *imageData = [NSData dataWithContentsOfURL:lo];
    NSData *imageData = [NSData dataWithContentsOfFile:[location path]];
    NSLog(@"location is %@",location.absoluteString);
    
    [self moveFileWithURL:location downloadTask:downloadTask];
    NSURL *URL = [[downloadTask originalRequest] URL];
    
    [[AppWebHandler sharedInstance].dictProgressTask setObject:@(1.0) forKey:[URL absoluteString]];
    
    // Update Table View Cell
    HomePostedGifCustomeCell *cell = [self cellForForDownloadTask:downloadTask];
    
    // Update Progress Buffer
    //    [self.progressBuffer setObject:@(1.0) forKey:[URL absoluteString]];
    
    __weak typeof(cell) weakCell =cell;
    __weak typeof(self) weakSelf = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        FLAnimatedImage *image = [FLAnimatedImage animatedImageWithGIFData:imageData generateThumbnail:NO];
        [weakCell.imgVwGif setAnimatedImage:image];
        [weakCell.progresView setHidden:true];
        [weakCell.imgVwGif setHidden:false];
        if (weakCell)
        {
            @try {
                [weakSelf.tableViewPost beginUpdates];
                [weakSelf.tableViewPost reloadRowsAtIndexPaths:@[[weakSelf.tableViewPost indexPathForCell:weakCell]] withRowAnimation:UITableViewRowAnimationNone];
                [weakSelf.tableViewPost endUpdates];
            }
            @catch (NSException *exception) {
                [weakSelf.tableViewPost reloadData];
            }
            @finally {
                
            }
        }
    });
    // Invoke Background Completion Handler
    [self invokeBackgroundSessionCompletionHandler];
}

#pragma mark -
#pragma mark Save Gif
- (void)moveFileWithURL:(NSURL *)URL downloadTask:(NSURLSessionDownloadTask *)downloadTask {
    // Filename
    NSString *fileName = [[[downloadTask originalRequest] URL] lastPathComponent];
    
    // Local URL
    NSURL *localURL = [self URLForGiffWithName:fileName];
    
    NSFileManager *fm = [NSFileManager defaultManager];
    
    if ([fm fileExistsAtPath:[URL path]]) {
        NSError *error = nil;
        [fm moveItemAtURL:URL toURL:localURL error:&error];
        //        [_tempDirectUrls addObject:[localURL path]];
        if (error) {
            NSLog(@"Unable to move temporary file to destination. %@, %@", error, error.userInfo);
        }
    }
}

#pragma mark -
#pragma mark Double Tap Like
- (void)onDoubleTapLikeAction:(UITapGestureRecognizer *)likeTap
{
    NSInteger index = likeTap.view.tag;
    
    HomePostedGifCustomeCell *cellView = (HomePostedGifCustomeCell*) [self.tableViewPost cellForRowAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0]];
    
    if (cellView.isHeartAnimationActive)
    {
        return;
    }
    
    NSNumber *isLike=arrayPost[index][@"is_like"];
    NSLog(@"%@",isLike);
    [cellView performHeartAnimation];
    
    if (![isLike boolValue])
    {
        NSInteger likeCount = [arrayPost[index][@"likes_count"] integerValue];
        
        if (cellView.imgLikeUnlikeImg.tag==0) {
            
            [cellView.imgLikeUnlikeImg setImage:[UIImage imageNamed:@"PG_LikeBtn"]];
            cellView.imgLikeUnlikeImg.tag=1;
            
            [CommonFunction showActivityIndicatorWithText:nil];
            likeCount += 1;
            
            cellView.lblLikeCount.text=[NSString stringWithFormat:@"%ld",(long)likeCount];
            
            [arrayPost[index] setValue:[NSNumber numberWithInteger:likeCount] forKey:@"likes_count"];
            [arrayPost[index] setValue:@1 forKey:@"is_like"];
            
            NSString *postID = arrayPost[index][@"post_id"];
            
//            for (NSInteger i=0; i<appDataManager.globalUserFeedArray.count; i++)
//            {
//                NSString *userPostID = appDataManager.globalUserFeedArray[i][@"post_id"];
//                if ([userPostID isEqualToString:postID])
//                {
//                    [appDataManager.globalUserFeedArray[i] setValue:[NSNumber numberWithInteger:likeCount] forKey:@"likes_count"];
//                    [appDataManager.globalUserFeedArray[i] setValue:@1 forKey:@"is_like"];
//                }
//            }
//            for (NSInteger i=0; i<appDataManager.globalUserReplugArray.count; i++)
//            {
//                NSString *userPostID = appDataManager.globalUserReplugArray[i][@"post_id"];
//                if ([userPostID isEqualToString:postID])
//                {
//                    [appDataManager.globalUserReplugArray[i] setValue:[NSNumber numberWithInteger:likeCount] forKey:@"likes_count"];
//                    [appDataManager.globalUserReplugArray[i] setValue:@1 forKey:@"is_like"];
//                }
//            }
            [self likeAPost:postID cell:cellView indexpath:index];
        }
    }
}

#pragma mark -
#pragma mark Background Session Completion Handler
- (void)invokeBackgroundSessionCompletionHandler {
    
    [[AppWebHandler sharedInstance].httpSessionBackground getTasksWithCompletionHandler:^(NSArray *dataTasks, NSArray *uploadTasks, NSArray *downloadTasks) {
        NSUInteger count = [dataTasks count] + [uploadTasks count] + [downloadTasks count];
        
        if (!count) {
            AppDelegate *applicationDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
            void (^backgroundSessionCompletionHandler)() = [applicationDelegate backgroundSessionCompletionHandler];
            
            if (backgroundSessionCompletionHandler) {
                [applicationDelegate setBackgroundSessionCompletionHandler:nil];
                backgroundSessionCompletionHandler();
            }
        }
    }];
}






-(void)likeAPost:(NSString *)postID cell:(HomePostedGifCustomeCell *)cell indexpath:(NSInteger)arrindex
{
    NSString *url = [NSString stringWithFormat:@"posts/like/%@",postID];
    //http://192.168.0.22:8353/v1/posts/like/{postId}
    
    //    for (AFHTTPRequestOperation *operation in [AFHTTPRequestOperationManager manager].operationQueue.operations)
    //    {
    //        NSString *path = operation.request.URL.path;
    //        if ([path containsString:url])
    //        {
    //            [operation cancel];
    //        }
    //    }
    
    NSMutableDictionary *header =[[NSMutableDictionary alloc]initWithObjectsAndKeys:@"application/json",@"Content-Type",[NSUSERDEFAULTS valueForKey:kUSERTOKEN],@"User-Token", nil];
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    
    __weak typeof(cell) weakCell = cell;
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypePost withHttpHeaders:header withServiceName:url withParameters:nil withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"--responce dict %@",responseDict);
        
        [CommonFunction removeActivityIndicator];
        if(responseDict==Nil)        {
            [CommonFunction alertTitle:@"" withMessage:@"Server Error"];
            
            NSInteger likeCount = [arrayPost[arrindex][@"likes_count"] integerValue];
            
            [weakCell.imgLikeUnlikeImg setImage:[UIImage imageNamed:@"HF_likeUnfillIcon"]];
            weakCell.imgLikeUnlikeImg.tag=0;
            
            likeCount -= 1;
            weakCell.lblLikeCount.text=[NSString stringWithFormat:@"%ld",(long)likeCount];
            [arrayPost[arrindex] setValue:[NSNumber numberWithInteger:likeCount] forKey:@"likes_count"];
            [arrayPost[arrindex] setValue:@0 forKey:@"is_like"];
            
//            NSString *postidstr = arrayPost[arrindex][@"post_id"];
//            for (NSInteger i=0; i<appDataManager.globalUserFeedArray.count; i++)
//            {
//                NSString *userPostID = appDataManager.globalUserFeedArray[i][@"post_id"];
//                if ([userPostID isEqualToString:postidstr])
//                {
//                    [appDataManager.globalUserFeedArray[i] setValue:[NSNumber numberWithInteger:likeCount] forKey:@"likes_count"];
//                    [appDataManager.globalUserFeedArray[i] setValue:@0 forKey:@"is_like"];
//                }
//            }
//            for (NSInteger i=0; i<appDataManager.globalUserReplugArray.count; i++)
//            {
//                NSString *userPostID = appDataManager.globalUserReplugArray[i][@"post_id"];
//                if ([userPostID isEqualToString:postID])
//                {
//                    [appDataManager.globalUserReplugArray[i] setValue:[NSNumber numberWithInteger:likeCount] forKey:@"likes_count"];
//                    [appDataManager.globalUserReplugArray[i] setValue:@0 forKey:@"is_like"];
//                }
//            }
        }
        else if([operation.response statusCode]==200)
        {
            
        }
        else
        {
            [CommonFunction alertTitle:@"" withMessage:[responseDict objectForKey:@"msg"]];
            NSInteger likeCount = [arrayPost[arrindex][@"likes_count"] integerValue];
            
            [weakCell.imgLikeUnlikeImg setImage:[UIImage imageNamed:@"HF_likeUnfillIcon"]];
            weakCell.imgLikeUnlikeImg.tag=0;
            
            likeCount -= 1;
            weakCell.lblLikeCount.text=[NSString stringWithFormat:@"%ld",(long)likeCount];
            [arrayPost[arrindex] setValue:[NSNumber numberWithInteger:likeCount] forKey:@"likes_count"];
            [arrayPost[arrindex] setValue:@0 forKey:@"is_like"];
            
//            NSString *postidstr = arrayPost[arrindex][@"post_id"];
//            for (NSInteger i=0; i<appDataManager.globalUserFeedArray.count; i++)
//            {
//                NSString *userPostID = appDataManager.globalUserFeedArray[i][@"post_id"];
//                if ([userPostID isEqualToString:postidstr])
//                {
//                    [appDataManager.globalUserFeedArray[i] setValue:[NSNumber numberWithInteger:likeCount] forKey:@"likes_count"];
//                    [appDataManager.globalUserFeedArray[i] setValue:@0 forKey:@"is_like"];
//                }
//            }
//            for (NSInteger i=0; i<appDataManager.globalUserReplugArray.count; i++)
//            {
//                NSString *userPostID = appDataManager.globalUserReplugArray[i][@"post_id"];
//                if ([userPostID isEqualToString:postID])
//                {
//                    [appDataManager.globalUserReplugArray[i] setValue:[NSNumber numberWithInteger:likeCount] forKey:@"likes_count"];
//                    [appDataManager.globalUserReplugArray[i] setValue:@0 forKey:@"is_like"];
//                }
//            }
        }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          
                                          NSInteger likeCount = [arrayPost[arrindex][@"likes_count"] integerValue];
                                          
                                          [weakCell.imgLikeUnlikeImg setImage:[UIImage imageNamed:@"HF_likeUnfillIcon"]];
                                          weakCell.imgLikeUnlikeImg.tag=0;
                                          
                                          likeCount -= 1;
                                          weakCell.lblLikeCount.text=[NSString stringWithFormat:@"%ld",(long)likeCount];
                                          [arrayPost[arrindex] setValue:[NSNumber numberWithInteger:likeCount] forKey:@"likes_count"];
                                          [arrayPost[arrindex] setValue:@0 forKey:@"is_like"];
                                          
                                          
//                                          NSString *postidstr = arrayPost[arrindex][@"post_id"];
//                                          for (NSInteger i=0; i<appDataManager.globalUserFeedArray.count; i++)
//                                          {
//                                              NSString *userPostID = appDataManager.globalUserFeedArray[i][@"post_id"];
//                                              if ([userPostID isEqualToString:postidstr])
//                                              {
//                                                  [appDataManager.globalUserFeedArray[i] setValue:[NSNumber numberWithInteger:likeCount] forKey:@"likes_count"];
//                                                  [appDataManager.globalUserFeedArray[i] setValue:@0 forKey:@"is_like"];
//                                              }
//                                          }
//                                          for (NSInteger i=0; i<appDataManager.globalUserReplugArray.count; i++)
//                                          {
//                                              NSString *userPostID = appDataManager.globalUserReplugArray[i][@"post_id"];
//                                              if ([userPostID isEqualToString:postID])
//                                              {
//                                                  [appDataManager.globalUserReplugArray[i] setValue:[NSNumber numberWithInteger:likeCount] forKey:@"likes_count"];
//                                                  [appDataManager.globalUserReplugArray[i] setValue:@0 forKey:@"is_like"];
//                                              }
//                                          }
                                          
                                          if([operation.response statusCode]  == 400 ){
                                              NSLog(@"impo response %@",operation.response);
                                              [CommonFunction removeActivityIndicator];
                                              APPDELEGATE.window.userInteractionEnabled=NO;
                                              [CommonFunction alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                          }
                                          else{
                                              
                                              if (wbServiceCount==1) {
                                                  wbServiceCount=0;
                                                  //  [CommonFunction showActivityIndicatorWithText:@""];
                                              }
                                              else
                                              {
                                                  [CommonFunction removeActivityIndicator];
                                                  [CommonFunction alertTitle:@"" withMessage:@"Network Error..."];
                                              }
                                          }
                                      }];
}

- (void)usernameClicked:(UITapGestureRecognizer *)sender {
    
    if (sender.state == UIGestureRecognizerStateEnded)
    {
        NSString *loggedInUser = [NSUSERDEFAULTS objectForKey:kUSERID];
        if ([loggedInUser isEqualToString:arrayPost [sender.view.tag][@"username"]])
        {
            [APPDELEGATE goToUserProfile];
        }
        else
        {
            OtherUserProfileVC *oupvc=[[OtherUserProfileVC alloc]initWithNibName:@"OtherUserProfileVC" bundle:nil];
            oupvc.hidesBottomBarWhenPushed=YES;
            oupvc.strUserName=arrayPost [sender.view.tag][@"username"];
            [self.navigationController pushViewController:oupvc animated:YES];
        }
    }
}

#pragma mark -
#pragma mark Handle UserName Tap
- (void)customTableViewCellDidTapOnUserHandle:(NSString *)userHandle
{
    NSString *loggedInUser = [NSUSERDEFAULTS objectForKey:kUSERID];
    if ([loggedInUser isEqualToString:userHandle])
    {
        [APPDELEGATE goToUserProfile];
    }
    else
    {
        OtherUserProfileVC *ouVC=[[OtherUserProfileVC alloc]init];
        ouVC.hidesBottomBarWhenPushed=YES;
        ouVC.strUserName=userHandle;
        [self.navigationController pushViewController:ouVC animated:YES];
    }
}

#pragma mark -
#pragma mark Handle HashTag Tap
- (void)customTableViewCellDidTapHashTagHandle:(NSString *)hashtagHandle
{
    NSString *categoryName = [NSString stringWithFormat:@"#%@",hashtagHandle];
    NSString *categoryTag = hashtagHandle;
    CategoryDetailVC *categorydetailVC = [[CategoryDetailVC alloc]initWithNibName:NSStringFromClass([CategoryDetailVC class]) bundle:nil];
    categorydetailVC.hidesBottomBarWhenPushed=YES;
    categorydetailVC.categoryTag = categoryTag;
    categorydetailVC.categoryName = categoryName;
    [self.navigationController pushViewController:categorydetailVC animated:YES];
}

#pragma mark -
#pragma mark Save Gif
- (void)saveGIFToGallery:(NSNumber *)indexNumber
{
    UIAlertController *controller = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *actionCancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
    
    UIAlertAction *action = [UIAlertAction actionWithTitle:@"Save GIF" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        NSString *imgGifUrl=arrayPost[[indexNumber integerValue]][@"post_image"];
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
            NSURL *localURL = [self URLForGiffWithName:[imgGifUrl lastPathComponent]];
            
            if ([[NSFileManager defaultManager] fileExistsAtPath:[localURL path]]) {
                
                NSData *animatedImageData = [[NSFileManager defaultManager] contentsAtPath:localURL.path];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    
                    if ([[[UIDevice currentDevice]systemVersion]floatValue]<9)
                    {
                        ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
                        [library writeImageDataToSavedPhotosAlbum:animatedImageData metadata:nil completionBlock:^(NSURL *assetURL, NSError *error) {
                            dispatch_async(dispatch_get_main_queue(), ^{
                                [CommonFunction removeActivityIndicator];
                                if (!error)
                                {
                                    [CommonFunction showAlertWithTitle:@"Gif Saved" message:@"Gif has been saved to Gallery" onViewController:self useAsDelegate:NO dismissBlock:nil];
                                }
                                else
                                {
                                    [CommonFunction showAlertWithTitle:@"Error" message:error.localizedDescription onViewController:self useAsDelegate:NO dismissBlock:nil];
                                }
                            });
                        }];
                    }
                    else
                    {
                        [[PHPhotoLibrary sharedPhotoLibrary] performChanges:^{
                            [[PHAssetCreationRequest creationRequestForAsset]addResourceWithType:PHAssetResourceTypePhoto data:animatedImageData options:nil];
                        } completionHandler:^(BOOL success, NSError *error) {
                            
                            dispatch_async(dispatch_get_main_queue(), ^{
                                [CommonFunction removeActivityIndicator];
                                if (success) {
                                    [CommonFunction showAlertWithTitle:@"Gif Saved" message:@"Your Gif has been saved to Gallery" onViewController:self useAsDelegate:NO dismissBlock:nil];
                                }
                                else {
                                    [CommonFunction showAlertWithTitle:@"Error" message:error.localizedDescription onViewController:self useAsDelegate:NO dismissBlock:nil];
                                }
                            });
                        }];
                    }
                });
            }
        });
    }];
    
    [controller addAction:actionCancel];
    [controller addAction:action];
    
    [self presentViewController:controller animated:YES completion:nil];
}

- (void)onSaveGifAction:(UILongPressGestureRecognizer *)likeTap
{
    if (likeTap.state == UIGestureRecognizerStateBegan)
    {
        if ([PHPhotoLibrary authorizationStatus] == PHAuthorizationStatusDenied || [PHPhotoLibrary authorizationStatus] == PHAuthorizationStatusRestricted)
        {
            PhotoPickerController *photoPicker = [[PhotoPickerController alloc]initWithNibName:NSStringFromClass([PhotoPickerController class]) bundle:nil];
            [self.navigationController presentViewController:photoPicker animated:YES completion:nil];
        }
        else
        {
            NSInteger index = likeTap.view.tag;
            
            [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status) {
                switch (status) {
                    case PHAuthorizationStatusAuthorized:
                    {
                        [self performSelectorOnMainThread:@selector(saveGIFToGallery:) withObject:[NSNumber numberWithInteger:index] waitUntilDone:NO];
                    }
                        break;
                        
                    default:
                        break;
                }
            }];
            
        }
    }
}

@end
