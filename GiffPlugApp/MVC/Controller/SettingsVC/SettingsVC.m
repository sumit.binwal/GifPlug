//
//  SettingsVC.m
//  GiffPlugApp
//
//  Created by Santosh on 31/05/16.
//  Copyright © 2016 Kshitij Godara. All rights reserved.
//

#import "SettingsVC.h"
#import "SettingsCell.h"
#import "UpdateSettingVC.h"
#import "AppDataManager.h"
#import "WebViewVC.h"
#import "FacebookInviteView.h"
@interface SettingsVC ()<UITableViewDelegate,UITableViewDataSource>
{
    NSMutableArray *arraySettingsTitle;
    NSMutableDictionary *settingsDict;
}
@property (strong, nonatomic) IBOutlet UIView *tableFooterView;
@property (weak, nonatomic) IBOutlet UITableView *tableSettingsView;
- (IBAction)onBackAction:(id)sender;
- (IBAction)onLogoutAction:(UIButton *)sender;
- (IBAction)onDeleteAccountAction:(UIButton *)sender;
- (IBAction)onCancelAction:(UIButton *)sender;

@end

@implementation SettingsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
//    CGRect frame=self.tableFooterView.frame;
//    frame.size = CGSizeMake(self.tableSettingsView.frame.size.width, frame.size.height *SCREEN_XScale);
//    self.tableFooterView.frame = frame;
//    self.tableSettingsView.tableFooterView = self.tableFooterView;
    
    arraySettingsTitle = [[NSMutableArray alloc]init];
    
    [arraySettingsTitle addObject:@{@"title":@"Account"}];
    [arraySettingsTitle addObject:@{@"title":@"Email", @"isArrow":@YES, @"type":@"email"}];
    [arraySettingsTitle addObject:@{@"title":@"Phone", @"isArrow":@YES, @"type":@"phone"}];
    [arraySettingsTitle addObject:@{@"title":@"Password", @"isArrow":@YES, @"type":@"password"}];
    [arraySettingsTitle addObject:@{@"title":@"Private Account", @"isArrow":@NO, @"type":@"account"}];
    [arraySettingsTitle addObject:@{@"title":@"Log Out", @"isArrow":@YES, @"type":@"logout"}];// Added Logout
    [arraySettingsTitle addObject:@{@"title":@"Notifications"}];
    [arraySettingsTitle addObject:@{@"title":@"Followers", @"isArrow":@NO, @"type":@"followers"}];
    [arraySettingsTitle addObject:@{@"title":@"New Contacts", @"isArrow":@NO, @"type":@"contact"}];
    [arraySettingsTitle addObject:@{@"title":@"Likes", @"isArrow":@NO, @"type":@"like"}];
    [arraySettingsTitle addObject:@{@"title":@"Comments", @"isArrow":@NO, @"type":@"comment"}];
    [arraySettingsTitle addObject:@{@"title":@"Replugs", @"isArrow":@NO, @"type":@"replug"}];
    [arraySettingsTitle addObject:@{@"title":@"Follow People"}];
    [arraySettingsTitle addObject:@{@"title":@"Facebook", @"isArrow":@YES, @"type":@"facebook"}];
//    [arraySettingsTitle addObject:@{@"title":@"Invite Friends", @"isArrow":@YES, @"type":@"invite"}];
    [arraySettingsTitle addObject:@{@"title":@"Support"}];
    [arraySettingsTitle addObject:@{@"title":@"Terms", @"isArrow":@YES, @"type":@"terms"}];
    [arraySettingsTitle addObject:@{@"title":@"Privacy", @"isArrow":@YES, @"type":@"privacy"}];
    
    settingsDict = [[NSUSERDEFAULTS objectForKey:@"settings"]mutableCopy];
    if (settingsDict && settingsDict.allKeys.count>0)
    {
        NSString *accountType = settingsDict[@"account_type"];
        if ([accountType isEqualToString:@"public"])
        {
            
        }
        else
        {
            [arraySettingsTitle insertObject:@{@"subtitle":@"Only people you accept can view your account of Gifplug"} atIndex:5];
        }
        [self.tableSettingsView reloadData];
    }
    else
    {
        [self getUserSettings];
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -
#pragma mark UITableView Methods
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arraySettingsTitle.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (arraySettingsTitle[indexPath.row][@"isArrow"] != nil)
    {
        return 44.0f * SCREEN_XScale;
    }
    else
    {
        if (arraySettingsTitle[indexPath.row][@"title"]) {
            return 50.0f * SCREEN_XScale; // Header
        }
        return 57.0f * SCREEN_XScale; // Private Subtitle
    }
    return UITableViewAutomaticDimension;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SettingsCell *cell = nil;
    
    if (arraySettingsTitle[indexPath.row][@"isArrow"] != nil)
    {
        cell = [tableView dequeueReusableCellWithIdentifier:kCellIdentifierContent];
        if (!cell)
        {
            cell = [[NSBundle mainBundle]loadNibNamed:NSStringFromClass([SettingsCell class]) owner:self options:nil][1];
        }
    }
    else
    {
        if (arraySettingsTitle[indexPath.row][@"title"]) {
             // Header
            cell = [tableView dequeueReusableCellWithIdentifier:kCellIdentifierHeader];
            if (!cell)
            {
                cell = [[NSBundle mainBundle]loadNibNamed:NSStringFromClass([SettingsCell class]) owner:self options:nil][0];
            }
        }
        else
        {
            // Private Subtitle
            cell = [tableView dequeueReusableCellWithIdentifier:kCellIdentifierContentPrivate];
            if (!cell)
            {
                cell = [[NSBundle mainBundle]loadNibNamed:NSStringFromClass([SettingsCell class]) owner:self options:nil][2];
            }
        }
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(SettingsCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (arraySettingsTitle[indexPath.row][@"isArrow"] != nil)
    {
        cell.titleLabel.text = arraySettingsTitle[indexPath.row][@"title"];
        BOOL isArrow = [arraySettingsTitle[indexPath.row][@"isArrow"] boolValue];
        if (!isArrow)
        {
            if (settingsDict.allKeys.count>0)
            {
                BOOL isOn = NO;
                NSString *type = arraySettingsTitle[indexPath.row][@"type"];
                if ([type isEqualToString:@"account"])
                {
                    NSString *accountType = settingsDict[@"account_type"];
                    if ([accountType isEqualToString:@"public"])
                    {
                        isOn = NO;
                    }
                    else
                    {
                        isOn = YES;
                    }
                }
                else if ([type isEqualToString:@"followers"])
                {
                    BOOL should = [settingsDict[@"notifications"][@"followRequest"]boolValue];
                    if (!should)
                    {
                        isOn = NO;
                    }
                    else
                    {
                        isOn = YES;
                    }
                }
                else if ([type isEqualToString:@"contact"])
                {
                    BOOL should = [settingsDict[@"notifications"][@"newContact"]boolValue];
                    if (!should)
                    {
                        isOn = NO;
                    }
                    else
                    {
                        isOn = YES;
                    }
                }
                else if ([type isEqualToString:@"like"])
                {
                    BOOL should = [settingsDict[@"notifications"][@"like"]boolValue];
                    if (!should)
                    {
                        isOn = NO;
                    }
                    else
                    {
                        isOn = YES;
                    }
                }
                else if ([type isEqualToString:@"replug"])
                {
                    BOOL should = [settingsDict[@"notifications"][@"replug"]boolValue];
                    if (!should)
                    {
                        isOn = NO;
                    }
                    else
                    {
                        isOn = YES;
                    }
                }
                else
                {
                    BOOL should = [settingsDict[@"notifications"][@"comment"]boolValue];
                    if (!should)
                    {
                        isOn = NO;
                    }
                    else
                    {
                        isOn = YES;
                    }
                }
                if (!isOn)
                {
                    cell.iconType = ICON_TYPE_UNSELECTED;
                    cell.iconSelected.image = [UIImage imageNamed:@"settingsUnSelected"];
                }
                else
                {
                    cell.iconType = ICON_TYPE_SELECTED;
                    cell.iconSelected.image = [UIImage imageNamed:@"settingsSelected"];
                }
                cell.iconSelected.hidden = NO;
                cell.icon.hidden = YES;
            }
        }
        else
        {
            NSString *type = arraySettingsTitle[indexPath.row][@"type"];
            cell.iconSelected.hidden = YES;
            cell.icon.hidden = NO;
            cell.iconType = ICON_TYPE_ARROW;
            if ([type isEqualToString:@"logout"])
            {
                cell.iconSelected.hidden = YES;
                cell.icon.hidden = YES;
                cell.iconType = ICON_TYPE_ARROW;
            }
        }
    }
    else
    {
        if (arraySettingsTitle[indexPath.row][@"title"]) {
            // Header
            cell.titleLabel.text = arraySettingsTitle[indexPath.row][@"title"];
        }
        else
        {
            // Private Subtitle
            cell.titleLabel.text = arraySettingsTitle[indexPath.row][@"subtitle"];
        }
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    if (arraySettingsTitle[indexPath.row][@"isArrow"] != nil)
    {
        BOOL isArrow = [arraySettingsTitle[indexPath.row][@"isArrow"] boolValue];
        NSString *type = arraySettingsTitle[indexPath.row][@"type"];
        if (!isArrow)
        {
            SettingsCell *cell = [tableView cellForRowAtIndexPath:indexPath];
            
            if ([type isEqualToString:@"account"]) {
                if (cell.iconType == ICON_TYPE_UNSELECTED)
                {
                    [settingsDict setValue:@"private" forKey:@"account_type"];
                    [NSUSERDEFAULTS setObject:settingsDict forKey:@"settings"];
                    [NSUSERDEFAULTS synchronize];
                    cell.iconType = ICON_TYPE_SELECTED;
                    cell.iconSelected.image = [UIImage imageNamed:@"settingsSelected"];
                    [arraySettingsTitle insertObject:@{@"subtitle":@"Only people you accept can view your account of Gifplug"} atIndex:5];
                    [tableView beginUpdates];
                    [tableView insertRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:5 inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
                    [tableView endUpdates];
                }
                else{
                    [settingsDict setValue:@"public" forKey:@"account_type"];
                    [NSUSERDEFAULTS setObject:settingsDict forKey:@"settings"];
                    [NSUSERDEFAULTS synchronize];
                    cell.iconSelected.image = [UIImage imageNamed:@"settingsUnSelected"];
                    cell.iconType = ICON_TYPE_UNSELECTED;
                    [arraySettingsTitle removeObjectAtIndex:5];
                    [tableView beginUpdates];
                    [tableView deleteRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:5 inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
                    [tableView endUpdates];
                }
            }
            else if ([type isEqualToString:@"followers"])
            {
                if (cell.iconType == ICON_TYPE_UNSELECTED)
                {
                    NSMutableDictionary *dict = [settingsDict[@"notifications"]mutableCopy];
                    [dict setValue:@YES forKey:@"followRequest"];
                    [settingsDict setValue:dict forKey:@"notifications"];
                    [NSUSERDEFAULTS setObject:settingsDict forKey:@"settings"];
                    [NSUSERDEFAULTS synchronize];
                    cell.iconType = ICON_TYPE_SELECTED;
                    cell.iconSelected.image = [UIImage imageNamed:@"settingsSelected"];
                }
                else{
                    NSMutableDictionary *dict = [settingsDict[@"notifications"]mutableCopy];
                    [dict setValue:@NO forKey:@"followRequest"];
                    [settingsDict setValue:dict forKey:@"notifications"];
                    [NSUSERDEFAULTS setObject:settingsDict forKey:@"settings"];
                    [NSUSERDEFAULTS synchronize];
                    cell.iconSelected.image = [UIImage imageNamed:@"settingsUnSelected"];
                    cell.iconType = ICON_TYPE_UNSELECTED;
                }
            }
            else if ([type isEqualToString:@"contact"])
            {
                if (cell.iconType == ICON_TYPE_UNSELECTED)
                {
                    NSMutableDictionary *dict = [settingsDict[@"notifications"]mutableCopy];
                    [dict setValue:@YES forKey:@"newContact"];
                    [settingsDict setValue:dict forKey:@"notifications"];
                    [NSUSERDEFAULTS setObject:settingsDict forKey:@"settings"];
                    [NSUSERDEFAULTS synchronize];
                    cell.iconType = ICON_TYPE_SELECTED;
                    cell.iconSelected.image = [UIImage imageNamed:@"settingsSelected"];
                }
                else{
                    NSMutableDictionary *dict = [settingsDict[@"notifications"]mutableCopy];
                    [dict setValue:@NO forKey:@"newContact"];
                    [settingsDict setValue:dict forKey:@"notifications"];
                    [NSUSERDEFAULTS setObject:settingsDict forKey:@"settings"];
                    [NSUSERDEFAULTS synchronize];
                    cell.iconSelected.image = [UIImage imageNamed:@"settingsUnSelected"];
                    cell.iconType = ICON_TYPE_UNSELECTED;
                }
            }
            else if ([type isEqualToString:@"like"])
            {
                if (cell.iconType == ICON_TYPE_UNSELECTED)
                {
                    NSMutableDictionary *dict = [settingsDict[@"notifications"]mutableCopy];
                    [dict setValue:@YES forKey:@"like"];
                    [settingsDict setValue:dict forKey:@"notifications"];
                    [NSUSERDEFAULTS setObject:settingsDict forKey:@"settings"];
                    [NSUSERDEFAULTS synchronize];
                    cell.iconType = ICON_TYPE_SELECTED;
                    cell.iconSelected.image = [UIImage imageNamed:@"settingsSelected"];
                }
                else{
                    NSMutableDictionary *dict = [settingsDict[@"notifications"]mutableCopy];
                    [dict setValue:@NO forKey:@"like"];
                    [settingsDict setValue:dict forKey:@"notifications"];
                    [NSUSERDEFAULTS setObject:settingsDict forKey:@"settings"];
                    [NSUSERDEFAULTS synchronize];
                    cell.iconSelected.image = [UIImage imageNamed:@"settingsUnSelected"];
                    cell.iconType = ICON_TYPE_UNSELECTED;
                }
            }
            else if ([type isEqualToString:@"replug"])
            {
                if (cell.iconType == ICON_TYPE_UNSELECTED)
                {
                    NSMutableDictionary *dict = [settingsDict[@"notifications"]mutableCopy];
                    [dict setValue:@YES forKey:@"replug"];
                    [settingsDict setValue:dict forKey:@"notifications"];
                    [NSUSERDEFAULTS setObject:settingsDict forKey:@"settings"];
                    [NSUSERDEFAULTS synchronize];
                    cell.iconType = ICON_TYPE_SELECTED;
                    cell.iconSelected.image = [UIImage imageNamed:@"settingsSelected"];
                }
                else{
                    NSMutableDictionary *dict = [settingsDict[@"notifications"]mutableCopy];
                    [dict setValue:@NO forKey:@"replug"];
                    [settingsDict setValue:dict forKey:@"notifications"];
                    [NSUSERDEFAULTS setObject:settingsDict forKey:@"settings"];
                    [NSUSERDEFAULTS synchronize];
                    cell.iconSelected.image = [UIImage imageNamed:@"settingsUnSelected"];
                    cell.iconType = ICON_TYPE_UNSELECTED;
                }
            }
            else
            {
                if (cell.iconType == ICON_TYPE_UNSELECTED)
                {
                    NSMutableDictionary *dict = [settingsDict[@"notifications"]mutableCopy];
                    [dict setValue:@YES forKey:@"comment"];
                    [settingsDict setValue:dict forKey:@"notifications"];
                    [NSUSERDEFAULTS setObject:settingsDict forKey:@"settings"];
                    [NSUSERDEFAULTS synchronize];
                    cell.iconType = ICON_TYPE_SELECTED;
                    cell.iconSelected.image = [UIImage imageNamed:@"settingsSelected"];
                }
                else{
                    NSMutableDictionary *dict = [settingsDict[@"notifications"]mutableCopy];
                    [dict setValue:@NO forKey:@"comment"];
                    [settingsDict setValue:dict forKey:@"notifications"];
                    [NSUSERDEFAULTS setObject:settingsDict forKey:@"settings"];
                    [NSUSERDEFAULTS synchronize];
                    cell.iconSelected.image = [UIImage imageNamed:@"settingsUnSelected"];
                    cell.iconType = ICON_TYPE_UNSELECTED;
                }
            }
            [self updateUserSettingsForType:type];
        }
        else
        {
            if ([type isEqualToString:@"facebook"]) {
                FacebookInviteView *fbVC = [[FacebookInviteView alloc]initWithNibName:NSStringFromClass([FacebookInviteView class]) bundle:nil];
                [self.navigationController pushViewController:fbVC animated:YES];
                return;
            }
            else if ([type isEqualToString:@"logout"]) {
                [self onLogoutAction:nil];
                return;
            }
            else if ([type isEqualToString:@"terms"])
            {
                NSString *path = [[NSBundle mainBundle]pathForResource:@"terms" ofType:@"docx"];
                WebViewVC *webVC = [[WebViewVC alloc]init];
                webVC.webURL = [NSURL fileURLWithPath:path];
                [self.navigationController pushViewController:webVC animated:YES];
                return;
            }
            else if ([type isEqualToString:@"privacy"])
            {
                NSString *path = [[NSBundle mainBundle]pathForResource:@"privacy" ofType:@"docx"];
                WebViewVC *webVC = [[WebViewVC alloc]init];
                webVC.webURL = [NSURL fileURLWithPath:path];
                [self.navigationController pushViewController:webVC animated:YES];
                return;
            }
            UpdateSettingVC *updateVC = [[UpdateSettingVC alloc]initWithNibName:NSStringFromClass([UpdateSettingVC class]) bundle:nil];
            updateVC.updateType = type;
            [self presentViewController:updateVC animated:YES completion:nil];
        }
    }
}

- (IBAction)onBackAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)onLogoutAction:(UIButton *)sender {
    
    __weak typeof(self) weakSelf = self;
    
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@"Logout"
                                  message:@"Are you sure you want to logout?"
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *ok=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [weakSelf logout];
        [alert dismissViewControllerAnimated:NO completion:nil];
        [CommonFunction removeAllUserDefaults];
        unReadCount = 0;
        [APPDELEGATE.communication closeStream];
        [[AppWebHandler sharedInstance]removeDataLogout];
        [[AppDataManager sharedInstance]onLogoutDataRemove];
        [CommonFunction changeRootViewController:NO];
    }];
    
    UIAlertAction* simpleCancle = [UIAlertAction
                                   actionWithTitle:@"Cancel"
                                   style:UIAlertActionStyleCancel
                                   handler:^(UIAlertAction * action)
                                   {
                                       [alert dismissViewControllerAnimated:YES completion:nil];
                                       
                                   }];
    [alert addAction:ok];
    [alert addAction:simpleCancle];
    [self presentViewController:alert animated:YES completion:nil];
}

- (IBAction)onDeleteAccountAction:(UIButton *)sender {
//    [CommonFunction showAlertWithTitle:@"Gifplug" message:@"We are working on it." onViewController:self withButtonsArray:@[@"OK"] dismissBlock:nil];
    __weak typeof(self) weakSelf = self;
    
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@"Delete Account"
                                  message:@"Are you sure you want to permanentaly delete your account from Gifplug?"
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *ok=[UIAlertAction actionWithTitle:@"Yes! I'm Sure" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [weakSelf deleteAccount];
        [alert dismissViewControllerAnimated:NO completion:nil];
    }];
    
    UIAlertAction* simpleCancle = [UIAlertAction
                                   actionWithTitle:@"Cancel"
                                   style:UIAlertActionStyleCancel
                                   handler:^(UIAlertAction * action)
                                   {
                                       [alert dismissViewControllerAnimated:YES completion:nil];
                                   }];
    [alert addAction:ok];
    [alert addAction:simpleCancle];
    [self presentViewController:alert animated:YES completion:nil];
}

- (IBAction)onCancelAction:(UIButton *)sender {
    [self onBackAction:nil];
}

-(void)getUserSettings
{
    NSString *url = [NSString stringWithFormat:@"users/userSettings/1"];
    
    NSMutableDictionary *header =[NSMutableDictionary dictionaryWithObjectsAndKeys:@"application/json",@"Content-Type",[NSUSERDEFAULTS valueForKey:kUSERTOKEN],@"User-Token", nil];
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    
    __weak typeof(self) weakSelf = self;
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypeGet withHttpHeaders:header withServiceName:url withParameters:nil withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"%ld",(long)[operation.response statusCode]);
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"--responce dict %@",responseDict);
        
        [CommonFunction removeActivityIndicator];
        if(responseDict==Nil){
            [CommonFunction alertTitle:@"" withMessage:@"Server Error"];
        }
        else if([operation.response statusCode]==200)
        {
            settingsDict = [responseDict mutableCopy];
            [settingsDict removeObjectForKey:@"msg"];
            NSString *accountType = settingsDict[@"account_type"];
            if ([accountType isEqualToString:@"public"])
            {
                
            }
            else
            {
                [arraySettingsTitle insertObject:@{@"subtitle":@"Only people you accept can view your account of Gifplug"} atIndex:5];
            }
            [NSUSERDEFAULTS setObject:settingsDict forKey:@"settings"];
            [NSUSERDEFAULTS synchronize];
        }
        else
        {
            [CommonFunction alertTitle:@"" withMessage:[responseDict objectForKey:@"msg"]];
        }
        [weakSelf.tableSettingsView reloadData];
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          [CommonFunction removeActivityIndicator];
                                          [self.tableSettingsView reloadData];
                                          if([operation.response statusCode]  == 400 ){
                                              NSLog(@"impo response %@",operation.response);
                                              [CommonFunction alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                          }
                                          else{
                                              
                                              if (wbServiceCount==1) {
                                                  wbServiceCount=0;
                                              }
                                              else
                                              {
                                                  [CommonFunction alertTitle:@"" withMessage:@"Network Error..."];
                                              }
                                          }
                                      }];
}

-(void)updateUserSettingsForType:(NSString *)updateType
{
    NSString *url = [NSString stringWithFormat:@"users/updateSettings"];
    
    NSMutableDictionary *header =[NSMutableDictionary dictionaryWithObjectsAndKeys:@"application/json",@"Content-Type",[NSUSERDEFAULTS valueForKey:kUSERTOKEN],@"User-Token", nil];
    
    NSMutableDictionary *params = [settingsDict mutableCopy];
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    
    __weak typeof(self) weakSelf = self;
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypePost withHttpHeaders:header withServiceName:url withParameters:params withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"--responce dict %@",responseDict);
        
        [CommonFunction removeActivityIndicator];
        if(responseDict==Nil){
            [CommonFunction alertTitle:@"" withMessage:@"Server Error"];
        }
        else if([operation.response statusCode]==200)
        {
            
        }
        else
        {
            [CommonFunction alertTitle:@"" withMessage:[responseDict objectForKey:@"msg"]];
        }
        [weakSelf.tableSettingsView reloadData];
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          [CommonFunction removeActivityIndicator];
                                          [self.tableSettingsView reloadData];
                                          if([operation.response statusCode]  == 400 ){
                                              NSLog(@"impo response %@",operation.response);
                                              [CommonFunction alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                          }
                                          else{
                                              
                                              if (wbServiceCount==1) {
                                                  wbServiceCount=0;
                                              }
                                              else
                                              {
                                                  [CommonFunction alertTitle:@"" withMessage:@"Network Error..."];
                                              }
                                          }
                                      }];
}

-(void)logout
{
    NSString *url = [NSString stringWithFormat:@"users/logout/1"];
    
    NSMutableDictionary *header =[NSMutableDictionary dictionaryWithObjectsAndKeys:@"application/json",@"Content-Type",[NSUSERDEFAULTS valueForKey:kUSERTOKEN],@"User-Token", nil];
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypeGet withHttpHeaders:header withServiceName:url withParameters:nil withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          
                                      }];
}

-(void)deleteAccount
{
    [CommonFunction showActivityIndicatorWithText:@""];
    NSString *url = [NSString stringWithFormat:@"users"];
    
    NSMutableDictionary *header =[NSMutableDictionary dictionaryWithObjectsAndKeys:@"application/json",@"Content-Type",[NSUSERDEFAULTS valueForKey:kUSERTOKEN],@"User-Token", nil];
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypeDelete withHttpHeaders:header withServiceName:url withParameters:nil withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        [CommonFunction removeActivityIndicator];
        
        [CommonFunction removeAllUserDefaults];
        [APPDELEGATE.communication closeStream];
        [[AppWebHandler sharedInstance]removeDataLogout];
        [[AppDataManager sharedInstance]onLogoutDataRemove];
        [CommonFunction changeRootViewController:NO];
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          [CommonFunction removeActivityIndicator];
                                          [CommonFunction showAlertWithTitle:@"Gifplug" message:@"Could not delete your account due to some issue, Try Later." onViewController:self withButtonsArray:nil dismissBlock:nil];
                                      }];
}

@end
