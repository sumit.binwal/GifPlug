//
//  ProfileVC.m
//  GiffPlugApp
//
//  Created by Kshitij Godara on 08/01/16.
//  Copyright © 2016 Kshitij Godara. All rights reserved.
//

#import "ProfileVC.h"
#import "LoginVC.h"
@interface ProfileVC ()

@end

@implementation ProfileVC

#pragma mark - View Life Cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

#pragma mark - IBActions

- (IBAction)logoutBtnPressed:(id)sender
{

    //Move to login Screen
    
    LoginVC *fufs=[[LoginVC alloc]initWithNibName:@"LoginVC" bundle:nil];
    APPDELEGATE.navigationController=[[UINavigationController alloc]initWithRootViewController:fufs];
    self.navigationController.navigationBarHidden=YES;
    [APPDELEGATE.window.window setRootViewController:self.navigationController];
    
    [NSUSERDEFAULTS removeObjectForKey:@"previousSelectedVC"];
    [NSUSERDEFAULTS removeObjectForKey:kUSERTOKEN];
    [CommonFunction changeRootViewController:NO];

}


#pragma mark  - Memory Managment

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


@end
