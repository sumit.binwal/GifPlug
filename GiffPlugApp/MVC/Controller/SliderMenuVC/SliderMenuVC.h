//
//  SliderMenuVC.h
//  GiffPlugApp
//
//  Created by Sumit Sharma on 16/03/16.
//  Copyright © 2016 Kshitij Godara. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SliderMenuVC : UIViewController
@property(nonatomic,strong)NSMutableDictionary *dictPostsData;
@property(nonatomic,strong)    NSString *strPostID;
@property(nonatomic,strong)    NSString *strUsername;
@property NSInteger gifCount;
@property(nonatomic)NSInteger index;
@end
