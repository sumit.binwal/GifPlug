//
//  LikeVC.m
//  GiffPlugApp
//
//  Created by Sumit Sharma on 09/03/16.
//  Copyright © 2016 Kshitij Godara. All rights reserved.
//

#import "CommentVC.h"
#import "CommentCustomeCell.h"
#import "OtherUserProfileVC.h"
#import "HomeFeedVC.h"
#import "AppDataManager.h"
#import <objc/runtime.h>
@interface CommentVC ()<UITextViewDelegate,UIGestureRecognizerDelegate,NSLayoutManagerDelegate>
{
    IBOutlet UIView *vwMessageBox;
    NSMutableArray *arrFollowersData;
    IBOutlet UILabel *lblErrorMsg;
    NSMutableArray *expandedIndexPaths;
    NSMutableArray *arrSelectedIndexPath;
    IBOutlet UITableView *tblVw;
    IBOutlet UITextView *txtVwComment;
    IBOutlet UILabel *lblCommentCount;
    IBOutlet NSLayoutConstraint *vwMsgBxHeightCnstrnt;
    float initialBoxHeightConstraint;
    
    NSInteger currentPage, totalPage;
    BOOL isPageRefreshing;
    
    NSMutableDictionary *removeableDict;
    NSInteger removeableIndex;
    AppDataManager *dataManager;
}
@end

@implementation CommentVC
@synthesize strLikeCount,strPostId;
- (void)viewDidLoad {
    [super viewDidLoad];
    
    dataManager = [AppDataManager sharedInstance];
    isPageRefreshing = YES;
    arrFollowersData = [[NSMutableArray alloc]init];
//    [APPDELEGATE.appTabBarController.tabBar setHidden:YES];
    lblCommentCount.text=strLikeCount;

    vwMsgBxHeightCnstrnt.constant=vwMsgBxHeightCnstrnt.constant*SCREEN_XScale;
//    self.sendBtnHeightConstraint.constant = self.sendBtnHeightConstraint.constant *SCREEN_XScale;
    currentPage =1;
    
    [tblVw registerNib:[UINib nibWithNibName:NSStringFromClass([CommentCustomeCell class]) bundle:nil] forCellReuseIdentifier:@"CommentCustomeCell"];
    
    initialBoxHeightConstraint=vwMsgBxHeightCnstrnt.constant;
    UITapGestureRecognizer *tapGesture=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(singleBtnClicked:)];
    tapGesture.numberOfTapsRequired=1;
    tapGesture.delegate=self;
    [self.view addGestureRecognizer:tapGesture];

    self.view.frame=CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height);
    
    txtVwComment.layoutManager.delegate=self;
    
    tblVw.estimatedRowHeight = 44.0f;
    tblVw.rowHeight = UITableViewAutomaticDimension;
    
    [txtVwComment becomeFirstResponder];
    
    // Do any additional setup after loading the view from its nib.
}

- (CGFloat)layoutManager:(NSLayoutManager *)layoutManager lineSpacingAfterGlyphAtIndex:(NSUInteger)glyphIndex withProposedLineFragmentRect:(CGRect)rect
{
    return 5.0f;
}

-(IBAction)singleBtnClicked:(id)sender
{
    [self.view endEditing:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillDisappear:(BOOL)animated
{
//    [APPDELEGATE.appTabBarController.tabBar setHidden:NO];
    [self deRegisterForKeyboardNotifications];

    [super viewWillDisappear:animated];
}

//- (void)viewDidAppear:(BOOL)animated
//{
//    [super viewDidAppear:animated];
//    [txtVwComment becomeFirstResponder];
//}

-(void)viewWillAppear:(BOOL)animated
{
    [self.navigationController.navigationBar setHidden:YES];
    [super viewWillAppear:YES];
    
    if ([CommonFunction reachabiltyCheck]) {
        [CommonFunction showActivityIndicatorWithText:@""];
        currentPage = 1;
        totalPage = 0;
        [self getPostedComment];
    }
    else
    {
        [CommonFunction showAlertWithTitle:@"GifPlug" message:@"Please check your network connection." onViewController:self useAsDelegate:NO dismissBlock:nil];
    }
    [self registerForKeyboardNotifications];
}


#pragma mark - IBAction Button Methods
- (IBAction)sendButtonClicked:(UIButton *)sender {
    
    sender.enabled = NO;
    [txtVwComment resignFirstResponder];
    [self.view endEditing:YES];
    if ([CommonFunction isValueNotEmpty:txtVwComment.text]) {
        if ([CommonFunction reachabiltyCheck]) {
            [CommonFunction showActivityIndicatorWithText:@""];
            
            [self postCommentAPI];
        }
        else
        {
            [CommonFunction showAlertWithTitle:@"" message:@"Please check your network connection." onViewController:self useAsDelegate:NO dismissBlock:nil];
        }
    }
    else
    {
        [CommonFunction showAlertWithTitle:@"" message:@"Please enter comment." onViewController:self useAsDelegate:NO dismissBlock:nil];
    }
    sender.enabled = YES;
    txtVwComment.text = @"";

}
- (IBAction)backBtnClicked:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
-(IBAction)profileBtnCLicked:(UITapGestureRecognizer *)sender
{
    NSString *loggedInUser = [NSUSERDEFAULTS objectForKey:kUSERID];
    if ([loggedInUser isEqualToString:arrFollowersData [sender.view.tag][@"username"]])
    {
    }
    else
    {
        OtherUserProfileVC *oupvc=[[OtherUserProfileVC alloc]initWithNibName:@"OtherUserProfileVC" bundle:nil];
        oupvc.hidesBottomBarWhenPushed=YES;
        oupvc.strUserName=arrFollowersData [sender.view.tag][@"username"];
        [self.navigationController pushViewController:oupvc animated:YES];
    }
}

-(IBAction)plusMinusBtnCLicked:(UIButton *)sender
{
    
    CommentCustomeCell *tableViewCell = (CommentCustomeCell *)sender.superview.superview;
    
    
    tableViewCell.btnFollowUnfollow.hidden = YES;
    tableViewCell.loader.hidden = NO;
    [tableViewCell.loader startAnimating];
    
    
    NSMutableDictionary *dataDict=[[NSMutableDictionary alloc]initWithObjectsAndKeys:[[arrFollowersData objectAtIndex:sender.tag]valueForKey:@"user_id"],@"user_id",[[arrFollowersData objectAtIndex:sender.tag]valueForKey:@"account_type"],@"account_type", nil];
    
    if ([[arrFollowersData[sender.tag][@"is_following"]stringValue] isEqualToString:@"0"]) {
        [self postFollowersListAPIAtIndex:sender.tag WithObject:dataDict];
    }
    else
    {
        [self unFollowUserApiAtIndex:sender.tag WithObject:dataDict];
    }
    
}


#pragma mark - UITableView Delegate Method

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *userdid = [NSUSERDEFAULTS objectForKey:kUSERID];
    if ([userdid isEqualToString:_strUsername])
    {
        return YES;
    }
    else
    {
        if ([[arrFollowersData [indexPath.row][@"is_my_comment"]stringValue]isEqualToString:@"1"]) {
            return YES;
        }
    }
    return NO;
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView
           editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewCellEditingStyleDelete;
}

- (NSString *)tableView:(UITableView *)tableView
titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return @"Delete    ";
}

- (void)tableView:(UITableView *)tableView
commitEditingStyle:(UITableViewCellEditingStyle)editingStyle
forRowAtIndexPath:(NSIndexPath *)indexPath
{
    removeableDict = [arrFollowersData[indexPath.row] mutableCopy];
    removeableIndex = indexPath.row;
    
    [arrFollowersData removeObjectAtIndex:indexPath.row];
    
    strLikeCount = [NSString stringWithFormat:@"%ld",(long)arrFollowersData.count];
    lblCommentCount.text=strLikeCount;
    
    
    NSInteger totalCount = [strLikeCount integerValue];
    
    for (NSInteger i=0; i<[AppDataManager sharedInstance].globalHomeFeedArray.count; i++)
    {
        NSString *postID = [AppDataManager sharedInstance].globalHomeFeedArray[i][@"post_id"];
        if ([postID isEqualToString:strPostId])
        {
            [[AppDataManager sharedInstance].globalHomeFeedArray[i] setObject:[NSNumber numberWithInteger:totalCount] forKey:@"comments_count"];
        }
    }
    
    for (NSInteger i=0; i<[AppDataManager sharedInstance].globalUserFeedArray.count; i++)
    {
        NSString *postID = [AppDataManager sharedInstance].globalUserFeedArray[i][@"post_id"];
        if ([postID isEqualToString:strPostId])
        {
            [[AppDataManager sharedInstance].globalUserFeedArray[i] setObject:[NSNumber numberWithInteger:totalCount] forKey:@"comments_count"];
        }
    }
    
    for (NSInteger i=0; i<[AppDataManager sharedInstance].globalUserReplugArray.count; i++)
    {
        NSString *postID = [AppDataManager sharedInstance].globalUserReplugArray[i][@"post_id"];
        if ([postID isEqualToString:strPostId])
        {
            [[AppDataManager sharedInstance].globalUserReplugArray[i] setObject:[NSNumber numberWithInteger:totalCount] forKey:@"comments_count"];
        }
    }
    
    
    [tableView beginUpdates];
    [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationLeft];
    [tableView endUpdates];
    [self deleteCommentWithDict:removeableDict atIndex:removeableIndex];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrFollowersData.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier=@"CommentCustomeCell";
    
    CommentCustomeCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    [cell setEditing:YES animated:YES];
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    
    for (UIGestureRecognizer *gesture in cell.imageViewProfile.gestureRecognizers)
    {
        [cell.imageViewProfile removeGestureRecognizer:gesture];
    }
    
    UITapGestureRecognizer *tappGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(profileBtnCLicked:)];
    tappGesture.numberOfTapsRequired=1;
    [cell.imageViewProfile addGestureRecognizer:tappGesture];
    cell.imageViewProfile.tag = indexPath.row;

    cell.btnFollowUnfollow.tag=indexPath.row;
    [cell.btnFollowUnfollow addTarget:self action:@selector(plusMinusBtnCLicked:) forControlEvents:UIControlEventTouchUpInside];
    
//    cell.imgProfileImg.tag=indexPath.row;
//    [cell.imgProfileImg addTarget:self action:@selector(profileBtnCLicked:) forControlEvents:UIControlEventTouchUpInside];
    
    
    if (arrFollowersData.count>0) {
        //lblErrormsg.text=@"";
        NSString *strAvtarimg=[NSString stringWithFormat:@"%@",[[arrFollowersData objectAtIndex:indexPath.row]valueForKey:@"avatar_image"]];
        
        if (strAvtarimg.length>1) {
            
            [cell.imageViewProfile sd_setImageWithURL:[NSURL URLWithString:strAvtarimg] placeholderImage:[UIImage imageNamed:@"default.jpg"] options:0 completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                if (error)
                {
                    cell.imageViewProfile.contentMode=UIViewContentModeScaleToFill;
                }
                else
                {
                    if ([[strAvtarimg pathExtension]isEqualToString:@"gif"])
                    {
                        cell.imageViewProfile.contentMode=UIViewContentModeScaleAspectFill;
                    }
                    else
                    {
                        cell.imageViewProfile.contentMode=UIViewContentModeScaleToFill;
                    }
                }
            }];
            
//            [cell.imgProfileImg sd_setImageWithURL:[NSURL URLWithString:strAvtarimg] forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"default.jpg"]];
        }
        else
        {
            [cell.imageViewProfile setImage:[UIImage imageNamed:@"default.jpg"]];
//            [cell.imgProfileImg setImage:[UIImage imageNamed:@"default.jpg"] forState:UIControlStateNormal];
        }
        
        if ([[arrFollowersData [indexPath.row][@"is_my_comment"]stringValue]isEqualToString:@"1"]) {
            [cell.btnFollowUnfollow setHidden:YES];
            [cell.labelRequested setHidden:YES];
        }
        else
        {
            if (arrFollowersData[indexPath.row][@"showDetail"])
            {
                if ([[dataManager.followersDataArray valueForKey:@"user_id"]containsObject:arrFollowersData[indexPath.row][@"user_id"]])
                {
                    NSInteger index = [[dataManager.followersDataArray valueForKey:@"user_id"]indexOfObject:arrFollowersData[indexPath.row][@"user_id"] ];
                    BOOL requestAcess = [dataManager.followersDataArray[index][@"request_status"]boolValue];
                    if (requestAcess) {
                        [cell.btnFollowUnfollow setImage:[UIImage imageNamed:@"FUFS_MinusIcon"] forState:UIControlStateNormal];
                        cell.btnFollowUnfollow.hidden = NO;
                        cell.labelRequested.hidden = YES;
                    }
                    else
                    {
                        cell.btnFollowUnfollow.hidden = YES;
                        cell.labelRequested.hidden = NO;
                    }
                }
                else
                {
                    BOOL isFollowing = [arrFollowersData [indexPath.row][@"is_following"]boolValue];
                    if (isFollowing)
                    {
                        BOOL isRequestAccepted = [arrFollowersData[indexPath.row][@"request_status"] boolValue];
                        if (isRequestAccepted) {
                            [cell.btnFollowUnfollow setImage:[UIImage imageNamed:@"FUFS_MinusIcon"] forState:UIControlStateNormal];
                            cell.btnFollowUnfollow.hidden = NO;
                            cell.labelRequested.hidden = YES;
                        }
                        else
                        {
                            cell.btnFollowUnfollow.hidden = YES;
                            cell.labelRequested.hidden = NO;
                        }
                    }
                    else
                    {
                        cell.btnFollowUnfollow.hidden = NO;
                        cell.labelRequested.hidden = YES;
                        [cell.btnFollowUnfollow setImage:[UIImage imageNamed:@"FUFS_PlusIcon"] forState:UIControlStateNormal];
                    }
                }
            }
            else
            {
                cell.btnFollowUnfollow.hidden = YES;
                cell.labelRequested.hidden = YES;
                cell.loader.hidden = YES;
            }
        }
        
        
        cell.lblUsername.text=[[arrFollowersData objectAtIndex:indexPath.row]valueForKey:@"username"];
        
        NSString *comentTxt = [NSString stringWithFormat:@"%@",[[arrFollowersData objectAtIndex:indexPath.row]valueForKey:@"message"]];
        
        NSMutableParagraphStyle *style = [[NSMutableParagraphStyle alloc] init];
        [style setLineSpacing:5];
//        [style setLineBreakMode:NSLineBreakByTruncatingTail];
        
        NSDictionary *attributtes = @{
                                      NSParagraphStyleAttributeName : style,
                                      };
        cell.lblComment.attributedText = [[NSAttributedString alloc] initWithString:comentTxt
                                                                         attributes:attributtes];
        
        cell.lblCommentTime.text = [NSDate prettyTimestampSinceDate:[NSDate dateWithTimeIntervalSince1970:[[[arrFollowersData objectAtIndex:indexPath.row]objectForKey:@"time"]doubleValue]]];
    }
    else
    {
//        lblErrorMsg.text=@"No User Found.";
    }

    
    return cell;
}


#pragma  mark - WebService API

-(void)getPostedComment
{
    NSString *url = [NSString stringWithFormat:@"posts/%@/comments/%ld",strPostId,(long)currentPage];
    //http://192.168.0.22:8353/v1//posts/{postId}/comments/{page}
    
    NSMutableDictionary *header =[[NSMutableDictionary alloc]initWithObjectsAndKeys:@"application/json",@"Content-Type",[NSUSERDEFAULTS valueForKey:kUSERTOKEN],@"User-Token", nil];
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    
    __weak typeof(self) weakSelf = self;
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypeGet withHttpHeaders:header withServiceName:url withParameters:nil withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"--responce dict %@",responseDict);
        
        [CommonFunction removeActivityIndicator];
        if(responseDict==Nil)        {
            [CommonFunction alertTitle:@"" withMessage:@"Server Error"];
            if (currentPage>1)
            {
                currentPage--;
            }
            isPageRefreshing = NO;
        }
        else if([operation.response statusCode]==200)
        {
            if ([responseDict[@"users"] isKindOfClass:[NSArray class]])
            {
                NSArray *arrListing = responseDict[@"users"];
                
                if (arrListing.count>0)
                {
//                    lblErrorMsg.text=@"";
                    if (currentPage == 1)
                    {
                        if (arrFollowersData.count > 0)
                        {
                            [arrFollowersData removeAllObjects];
                        }
                        [arrFollowersData addObjectsFromArray:[arrListing mutableCopy]];
                    }
                    else
                    {
                        [arrFollowersData addObjectsFromArray:[arrListing mutableCopy]];
                    }
                    totalPage=[[responseDict objectForKey:@"total_pages"] integerValue];
                    [tblVw reloadData];
                    
                    strLikeCount = [NSString stringWithFormat:@"%ld",(long)arrFollowersData.count];
                    lblCommentCount.text=strLikeCount;
                    
                    for (NSInteger i=0; i<arrFollowersData.count; i++)
                    {
                        if ([[dataManager.followersDataArray valueForKey:@"user_id"]containsObject:arrFollowersData[i][@"user_id"]])
                        {
                            NSInteger index = [[dataManager.followersDataArray valueForKey:@"user_id"]indexOfObject:arrFollowersData[i][@"user_id"]];
//                            BOOL requestAcess = [dataManager.followersDataArray[index][@"request_status"]boolValue];
                            BOOL requestAcess = [arrFollowersData[i][@"request_status"] boolValue];
                            [dataManager.followersDataArray[index] setValue:[NSNumber numberWithBool:requestAcess] forKey:@"request_status"];
                            BOOL isFolowing = [arrFollowersData[i][@"is_following"] boolValue];
                            if (!isFolowing)
                            {
                                [dataManager.followersDataArray removeObjectAtIndex:index];
                            }
                        }
                        NSInteger someIndex = [[arrFollowersData valueForKey:@"username"]indexOfObject:arrFollowersData[i][@"username"]];
                        [arrFollowersData[someIndex] setValue:@YES forKey:@"showDetail"];
                    }
                }
            }
            
            if (arrFollowersData.count==0) {
//                lblErrorMsg.text=@"No Comments Found.";
            }
            else
            {
//                lblErrorMsg.text=@"";
            }
            isPageRefreshing = NO;
        }
        else
        {
            if (currentPage>1)
            {
                currentPage--;
            }
            isPageRefreshing = NO;
            [CommonFunction alertTitle:@"" withMessage:[responseDict objectForKey:@"msg"]];
        }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          
                                          if (currentPage>1)
                                          {
                                              currentPage--;
                                          }
                                          isPageRefreshing = NO;
                                          
                                          if([operation.response statusCode]  == 400 ){
                                              NSLog(@"impo response %@",operation.response);
                                              
                                              [CommonFunction removeActivityIndicator];
                                              [CommonFunction alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                          }
                                          else{
                                              if (wbServiceCount==1) {
                                                  wbServiceCount=0;
                                                  [CommonFunction showActivityIndicatorWithText:@""];
                                                  [weakSelf getPostedComment];
                                              }
                                              else
                                              {
                                                  [CommonFunction removeActivityIndicator];
                                                  [CommonFunction alertTitle:@"" withMessage:@"Network Error..."];
                                              }
                                          }
                                      }];
}



-(void)postCommentAPI
{
    NSString *url = [NSString stringWithFormat:@"posts/%@/comments",strPostId];
    //http://192.168.0.22:8353/v1//posts/{postId}/comments/{page}
    
    NSMutableDictionary *header =[[NSMutableDictionary alloc]initWithObjectsAndKeys:@"application/json",@"Content-Type",[NSUSERDEFAULTS valueForKey:kUSERTOKEN],@"User-Token", nil];
    
        NSMutableDictionary *param=[[NSMutableDictionary alloc]initWithObjectsAndKeys:[CommonFunction trimSpaceInString:txtVwComment.text],@"message", nil];
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypePost withHttpHeaders:header withServiceName:url withParameters:param withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"--responce dict %@",responseDict);
        
        [CommonFunction removeActivityIndicator];
        if(responseDict==Nil)        {
            [CommonFunction alertTitle:@"" withMessage:@"Server Error"];
        }
        else if([operation.response statusCode]==200)
        {
            
            if (self.commentDelegate && [self.commentDelegate respondsToSelector:@selector(getGifLatestDetailsWithID:)])
            {
                [self.commentDelegate getGifLatestDetailsWithID:strPostId];
            }
            
//            NSInteger totalCount = [strLikeCount integerValue]+1;
            
//            for (NSInteger i=0; i<[AppDataManager sharedInstance].globalHomeFeedArray.count; i++)
//            {
//                NSString *postID = [AppDataManager sharedInstance].globalHomeFeedArray[i][@"post_id"];
//                if ([postID isEqualToString:strPostId])
//                {
//                    [[AppDataManager sharedInstance].globalHomeFeedArray[i] setObject:[NSNumber numberWithInteger:totalCount] forKey:@"comments_count"];
//                    [[AppDataManager sharedInstance].globalHomeFeedArray[i] setObject:@1 forKey:@"is_comment"];
//                }
//            }
//            
//            for (NSInteger i=0; i<[AppDataManager sharedInstance].globalUserFeedArray.count; i++)
//            {
//                NSString *postID = [AppDataManager sharedInstance].globalUserFeedArray[i][@"post_id"];
//                if ([postID isEqualToString:strPostId])
//                {
//                    [[AppDataManager sharedInstance].globalUserFeedArray[i] setObject:[NSNumber numberWithInteger:totalCount] forKey:@"comments_count"];
//                    
//                    [[AppDataManager sharedInstance].globalUserFeedArray[i] setObject:@1 forKey:@"is_comment"];
//                }
//            }
//            
//            for (NSInteger i=0; i<[AppDataManager sharedInstance].globalUserReplugArray.count; i++)
//            {
//                NSString *postID = [AppDataManager sharedInstance].globalUserReplugArray[i][@"post_id"];
//                if ([postID isEqualToString:strPostId])
//                {
//                    [[AppDataManager sharedInstance].globalUserReplugArray[i] setObject:[NSNumber numberWithInteger:totalCount] forKey:@"comments_count"];
//                    
//                    [[AppDataManager sharedInstance].globalUserReplugArray[i] setObject:@1 forKey:@"is_comment"];
//                }
//            }
            
//            strLikeCount = [NSString stringWithFormat:@"%ld",(long)totalCount];
//            lblCommentCount.text=strLikeCount;
            txtVwComment.text=@"";
            [vwMessageBox viewWithTag:201].hidden=NO;
            [txtVwComment resignFirstResponder];
            vwMsgBxHeightCnstrnt.constant=initialBoxHeightConstraint;
            //[txtVwComment becomeFirstResponder];
            currentPage = 1;
            totalPage = 0;
            [self getPostedComment];
            // strMainImgURl=[responseDict objectForKey:@"site_url"];
            //            if (!arrFollowersData.count>0) {
            //                lblErrorMsg.text=@"No Comments Found.";
            //            }
            //            else
            //            {
            //                lblErrorMsg.text=@"";
            //                arrFollowersData=[responseDict objectForKey:@"users"];
            //                [tblVw reloadData];
            //            }
        }
        else
        {
            [CommonFunction alertTitle:@"" withMessage:[responseDict objectForKey:@"msg"]];
        }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          if([operation.response statusCode]  == 400 ){
                                              NSLog(@"impo response %@",operation.response);
                                              
                                              [CommonFunction removeActivityIndicator];
                                              [CommonFunction alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                          }
                                          else{
                                              
                                              if (wbServiceCount==1) {
                                                  wbServiceCount=0;
                                                  currentPage = 1;
                                                  totalPage = 0;
                                                  [CommonFunction showActivityIndicatorWithText:@""];
                                                  [self getPostedComment];
                                              }
                                              else
                                              {
                                                  [CommonFunction removeActivityIndicator];
                                                  [CommonFunction alertTitle:@"" withMessage:@"Network Error..."];
                                              }
                                              
                                          }
                                          
                                      }];
}


- (void)deleteCommentWithDict:(NSMutableDictionary *)dict atIndex:(NSInteger)index
{
    if (!dict) {
        return;
    }
    NSString *commentID = dict[@"comment_id"];
    NSString *url = [NSString stringWithFormat:@"posts/%@/comments/%@",strPostId,commentID];
    
    NSMutableDictionary *header =[[NSMutableDictionary alloc]initWithObjectsAndKeys:@"application/json",@"Content-Type",[NSUSERDEFAULTS valueForKey:kUSERTOKEN],@"User-Token", nil];
        
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypeDelete withHttpHeaders:header withServiceName:url withParameters:nil withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"--responce dict %@",responseDict);
        
        [CommonFunction removeActivityIndicator];
        if(responseDict==Nil)
        {
            [CommonFunction alertTitle:@"" withMessage:@"Server Error"];
            [arrFollowersData insertObject:dict atIndex:index];
            [tblVw beginUpdates];
            [tblVw insertRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:index inSection:0]]withRowAnimation:UITableViewRowAnimationLeft];
            [tblVw endUpdates];
            
            strLikeCount = [NSString stringWithFormat:@"%ld",(long)arrFollowersData.count];
            lblCommentCount.text=strLikeCount;
            
            
            NSInteger totalCount = [strLikeCount integerValue];
            
            for (NSInteger i=0; i<[AppDataManager sharedInstance].globalHomeFeedArray.count; i++)
            {
                NSString *postID = [AppDataManager sharedInstance].globalHomeFeedArray[i][@"post_id"];
                if ([postID isEqualToString:strPostId])
                {
                    [[AppDataManager sharedInstance].globalHomeFeedArray[i] setObject:[NSNumber numberWithInteger:totalCount] forKey:@"comments_count"];
                }
            }
            
            for (NSInteger i=0; i<[AppDataManager sharedInstance].globalUserFeedArray.count; i++)
            {
                NSString *postID = [AppDataManager sharedInstance].globalUserFeedArray[i][@"post_id"];
                if ([postID isEqualToString:strPostId])
                {
                    [[AppDataManager sharedInstance].globalUserFeedArray[i] setObject:[NSNumber numberWithInteger:totalCount] forKey:@"comments_count"];
                }
            }
            
            for (NSInteger i=0; i<[AppDataManager sharedInstance].globalUserReplugArray.count; i++)
            {
                NSString *postID = [AppDataManager sharedInstance].globalUserReplugArray[i][@"post_id"];
                if ([postID isEqualToString:strPostId])
                {
                    [[AppDataManager sharedInstance].globalUserReplugArray[i] setObject:[NSNumber numberWithInteger:totalCount] forKey:@"comments_count"];
                }
            }
        }
        else if([operation.response statusCode]==200)
        {
            if (self.commentDelegate && [self.commentDelegate respondsToSelector:@selector(getGifLatestDetailsWithID:)])
            {
                [self.commentDelegate getGifLatestDetailsWithID:strPostId];
            }
            for (NSInteger i=0; i<arrFollowersData.count; i++)
            {
                NSInteger someIndex = [[arrFollowersData valueForKey:@"username"]indexOfObject:arrFollowersData[i][@"username"]];
                [arrFollowersData[someIndex] setValue:@YES forKey:@"showDetail"];
            }
        }
        else
        {
            [arrFollowersData insertObject:dict atIndex:index];
            [tblVw beginUpdates];
            [tblVw insertRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:index inSection:0]]withRowAnimation:UITableViewRowAnimationLeft];
            [tblVw endUpdates];
            
            strLikeCount = [NSString stringWithFormat:@"%ld",(long)arrFollowersData.count];
            lblCommentCount.text=strLikeCount;
            
            
            NSInteger totalCount = [strLikeCount integerValue];
            
            for (NSInteger i=0; i<[AppDataManager sharedInstance].globalHomeFeedArray.count; i++)
            {
                NSString *postID = [AppDataManager sharedInstance].globalHomeFeedArray[i][@"post_id"];
                if ([postID isEqualToString:strPostId])
                {
                    [[AppDataManager sharedInstance].globalHomeFeedArray[i] setObject:[NSNumber numberWithInteger:totalCount] forKey:@"comments_count"];
                }
            }
            
            for (NSInteger i=0; i<[AppDataManager sharedInstance].globalUserFeedArray.count; i++)
            {
                NSString *postID = [AppDataManager sharedInstance].globalUserFeedArray[i][@"post_id"];
                if ([postID isEqualToString:strPostId])
                {
                    [[AppDataManager sharedInstance].globalUserFeedArray[i] setObject:[NSNumber numberWithInteger:totalCount] forKey:@"comments_count"];
                }
            }
            
            for (NSInteger i=0; i<[AppDataManager sharedInstance].globalUserReplugArray.count; i++)
            {
                NSString *postID = [AppDataManager sharedInstance].globalUserReplugArray[i][@"post_id"];
                if ([postID isEqualToString:strPostId])
                {
                    [[AppDataManager sharedInstance].globalUserReplugArray[i] setObject:[NSNumber numberWithInteger:totalCount] forKey:@"comments_count"];
                }
            }
            
            [CommonFunction alertTitle:@"" withMessage:[responseDict objectForKey:@"msg"]];
        }
        [tblVw reloadData];
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          [arrFollowersData insertObject:dict atIndex:removeableIndex];
                                          [tblVw beginUpdates];
                                          [tblVw insertRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:removeableIndex inSection:0]]withRowAnimation:UITableViewRowAnimationLeft];
                                          [tblVw endUpdates];
                                          
                                          strLikeCount = [NSString stringWithFormat:@"%ld",(long)arrFollowersData.count];
                                          lblCommentCount.text=strLikeCount;
                                          
                                          
                                          NSInteger totalCount = [strLikeCount integerValue];
                                          
                                          for (NSInteger i=0; i<[AppDataManager sharedInstance].globalHomeFeedArray.count; i++)
                                          {
                                              NSString *postID = [AppDataManager sharedInstance].globalHomeFeedArray[i][@"post_id"];
                                              if ([postID isEqualToString:strPostId])
                                              {
                                                  [[AppDataManager sharedInstance].globalHomeFeedArray[i] setObject:[NSNumber numberWithInteger:totalCount] forKey:@"comments_count"];
                                              }
                                          }
                                          
                                          for (NSInteger i=0; i<[AppDataManager sharedInstance].globalUserFeedArray.count; i++)
                                          {
                                              NSString *postID = [AppDataManager sharedInstance].globalUserFeedArray[i][@"post_id"];
                                              if ([postID isEqualToString:strPostId])
                                              {
                                                  [[AppDataManager sharedInstance].globalUserFeedArray[i] setObject:[NSNumber numberWithInteger:totalCount] forKey:@"comments_count"];
                                              }
                                          }
                                          
                                          for (NSInteger i=0; i<[AppDataManager sharedInstance].globalUserReplugArray.count; i++)
                                          {
                                              NSString *postID = [AppDataManager sharedInstance].globalUserReplugArray[i][@"post_id"];
                                              if ([postID isEqualToString:strPostId])
                                              {
                                                  [[AppDataManager sharedInstance].globalUserReplugArray[i] setObject:[NSNumber numberWithInteger:totalCount] forKey:@"comments_count"];
                                              }
                                          }
                                          
                                          if([operation.response statusCode]  == 400 ){
                                              NSLog(@"impo response %@",operation.response);
                                              
                                              [CommonFunction removeActivityIndicator];
                                              [CommonFunction alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                          }
                                          else{
                                              
                                              if (wbServiceCount==1) {
                                              }
                                              else
                                              {
                                                  [CommonFunction removeActivityIndicator];
                                                  [CommonFunction alertTitle:@"" withMessage:@"Network Error..."];
                                              }
                                              
                                          }
                                          [tblVw reloadData];
                                      }];
}

-(void)postFollowersListAPIAtIndex:(NSInteger)index WithObject:(NSDictionary *)dataDict
{
    NSString *url = [NSString stringWithFormat:@"users/following"];
    //http://192.168.0.22:8353/v1/users/following
    
    NSMutableDictionary *header =[[NSMutableDictionary alloc]initWithObjectsAndKeys:@"application/json",@"Content-Type",[NSUSERDEFAULTS valueForKey:kUSERTOKEN],@"User-Token", nil];
    
    NSMutableDictionary *param=[[NSMutableDictionary alloc]initWithObjectsAndKeys:@[dataDict],@"following", nil];
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypePost withHttpHeaders:header withServiceName:url withParameters:param withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        CommentCustomeCell *tableViewCell = (CommentCustomeCell *)[tblVw cellForRowAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0]];
        
        tableViewCell.btnFollowUnfollow.hidden = NO;
        tableViewCell.loader.hidden = YES;
        [tableViewCell.loader stopAnimating];
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"responce dict %@",responseDict);
        
        [CommonFunction removeActivityIndicator];
        if(responseDict==Nil)        {
            [CommonFunction alertTitle:@"" withMessage:@"Server Error"];
        }
        else if([operation.response statusCode]==200)
        {
            NSMutableDictionary *newDict = [NSMutableDictionary dictionaryWithDictionary:dataDict];
            [arrFollowersData[index] setValue:@1 forKey:@"is_following"];
            if ([arrFollowersData[index][@"account_type"] isEqualToString:@"private"])
            {
                [newDict setValue:[NSNumber numberWithBool:NO] forKey:@"request_status"];
                [arrFollowersData[index] setValue:[NSNumber numberWithBool:NO] forKey:@"request_status"];
            }
            else
            {
                [newDict setValue:[NSNumber numberWithBool:YES] forKey:@"request_status"];
                [arrFollowersData[index] setValue:[NSNumber numberWithBool:YES] forKey:@"request_status"];
            }
            if (dataManager.followersDataArray) {
                if ([[dataManager.followersDataArray valueForKey:@"user_id"]containsObject:newDict[@"user_id"]])
                {
                    NSInteger index = [[dataManager.followersDataArray valueForKey:@"user_id"]indexOfObject:newDict[@"user_id"]];
                    [dataManager.followersDataArray removeObjectAtIndex:index];
                }
                [dataManager.followersDataArray addObject:newDict];
            }
        }
        else
        {
            [CommonFunction alertTitle:@"" withMessage:[responseDict objectForKey:@"msg"]];
        }
        [tblVw reloadData];
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          [CommonFunction removeActivityIndicator];

                                          CommentCustomeCell *tableViewCell = (CommentCustomeCell *)[tblVw cellForRowAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0]];
                                          tableViewCell.btnFollowUnfollow.hidden = NO;
                                          tableViewCell.loader.hidden = YES;
                                          [tableViewCell.loader stopAnimating];
                                          
                                          if([operation.response statusCode]  == 400 ){
                                              [CommonFunction alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                          }
                                          else{
                                              
                                              if (wbServiceCount==1) {
                                                  wbServiceCount=0;
                                              }
                                              else
                                              {
                                                  [CommonFunction alertTitle:@"" withMessage:@"Network Error..."];
                                              }
                                          }
                                          [tblVw reloadData];
                                      }];
}


- (void)unFollowUserApiAtIndex:(NSInteger)index WithObject:(NSDictionary *)dataDict
{
    NSString *url = [NSString stringWithFormat:@"users/following/%@",dataDict[@"user_id"]];
    //http://192.168.0.22:8353/v1//users/following/{userId}
    
    NSMutableDictionary *header =[[NSMutableDictionary alloc]initWithObjectsAndKeys:@"application/json",@"Content-Type",[NSUSERDEFAULTS valueForKey:kUSERTOKEN],@"User-Token", nil];
    
    //    NSMutableDictionary *param=[[NSMutableDictionary alloc]initWithObjectsAndKeys:strUserId,@"userId", nil];
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypeDelete withHttpHeaders:header withServiceName:url withParameters:nil withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        CommentCustomeCell *tableViewCell = (CommentCustomeCell *)[tblVw cellForRowAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0]];
        
        tableViewCell.btnFollowUnfollow.hidden = NO;
        tableViewCell.loader.hidden = YES;
        [tableViewCell.loader stopAnimating];
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"responce dict %@",responseDict);
        
        [CommonFunction removeActivityIndicator];
        [CommonFunction removeActivityIndicatorWithWait];
        if(responseDict==Nil)        {
            [CommonFunction alertTitle:@"" withMessage:@"Server Error"];
        }
        else if([operation.response statusCode]==200)
        {
            NSMutableDictionary *newDict = [NSMutableDictionary dictionaryWithDictionary:dataDict];
            [arrFollowersData[index] setValue:@0 forKey:@"is_following"];
            
            [newDict setValue:[NSNumber numberWithBool:NO] forKey:@"request_status"];
            [arrFollowersData[index] setValue:[NSNumber numberWithBool:NO] forKey:@"request_status"];
            
            if (dataManager.followersDataArray) {
                if ([[dataManager.followersDataArray valueForKey:@"user_id"]containsObject:newDict[@"user_id"]])
                {
                    NSInteger index = [[dataManager.followersDataArray valueForKey:@"user_id"]indexOfObject:newDict[@"user_id"]];
                    [dataManager.followersDataArray removeObjectAtIndex:index];
                }
            }
        }
        else
        {
            [CommonFunction alertTitle:@"" withMessage:[responseDict objectForKey:@"msg"]];
        }
        [tblVw reloadData];
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          [CommonFunction removeActivityIndicator];

                                          CommentCustomeCell *tableViewCell = (CommentCustomeCell *)[tblVw cellForRowAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0]];
                                          tableViewCell.btnFollowUnfollow.hidden = NO;
                                          tableViewCell.loader.hidden = YES;
                                          [tableViewCell.loader stopAnimating];
                                          
                                          [arrFollowersData[index] setValue:@1 forKey:@"is_following"];
                                          
                                          if([operation.response statusCode]  == 400 ){
                                              [CommonFunction alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                          }
                                          else{
                                              
                                              if (wbServiceCount==1) {
                                                  wbServiceCount=0;
                                              }
                                              else
                                              {
                                                  [CommonFunction alertTitle:@"" withMessage:@"Network Error..."];
                                              }
                                          }
                                          [tblVw reloadData];
                                      }];
}



#pragma mark -
#pragma mark UIKeyBoard Activity Handling
// Call this method somewhere in your view controller setup code.
- (void)registerForKeyboardNotifications
{
    //    [[NSNotificationCenter defaultCenter] addObserver:self
    //                                             selector:@selector(keyboardWasShown:)
    //                                                 name:UIKeyboardWillShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillChangeFrame:)
                                                 name:UIKeyboardWillChangeFrameNotification object:nil];
    
}

- (void)deRegisterForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter]removeObserver:self name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}


- (void)keyboardWillChangeFrame:(NSNotification *)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    
//    CGFloat heightRemained = self.view.frame.size.height - kbSize.height;
//    
//    CGFloat heightToShift = self.shareBtnContainer.frame.origin.y - heightRemained;
    
    
    NSTimeInterval timDuration = [[info objectForKey:UIKeyboardAnimationDurationUserInfoKey]doubleValue];
    UIViewAnimationCurve curve = [[info objectForKey:UIKeyboardAnimationCurveUserInfoKey]integerValue];
    
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationCurve:curve];
    [UIView setAnimationDuration:timDuration];
    vwMessageBox.transform = CGAffineTransformMakeTranslation(0, -kbSize.height);
    [UIView commitAnimations];
}

// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    
    NSTimeInterval timDuration = [[info objectForKey:UIKeyboardAnimationDurationUserInfoKey]doubleValue];
    UIViewAnimationCurve curve = [[info objectForKey:UIKeyboardAnimationCurveUserInfoKey]integerValue];
    
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationCurve:curve];
    [UIView setAnimationDuration:timDuration];
    vwMessageBox.transform = CGAffineTransformIdentity;
    [UIView commitAnimations];
}

#pragma mark - UItextView Delegate Methods

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    [self.view layoutIfNeeded];
    
    if (textView.text.length==0) {
        vwMsgBxHeightCnstrnt.constant=initialBoxHeightConstraint;
    }
    else
    {
        CGRect rect=[textView.layoutManager usedRectForTextContainer:textView.textContainer];
        vwMsgBxHeightCnstrnt.constant=MIN(MAX(initialBoxHeightConstraint, rect.size.height+8), 100);
    }
    NSLog(@"%f",vwMsgBxHeightCnstrnt.constant);
    [UIView animateWithDuration:.25 delay:0 options:7 animations:^{
        [self.view layoutIfNeeded];
        if ([[[UIDevice currentDevice]systemVersion]integerValue] >= 9)
        {
            textView.scrollEnabled = NO;
            textView.scrollEnabled = YES;
        }
    } completion:nil];
    return YES;
}
- (BOOL)textViewShouldEndEditing:(UITextView *)textView{
    [self.view layoutIfNeeded];
    
    if (textView.text.length==0) {
        vwMsgBxHeightCnstrnt.constant=initialBoxHeightConstraint;
    }
    else
    {
        CGRect rect=[textView.layoutManager usedRectForTextContainer:textView.textContainer];
        vwMsgBxHeightCnstrnt.constant=MIN(MAX(initialBoxHeightConstraint, rect.size.height+8), 100);
    }
    NSLog(@"%f",vwMsgBxHeightCnstrnt.constant);
    [UIView animateWithDuration:.25 delay:0 options:7 animations:^{
        [self.view layoutIfNeeded];
        if ([[[UIDevice currentDevice]systemVersion]integerValue] >= 9)
        {
            textView.scrollEnabled = NO;
            textView.scrollEnabled = YES;
        }
    } completion:nil];
    return YES;
}


- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    if([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    
//    NSString *searchString;
//    if (text.length > 0) {
//        searchString = [NSString stringWithFormat:@"%@%@",textView.text, text];
//    } else {
//        if (textView.text.length>0)
//        {
//            searchString = [textView.text substringToIndex:[textView.text length] - 1];
//        }
//        else
//        {
//            searchString = @"";
//        }
//    }
   
//    [self.view layoutIfNeeded];
    
//    if (searchString.length==0) {
//        vwMsgBxHeightCnstrnt.constant=initialBoxHeightConstraint;
//    }
//    else
//    {
//        vwMsgBxHeightCnstrnt.constant=MIN(textView.contentSize.height+8, 100);
//    }
//     NSLog(@"%f",vwMsgBxHeightCnstrnt.constant);
//    [UIView animateWithDuration:.25 delay:0 options:7 animations:^{
//        [self.view layoutIfNeeded];
//        textView.scrollEnabled = NO;
//        textView.scrollEnabled = YES;
//    } completion:nil];
    return YES;
}
- (void)textViewDidChange:(UITextView *)textView{
    [self.view layoutIfNeeded];
    if (textView.text.length==0) {
        [vwMessageBox viewWithTag:201].hidden=NO;
    }
    else
    {
        [vwMessageBox viewWithTag:201].hidden=YES;
    }
    if (textView.text.length==0) {
        vwMsgBxHeightCnstrnt.constant=initialBoxHeightConstraint;
    }
    else
    {
        CGRect rect=[textView.layoutManager usedRectForTextContainer:textView.textContainer];
        vwMsgBxHeightCnstrnt.constant=MIN(MAX(initialBoxHeightConstraint, rect.size.height+8), 100);
    }
    NSLog(@"%f",vwMsgBxHeightCnstrnt.constant);
    [UIView animateWithDuration:.25 delay:0 options:7 animations:^{
        [self.view layoutIfNeeded];
        if ([[[UIDevice currentDevice]systemVersion]integerValue] >= 9)
        {
            textView.scrollEnabled = NO;
            textView.scrollEnabled = YES;
        }
    } completion:nil];
}



#pragma mark - UIScrollViewDelegate Method

-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (([tblVw contentOffset].y + tblVw.frame.size.height) >= [tblVw contentSize].height)
    {
        NSLog(@"scrolled to bottom");
        if (!isPageRefreshing) {
            
            if(totalPage <= currentPage)
            {
                return;
            }
            else
            {
                currentPage++;
            }
            isPageRefreshing = YES;
            [self getPostedComment];
        }
    }
}


@end
