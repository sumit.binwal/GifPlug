//
//  LikeVC.h
//  GiffPlugApp
//
//  Created by Sumit Sharma on 09/03/16.
//  Copyright © 2016 Kshitij Godara. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol UpdateComments;

@interface CommentVC : UIViewController
@property(nonatomic,strong)NSString *strPostId;
@property(nonatomic,strong)NSString *strUsername;
@property(nonatomic,strong)NSString *strLikeCount;
@property(nonatomic)NSInteger index;
@property(atomic,weak)id<UpdateComments>commentDelegate;
@end

@protocol UpdateComments <NSObject>

- (void)getGifLatestDetailsWithID:(NSString *)postId;

@end