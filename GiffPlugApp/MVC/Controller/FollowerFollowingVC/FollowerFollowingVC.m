//
//  FollowerFollowingVC.m
//  GiffPlugApp
//
//  Created by Santosh on 11/05/16.
//  Copyright © 2016 Kshitij Godara. All rights reserved.
//

#import "FollowerFollowingVC.h"
#import "FollowUnFollowScreen.h"
#import "FollowUnfollowCustomeCell.h"
#import "OtherUserProfileVC.h"
#import "AppDataManager.h"

@interface FollowerFollowingVC ()
{
    NSMutableArray *arrayList;
    NSInteger currentPage, totalPage;
    BOOL isPageRefreshing;
    AppDataManager *dataManager;
}
@property (weak, nonatomic) IBOutlet UITableView *tableFollowerList;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *messageLabel;

- (IBAction)onBackAction:(id)sender;
@end

@implementation FollowerFollowingVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    dataManager = [AppDataManager sharedInstance];
    
    currentPage = 1;
    totalPage = 0;
    isPageRefreshing = NO;
    
    self.messageLabel.hidden=YES;
    
    arrayList = [[NSMutableArray alloc]init];
    
//    if (self.type == 0)
//    {
//        self.titleLabel.text = @"Followers";
//        [self getFollowersListAPI];
//    }
//    else
//    {
//        self.titleLabel.text = @"Following";
//        [self getFollowingListAPI];
//    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    currentPage = 1;
    totalPage = 0;
    isPageRefreshing = NO;
    if (self.type == 0)
    {
        self.titleLabel.text = @"Followers";
        [self getFollowersListAPI];
    }
    else
    {
        self.titleLabel.text = @"Following";
        [self getFollowingListAPI];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)onBackAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)profileBtnCLicked:(UITapGestureRecognizer *)sender {
    if (arrayList.count>0) {
        
        NSString *loggedInUser = [NSUSERDEFAULTS objectForKey:kUSERID];
        if ([loggedInUser isEqualToString:arrayList[sender.view.tag][@"username"]])
        {
            
        }
        else
        {
            OtherUserProfileVC *ouVC=[[OtherUserProfileVC alloc]init];
        ouVC.hidesBottomBarWhenPushed=YES;
            ouVC.strUserName=arrayList[sender.view.tag][@"username"];
            [self.navigationController pushViewController:ouVC animated:YES];
        }
    }
}


-(void)plusMinusBtnCLicked:(UIButton *)sender
{
    FollowUnfollowCustomeCell *tableViewCell = (FollowUnfollowCustomeCell *)sender.superview.superview;
    
    tableViewCell.btnFollowUnfollow.hidden = YES;
    tableViewCell.loader.hidden = NO;
    [tableViewCell.loader startAnimating];
    
    NSMutableDictionary *dataDict=[[NSMutableDictionary alloc]initWithObjectsAndKeys:[[arrayList objectAtIndex:sender.tag]valueForKey:@"user_id"],@"user_id",[[arrayList objectAtIndex:sender.tag]valueForKey:@"account_type"],@"account_type", nil];
    
    if ([[arrayList[sender.tag][@"is_following"]stringValue] isEqualToString:@"0"]) {
        [self postFollowersListAPIAtIndex:sender.tag WithObject:dataDict];
    }
    else
    {
        [self unFollowUserApiAtIndex:sender.tag WithObject:dataDict];
    }
}


-(void)postFollowersListAPIAtIndex:(NSInteger)index WithObject:(NSDictionary *)dataDict
{
    NSString *url = [NSString stringWithFormat:@"users/following"];
    //http://192.168.0.22:8353/v1/users/following
    
    NSMutableDictionary *header =[[NSMutableDictionary alloc]initWithObjectsAndKeys:@"application/json",@"Content-Type",[NSUSERDEFAULTS valueForKey:kUSERTOKEN],@"User-Token", nil];
    
    NSMutableDictionary *param=[[NSMutableDictionary alloc]initWithObjectsAndKeys:@[dataDict],@"following", nil];
    
    __weak typeof(self) weakSelf = self;
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypePost withHttpHeaders:header withServiceName:url withParameters:param withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        FollowUnfollowCustomeCell *tableViewCell = (FollowUnfollowCustomeCell *)[self.tableFollowerList cellForRowAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0]];
        
        tableViewCell.btnFollowUnfollow.hidden = NO;
        tableViewCell.loader.hidden = YES;
        [tableViewCell.loader stopAnimating];
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"responce dict %@",responseDict);
        
        [CommonFunction removeActivityIndicator];
        if(responseDict==Nil)        {
            [CommonFunction alertTitle:@"" withMessage:@"Server Error"];
        }
        else if([operation.response statusCode]==200)
        {
            NSMutableDictionary *newDict = [NSMutableDictionary dictionaryWithDictionary:dataDict];
            [arrayList[index] setValue:@1 forKey:@"is_following"];
            if ([arrayList[index][@"account_type"] isEqualToString:@"private"])
            {
                [newDict setValue:[NSNumber numberWithBool:NO] forKey:@"request_status"];
                [arrayList[index] setValue:[NSNumber numberWithBool:NO] forKey:@"request_status"];
            }
            else
            {
                [newDict setValue:[NSNumber numberWithBool:YES] forKey:@"request_status"];
                [arrayList[index] setValue:[NSNumber numberWithBool:YES] forKey:@"request_status"];
            }
            if (dataManager.followersDataArray) {
                if ([[dataManager.followersDataArray valueForKey:@"user_id"]containsObject:newDict[@"user_id"]])
                {
                    NSInteger index = [[dataManager.followersDataArray valueForKey:@"user_id"]indexOfObject:newDict[@"user_id"]];
                    [dataManager.followersDataArray removeObjectAtIndex:index];
                }
                [dataManager.followersDataArray addObject:newDict];
            }
        }
        else
        {
            [CommonFunction alertTitle:@"" withMessage:[responseDict objectForKey:@"msg"]];
        }
        [weakSelf.tableFollowerList reloadData];
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          
                                          [CommonFunction removeActivityIndicator];

                                          FollowUnfollowCustomeCell *tableViewCell = (FollowUnfollowCustomeCell *)[self.tableFollowerList cellForRowAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0]];
                                          tableViewCell.btnFollowUnfollow.hidden = NO;
                                          tableViewCell.loader.hidden = YES;
                                          [tableViewCell.loader stopAnimating];
                                          
                                          if([operation.response statusCode]  == 400 ){
                                              NSLog(@"impo response %@",operation.response);
                                              [CommonFunction alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                          }
                                          else{
                                              
                                              if (wbServiceCount==1) {
                                                  wbServiceCount=0;
                                              }
                                              else
                                              {
                                                  [CommonFunction alertTitle:@"" withMessage:@"Network Error..."];
                                              }
                                          }
                                          [weakSelf.tableFollowerList reloadData];
                                      }];
}


- (void)unFollowUserApiAtIndex:(NSInteger)index WithObject:(NSDictionary *)dataDict
{
    NSString *url = [NSString stringWithFormat:@"users/following/%@",dataDict[@"user_id"]];
    //http://192.168.0.22:8353/v1//users/following/{userId}
    
    NSMutableDictionary *header =[[NSMutableDictionary alloc]initWithObjectsAndKeys:@"application/json",@"Content-Type",[NSUSERDEFAULTS valueForKey:kUSERTOKEN],@"User-Token", nil];
    
    //    NSMutableDictionary *param=[[NSMutableDictionary alloc]initWithObjectsAndKeys:strUserId,@"userId", nil];
    
    __weak typeof(self) weakSelf = self;
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypeDelete withHttpHeaders:header withServiceName:url withParameters:nil withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        FollowUnfollowCustomeCell *tableViewCell = (FollowUnfollowCustomeCell *)[weakSelf.tableFollowerList cellForRowAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0]];
        
        tableViewCell.btnFollowUnfollow.hidden = NO;
        tableViewCell.loader.hidden = YES;
        [tableViewCell.loader stopAnimating];
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"responce dict %@",responseDict);
        
        [CommonFunction removeActivityIndicator];
        [CommonFunction removeActivityIndicatorWithWait];
        if(responseDict==Nil)        {
            [CommonFunction alertTitle:@"" withMessage:@"Server Error"];
        }
        else if([operation.response statusCode]==200)
        {
            NSMutableDictionary *newDict = [NSMutableDictionary dictionaryWithDictionary:dataDict];
            [arrayList[index] setValue:@0 forKey:@"is_following"];
            
            [newDict setValue:[NSNumber numberWithBool:NO] forKey:@"request_status"];
            [arrayList[index] setValue:[NSNumber numberWithBool:NO] forKey:@"request_status"];
            
            if (dataManager.followersDataArray) {
                if ([[dataManager.followersDataArray valueForKey:@"user_id"]containsObject:newDict[@"user_id"]])
                {
                    NSInteger index = [[dataManager.followersDataArray valueForKey:@"user_id"]indexOfObject:newDict[@"user_id"]];
                    [dataManager.followersDataArray removeObjectAtIndex:index];
                }
            }
        }
        else
        {
            [CommonFunction alertTitle:@"" withMessage:[responseDict objectForKey:@"msg"]];
        }
        [weakSelf.tableFollowerList reloadData];
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          
                                          [CommonFunction removeActivityIndicator];
                                          
                                          FollowUnfollowCustomeCell *tableViewCell = (FollowUnfollowCustomeCell *)[weakSelf.tableFollowerList cellForRowAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0]];
                                          tableViewCell.btnFollowUnfollow.hidden = NO;
                                          tableViewCell.loader.hidden = YES;
                                          [tableViewCell.loader stopAnimating];
                                          
                                          if([operation.response statusCode]  == 400 ){
                                              NSLog(@"impo response %@",operation.response);
                                              [CommonFunction alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                          }
                                          else{
                                              if (wbServiceCount==1) {
                                                  wbServiceCount=0;
                                              }
                                              else
                                              {
                                                  [CommonFunction alertTitle:@"" withMessage:@"Network Error..."];
                                              }
                                          }
                                          [weakSelf.tableFollowerList reloadData];
                                      }];
}




#pragma mark - GetFollowersList API
-(void)getFollowersListAPI
{
    NSString *url = [NSString stringWithFormat:@"users/%@/followers/%ld",self.userID,(long)currentPage];
    
    NSMutableDictionary *header =[[NSMutableDictionary alloc]initWithObjectsAndKeys:@"application/json",@"Content-Type",[NSUSERDEFAULTS valueForKey:kUSERTOKEN],@"User-Token", nil];
    
    [CommonFunction showActivityIndicatorWithText:@""];
    
    __weak typeof(self) weakSelf = self;
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypeGet withHttpHeaders:header withServiceName:url withParameters:nil withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"responce dict %@",responseDict);
        
        [CommonFunction removeActivityIndicator];
        if(responseDict==Nil)        {
            [CommonFunction alertTitle:@"" withMessage:@"Server Error"];
            if (currentPage>1)
            {
                currentPage--;
            }
            isPageRefreshing = NO;
        }
        else if([operation.response statusCode]==200)
        {
            if (responseDict[@"followers"] && [responseDict[@"followers"] isKindOfClass:[NSArray class]])
            {
                NSArray *array = responseDict[@"followers"];
                if (array.count>0)
                {
                    if (currentPage == 1)
                    {
                        [arrayList removeAllObjects];
                    }
                    [arrayList addObjectsFromArray:[array mutableCopy]];
                    totalPage = [responseDict[@"total_pages"]integerValue];
                    
                    for (NSInteger i=0; i<arrayList.count; i++)
                    {
                        if ([[dataManager.followersDataArray valueForKey:@"user_id"]containsObject:arrayList[i][@"user_id"]])
                        {
                            NSInteger index = [[dataManager.followersDataArray valueForKey:@"user_id"]indexOfObject:arrayList[i][@"user_id"]];
                            BOOL requestAcess = [arrayList[i][@"request_status"] boolValue];
                            [dataManager.followersDataArray[index] setValue:[NSNumber numberWithBool:requestAcess] forKey:@"request_status"];
                            
                            BOOL isFolowing = [arrayList[i][@"is_following"] boolValue];
                            if (!isFolowing)
                            {
                                [dataManager.followersDataArray removeObjectAtIndex:index];
                            }
                        }
                    }
                    
                    [weakSelf.tableFollowerList reloadData];
                }
                if (arrayList.count==0)
                {
                    weakSelf.messageLabel.hidden = NO;
                }
                else
                {
                    weakSelf.messageLabel.hidden = YES;
                }
            }
            isPageRefreshing = NO;
        }
        else
        {
            if (currentPage>1)
            {
                currentPage--;
            }
            isPageRefreshing = NO;
            [CommonFunction alertTitle:@"" withMessage:[responseDict objectForKey:@"msg"]];
        }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          
                                          if (currentPage>1)
                                          {
                                              currentPage--;
                                          }
                                          isPageRefreshing = NO;
                                          
                                          if([operation.response statusCode]  == 400 ){
                                              NSLog(@"impo response %@",operation.response);
                                              
                                              [CommonFunction removeActivityIndicator];
                                              [CommonFunction alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                        }
                                          else{
                                              
                                              if (wbServiceCount==1) {
                                                  wbServiceCount=0;
                                              }
                                              else
                                              {
                                                  [CommonFunction removeActivityIndicator];
                                                  [CommonFunction alertTitle:@"" withMessage:@"Network Error..."];
                                              }
                                              
                                          }
                                          
                                      }];
}


#pragma mark - GetFollowingList API
-(void)getFollowingListAPI
{
    NSString *url = [NSString stringWithFormat:@"users/%@/following/%ld",self.userID,(long)currentPage];
    
    NSMutableDictionary *header =[[NSMutableDictionary alloc]initWithObjectsAndKeys:@"application/json",@"Content-Type",[NSUSERDEFAULTS valueForKey:kUSERTOKEN],@"User-Token", nil];
    
    [CommonFunction showActivityIndicatorWithText:@""];
    
    __weak typeof(self) weakSelf = self;
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypeGet withHttpHeaders:header withServiceName:url withParameters:nil withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"responce dict %@",responseDict);
        
        [CommonFunction removeActivityIndicator];
        if(responseDict==Nil)        {
            if (currentPage>1)
            {
                currentPage--;
            }
            isPageRefreshing = NO;
            [CommonFunction alertTitle:@"" withMessage:@"Server Error"];
        }
        else if([operation.response statusCode]==200)
        {
            if (responseDict[@"following"] && [responseDict[@"following"] isKindOfClass:[NSArray class]])
            {
                NSArray *array = responseDict[@"following"];
                if (array.count>0)
                {
                    if (currentPage == 1)
                    {
                        [arrayList removeAllObjects];
                    }
                    [arrayList addObjectsFromArray:[array mutableCopy]];
                    totalPage = [responseDict[@"total_pages"]integerValue];
                    
                    
                    for (NSInteger i=0; i<arrayList.count; i++)
                    {
                        if ([[dataManager.followersDataArray valueForKey:@"user_id"]containsObject:arrayList[i][@"user_id"]])
                        {
                            NSInteger index = [[dataManager.followersDataArray valueForKey:@"user_id"]indexOfObject:arrayList[i][@"user_id"]];
                            BOOL requestAcess = [arrayList[i][@"request_status"] boolValue];
                            [dataManager.followersDataArray[index] setValue:[NSNumber numberWithBool:requestAcess] forKey:@"request_status"];
                            
                            BOOL isFolowing = [arrayList[i][@"is_following"] boolValue];
                            if (!isFolowing)
                            {
                                [dataManager.followersDataArray removeObjectAtIndex:index];
                            }
                        }
                    }
                    
                    [weakSelf.tableFollowerList reloadData];
                }
                if (arrayList.count==0)
                {
                    weakSelf.messageLabel.hidden = NO;
                }
                else
                {
                    weakSelf.messageLabel.hidden = YES;
                }
            }
            isPageRefreshing = NO;
        }
        else
        {
            if (currentPage>1)
            {
                currentPage--;
            }
            isPageRefreshing = NO;
            [CommonFunction alertTitle:@"" withMessage:[responseDict objectForKey:@"msg"]];
        }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          
                                          if (currentPage>1)
                                          {
                                              currentPage--;
                                          }
                                          isPageRefreshing = NO;
                                          
                                          if([operation.response statusCode]  == 400 ){
                                              NSLog(@"impo response %@",operation.response);
                                              
                                              [CommonFunction removeActivityIndicator];
                                              [CommonFunction alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                          }
                                          else{
                                              
                                              if (wbServiceCount==1) {
                                                  wbServiceCount=0;
                                              }
                                              else
                                              {
                                                  [CommonFunction removeActivityIndicator];
                                                  [CommonFunction alertTitle:@"" withMessage:@"Network Error..."];
                                              }
                                              
                                          }
                                          
                                      }];
}


#pragma mark - UITableView Delegate Method
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrayList.count;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 70.0f;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier=@"FollowerFollowingCustomeCell";
    
    FollowUnfollowCustomeCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (!cell)
    {
        cell = [[NSBundle mainBundle]loadNibNamed:NSStringFromClass([FollowUnfollowCustomeCell class]) owner:self options:nil][1];
    }
    
    return cell;
    
}



- (void)tableView:(UITableView *)tableView willDisplayCell:(FollowUnfollowCustomeCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    for (UIGestureRecognizer *gesture in cell.imageViewProfile.gestureRecognizers)
    {
        [cell.imageViewProfile removeGestureRecognizer:gesture];
    }
    
    UITapGestureRecognizer *tappGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(profileBtnCLicked:)];
    tappGesture.numberOfTapsRequired=1;
    [cell.imageViewProfile addGestureRecognizer:tappGesture];
    cell.imageViewProfile.tag = indexPath.row;
    
    
    cell.btnFollowUnfollow.tag=indexPath.row;
    [cell.btnFollowUnfollow addTarget:self action:@selector(plusMinusBtnCLicked:) forControlEvents:UIControlEventTouchUpInside];
    
//    cell.imgProfileImg.tag=indexPath.row;
//    [cell.imgProfileImg addTarget:self action:@selector(profileBtnCLicked:) forControlEvents:UIControlEventTouchUpInside];
    
    
    if (arrayList.count>0) {
        NSString *strAvtarimg=[NSString stringWithFormat:@"%@",[[arrayList objectAtIndex:indexPath.row]valueForKey:@"avatar_image"]];
        
        if (strAvtarimg.length>1) {
            
            [cell.imageViewProfile sd_setImageWithURL:[NSURL URLWithString:strAvtarimg] placeholderImage:[UIImage imageNamed:@"default.jpg"] options:0 completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                if (error)
                {
                    cell.imageViewProfile.contentMode=UIViewContentModeScaleToFill;
                }
                else
                {
                    if ([[strAvtarimg pathExtension]isEqualToString:@"gif"])
                    {
                        cell.imageViewProfile.contentMode=UIViewContentModeScaleAspectFill;
                    }
                    else
                    {
                        cell.imageViewProfile.contentMode=UIViewContentModeScaleToFill;
                    }
                }
            }];
//            [cell.imgProfileImg sd_setImageWithURL:[NSURL URLWithString:strAvtarimg] forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"default.jpg"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
//                
//            }];
        }
        else
        {
            [cell.imageViewProfile setImage:[UIImage imageNamed:@"default.jpg"]];
//            [cell.imgProfileImg setImage:[UIImage imageNamed:@"default.jpg"] forState:UIControlStateNormal];
        }
        
        if ([[dataManager.followersDataArray valueForKey:@"user_id"]containsObject:arrayList[indexPath.row][@"user_id"]])
        {
            NSInteger index = [[dataManager.followersDataArray valueForKey:@"user_id"]indexOfObject:arrayList[indexPath.row][@"user_id"] ];
            BOOL requestAcess = [dataManager.followersDataArray[index][@"request_status"]boolValue];
            if (requestAcess) {
                [cell.btnFollowUnfollow setImage:[UIImage imageNamed:@"FUFS_MinusIcon"] forState:UIControlStateNormal];
                cell.btnFollowUnfollow.hidden = NO;
                cell.labelRequested.hidden = YES;
            }
            else
            {
                cell.btnFollowUnfollow.hidden = YES;
                cell.labelRequested.hidden = NO;
            }
        }
        else
        {
            BOOL isFollowing = [arrayList [indexPath.row][@"is_following"]boolValue];
            
            if (isFollowing)
            {
                BOOL isRequestAccepted = [arrayList[indexPath.row][@"request_status"] boolValue];
                if (isRequestAccepted) {
                    [cell.btnFollowUnfollow setImage:[UIImage imageNamed:@"FUFS_MinusIcon"] forState:UIControlStateNormal];
                    cell.btnFollowUnfollow.hidden = NO;
                    cell.labelRequested.hidden = YES;
                }
                else
                {
                    cell.btnFollowUnfollow.hidden = YES;
                    cell.labelRequested.hidden = NO;
                }
            }
            else
            {
                cell.btnFollowUnfollow.hidden = NO;
                cell.labelRequested.hidden = YES;
                [cell.btnFollowUnfollow setImage:[UIImage imageNamed:@"FUFS_PlusIcon"] forState:UIControlStateNormal];
            }
        }
        
        cell.lblUsername.text=[[arrayList objectAtIndex:indexPath.row]valueForKey:@"username"];
        
        NSString *loggedInUser = [NSUSERDEFAULTS objectForKey:kUSERID];
        if ([cell.lblUsername.text isEqualToString:loggedInUser])
        {
            cell.imgProfileImg.userInteractionEnabled = NO;
            cell.btnFollowUnfollow.hidden = YES;
        }
    }
}


#pragma mark - UIScrollViewDelegate Method

-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (([self.tableFollowerList contentOffset].y + self.tableFollowerList.frame.size.height) >= [self.tableFollowerList contentSize].height)
    {
        NSLog(@"scrolled to bottom");
        if (!isPageRefreshing)
        {
            
            if(totalPage <= currentPage)
            {
                return;
            }
            else
            {
                currentPage++;
            }
            
            isPageRefreshing = YES;
            if (self.type == 0)
            {
                [self getFollowersListAPI];
            }
            else
            {
                [self getFollowingListAPI];
            }
        }
    }
}

@end
