//
//  OtherUserProfileVC.m
//  GiffPlugApp
//
//  Created by Sumit Sharma on 22/02/16.
//  Copyright © 2016 Kshitij Godara. All rights reserved.
//

#import "OtherUserProfileVC.h"
#import "PostedGifsCustomeCell.h"
#import "CommentVC.h"
#import "HomeFeedVC.h"
#import "LikeVC.h"
#import "ReplugVC.h"
#import "SearchVC.h"
#import "AppDataManager.h"
#import "FollowerFollowingVC.h"
#import "SliderMenuVC.h"
#import "KIHTTPRequestOperationManager.h"
#import "CategoryDetailVC.h"
#import "PhotoPickerController.h"
#import <AssetsLibrary/AssetsLibrary.h>

#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)

//#define SCREEN_WIDTH ([[UIScreen mainScreen] bounds].size.width)
//#define SCREEN_HEIGHT ([[UIScreen mainScreen] bounds].size.height)
#define SCREEN_MAX_LENGTH (MAX(SCREEN_WIDTH, SCREEN_HEIGHT))
#define SCREEN_MIN_LENGTH (MIN(SCREEN_WIDTH, SCREEN_HEIGHT))
#define IS_IPHONE_6 (IS_IPHONE && SCREEN_MAX_LENGTH == 667.0)
#define IS_IPHONE_6P (IS_IPHONE && SCREEN_MAX_LENGTH == 736.0)

@interface OtherUserProfileVC ()<UITableViewDataSource,UITableViewDelegate,UIGestureRecognizerDelegate,UpdateComments,UserHandleDelegate>
{
    IBOutlet UIView *vwGifPosted;
    IBOutlet UILabel *lblFollowersCount;
    IBOutlet UILabel *lblFollowingCount;
    IBOutlet UIImageView *imgProfileImg;
    IBOutlet UIImageView *imgProfileBgImg;
    IBOutlet UILabel *lblUsername;
    IBOutlet UILabel *lblUserBio;
    IBOutlet UILabel *lblGifPostedCOunt;
    IBOutlet UILabel *lblReplugCount;
    IBOutlet UIButton *btnSetting;
    IBOutlet UITableView *tblVwData;
    CGFloat scaleX, scaleY, scaleSixPlusX, scaleSixPlusY;
    IBOutlet UILabel *lblMsgError;
    NSInteger currentPage, totalPage,totalPost;
    NSString *strMainImgURl;
    NSMutableDictionary *dictData;
    IBOutlet UIView *vwReplug;

    NSMutableArray * arrUserPost;
    NSMutableArray *arrUserPostReplug;
    
    UIRefreshControl *refreshControl;
    IBOutlet UIButton *followBtn;
    __weak IBOutlet UILabel *labelRequested;
    
    IBOutlet UIView *vwBg;

    IBOutlet UIActivityIndicatorView *actiVtyIndictor;
    IBOutlet UIView *vwFoterVw;
    
    
    __weak IBOutlet UIButton *btnPostNotifications;
    __weak IBOutlet UIButton *btnReportUser;
    __weak IBOutlet UIButton *btnBlockUser;
    IBOutlet UIButton *btnCancel;
    IBOutlet UIView *vwCstmeActionSheet;
    IBOutlet UIView *vwReportSheet;
    
    
    __weak IBOutlet UIButton *btnInfoView;
    
    IBOutlet UIImageView *replugCircle;
    NSString *strUserId;
    NSMutableArray *arrVwControllers;
    
    AppDataManager *dataManager;
    
    BOOL isReplugSelected;
    NSInteger currentPageReplugd, totalPageReplugd,totalPostReplugd;
    BOOL isPageRefreshingGif, isPageRefreshingReplug;
    
    NSIndexPath *visibleIndexPath, *prevIndexPath;
    
    BOOL isBlocked, canViewProfile, isPostNotificationEnabled;
    BOOL viewDidLoaded;
    __weak IBOutlet UIImageView *popUpImageView;
    

    NSTimer *timerGifCount;

    BOOL isFollowing, isRequestAccepted, isPrivate;

}
- (IBAction)onInfoSliderView:(UIButton *)sender;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *lblErrorTopCnstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *settingBtnWidthCnstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *settingBtnTrailingCnstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *settingBtnTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *gifRepluggedCountTopConstrant;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *gifpostedCountTopConstrant;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *headerTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *headerHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *nameTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imgProfileVwCenterContraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imgProfileVwWidthContraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imgProfileVwTopConstraint;
@property (weak, nonatomic) IBOutlet UIView *tableHeaderView;

@property (weak, nonatomic) IBOutlet UIImageView *gifImageIcon;
@property (weak, nonatomic) IBOutlet UIImageView *replugImageIcon;

- (IBAction)reportActionSheetClickedWithButton:(id)sender;
@end

@implementation OtherUserProfileVC


-(void)dealloc
{
    [[NSNotificationCenter defaultCenter]removeObserver:self];
    [[AppWebHandler sharedInstance]removeDelegate:self];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    viewDidLoaded = YES;
    
    //------------------------------------------------------
    
    [[AppWebHandler sharedInstance]addDelegate:self];
    dataManager = [AppDataManager sharedInstance];
    
    isReplugSelected = NO;
    isPageRefreshingGif = NO;
    isPageRefreshingReplug = NO;
    
    [tblVwData registerNib:[UINib nibWithNibName:NSStringFromClass([PostedGifsCustomeCell class]) bundle:nil] forCellReuseIdentifier:@"PostedGifsCustomeCell"];
    
    currentPage = 1;
    currentPageReplugd = 1;
    totalPage=0;
    totalPageReplugd =0;
    
    arrUserPost = [[NSMutableArray alloc]init];
    arrUserPostReplug = [[NSMutableArray alloc]init];
    
    [self setUpView];
    self.headerHeightConstraint.constant=23.0f*SCREEN_XScale;

    [tblVwData setTableHeaderView:_tableHeaderView];
    self.edgesForExtendedLayout = UIRectEdgeNone;
    
    [lblGifPostedCOunt setTextColor:[UIColor colorWithRed:228/255.0f green:15/255.0f blue:73/255.0f alpha:1.0f]];
    [_gifImageIcon setImage:[UIImage imageNamed:@"UP_roundIcon_Selected"]];
    
    // Do any additional setup after loading the view from its nib.
}

-(void)setUpView
{
    imgProfileImg.clipsToBounds=YES;
    imgProfileImg.layer.borderColor=[UIColor colorWithRed:106.0f/255.0f green:106.0f/255.0f blue:106.0f/255.0f alpha:1.0f].CGColor;
    imgProfileImg.layer.borderWidth=1.0f;
    
    scaleX = [[UIScreen mainScreen]bounds].size.width/320;
    
    scaleY = [[UIScreen mainScreen]bounds].size.height/568;
    
    scaleSixPlusX = [[UIScreen mainScreen]bounds].size.width/414;
    
    scaleSixPlusY = [[UIScreen mainScreen]bounds].size.height/736;
    
    self.settingBtnTopConstraint.constant=32*scaleSixPlusY;
    
    imgProfileImg.layer.cornerRadius=imgProfileImg.frame.size.width/2 *scaleX;
    
    
    self.headerHeightConstraint.constant = self.headerHeightConstraint.constant * scaleX;
    lblUsername.font = [UIFont fontWithName:lblUsername.font.fontName size:26*scaleX];
//    tblVwData.scrollEnabled=NO;
    
    
    
    UITapGestureRecognizer *tapGesture=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(clickOnTapGesture)];
    tapGesture.numberOfTapsRequired=1;
    tapGesture.delegate=self;
    [vwBg addGestureRecognizer:tapGesture];

    
    //Add Refresh Contrl on TableView
    refreshControl = [[UIRefreshControl alloc] initWithFrame:CGRectMake(0, 150, 320.0f, 30.0f)];
    refreshControl.backgroundColor=[UIColor colorWithRed:33.0f/255.0f green:31.0f/255.0f blue:31.0f/255.0f alpha:1];
    refreshControl.tintColor=[UIColor whiteColor];
    
//    NSAttributedString *title = [[NSAttributedString alloc] initWithString:@"Please Wait"
//                                                                attributes: @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
//    refreshControl.attributedTitle = [[NSAttributedString alloc]initWithAttributedString:title];
    
    
    [refreshControl addTarget:self action:@selector(refresh:) forControlEvents:UIControlEventValueChanged];
    
    [tblVwData addSubview:refreshControl];

}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)clickOnTapGesture
{
    if ([APPDELEGATE.window.rootViewController.view.subviews containsObject:vwCstmeActionSheet])
    {
        [UIView animateWithDuration:0.3
                              delay:0.0
                            options: UIViewAnimationOptionTransitionFlipFromBottom
                         animations:^{
                             [vwCstmeActionSheet setFrame:CGRectMake(0.0f, [UIScreen mainScreen].bounds.size.height, [UIScreen mainScreen].bounds.size.width,280*SCREEN_XScale)];
                             
                         }
                         completion:^(BOOL finished){
                             
                             for (UIButton *button in vwCstmeActionSheet.subviews)
                             {
                                 if (button.tag == 0)
                                 {
                                     
                                }
                                 else
                                 {
                                     [button removeObserver:self forKeyPath:@"highlighted"];
                                 }
                             }
                             
                             [vwCstmeActionSheet removeFromSuperview];
                             [vwBg removeFromSuperview];
                         }];
    }
    else
    {
        [UIView animateWithDuration:0.3
                              delay:0.0
                            options: UIViewAnimationOptionTransitionFlipFromBottom
                         animations:^{
                             [vwReportSheet setFrame:CGRectMake(0.0f, [UIScreen mainScreen].bounds.size.height, [UIScreen mainScreen].bounds.size.width,342*SCREEN_XScale)];
                             
                         }
                         completion:^(BOOL finished){
                             
                             for (UIButton *button in vwReportSheet.subviews)
                             {
                                 if (button.tag == 0)
                                 {
                                     
                                 }
                                 else
                                 {
                                     [button removeObserver:self forKeyPath:@"highlighted"];
                                 }
                             }

                             
                             [vwReportSheet removeFromSuperview];
                             [vwBg removeFromSuperview];
                         }];
    }
}
- (void)refresh:(UIRefreshControl *)refreshControls
{
    if (isReplugSelected)
    {
        currentPageReplugd=1;
        totalPageReplugd=0;
    }
    else
    {
        currentPage=1;
        totalPage=0;
    }
//    currentPage=1;
    if ([CommonFunction reachabiltyCheck]) {
        [self getUserProfileAndShouldGetPost:YES];
    }
    else
    {
        [CommonFunction alertTitle:@"" withMessage:@"Please check your network connection."];
        [refreshControl endRefreshing];
    }
    
}


-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    
    
     arrVwControllers=[[self.navigationController viewControllers]mutableCopy];
    [arrVwControllers removeLastObject];;
    [[UIApplication sharedApplication]setStatusBarHidden:NO withAnimation:UIStatusBarAnimationFade];
    [[UIApplication sharedApplication]setStatusBarStyle:UIStatusBarStyleLightContent];
    [self.navigationController.navigationBar setHidden:YES];
    
    if ([CommonFunction reachabiltyCheck]) {
        [CommonFunction showActivityIndicatorWithText:@""];
        if (viewDidLoaded)
        {
            viewDidLoaded = NO;
            [self getUserProfileAndShouldGetPost:YES];
        }
        else
        {
            [self getUserProfileAndShouldGetPost:NO];
        }

    }
    else
    {
        [CommonFunction alertTitle:@"" withMessage:@"Please check your network connection."];
    }
}

-(void)viewWillDisappear:(BOOL)animated
{
    if (self.isMovingFromParentViewController)
    {
        NSString *url = [NSString stringWithFormat:@"users/%@",_strUserName];
        [[KIHTTPRequestOperationManager manager]cancelAllHTTPOperationsWithPath:url];
        NSString *url1 = [NSString stringWithFormat:@"posts/userPosts"];
        [[KIHTTPRequestOperationManager manager]cancelAllHTTPOperationsWithPath:url1];
    }
    [self.navigationController.navigationBar setHidden:NO];
    [timerGifCount invalidate],timerGifCount=nil;
    prevIndexPath = visibleIndexPath = nil;
    [super viewWillDisappear:animated];
}


- (void)onDoubleTapUserLikeAction:(UITapGestureRecognizer *)likeTap
{
    NSInteger index = likeTap.view.tag;
    
    PostedGifsCustomeCell *cellView = (PostedGifsCustomeCell*) [tblVwData cellForRowAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0]];
    
    if (cellView.isHeartAnimationActive)
    {
        return;
    }
    
    if (isReplugSelected)
    {
        NSNumber *isLike=arrUserPostReplug[index][@"is_like"];
        NSLog(@"%@",isLike);
        [cellView performHeartAnimation];
        
        if (![isLike boolValue])
        {
            NSInteger likeCount = [arrUserPostReplug[index][@"likes_count"] integerValue];
            
            if (cellView.imgLikeFillUnfill.tag==0) {
                
                [cellView.imgLikeFillUnfill setImage:[UIImage imageNamed:@"PG_LikeBtn"]];
                cellView.imgLikeFillUnfill.tag=1;
                
                [CommonFunction showActivityIndicatorWithText:nil];
                likeCount += 1;
                
                cellView.lblLikeCount.text=[NSString stringWithFormat:@"%ld",(long)likeCount];
                
                [arrUserPostReplug[index] setValue:[NSNumber numberWithInteger:likeCount] forKey:@"likes_count"];
                [arrUserPostReplug[index] setValue:@1 forKey:@"is_like"];
                
                NSString *postID = arrUserPostReplug[index][@"id"];
                
                for (NSInteger i=0; i<dataManager.globalHomeFeedArray.count; i++)
                {
                    NSString *userPostID = dataManager.globalHomeFeedArray[i][@"post_id"];
                    if ([userPostID isEqualToString:postID])
                    {
                        [dataManager.globalHomeFeedArray[i] setValue:[NSNumber numberWithInteger:likeCount] forKey:@"likes_count"];
                        [dataManager.globalHomeFeedArray[i] setValue:@1 forKey:@"is_like"];
                    }
                }
                for (NSInteger i=0; i<dataManager.globalUserFeedArray.count; i++)
                {
                    NSString *userPostID = dataManager.globalUserFeedArray[i][@"post_id"];
                    if ([userPostID isEqualToString:postID])
                    {
                        [dataManager.globalUserFeedArray[i] setValue:[NSNumber numberWithInteger:likeCount] forKey:@"likes_count"];
                        [dataManager.globalUserFeedArray[i] setValue:@1 forKey:@"is_like"];
                    }
                }
                for (NSInteger i=0; i<dataManager.globalUserReplugArray.count; i++)
                {
                    NSString *userPostID = dataManager.globalUserReplugArray[i][@"post_id"];
                    if ([userPostID isEqualToString:postID])
                    {
                        [dataManager.globalUserReplugArray[i] setValue:[NSNumber numberWithInteger:likeCount] forKey:@"likes_count"];
                        [dataManager.globalUserReplugArray[i] setValue:@1 forKey:@"is_like"];
                    }
                }
                [self likeAPost:postID cell:cellView indexpath:index replug:isReplugSelected];
            }
        }
    }
    else
    {
        NSNumber *isLike=arrUserPost[index][@"is_like"];
        NSLog(@"%@",isLike);
        [cellView performHeartAnimation];
        
        if (![isLike boolValue])
        {
            NSInteger likeCount = [arrUserPost[index][@"likes_count"] integerValue];
            
            if (cellView.imgLikeFillUnfill.tag==0) {
                
                [cellView.imgLikeFillUnfill setImage:[UIImage imageNamed:@"PG_LikeBtn"]];
                cellView.imgLikeFillUnfill.tag=1;
                
                [CommonFunction showActivityIndicatorWithText:nil];
                likeCount += 1;
                
                cellView.lblLikeCount.text=[NSString stringWithFormat:@"%ld",(long)likeCount];
                
                [arrUserPost[index] setValue:[NSNumber numberWithInteger:likeCount] forKey:@"likes_count"];
                [arrUserPost[index] setValue:@1 forKey:@"is_like"];
                
                NSString *postID = arrUserPost[index][@"id"];
                
                for (NSInteger i=0; i<dataManager.globalHomeFeedArray.count; i++)
                {
                    NSString *userPostID = dataManager.globalHomeFeedArray[i][@"post_id"];
                    if ([userPostID isEqualToString:postID])
                    {
                        [dataManager.globalHomeFeedArray[i] setValue:[NSNumber numberWithInteger:likeCount] forKey:@"likes_count"];
                        [dataManager.globalHomeFeedArray[i] setValue:@1 forKey:@"is_like"];
                    }
                }
                for (NSInteger i=0; i<dataManager.globalUserFeedArray.count; i++)
                {
                    NSString *userPostID = dataManager.globalUserFeedArray[i][@"post_id"];
                    if ([userPostID isEqualToString:postID])
                    {
                        [dataManager.globalUserFeedArray[i] setValue:[NSNumber numberWithInteger:likeCount] forKey:@"likes_count"];
                        [dataManager.globalUserFeedArray[i] setValue:@1 forKey:@"is_like"];
                    }
                }
                for (NSInteger i=0; i<dataManager.globalUserReplugArray.count; i++)
                {
                    NSString *userPostID = dataManager.globalUserReplugArray[i][@"post_id"];
                    if ([userPostID isEqualToString:postID])
                    {
                        [dataManager.globalUserReplugArray[i] setValue:[NSNumber numberWithInteger:likeCount] forKey:@"likes_count"];
                        [dataManager.globalUserReplugArray[i] setValue:@1 forKey:@"is_like"];
                    }
                }
                [self likeAPost:postID cell:cellView indexpath:index replug:isReplugSelected];
            }
        }
    }
}


-(void)likeAPost:(NSString *)postID cell:(PostedGifsCustomeCell *)cell indexpath:(NSInteger)arrindex replug:(BOOL)isReplugPost
{
    NSString *url = [NSString stringWithFormat:@"posts/like/%@",postID];
    //http://192.168.0.22:8353/v1/posts/like/{postId}
    
    NSMutableDictionary *header =[[NSMutableDictionary alloc]initWithObjectsAndKeys:@"application/json",@"Content-Type",[NSUSERDEFAULTS valueForKey:kUSERTOKEN],@"User-Token", nil];
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    
    __weak typeof(cell) weakCell = cell;
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypePost withHttpHeaders:header withServiceName:url withParameters:nil withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"--responce dict %@",responseDict);
        
        [CommonFunction removeActivityIndicator];
        [refreshControl endRefreshing];
        if(responseDict==Nil)        {
            [CommonFunction alertTitle:@"" withMessage:@"Server Error"];
            
            NSString *postidstr;
            NSInteger likeCount;
            if (isReplugPost)
            {
                likeCount = [arrUserPostReplug[arrindex][@"likes_count"] integerValue];
                
                [weakCell.imgLikeFillUnfill setImage:[UIImage imageNamed:@"HF_likeUnfillIcon"]];
                weakCell.imgLikeFillUnfill.tag=0;
                
                likeCount -= 1;
                weakCell.lblLikeCount.text=[NSString stringWithFormat:@"%ld",(long)likeCount];
                [arrUserPostReplug[arrindex] setValue:[NSNumber numberWithInteger:likeCount] forKey:@"likes_count"];
                [arrUserPostReplug[arrindex] setValue:@0 forKey:@"is_like"];
                
                postidstr = arrUserPostReplug[arrindex][@"id"];
            }
            else
            {
                likeCount = [arrUserPost[arrindex][@"likes_count"] integerValue];
                
                [weakCell.imgLikeFillUnfill setImage:[UIImage imageNamed:@"HF_likeUnfillIcon"]];
                weakCell.imgLikeFillUnfill.tag=0;
                
                likeCount -= 1;
                weakCell.lblLikeCount.text=[NSString stringWithFormat:@"%ld",(long)likeCount];
                [arrUserPost[arrindex] setValue:[NSNumber numberWithInteger:likeCount] forKey:@"likes_count"];
                [arrUserPost[arrindex] setValue:@0 forKey:@"is_like"];
                
                postidstr = arrUserPost[arrindex][@"id"];
            }
            
            for (NSInteger i=0; i<dataManager.globalHomeFeedArray.count; i++)
            {
                NSString *userPostID = dataManager.globalHomeFeedArray[i][@"post_id"];
                if ([userPostID isEqualToString:postidstr])
                {
                    [dataManager.globalHomeFeedArray[i] setValue:[NSNumber numberWithInteger:likeCount] forKey:@"likes_count"];
                    [dataManager.globalHomeFeedArray[i] setValue:@0 forKey:@"is_like"];
                }
            }
            
            for (NSInteger i=0; i<dataManager.globalUserReplugArray.count; i++)
            {
                NSString *userPostID = dataManager.globalUserReplugArray[i][@"post_id"];
                if ([userPostID isEqualToString:postidstr])
                {
                    [dataManager.globalUserReplugArray[i] setValue:[NSNumber numberWithInteger:likeCount] forKey:@"likes_count"];
                    [dataManager.globalUserReplugArray[i] setValue:@0 forKey:@"is_like"];
                }
            }
            
            for (NSInteger i=0; i<dataManager.globalUserFeedArray.count; i++)
            {
                NSString *userPostID = dataManager.globalUserFeedArray[i][@"post_id"];
                if ([userPostID isEqualToString:postidstr])
                {
                    [dataManager.globalUserFeedArray[i] setValue:[NSNumber numberWithInteger:likeCount] forKey:@"likes_count"];
                    [dataManager.globalUserFeedArray[i] setValue:@0 forKey:@"is_like"];
                }
            }
        }
        else if([operation.response statusCode]==200)
        {
            
        }
        else
        {
            [CommonFunction alertTitle:@"" withMessage:[responseDict objectForKey:@"msg"]];
            NSString *postidstr;
            NSInteger likeCount;
            if (isReplugPost)
            {
                likeCount = [arrUserPostReplug[arrindex][@"likes_count"] integerValue];
                
                [weakCell.imgLikeFillUnfill setImage:[UIImage imageNamed:@"HF_likeUnfillIcon"]];
                weakCell.imgLikeFillUnfill.tag=0;
                
                likeCount -= 1;
                weakCell.lblLikeCount.text=[NSString stringWithFormat:@"%ld",(long)likeCount];
                [arrUserPostReplug[arrindex] setValue:[NSNumber numberWithInteger:likeCount] forKey:@"likes_count"];
                [arrUserPostReplug[arrindex] setValue:@0 forKey:@"is_like"];
                
                postidstr = arrUserPostReplug[arrindex][@"id"];
            }
            else
            {
                likeCount = [arrUserPost[arrindex][@"likes_count"] integerValue];
                
                [weakCell.imgLikeFillUnfill setImage:[UIImage imageNamed:@"HF_likeUnfillIcon"]];
                weakCell.imgLikeFillUnfill.tag=0;
                
                likeCount -= 1;
                weakCell.lblLikeCount.text=[NSString stringWithFormat:@"%ld",(long)likeCount];
                [arrUserPost[arrindex] setValue:[NSNumber numberWithInteger:likeCount] forKey:@"likes_count"];
                [arrUserPost[arrindex] setValue:@0 forKey:@"is_like"];
                
                postidstr = arrUserPost[arrindex][@"id"];
            }
            
            for (NSInteger i=0; i<dataManager.globalHomeFeedArray.count; i++)
            {
                NSString *userPostID = dataManager.globalHomeFeedArray[i][@"post_id"];
                if ([userPostID isEqualToString:postidstr])
                {
                    [dataManager.globalHomeFeedArray[i] setValue:[NSNumber numberWithInteger:likeCount] forKey:@"likes_count"];
                    [dataManager.globalHomeFeedArray[i] setValue:@0 forKey:@"is_like"];
                }
            }
            
            for (NSInteger i=0; i<dataManager.globalUserReplugArray.count; i++)
            {
                NSString *userPostID = dataManager.globalUserReplugArray[i][@"post_id"];
                if ([userPostID isEqualToString:postidstr])
                {
                    [dataManager.globalUserReplugArray[i] setValue:[NSNumber numberWithInteger:likeCount] forKey:@"likes_count"];
                    [dataManager.globalUserReplugArray[i] setValue:@0 forKey:@"is_like"];
                }
            }
            
            for (NSInteger i=0; i<dataManager.globalUserFeedArray.count; i++)
            {
                NSString *userPostID = dataManager.globalUserFeedArray[i][@"post_id"];
                if ([userPostID isEqualToString:postidstr])
                {
                    [dataManager.globalUserFeedArray[i] setValue:[NSNumber numberWithInteger:likeCount] forKey:@"likes_count"];
                    [dataManager.globalUserFeedArray[i] setValue:@0 forKey:@"is_like"];
                }
            }
        }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          
                                          NSString *postidstr;
                                          NSInteger likeCount;
                                          if (isReplugPost)
                                          {
                                              likeCount = [arrUserPostReplug[arrindex][@"likes_count"] integerValue];
                                              
                                              [weakCell.imgLikeFillUnfill setImage:[UIImage imageNamed:@"HF_likeUnfillIcon"]];
                                              weakCell.imgLikeFillUnfill.tag=0;
                                              
                                              likeCount -= 1;
                                              weakCell.lblLikeCount.text=[NSString stringWithFormat:@"%ld",(long)likeCount];
                                              [arrUserPostReplug[arrindex] setValue:[NSNumber numberWithInteger:likeCount] forKey:@"likes_count"];
                                              [arrUserPostReplug[arrindex] setValue:@0 forKey:@"is_like"];
                                              
                                              postidstr = arrUserPostReplug[arrindex][@"id"];
                                          }
                                          else
                                          {
                                              likeCount = [arrUserPost[arrindex][@"likes_count"] integerValue];
                                              
                                              [weakCell.imgLikeFillUnfill setImage:[UIImage imageNamed:@"HF_likeUnfillIcon"]];
                                              weakCell.imgLikeFillUnfill.tag=0;
                                              
                                              likeCount -= 1;
                                              weakCell.lblLikeCount.text=[NSString stringWithFormat:@"%ld",(long)likeCount];
                                              [arrUserPost[arrindex] setValue:[NSNumber numberWithInteger:likeCount] forKey:@"likes_count"];
                                              [arrUserPost[arrindex] setValue:@0 forKey:@"is_like"];
                                              
                                              postidstr = arrUserPost[arrindex][@"id"];
                                          }
                                          
                                          for (NSInteger i=0; i<dataManager.globalHomeFeedArray.count; i++)
                                          {
                                              NSString *userPostID = dataManager.globalHomeFeedArray[i][@"post_id"];
                                              if ([userPostID isEqualToString:postidstr])
                                              {
                                                  [dataManager.globalHomeFeedArray[i] setValue:[NSNumber numberWithInteger:likeCount] forKey:@"likes_count"];
                                                  [dataManager.globalHomeFeedArray[i] setValue:@0 forKey:@"is_like"];
                                              }
                                          }
                                          
                                          for (NSInteger i=0; i<dataManager.globalUserReplugArray.count; i++)
                                          {
                                              NSString *userPostID = dataManager.globalUserReplugArray[i][@"post_id"];
                                              if ([userPostID isEqualToString:postidstr])
                                              {
                                                  [dataManager.globalUserReplugArray[i] setValue:[NSNumber numberWithInteger:likeCount] forKey:@"likes_count"];
                                                  [dataManager.globalUserReplugArray[i] setValue:@0 forKey:@"is_like"];
                                              }
                                          }
                                          
                                          for (NSInteger i=0; i<dataManager.globalUserFeedArray.count; i++)
                                          {
                                              NSString *userPostID = dataManager.globalUserFeedArray[i][@"post_id"];
                                              if ([userPostID isEqualToString:postidstr])
                                              {
                                                  [dataManager.globalUserFeedArray[i] setValue:[NSNumber numberWithInteger:likeCount] forKey:@"likes_count"];
                                                  [dataManager.globalUserFeedArray[i] setValue:@0 forKey:@"is_like"];
                                              }
                                          }
                                          
                                          if([operation.response statusCode]  == 400 ){
                                              NSLog(@"impo response %@",operation.response);
                                              [refreshControl endRefreshing];
                                              [CommonFunction removeActivityIndicator];
                                              APPDELEGATE.window.userInteractionEnabled=NO;
                                              [CommonFunction alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                          }
                                          else{
                                              
                                              if (wbServiceCount==1) {
                                                  wbServiceCount=0;
                                                  //  [CommonFunction showActivityIndicatorWithText:@""];
                                                  
                                              }
                                              else
                                              {
                                                  [CommonFunction removeActivityIndicator];
                                                  [CommonFunction alertTitle:@"" withMessage:@"Network Error..."];
                                              }
                                              
                                          }
                                          
                                      }];
}

-(void)unLikeAPost:(NSString *)postID cell:(PostedGifsCustomeCell *)cell indexpath:(NSInteger)arrindex replug:(BOOL)isReplugPost
{
    NSString *url = [NSString stringWithFormat:@"posts/like/%@",postID];
    //http://192.168.0.22:8353/v1/posts/like/{postId}
    
    NSMutableDictionary *header =[[NSMutableDictionary alloc]initWithObjectsAndKeys:@"application/json",@"Content-Type",[NSUSERDEFAULTS valueForKey:kUSERTOKEN],@"User-Token", nil];
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    
    __weak typeof(cell) weakCell = cell;
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypeDelete withHttpHeaders:header withServiceName:url withParameters:nil withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"--responce dict %@",responseDict);
        
        [CommonFunction removeActivityIndicator];
        [refreshControl endRefreshing];
        if(responseDict==Nil)        {
            [CommonFunction alertTitle:@"" withMessage:@"Server Error"];
            
            NSString *postidstr;
            NSInteger likeCount;
            if (isReplugPost)
            {
                likeCount = [arrUserPostReplug[arrindex][@"likes_count"] integerValue];
                
                [weakCell.imgLikeFillUnfill setImage:[UIImage imageNamed:@"PG_LikeBtn"]];
                weakCell.imgLikeFillUnfill.tag=1;
                
                likeCount += 1;
                weakCell.lblLikeCount.text=[NSString stringWithFormat:@"%ld",(long)likeCount];
                [arrUserPostReplug[arrindex] setValue:[NSNumber numberWithInteger:likeCount] forKey:@"likes_count"];
                [arrUserPostReplug[arrindex] setValue:@1 forKey:@"is_like"];
                
                postidstr = arrUserPostReplug[arrindex][@"post_id"];
            }
            else
            {
                likeCount = [arrUserPost[arrindex][@"likes_count"] integerValue];
                
                [weakCell.imgLikeFillUnfill setImage:[UIImage imageNamed:@"PG_LikeBtn"]];
                weakCell.imgLikeFillUnfill.tag=1;
                
                likeCount += 1;
                weakCell.lblLikeCount.text=[NSString stringWithFormat:@"%ld",(long)likeCount];
                [arrUserPost[arrindex] setValue:[NSNumber numberWithInteger:likeCount] forKey:@"likes_count"];
                [arrUserPost[arrindex] setValue:@1 forKey:@"is_like"];
                
                postidstr = arrUserPost[arrindex][@"post_id"];
            }
            
            for (NSInteger i=0; i<dataManager.globalHomeFeedArray.count; i++)
            {
                NSString *userPostID = dataManager.globalHomeFeedArray[i][@"post_id"];
                if ([userPostID isEqualToString:postidstr])
                {
                    [dataManager.globalHomeFeedArray[i] setValue:[NSNumber numberWithInteger:likeCount] forKey:@"likes_count"];
                    [dataManager.globalHomeFeedArray[i] setValue:@1 forKey:@"is_like"];
                }
            }
            for (NSInteger i=0; i<dataManager.globalUserFeedArray.count; i++)
            {
                NSString *userPostID = dataManager.globalUserFeedArray[i][@"post_id"];
                if ([userPostID isEqualToString:postidstr])
                {
                    [dataManager.globalUserFeedArray[i] setValue:[NSNumber numberWithInteger:likeCount] forKey:@"likes_count"];
                    [dataManager.globalUserFeedArray[i] setValue:@1 forKey:@"is_like"];
                }
            }
            for (NSInteger i=0; i<dataManager.globalUserReplugArray.count; i++)
            {
                NSString *userPostID = dataManager.globalUserReplugArray[i][@"post_id"];
                if ([userPostID isEqualToString:postidstr])
                {
                    [dataManager.globalUserReplugArray[i] setValue:[NSNumber numberWithInteger:likeCount] forKey:@"likes_count"];
                    [dataManager.globalUserReplugArray[i] setValue:@1 forKey:@"is_like"];
                }
            }
        }
        else if([operation.response statusCode]==200)
        {
            
        }
        else
        {
            [CommonFunction alertTitle:@"" withMessage:[responseDict objectForKey:@"msg"]];
            NSString *postidstr;
            NSInteger likeCount;
            if (isReplugPost)
            {
                likeCount = [arrUserPostReplug[arrindex][@"likes_count"] integerValue];
                
                [weakCell.imgLikeFillUnfill setImage:[UIImage imageNamed:@"PG_LikeBtn"]];
                weakCell.imgLikeFillUnfill.tag=1;
                
                likeCount += 1;
                weakCell.lblLikeCount.text=[NSString stringWithFormat:@"%ld",(long)likeCount];
                [arrUserPostReplug[arrindex] setValue:[NSNumber numberWithInteger:likeCount] forKey:@"likes_count"];
                [arrUserPostReplug[arrindex] setValue:@1 forKey:@"is_like"];
                
                postidstr = arrUserPostReplug[arrindex][@"post_id"];
            }
            else
            {
                likeCount = [arrUserPost[arrindex][@"likes_count"] integerValue];
                
                [weakCell.imgLikeFillUnfill setImage:[UIImage imageNamed:@"PG_LikeBtn"]];
                weakCell.imgLikeFillUnfill.tag=1;
                
                likeCount += 1;
                weakCell.lblLikeCount.text=[NSString stringWithFormat:@"%ld",(long)likeCount];
                [arrUserPost[arrindex] setValue:[NSNumber numberWithInteger:likeCount] forKey:@"likes_count"];
                [arrUserPost[arrindex] setValue:@1 forKey:@"is_like"];
                
                postidstr = arrUserPost[arrindex][@"post_id"];
            }
            
            for (NSInteger i=0; i<dataManager.globalHomeFeedArray.count; i++)
            {
                NSString *userPostID = dataManager.globalHomeFeedArray[i][@"post_id"];
                if ([userPostID isEqualToString:postidstr])
                {
                    [dataManager.globalHomeFeedArray[i] setValue:[NSNumber numberWithInteger:likeCount] forKey:@"likes_count"];
                    [dataManager.globalHomeFeedArray[i] setValue:@1 forKey:@"is_like"];
                }
            }
            for (NSInteger i=0; i<dataManager.globalUserFeedArray.count; i++)
            {
                NSString *userPostID = dataManager.globalUserFeedArray[i][@"post_id"];
                if ([userPostID isEqualToString:postidstr])
                {
                    [dataManager.globalUserFeedArray[i] setValue:[NSNumber numberWithInteger:likeCount] forKey:@"likes_count"];
                    [dataManager.globalUserFeedArray[i] setValue:@1 forKey:@"is_like"];
                }
            }
            for (NSInteger i=0; i<dataManager.globalUserReplugArray.count; i++)
            {
                NSString *userPostID = dataManager.globalUserReplugArray[i][@"post_id"];
                if ([userPostID isEqualToString:postidstr])
                {
                    [dataManager.globalUserReplugArray[i] setValue:[NSNumber numberWithInteger:likeCount] forKey:@"likes_count"];
                    [dataManager.globalUserReplugArray[i] setValue:@1 forKey:@"is_like"];
                }
            }
        }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          [CommonFunction removeActivityIndicator];
                                          
                                          NSString *postidstr;
                                          NSInteger likeCount;
                                          if (isReplugPost)
                                          {
                                              likeCount = [arrUserPostReplug[arrindex][@"likes_count"] integerValue];
                                              
                                              [weakCell.imgLikeFillUnfill setImage:[UIImage imageNamed:@"PG_LikeBtn"]];
                                              weakCell.imgLikeFillUnfill.tag=1;
                                              
                                              likeCount += 1;
                                              weakCell.lblLikeCount.text=[NSString stringWithFormat:@"%ld",(long)likeCount];
                                              [arrUserPostReplug[arrindex] setValue:[NSNumber numberWithInteger:likeCount] forKey:@"likes_count"];
                                              [arrUserPostReplug[arrindex] setValue:@1 forKey:@"is_like"];
                                              
                                              postidstr = arrUserPostReplug[arrindex][@"post_id"];
                                          }
                                          else
                                          {
                                              likeCount = [arrUserPost[arrindex][@"likes_count"] integerValue];
                                              
                                              [weakCell.imgLikeFillUnfill setImage:[UIImage imageNamed:@"PG_LikeBtn"]];
                                              weakCell.imgLikeFillUnfill.tag=1;
                                              
                                              likeCount += 1;
                                              weakCell.lblLikeCount.text=[NSString stringWithFormat:@"%ld",(long)likeCount];
                                              [arrUserPost[arrindex] setValue:[NSNumber numberWithInteger:likeCount] forKey:@"likes_count"];
                                              [arrUserPost[arrindex] setValue:@1 forKey:@"is_like"];
                                              
                                              postidstr = arrUserPost[arrindex][@"post_id"];
                                          }
                                          
                                          for (NSInteger i=0; i<dataManager.globalHomeFeedArray.count; i++)
                                          {
                                              NSString *userPostID = dataManager.globalHomeFeedArray[i][@"post_id"];
                                              if ([userPostID isEqualToString:postidstr])
                                              {
                                                  [dataManager.globalHomeFeedArray[i] setValue:[NSNumber numberWithInteger:likeCount] forKey:@"likes_count"];
                                                  [dataManager.globalHomeFeedArray[i] setValue:@1 forKey:@"is_like"];
                                              }
                                          }
                                          for (NSInteger i=0; i<dataManager.globalUserFeedArray.count; i++)
                                          {
                                              NSString *userPostID = dataManager.globalUserFeedArray[i][@"post_id"];
                                              if ([userPostID isEqualToString:postidstr])
                                              {
                                                  [dataManager.globalUserFeedArray[i] setValue:[NSNumber numberWithInteger:likeCount] forKey:@"likes_count"];
                                                  [dataManager.globalUserFeedArray[i] setValue:@1 forKey:@"is_like"];
                                              }
                                          }
                                          for (NSInteger i=0; i<dataManager.globalUserReplugArray.count; i++)
                                          {
                                              NSString *userPostID = dataManager.globalUserReplugArray[i][@"post_id"];
                                              if ([userPostID isEqualToString:postidstr])
                                              {
                                                  [dataManager.globalUserReplugArray[i] setValue:[NSNumber numberWithInteger:likeCount] forKey:@"likes_count"];
                                                  [dataManager.globalUserReplugArray[i] setValue:@1 forKey:@"is_like"];
                                              }
                                          }
                                          
                                          if([operation.response statusCode]  == 400 ){
                                              NSLog(@"impo response %@",operation.response);
                                              [refreshControl endRefreshing];
                                              [CommonFunction removeActivityIndicator];
                                              APPDELEGATE.window.userInteractionEnabled=NO;
                                              [CommonFunction alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                          }
                                          else{
                                              
                                              if (wbServiceCount==1) {
                                                  wbServiceCount=0;
                                              }
                                              else
                                              {
                                                  [CommonFunction alertTitle:@"" withMessage:@"Network Error..."];
                                              }
                                          }
                                      }];
}


#pragma mark - IBAction Button Methods
-(IBAction)likeBtnClicked:(UIButton *)sender
{
    if (!isInternetAvailabel) {
        return;
    }
    NSLog(@"%ld",(long)sender.tag);
    PostedGifsCustomeCell *cellView = (PostedGifsCustomeCell*) [tblVwData cellForRowAtIndexPath:[NSIndexPath indexPathForRow:sender.tag inSection:0]];
    NSMutableArray *array = [NSMutableArray array];
    if (isReplugSelected)
    {
        [array addObjectsFromArray:arrUserPostReplug];
    }
    else
    {
        [array addObjectsFromArray:arrUserPost];
    }
    NSInteger likeCount = [array[sender.tag][@"likes_count"] integerValue];
    
    if (cellView.imgLikeFillUnfill.tag==0) {
        
        [cellView.imgLikeFillUnfill setImage:[UIImage imageNamed:@"PG_LikeBtn"]];
        cellView.imgLikeFillUnfill.tag=1;
        
        [CommonFunction showActivityIndicatorWithText:nil];
        likeCount += 1;
        
        cellView.lblLikeCount.text=[NSString stringWithFormat:@"%ld",(long)likeCount];
        
        [array[sender.tag] setValue:[NSNumber numberWithInteger:likeCount] forKey:@"likes_count"];
        [array[sender.tag] setValue:@1 forKey:@"is_like"];
        
        NSString *postID = array[sender.tag][@"id"];
        
        if (isReplugSelected)
        {
            [arrUserPostReplug removeAllObjects];
            [arrUserPostReplug addObjectsFromArray:array];
        }
        else
        {
            [arrUserPost removeAllObjects];
            [arrUserPost addObjectsFromArray:array];
        }
        
        
        for (NSInteger i=0; i<dataManager.globalHomeFeedArray.count; i++)
        {
            NSString *userPostID = dataManager.globalHomeFeedArray[i][@"post_id"];
            if ([userPostID isEqualToString:postID])
            {
                [dataManager.globalHomeFeedArray[i] setValue:[NSNumber numberWithInteger:likeCount] forKey:@"likes_count"];
                [dataManager.globalHomeFeedArray[i] setValue:@1 forKey:@"is_like"];
            }
        }
        for (NSInteger i=0; i<dataManager.globalUserReplugArray.count; i++)
        {
            NSString *userPostID = dataManager.globalUserReplugArray[i][@"post_id"];
            if ([userPostID isEqualToString:postID])
            {
                [dataManager.globalUserReplugArray[i] setValue:[NSNumber numberWithInteger:likeCount] forKey:@"likes_count"];
                [dataManager.globalUserReplugArray[i] setValue:@1 forKey:@"is_like"];
            }
        }
        for (NSInteger i=0; i<dataManager.globalUserFeedArray.count; i++)
        {
            NSString *userPostID = dataManager.globalUserFeedArray[i][@"post_id"];
            if ([userPostID isEqualToString:postID])
            {
                [dataManager.globalUserFeedArray[i] setValue:[NSNumber numberWithInteger:likeCount] forKey:@"likes_count"];
                [dataManager.globalUserFeedArray[i] setValue:@1 forKey:@"is_like"];
            }
        }
        
        [self likeAPost:postID cell:cellView indexpath:sender.tag replug:isReplugSelected];
    }
    else
    {
        [CommonFunction showActivityIndicatorWithText:nil];
        
        [cellView.imgLikeFillUnfill setImage:[UIImage imageNamed:@"HF_likeUnfillIcon"]];
        cellView.imgLikeFillUnfill.tag=0;
        
        likeCount -= 1;
        cellView.lblLikeCount.text=[NSString stringWithFormat:@"%ld",(long)likeCount];
        [array[sender.tag] setValue:[NSNumber numberWithInteger:likeCount] forKey:@"likes_count"];
        [array[sender.tag] setValue:@0 forKey:@"is_like"];
        
        
        NSString *postID = array[sender.tag][@"id"];
        
        if (isReplugSelected)
        {
            [arrUserPostReplug removeAllObjects];
            [arrUserPostReplug addObjectsFromArray:array];
        }
        else
        {
            [arrUserPost removeAllObjects];
            [arrUserPost addObjectsFromArray:array];
        }
        
        for (NSInteger i=0; i<dataManager.globalHomeFeedArray.count; i++)
        {
            NSString *userPostID = dataManager.globalHomeFeedArray[i][@"post_id"];
            if ([userPostID isEqualToString:postID])
            {
                [dataManager.globalHomeFeedArray[i] setValue:[NSNumber numberWithInteger:likeCount] forKey:@"likes_count"];
                [dataManager.globalHomeFeedArray[i] setValue:@0 forKey:@"is_like"];
            }
        }
        for (NSInteger i=0; i<dataManager.globalUserReplugArray.count; i++)
        {
            NSString *userPostID = dataManager.globalUserReplugArray[i][@"post_id"];
            if ([userPostID isEqualToString:postID])
            {
                [dataManager.globalUserReplugArray[i] setValue:[NSNumber numberWithInteger:likeCount] forKey:@"likes_count"];
                [dataManager.globalUserReplugArray[i] setValue:@0 forKey:@"is_like"];
            }
        }
        for (NSInteger i=0; i<dataManager.globalUserFeedArray.count; i++)
        {
            NSString *userPostID = dataManager.globalUserFeedArray[i][@"post_id"];
            if ([userPostID isEqualToString:postID])
            {
                [dataManager.globalUserFeedArray[i] setValue:[NSNumber numberWithInteger:likeCount] forKey:@"likes_count"];
                [dataManager.globalUserFeedArray[i] setValue:@0 forKey:@"is_like"];
            }
        }
        
        
        [self unLikeAPost:postID cell:cellView indexpath:sender.tag replug:isReplugSelected];
    }
    
    
    
    
    //    LikeVC *lvc=[[LikeVC alloc]init];
    //    if (isReplugSelected)
    //    {
    //        lvc.strPostId=dataManager.globalUserReplugArray[sender.tag][@"post_id"];
    //        lvc.strLikeCount=[NSString stringWithFormat:@"%@",dataManager.globalUserReplugArray[sender.tag][@"likes_count"]];
    //    }
    //    else
    //    {
    //        lvc.strPostId=dataManager.globalUserFeedArray[sender.tag][@"post_id"];
    //        lvc.strLikeCount=[NSString stringWithFormat:@"%@",dataManager.globalUserFeedArray[sender.tag][@"likes_count"]];
    //    }
    //    lvc.hidesBottomBarWhenPushed=YES;
    //    [self.navigationController pushViewController:lvc animated:YES];
}
-(IBAction)commentBtnClicked:(UIButton *)sender
{
    
    CommentVC *lvc=[[CommentVC alloc]init];
    lvc.commentDelegate=self;
    if (isReplugSelected)
    {
        lvc.strPostId=arrUserPostReplug[sender.tag][@"id"];
        lvc.strLikeCount=[NSString stringWithFormat:@"%@",arrUserPostReplug[sender.tag][@"comments_count"]];
    }
    else
    {
        lvc.strPostId=arrUserPost[sender.tag][@"id"];
        lvc.strLikeCount=[NSString stringWithFormat:@"%@",arrUserPost[sender.tag][@"comments_count"]];
    }
    lvc.hidesBottomBarWhenPushed=YES;
    [self.navigationController pushViewController:lvc animated:YES];
}
-(IBAction)rePlugBtnClicked:(UIButton *)sender
{
    ReplugVC *rpvc=[[ReplugVC alloc]init];
    
    if (isReplugSelected)
    {
        rpvc.strPostId=arrUserPostReplug[sender.tag][@"id"];
        rpvc.strLikeCount=[NSString stringWithFormat:@"%@",arrUserPostReplug[sender.tag][@"replugged_count"]];
    }
    else
    {
        rpvc.strPostId=arrUserPost[sender.tag][@"id"];
        rpvc.strLikeCount=[NSString stringWithFormat:@"%@",arrUserPost[sender.tag][@"replugged_count"]];
    }
    rpvc.hidesBottomBarWhenPushed=YES;
    [self.navigationController pushViewController:rpvc animated:YES];
}
- (IBAction)actionSheetBtnClicked:(UIButton *)sender {
    
//    self.view.alpha=1;
    
    if (sender.tag==0) {
        [self clickOnTapGesture];
    }
    else if (sender.tag == 1)
    {
        [CommonFunction showAlertWithTitle:@"Are you sure?" message:nil onViewController:self withButtonsArray:@[@"Cancel",@"Yes, I'm sure"] dismissBlock:^(NSInteger buttonIndex) {
            if (buttonIndex == 1)
            {
                if (isPostNotificationEnabled)
                {
                    [self stopNotificationForID:strUserId];
                }
                else
                {
                    [self startNotificationForID:strUserId];
                }
            }
            else
            {
                [sender setBackgroundImage:nil forState:UIControlStateNormal];
            }
        }];
    }
    else if (sender.tag == 2)
    {
        [UIView animateWithDuration:0.3
                              delay:0.0
                            options: UIViewAnimationOptionTransitionFlipFromBottom
                         animations:^{
                             [vwCstmeActionSheet setFrame:CGRectMake(0.0f, [UIScreen mainScreen].bounds.size.height, [UIScreen mainScreen].bounds.size.width,280*SCREEN_XScale)];
                         }
                         completion:^(BOOL finished){
                             for (UIButton *button in vwCstmeActionSheet.subviews)
                             {
                                 if (button.tag == 0)
                                 {
                                     
                                 }
                                 else
                                 {
                                     [button removeObserver:self forKeyPath:@"highlighted"];
                                 }
                             }
                             [vwCstmeActionSheet removeFromSuperview];
                             [self showReportSheet];
                         }];
    }
    else
    {
        [CommonFunction showAlertWithTitle:@"Are you sure?" message:nil onViewController:self withButtonsArray:@[@"Cancel",@"Yes, I'm sure"] dismissBlock:^(NSInteger buttonIndex) {
            if (buttonIndex == 1)
            {
                if (isBlocked)
                {
                    [self unBlockUserWithID:strUserId];
                }
                else
                {
                    [self blockUserWithID:strUserId];
                }
            }
            else
            {
                [sender setBackgroundImage:nil forState:UIControlStateNormal];
            }
        }];
    }
}

- (void)showReportSheet
{
    [vwReportSheet setFrame:CGRectMake(0.0f, [UIScreen mainScreen].bounds.size.height, [UIScreen mainScreen].bounds.size.width,342*SCREEN_XScale)];
    [APPDELEGATE.window.rootViewController.view addSubview:vwReportSheet];
    
    for (UIButton *button in vwReportSheet.subviews)
    {
        if (button.tag == 0)
        {
            button.layer.cornerRadius=10.0f;
            button.layer.borderColor=[UIColor colorWithRed:288/255.0 green:15/255.0 blue:73/255.0 alpha:1].CGColor;
            button.layer.borderWidth=2.0f;
        }
        else
        {
            button.layer.cornerRadius=10.0f;
            button.layer.borderColor=[UIColor whiteColor].CGColor;
            button.layer.borderWidth=2.0f;
            [button addObserver:self forKeyPath:@"highlighted" options:NSKeyValueObservingOptionNew|NSKeyValueObservingOptionOld context:NULL];
        }
    }
    
    
    [UIView animateWithDuration:0.3
                          delay:0.0
                        options: UIViewAnimationOptionTransitionFlipFromBottom
                     animations:^{
                         [vwReportSheet setFrame:CGRectMake(0.0f, [UIScreen mainScreen].bounds.size.height - 342*SCREEN_XScale, [UIScreen mainScreen].bounds.size.width,342*SCREEN_XScale)];
                         vwBg.userInteractionEnabled=YES;
                         [self.view addSubview:vwBg];
                     }
                     completion:^(BOOL finished){
                     }];
}

-(IBAction)menuBtnClicked:(UIButton *)sender
{
    NSURL *URL = nil;
    
    
    [tblVwData scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:sender.tag inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
    
    if (isReplugSelected)
    {
        URL=[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",strMainImgURl,arrUserPostReplug[sender.tag][@"watermark_image"]]];
    }
    else
    {
        URL=[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",strMainImgURl,arrUserPost[sender.tag][@"watermark_image"]]];
    }
    
    UIActivityViewController *activityViewController =
    [[UIActivityViewController alloc] initWithActivityItems:@[URL]
                                      applicationActivities:nil];
    
    NSArray *excludedActivities = @[
                                    UIActivityTypePostToWeibo,
                                    UIActivityTypePrint,
                                    UIActivityTypeAssignToContact,
                                    UIActivityTypeAirDrop
                                    ];
    activityViewController.excludedActivityTypes = excludedActivities;
    
    // Present the controller
    [self presentViewController:activityViewController animated:YES completion:nil];
    
//    [vwCstmeActionSheet setFrame:CGRectMake(0.0f, [UIScreen mainScreen].bounds.size.height, [UIScreen mainScreen].bounds.size.width,280*SCREEN_XScale)];
//    [APPDELEGATE.window.rootViewController.view addSubview:vwCstmeActionSheet];
//
//    btnCancel.layer.cornerRadius=10.0f;
//    btnCancel.layer.borderColor=[UIColor colorWithRed:288/255.0 green:15/255.0 blue:73/255.0 alpha:1].CGColor;
//    btnCancel.layer.borderWidth=2.0f;
//    
//    btnBlockUser.layer.cornerRadius=10.0f;
//    btnBlockUser.layer.borderColor=[UIColor whiteColor].CGColor;
//    btnBlockUser.layer.borderWidth=2.0f;
//    
//    btnReportUser.layer.cornerRadius=10.0f;
//    btnReportUser.layer.borderColor=[UIColor whiteColor].CGColor;
//    btnReportUser.layer.borderWidth=2.0f;
//    
//    btnPostNotifications.layer.cornerRadius=10.0f;
//    btnPostNotifications.layer.borderColor=[UIColor whiteColor].CGColor;
//    btnPostNotifications.layer.borderWidth=2.0f;
//    
////    self.view.alpha=0.5;
//    
//    
//    
//    [UIView animateWithDuration:0.3
//                          delay:0.0
//                        options: UIViewAnimationOptionTransitionFlipFromBottom
//                     animations:^{
//                         [vwCstmeActionSheet setFrame:CGRectMake(0.0f, [UIScreen mainScreen].bounds.size.height - 280*SCREEN_XScale, [UIScreen mainScreen].bounds.size.width,280*SCREEN_XScale)];
//                         [self.view addSubview:vwBg];
//                     }
//                     completion:^(BOOL finished){
//                         //
//                         
//                     }];
}



- (IBAction)backbtnClicked:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)settingBtnClicked:(UIButton *)sender
{
    [vwBg setFrame:self.view.frame];
    [vwCstmeActionSheet setFrame:CGRectMake(0.0f, [UIScreen mainScreen].bounds.size.height, [UIScreen mainScreen].bounds.size.width,280*SCREEN_XScale)];
    [APPDELEGATE.window.rootViewController.view addSubview:vwCstmeActionSheet];
    
    for (UIButton *button in vwCstmeActionSheet.subviews)
    {
        if (button.tag == 0)
        {
            button.layer.cornerRadius=10.0f;
            button.layer.borderColor=[UIColor colorWithRed:288/255.0 green:15/255.0 blue:73/255.0 alpha:1].CGColor;
            button.layer.borderWidth=2.0f;
        }
        else
        {
            button.layer.cornerRadius=10.0f;
            button.layer.borderColor=[UIColor whiteColor].CGColor;
            button.layer.borderWidth=2.0f;
            
            [button addObserver:self forKeyPath:@"highlighted" options:NSKeyValueObservingOptionNew|NSKeyValueObservingOptionOld context:NULL];
        }
    }

    if (isBlocked)
    {
        [btnBlockUser setTitle:@"Unblock" forState:UIControlStateNormal];
    }
    else
    {
        [btnBlockUser setTitle:@"Block User" forState:UIControlStateNormal];
    }
    
//    self.view.alpha=0.5;
    
    [UIView animateWithDuration:0.3
                          delay:0.0
                        options: UIViewAnimationOptionTransitionFlipFromBottom
                     animations:^{
                         [vwCstmeActionSheet setFrame:CGRectMake(0.0f, [UIScreen mainScreen].bounds.size.height - 280*SCREEN_XScale, [UIScreen mainScreen].bounds.size.width,280*SCREEN_XScale)];
                         [self.view addSubview:vwBg];
                     }
                     completion:^(BOOL finished){
                         //
                         
                     }];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if ([@"highlighted" isEqualToString:keyPath]) {
        
        NSNumber *new = [change objectForKey:@"new"];
        NSNumber *old = [change objectForKey:@"old"];
        
        if (old && [new isEqualToNumber:old]) {
            NSLog(@"Highlight state has not changed");
        } else {
            NSLog(@"Highlight state has changed to %d", [object isHighlighted]);
            if ([object isHighlighted])
            {
                UIButton *btn = object;
                btn.layer.borderWidth=0;
                btn.backgroundColor=[UIColor colorWithRed:288/255.0 green:15/255.0 blue:73/255.0 alpha:1];
            }
            else
            {
                UIButton *btn = object;
                btn.layer.borderWidth=2.0f;
                btn.backgroundColor=[UIColor clearColor];
            }
        }
    }
}

- (IBAction)gifRepluggedBtnClicked:(id)sender {
    NSLog(@"Gif Replugged Btn Clicked");
    
    if (isReplugSelected || isPageRefreshingGif)
    {
        return;
    }
    
    [timerGifCount invalidate],timerGifCount=nil;
    prevIndexPath = visibleIndexPath = nil;
    
    [lblReplugCount setTextColor:[UIColor colorWithRed:228/255.0f green:15/255.0f blue:73/255.0f alpha:1.0f]];
    [_replugImageIcon setImage:[UIImage imageNamed:@"UP_refreshIcon_Selected"]];
    
    [lblGifPostedCOunt setTextColor:[UIColor colorWithRed:148/255.0f green:148/255.0f blue:148/255.0f alpha:1.0f]];
    [_gifImageIcon setImage:[UIImage imageNamed:@"UP_roundIcon"]];
    
    isReplugSelected = YES;
    [tblVwData reloadData];
    if (canViewProfile)
    {
        if (!isRequestAccepted && isPrivate)
        {
            
        }
        else
        {
            if (arrUserPostReplug.count>0)
            {
            }
            else
            {
                [self getUserPost:strUserId];
            }
        }
    }
    
    CGRect visibleRect = (CGRect){.origin = tblVwData.contentOffset, .size = tblVwData.bounds.size};
    CGPoint visiblePoint = CGPointMake(CGRectGetMidX(visibleRect), CGRectGetMidY(visibleRect));
    visibleIndexPath = [tblVwData indexPathForRowAtPoint:visiblePoint];
    if (visibleIndexPath)
    {
        btnSetting.tag=visibleIndexPath.row;
    }
}
- (IBAction)followBtnClicked:(id)sender {
    NSLog(@"%ld",(long)followBtn.tag);
    
    if (isBlocked)
    {
        [CommonFunction showAlertWithTitle:@"You cannot Follow/UnFollow blocked user. Please unblock first." message:nil onViewController:self withButtonsArray:@[@"OK"] dismissBlock:nil];
        return;
    }
    if (followBtn.tag==0) {
        [CommonFunction showActivityIndicatorWithWait];
           followBtn.userInteractionEnabled=NO;
        [self postFollowersListAPI];
    }
    else
    {
        [CommonFunction showActivityIndicatorWithWait];
        followBtn.userInteractionEnabled=NO;
        [self unFollowUserAPI:dictData[@"id"]];
    }
}
- (IBAction)gifPostedBtnClicked:(id)sender {
    NSLog(@"Gif Posted Btn Clicked");
    if (!isReplugSelected || isPageRefreshingReplug)
    {
        return;
    }
    
    [timerGifCount invalidate],timerGifCount=nil;
    prevIndexPath = visibleIndexPath = nil;
    
    [lblGifPostedCOunt setTextColor:[UIColor colorWithRed:228/255.0f green:15/255.0f blue:73/255.0f alpha:1.0f]];
    [_gifImageIcon setImage:[UIImage imageNamed:@"UP_roundIcon_Selected"]];
    
    [lblReplugCount setTextColor:[UIColor colorWithRed:148/255.0f green:148/255.0f blue:148/255.0f alpha:1.0f]];
    [_replugImageIcon setImage:[UIImage imageNamed:@"UP_refreshIcon"]];
    
    isReplugSelected = NO;
    [tblVwData reloadData];
    if (canViewProfile)
    {
        if (!isRequestAccepted && isPrivate)
        {
            
        }
        else
        {
            if (arrUserPost.count>0)
            {
                
            }
            else
            {
                [self getUserPost:strUserId];
            }
        }
    }
    
    CGRect visibleRect = (CGRect){.origin = tblVwData.contentOffset, .size = tblVwData.bounds.size};
    CGPoint visiblePoint = CGPointMake(CGRectGetMidX(visibleRect), CGRectGetMidY(visibleRect));
    visibleIndexPath = [tblVwData indexPathForRowAtPoint:visiblePoint];
    if (visibleIndexPath)
    {
        btnSetting.tag=visibleIndexPath.row;
    }
}
- (IBAction)followersBtnClicked:(id)sender {
    NSLog(@"followersBtnClicked Posted Btn Clicked");
    
    FollowerFollowingVC *followVC = [[FollowerFollowingVC alloc]initWithNibName:NSStringFromClass([FollowerFollowingVC class]) bundle:nil];
    followVC.userID = dictData[@"id"];
    followVC.type = 0;
    followVC.hidesBottomBarWhenPushed=YES;
    [self.navigationController pushViewController:followVC animated:YES];
}
- (IBAction)followingBtnClicked:(id)sender {
    NSLog(@"followingBtnClicked Posted Btn Clicked");
    
    FollowerFollowingVC *followVC = [[FollowerFollowingVC alloc]initWithNibName:NSStringFromClass([FollowerFollowingVC class]) bundle:nil];
    followVC.userID = dictData[@"id"];
    followVC.type = 1;
    followVC.hidesBottomBarWhenPushed=YES;
    [self.navigationController pushViewController:followVC animated:YES];
}


//- (UIImage *)imageWithColor:(UIColor *)color {
//    CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
//    UIGraphicsBeginImageContext(rect.size);
//    CGContextRef context = UIGraphicsGetCurrentContext();
//    
//    CGContextSetFillColorWithColor(context, [color CGColor]);
//    CGContextFillRect(context, rect);
//    
//    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
//    UIGraphicsEndImageContext();
//    
//    return image;
//}

#pragma mark - ScrollView Delegate method


- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    
    BOOL flag = NO;
    if (isReplugSelected && arrUserPostReplug.count>0)
    {
        flag = YES;
    }
    else if (!isReplugSelected && arrUserPost.count>0)
    {
        flag = YES;
    }
    if (flag)
    {
        CGRect visibleRect = (CGRect){.origin = tblVwData.contentOffset, .size = tblVwData.bounds.size};
        CGPoint visiblePoint = CGPointMake(CGRectGetMidX(visibleRect), CGRectGetMidY(visibleRect));
        visibleIndexPath = [tblVwData indexPathForRowAtPoint:visiblePoint];
        btnSetting.tag=visibleIndexPath.row;
        if (!prevIndexPath)
        {
            prevIndexPath = visibleIndexPath;
            timerGifCount = [NSTimer scheduledTimerWithTimeInterval:2.0f target:self selector:@selector(fireGifCountApi) userInfo:nil repeats:NO];
        }
        
        // For Gif Count......................................................
        if (prevIndexPath != visibleIndexPath)
        {
            if (timerGifCount.isValid || timerGifCount)
            {
                [timerGifCount invalidate],timerGifCount=nil;
            }
            prevIndexPath = visibleIndexPath;
            timerGifCount = [NSTimer scheduledTimerWithTimeInterval:2.0f target:self selector:@selector(fireGifCountApi) userInfo:nil repeats:NO];
        }
    }
    else
    {
        [timerGifCount invalidate],timerGifCount=nil;
        prevIndexPath = visibleIndexPath = nil;
    }
    
    if (scrollView.contentOffset.y<=0.0f)
    {
//        [self.tableHeaderView setFrame:CGRectMake(0.0f, 0.0f, 320.0f*scaleX, 262.0f*scaleY)];
        self.lblErrorTopCnstraint.constant=126-scrollView.contentOffset.y;
        self.headerTopConstraint.constant=-scrollView.contentOffset.y;
        self.nameTopConstraint.constant=-scrollView.contentOffset.y;
        self.imgProfileVwTopConstraint.constant=-scrollView.contentOffset.y;
        
        self.settingBtnTopConstraint.constant=25*scaleY;
        self.settingBtnTrailingCnstraint.constant=10*scaleX;

        
        self.tableHeaderView.alpha = 1;
        lblUserBio.alpha=1;
        vwGifPosted.alpha=1;
        vwReplug.alpha=1;
        followBtn.alpha=1;
        labelRequested.alpha=1;
        
        //self.settingBtnTopConstraint.constant=25*scaleY;
        self.settingBtnTrailingCnstraint.constant=10*scaleX;
        
        
        [imgProfileImg setFrame:CGRectMake(117*scaleX, 30.0f*scaleY, 86*scaleX, 86*scaleX)];
       // self.nameTopConstraint.constant=0/568*scaleY;
        self.imgProfileVwCenterContraint.constant = 0;
    
        self.imgProfileVwWidthContraint.constant=86/320*scaleX;
        
        imgProfileImg.layer.cornerRadius=imgProfileImg.frame.size.width/2 ;
        imgProfileImg.clipsToBounds=YES;
        
        
        
        self.gifpostedCountTopConstrant.constant=0;
        self.gifRepluggedCountTopConstrant.constant=0;
        self.imgProfileVwCenterContraint.constant =0;
        
        
        btnSetting.hidden = NO;
        btnInfoView.hidden = YES;
        
        //        self.nameTopConstraint.constant= 0;
        
        //        self.imgProfileVwWidthContraint.constant = MAX(0*scaleX, scrollView.contentOffset.x*1.2);
        //
        //        self.imgProfileVwCenterContraint.constant = MAX(0*scaleX, scrollView.contentOffset.x);
        //        self.imgProfileVwTopConstraint.constant = MAX(0*scaleX, scrollView.contentOffset.y);
        //        self.gifpostedCountTopConstrant.constant=MAX(0*scaleX, scrollView.contentOffset.y*2);
        //        self.gifRepluggedCountTopConstrant.constant=MAX(0*scaleX, scrollView.contentOffset.y*2);
        //
        //        imgProfileImg.layer.cornerRadius=imgProfileImg.frame.size.width/2 *scaleX;
        
    }
    else
    {
        CGFloat heightRemained;
//        if ([UIScreen mainScreen].bounds.size.width>375) {
//        heightRemained=self.tableHeaderView.frame.size.height - 68.0f*scaleX;
//        }
//        else
//        {
            heightRemained=self.tableHeaderView.frame.size.height - 58.0f*SCREEN_YScale;
//        }
        
        
        CGFloat offsetY = -+scrollView.contentOffset.y;
        
        if (offsetY <= -heightRemained)
        {
            lblUsername.lineBreakMode = NSLineBreakByTruncatingTail;
            self.headerTopConstraint.constant = - heightRemained;
            [UIView transitionWithView:self.tableHeaderView duration:0 options:UIViewAnimationOptionCurveEaseIn animations:^{
                self.tableHeaderView.alpha=1.0f;
            } completion:nil];

            if ([[arrVwControllers lastObject]isKindOfClass:[HomeFeedVC class]]) {
                if (IS_IPHONE_6P) {
                    self.nameTopConstraint.constant= MAX(-100*scaleX, offsetY/1.5);

                }
                else{
                    self.nameTopConstraint.constant= MAX(-90*scaleX, offsetY/1.5);

                }
            }
            else
            {
                self.nameTopConstraint.constant= MAX(-100*scaleX, offsetY/1.5);
            }
            
//            }
            self.imgProfileVwCenterContraint.constant = MAX(-115*scaleX, offsetY);
            self.imgProfileVwTopConstraint.constant = MAX(-4*scaleX, offsetY);
            [UIView animateWithDuration:.1 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
                CGRect frame = imgProfileImg.frame;
                imgProfileImg.frame= frame;
                imgProfileImg.layer.cornerRadius=imgProfileImg.frame.size.width/2;
            } completion:nil];
            
            btnSetting.hidden = YES;
            btnInfoView.hidden = NO;
        }
        else
        {
            
            if (offsetY <= -heightRemained+12*SCREEN_YScale)
            {
                btnSetting.hidden = YES;
                btnInfoView.hidden = NO;
            }
            else
            {
                btnSetting.hidden = NO;
                btnInfoView.hidden = YES;
            }
            
            lblUsername.lineBreakMode = NSLineBreakByTruncatingTail;
            self.headerTopConstraint.constant = offsetY;

            CGFloat origin = self.tableHeaderView.frame.origin.y;
            CGFloat delta = fabs(origin - (-offsetY));
            //self.settingBtnTopConstraint.constant=MAX(26*scaleY,offsetY );
//            self.tableHeaderView.alpha = 1 - delta/self.tableHeaderView.frame.size.height*0.4;
            lblUserBio.alpha=1-delta/vwGifPosted.frame.size.height*1;
            vwGifPosted.alpha=1-delta/vwGifPosted.frame.size.height*1.8;;
            vwReplug.alpha=1-delta/vwGifPosted.frame.size.height*1.8;
            followBtn.alpha=1-delta/vwGifPosted.frame.size.height*1.8;
            labelRequested.alpha=1-delta/vwGifPosted.frame.size.height*1.8;

            
            if ([[arrVwControllers firstObject]isKindOfClass:[HomeFeedVC class]]) {
                if (IS_IPHONE_6P) {
                    self.nameTopConstraint.constant= MAX(-100*scaleX, offsetY/1.5);
                    
                }
                else{
                    self.nameTopConstraint.constant= MAX(-90*scaleX, offsetY/1.5);
                    
                }

            }
            else
            {
                self.nameTopConstraint.constant= MAX(-100*scaleX, offsetY/1.5);
            }
            self.imgProfileVwWidthContraint.constant = MAX(-60*scaleX, offsetY*1.2);
            self.imgProfileVwCenterContraint.constant = MAX(-115*scaleX, offsetY);
            self.imgProfileVwTopConstraint.constant = MAX(-4*scaleX, offsetY);
            self.gifpostedCountTopConstrant.constant=MAX(-99*scaleX, offsetY*2);
            self.gifRepluggedCountTopConstrant.constant=MAX(-99*scaleX, offsetY*2);
            imgProfileImg.layer.cornerRadius=imgProfileImg.frame.size.width/2;
        }
    }
    
    if (([tblVwData contentOffset].y + tblVwData.frame.size.height) >= [tblVwData contentSize].height)
    {
        NSLog(@"scrolled to bottom");
        if (isReplugSelected)
        {
            if (!isPageRefreshingReplug) {
                
                if(totalPageReplugd <= currentPageReplugd)
                {
                    return;
                }
                else
                {
                    currentPageReplugd++;
                    [self animateFooterView];
                    [tblVwData setTableFooterView:vwFoterVw];
                }
                
                isPageRefreshingReplug = YES;
                [self getUserPost:strUserId];
            }
        }
        else
        {
            if (!isPageRefreshingGif) {
                
                if(totalPage <= currentPage)
                {
                    return;
                }
                else
                {
                    currentPage++;
                    [self animateFooterView];
                    [tblVwData setTableFooterView:vwFoterVw];
                }
                isPageRefreshingGif = YES;
                [self getUserPost:strUserId];
            }
        }
    }
}


#pragma mark -
#pragma mark Gif Count Update
- (void)fireGifCountApi
{
    if (!prevIndexPath) {
        return;
    }
    NSIndexPath *indexPath = prevIndexPath;
    
    NSString *postid = nil;
    BOOL isViewed = NO;
    if (isReplugSelected)
    {
        if (arrUserPostReplug.count>0) {
            postid = arrUserPostReplug[indexPath.row][@"id"];
            isViewed = [arrUserPostReplug[indexPath.row][@"is_view"]boolValue];
        }
        else
            return;
    }
    else
    {
        if (arrUserPost.count>0) {
            postid = arrUserPost[indexPath.row][@"id"];
            isViewed = [arrUserPost[indexPath.row][@"is_view"]boolValue];
        }
        else
            return;
    }
    if (isViewed) {
        return;
    }
    
    NSString *url = [NSString stringWithFormat:@"posts/views/%@",postid];
    
    NSMutableDictionary *header =[[NSMutableDictionary alloc]initWithObjectsAndKeys:@"application/json",@"Content-Type",[NSUSERDEFAULTS valueForKey:kUSERTOKEN],@"User-Token", nil];
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypePost withHttpHeaders:header withServiceName:url withParameters:nil withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"--responce dict %@",responseDict);
        if(responseDict==Nil){
        }
        else if([operation.response statusCode]==200)
        {
            if ([responseDict[@"success"] boolValue])
            {
                if (!prevIndexPath) {
                    return;
                }
                if (isReplugSelected)
                {
                    if (arrUserPostReplug.count>0) {
                        NSInteger gifCount = [arrUserPostReplug[indexPath.row][@"view_count"]integerValue];
                        gifCount +=1;
                        [arrUserPostReplug[indexPath.row] setValue:[NSNumber numberWithInteger:gifCount] forKey:@"view_count"];
                        [arrUserPostReplug[indexPath.row] setValue:[NSNumber numberWithBool:YES] forKey:@"is_view"];
                    }
                }
                else
                {
                    if (arrUserPost.count>0) {
                        NSInteger gifCount = [arrUserPost[indexPath.row][@"view_count"]integerValue];
                        gifCount +=1;
                        [arrUserPost[indexPath.row] setValue:[NSNumber numberWithInteger:gifCount] forKey:@"view_count"];
                        
                        
                        [arrUserPost[indexPath.row] setValue:[NSNumber numberWithBool:YES] forKey:@"is_view"];
                        NSString *gifID = arrUserPost[indexPath.row][@"id"];
                        for (NSInteger i=0; i<dataManager.globalHomeFeedArray.count; i++)
                        {
                            NSString *postID = dataManager.globalHomeFeedArray[i][@"post_id"];
                            
                            if ([gifID isEqualToString:postID])
                            {
                                NSInteger gifCount = [dataManager.globalHomeFeedArray[i][@"view_count"]integerValue];
                                gifCount +=1;
                                [dataManager.globalHomeFeedArray[i] setValue:[NSNumber numberWithInteger:gifCount] forKey:@"view_count"];
                                [dataManager.globalHomeFeedArray[i] setValue:[NSNumber numberWithBool:YES] forKey:@"is_view"];
                            }
                        }
                    }
                }
            }
        }
        else
        {
        }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                      }];
}


#pragma mark - UITableView Delegate Method
#pragma mark - UITableView Datasource Method

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    BOOL flag = NO;
    if (isReplugSelected) {
        if (arrUserPostReplug.count == 1)
        {
            flag = YES;
        }
    }
    else
    {
        if (arrUserPost.count == 1)
        {
            flag = YES;
        }
    }
    if (flag) {
        return 58.0f*SCREEN_YScale;
    }
    return 0;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    // set this according to that you want...
    if(IS_IPHONE6PLUS)
    {
        return 108;
    }
    else if (IS_IPHONE6) {
        return 74;
    }
    else
    {
        return 24.0f;
    }
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView *vw=[[UIView alloc]init];
    vw.userInteractionEnabled = NO;
    vw.backgroundColor=[UIColor clearColor];
    return vw;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *vw=[[UIView alloc]init];
    vw.userInteractionEnabled = NO;
    vw.backgroundColor=[UIColor clearColor];
    return vw;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 461*SCREEN_YScale;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (isReplugSelected)
    {
        return arrUserPostReplug.count;
    }
    return arrUserPost.count;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *cellIdentifier=@"PostedGifsCustomeCell";
    
    PostedGifsCustomeCell *cell=[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    cell.delegate=nil;
    cell.delegate=self;
    return cell;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(PostedGifsCustomeCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    
    
    NSMutableArray *array = [NSMutableArray array];
    if (isReplugSelected)
    {
        [array addObjectsFromArray:arrUserPostReplug];
    }
    else
    {
        [array addObjectsFromArray:arrUserPost];
    }
    
    if (array.count>0)
    {
        lblMsgError.text=@"";
        NSString *strImgUrl=array[indexPath.row][@"post_image"];
        NSString *mainURL=[NSString stringWithFormat:@"%@%@",strMainImgURl,strImgUrl];
        
        [cell.btnLike addTarget:self action:@selector(likeBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
        [cell.btnComment addTarget:self action:@selector(commentBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
        [cell.btnReplug addTarget:self action:@selector(rePlugBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
        [cell.btnMoreClicked addTarget:self action:@selector(menuBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
        
        for (NSInteger i=0; i<cell.imgVwGif.gestureRecognizers.count; i++)
        {
            UITapGestureRecognizer *imgLikeTap = cell.imgVwGif.gestureRecognizers[i];
            [cell.imgVwGif removeGestureRecognizer:imgLikeTap];
        }
        UILongPressGestureRecognizer *imgsavepress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(onSaveGifAction:)];
        [cell.imgVwGif addGestureRecognizer:imgsavepress];
        
        UITapGestureRecognizer *imgLikeTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onDoubleTapUserLikeAction:)];
        imgLikeTap.numberOfTapsRequired=2;
        cell.imgVwGif.tag = indexPath.row;
        [cell.imgVwGif addGestureRecognizer:imgLikeTap];
        cell.imgVwGif.userInteractionEnabled = YES;
        
        cell.btnLike.tag=indexPath.row;
        cell.btnComment.tag=indexPath.row;
        cell.btnReplug.tag=indexPath.row;
        cell.btnMoreClicked.tag=indexPath.row;
        
        if ([[array[indexPath.row][@"is_like"]stringValue] isEqualToString:@"0"]) {
            [cell.imgLikeFillUnfill setImage:[UIImage imageNamed:@"HF_likeUnfillIcon"]];
            cell.imgLikeFillUnfill.tag=0;
        }
        else
        {
            [cell.imgLikeFillUnfill setImage:[UIImage imageNamed:@"PG_LikeBtn"]];
            cell.imgLikeFillUnfill.tag=1;
        }
        
        [[array[indexPath.row][@"is_comment"]stringValue] isEqualToString:@"0"]
        ?[cell.imgCommentFillUnfill setImage:[UIImage imageNamed:@"PG_Comment"]]
        :[cell.imgCommentFillUnfill setImage:[UIImage imageNamed:@"PG_CommentSelected"]];
        
        
        
        cell.lblCommentCount.text=[NSString stringWithFormat:@"%@",array[indexPath.row][@"comments_count"]];
        cell.lblLikeCount.text=[NSString stringWithFormat:@"%@",array[indexPath.row][@"likes_count"]];
        cell.lblReplugCount.text=[NSString stringWithFormat:@"%@",array[indexPath.row][@"replugged_count"]];
        
        NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
        paragraphStyle.lineSpacing = 5.0f;
        paragraphStyle.lineBreakMode = NSLineBreakByTruncatingTail;
        //    paragraphStyle.maximumLineHeight = 5.0f;
        
        NSString *string = [NSString stringWithFormat:@"%@",array[indexPath.row][@"caption"]];
        NSDictionary *attributtes = @{
                                      NSParagraphStyleAttributeName : paragraphStyle,
                                      NSForegroundColorAttributeName : [UIColor whiteColor],
                                      NSFontAttributeName : [UIFont fontWithName:cell.lblCaption.font.fontName size:14.0f]
                                      };
        cell.lblCaption.attributedText = [[NSAttributedString alloc] initWithString:string
                                                                         attributes:attributtes];
        
        
        cell.tag = indexPath.row;
        
        NSURL *url1 = [NSURL URLWithString:mainURL];
        NSNumber *progress = [[AppWebHandler sharedInstance].dictProgressTask objectForKey:[url1 absoluteString]];
        if (!progress) progress = @(0.0);
        
        [cell setProgress:[progress floatValue]];
        
        if ([progress intValue]==1) {
            
            [cell.progresView setHidden:true];
            [cell.imgVwGif setHidden:false];
        }
        else
        {
            [cell.progresView setHidden:false];
            [cell.imgVwGif setHidden:true];
        }
        
        if (![[AppWebHandler sharedInstance].dictProgressTask objectForKey:[url1 absoluteString]]) {
            // Download Giff with URL Item
            [self downloadGifFromUrl:url1];
        }
        else
        {
            // Update Progress Buffer
            
            NSURL *localURL = [self URLForGiffWithName:[mainURL lastPathComponent]];
            
            if ([[NSFileManager defaultManager] fileExistsAtPath:[localURL path]]) {
                
                [cell.progresView setHidden:true];
                [cell.imgVwGif setHidden:false];
                
                NSData *animatedImageData = [[NSFileManager defaultManager] contentsAtPath:localURL.path];
                
                FLAnimatedImage *animatedImage = [[FLAnimatedImage alloc] initWithAnimatedGIFData:animatedImageData generateThumbnail:NO];
                [cell.imgVwGif setAnimatedImage:nil];
                [cell.imgVwGif setAnimatedImage:animatedImage];
            }
        }
    }
    else
    {
        lblMsgError.text=@"No post found.";
    }
    [cell handleCaptionBar];
}

- (void)tableView:(UITableView *)tableView didEndDisplayingCell:(PostedGifsCustomeCell *)cell forRowAtIndexPath:(NSIndexPath*)indexPath
{
    if (cell) {
        [cell resetCaptionBar];
    }
}

//- (void)tableView:(UITableView *)tableView didEndDisplayingCell:(PostedGifsCustomeCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    NSString *strImgUrl=arrUserPost[indexPath.row][@"post_image"];
//    NSString *mainURL=[NSString stringWithFormat:@"%@%@",strMainImgURl,strImgUrl];
//    NSURL *localURL = [self URLForGiffWithName:[mainURL lastPathComponent]];
//    
//    if ([[NSFileManager defaultManager] fileExistsAtPath:[localURL path]]) {
//        
//        [cell.imgVwGif stopAnimating];
//    }
//    
//}



/// Even though NSURLCache *may* cache the results for remote images, it doesn't guarantee it.
/// Cache control headers or internal parts of NSURLCache's implementation may cause these images to become uncache.
/// Here we enfore strict disk caching so we're sure the images stay around.
- (void)loadAnimatedImageWithURL:(NSURL *const)url completion:(void (^)(FLAnimatedImage *animatedImage))completion
{
    NSString *const filename = url.lastPathComponent;
    NSString *const diskPath = [NSHomeDirectory() stringByAppendingPathComponent:filename];
    
    NSData * __block animatedImageData = [[NSFileManager defaultManager] contentsAtPath:diskPath];
    FLAnimatedImage * __block animatedImage = [[FLAnimatedImage alloc] initWithAnimatedGIFData:animatedImageData generateThumbnail:NO];
    
    if (animatedImage) {
        if (completion) {
            completion(animatedImage);
        }
    } else {
        [[[NSURLSession sharedSession] dataTaskWithURL:url completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            animatedImageData = data;
            animatedImage = [[FLAnimatedImage alloc] initWithAnimatedGIFData:animatedImageData generateThumbnail:NO];
            if (animatedImage) {
                if (completion) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        completion(animatedImage);
                    });
                }
                [data writeToFile:diskPath atomically:YES];
            }
        }] resume];
    }
}


#pragma mark - FooterView Animating
-(void)animateFooterView
{
    
    [UIView animateWithDuration:1.0f delay:0.0f options:UIViewAnimationOptionRepeat|UIViewAnimationOptionCurveLinear
                     animations:^{
                         [replugCircle setTransform:CGAffineTransformRotate([replugCircle transform], M_PI-0.00001f)];
                     } completion:nil];
    
}
#pragma mark - NSURLSession Delegate



- (NSURLSession *)backgroundSession {
    static NSURLSession *session = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        // Session Configuration
        NSURLSessionConfiguration *sessionConfiguration = [NSURLSessionConfiguration backgroundSessionConfigurationWithIdentifier:@"com.Singlecast.BackgroundSession"];
        
        // Initialize Session
        session = [NSURLSession sessionWithConfiguration:sessionConfiguration delegate:self delegateQueue:nil];
    });
    
    return session;
}

- (void)downloadGifFromUrl:(NSURL *)gifUrl {
    
    [[AppWebHandler sharedInstance]initializeBackgroundSession];
    
    if (gifUrl) {
        // Schedule Download Task
        [[AppWebHandler sharedInstance]downloadTaskForUrl:gifUrl];
        
        // Update Progress Buffer
        [[AppWebHandler sharedInstance].dictProgressTask setObject:@(0.0) forKey:[gifUrl absoluteString]];
    }
}


- (void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didResumeAtOffset:(int64_t)fileOffset expectedTotalBytes:(int64_t)expectedTotalBytes {
    NSLog(@"%s", __PRETTY_FUNCTION__);
}


- (void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didWriteData:(int64_t)bytesWritten totalBytesWritten:(int64_t)totalBytesWritten totalBytesExpectedToWrite:(int64_t)totalBytesExpectedToWrite {
    // Calculate Progress
    double progress = (double)totalBytesWritten / (double)totalBytesExpectedToWrite;
    
    // Update Progress Buffer
    NSURL *URL = [[downloadTask originalRequest] URL];
    [[AppWebHandler sharedInstance].dictProgressTask setObject:@(progress) forKey:[URL absoluteString]];
    
    // Update Table View Cell
    PostedGifsCustomeCell *cell = [self cellForForDownloadTask:downloadTask];
    __weak typeof(cell) weakCell = cell;
    dispatch_async(dispatch_get_main_queue(), ^{
        [weakCell setProgress:progress];
    });
    
}

- (void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didFinishDownloadingToURL:(NSURL *)location {
    // Write File to Disk
    
    // NSData *imageData = [NSData dataWithContentsOfURL:lo];
    NSData *imageData = [NSData dataWithContentsOfFile:[location path]];
    NSLog(@"location is %@",location.absoluteString);
    
    [self moveFileWithURL:location downloadTask:downloadTask];
    
    // Update Table View Cell
    PostedGifsCustomeCell *cell = [self cellForForDownloadTask:downloadTask];
    
    // Update Progress Buffer
    NSURL *URL = [[downloadTask originalRequest] URL];
    [[AppWebHandler sharedInstance].dictProgressTask setObject:@(1.0) forKey:[URL absoluteString]];
    
    
    __weak typeof(cell) weakCell = cell;
    dispatch_async(dispatch_get_main_queue(), ^{
        FLAnimatedImage *image = [[FLAnimatedImage alloc] initWithAnimatedGIFData:imageData generateThumbnail:NO];
        [weakCell.imgVwGif setAnimatedImage:nil];
        [weakCell.imgVwGif setAnimatedImage:image];
        [weakCell.progresView setHidden:true];
        [weakCell.imgVwGif setHidden:false];
        if (weakCell) {
            [tblVwData reloadData];
        }
    });
    // Invoke Background Completion Handler
    [self invokeBackgroundSessionCompletionHandler];
}


- (void)moveFileWithURL:(NSURL *)URL downloadTask:(NSURLSessionDownloadTask *)downloadTask {
    // Filename
    NSString *fileName = [[[downloadTask originalRequest] URL] lastPathComponent];
    
    // Local URL
    NSURL *localURL = [self URLForGiffWithName:fileName];
    
    NSFileManager *fm = [NSFileManager defaultManager];
    
    if ([fm fileExistsAtPath:[URL path]]) {
        NSError *error = nil;
        [fm moveItemAtURL:URL toURL:localURL error:&error];
        [_tempDirectUrls addObject:[localURL path]];
        if (error) {
            NSLog(@"Unable to move temporary file to destination. %@, %@", error, error.userInfo);
        }
    }
}


- (NSURL *)URLForGiffWithName:(NSString *)name {
    if (!name) return nil;
    return [self.gifDirectory URLByAppendingPathComponent:name];
}

- (NSURL *)gifDirectory {
//    NSURL *documents = [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
//    NSURL *gifs = [documents URLByAppendingPathComponent:@"Gif"];
//    
//    NSFileManager *fm = [NSFileManager defaultManager];
//    
//    if (![fm fileExistsAtPath:[gifs path]]) {
//        NSError *error = nil;
//        [fm createDirectoryAtURL:gifs withIntermediateDirectories:YES attributes:nil error:&error];
//        
//        if (error) {
//            NSLog(@"Unable to create episodes directory. %@, %@", error, error.userInfo);
//        }
//    }
    
    return [AppWebHandler sharedInstance].tempDirectory;
}


- (PostedGifsCustomeCell *)cellForForDownloadTask:(NSURLSessionDownloadTask *)downloadTask {
    // Helpers
    PostedGifsCustomeCell *cell = nil;
    NSURL *URL = [[downloadTask originalRequest] URL];
    
    
    NSMutableArray *array = [NSMutableArray array];
    if (isReplugSelected)
    {
        [array addObjectsFromArray:arrUserPostReplug];
    }
    else
    {
        [array addObjectsFromArray:arrUserPost];
    }
    
    
    for (NSInteger i =0; i<array.count; i++)
    {
        
        BOOL flag = NO;
        NSDictionary *dict = array[i];
        
        NSString *strImgUrl=[dict valueForKey:@"post_image"];
        NSURL *mainURL=[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",strMainImgURl,strImgUrl]];
        //   NSURL *feedItemURL = [self urlForFeedItem:feedItem];
        
        
        
        @try {
            if ([URL isEqual:mainURL]) {
                NSUInteger index = [array indexOfObject:dict];
                NSIndexPath *indexpath = [NSIndexPath indexPathForRow:index inSection:0];
                if ([[tblVwData indexPathsForVisibleRows]containsObject:indexpath]) {
                    cell = (PostedGifsCustomeCell *)[tblVwData cellForRowAtIndexPath:indexpath];
                    flag = YES;
                }
            }
        } @catch (NSException *exception) {
            [tblVwData reloadData];
        } @finally {
            if (flag) {
                break;
            }
        }
    }
//    for (NSDictionary *dict in array) {
//        
//        NSString *strImgUrl=[dict valueForKey:@"post_image"];
//        NSURL *mainURL=[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",strMainImgURl,strImgUrl]];
//        //   NSURL *feedItemURL = [self urlForFeedItem:feedItem];
//        
//        
//        if ([URL isEqual:mainURL]) {
//            NSUInteger index = [array indexOfObject:dict];
//            NSIndexPath *indexpath = [NSIndexPath indexPathForRow:index inSection:0];
////            if ([[tblVwData indexPathsForVisibleRows]containsObject:indexpath]) {
//                cell = (PostedGifsCustomeCell *)[tblVwData cellForRowAtIndexPath:indexpath];
//                break;
////            }
//        }
//    }
    
    
    return cell;
}

- (void)invokeBackgroundSessionCompletionHandler {
    
    [[AppWebHandler sharedInstance].httpSessionBackground getTasksWithCompletionHandler:^(NSArray *dataTasks, NSArray *uploadTasks, NSArray *downloadTasks) {
        NSUInteger count = [dataTasks count] + [uploadTasks count] + [downloadTasks count];
        
        if (!count) {
            AppDelegate *applicationDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
            void (^backgroundSessionCompletionHandler)() = [applicationDelegate backgroundSessionCompletionHandler];
            
            if (backgroundSessionCompletionHandler) {
                [applicationDelegate setBackgroundSessionCompletionHandler:nil];
                backgroundSessionCompletionHandler();
            }
        }
    }];
}



#pragma mark - WebService API


- (void)startNotificationForID:(NSString *)userID
{
    NSString *url = [NSString stringWithFormat:@"users/postNotification/%@",userID];
    
    NSMutableDictionary *header =[[NSMutableDictionary alloc]initWithObjectsAndKeys:@"application/json",@"Content-Type",[NSUSERDEFAULTS valueForKey:kUSERTOKEN],@"User-Token", nil];
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    
    __weak typeof(self) weakSelf = self;
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypePost withHttpHeaders:header withServiceName:url withParameters:nil withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"responce dict %@",responseDict);
        
        [CommonFunction removeActivityIndicator];
        if(responseDict==Nil)        {
            [CommonFunction alertTitle:@"" withMessage:@"Server Error"];
        }
        else if([operation.response statusCode]==200)
        {
            isPostNotificationEnabled = YES;
            [CommonFunction showAlertWithTitle:@"Post Notification Enabled" message:nil onViewController:weakSelf withButtonsArray:@[@"Dismiss"] dismissBlock:nil];
        }
        else
        {
            [CommonFunction alertTitle:@"" withMessage:[responseDict objectForKey:@"msg"]];
        }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          
                                          [CommonFunction removeActivityIndicator];
                                          if([operation.response statusCode]  == 400 ){
                                              NSLog(@"impo response %@",operation.response);
                                              [CommonFunction alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                          }
                                          else{
                                              
                                              if (wbServiceCount==1) {
                                                  wbServiceCount=0;
                                              }
                                              else
                                              {
                                                  [CommonFunction alertTitle:@"" withMessage:@"Network Error..."];
                                              }
                                          }
                                      }];
}

- (void)stopNotificationForID:(NSString *)userID
{
    NSString *url = [NSString stringWithFormat:@"users/postNotification/%@",userID];
    
    NSMutableDictionary *header =[[NSMutableDictionary alloc]initWithObjectsAndKeys:@"application/json",@"Content-Type",[NSUSERDEFAULTS valueForKey:kUSERTOKEN],@"User-Token", nil];
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    
    __weak typeof(self) weakSelf = self;
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypeDelete withHttpHeaders:header withServiceName:url withParameters:nil withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"responce dict %@",responseDict);
        
        [CommonFunction removeActivityIndicator];
        if(responseDict==Nil)        {
            [CommonFunction alertTitle:@"" withMessage:@"Server Error"];
        }
        else if([operation.response statusCode]==200)
        {
            isPostNotificationEnabled = NO;
            [CommonFunction showAlertWithTitle:@"Post Notification Disbaled" message:nil onViewController:weakSelf withButtonsArray:@[@"Dismiss"] dismissBlock:nil];
        }
        else
        {
            [CommonFunction alertTitle:@"" withMessage:[responseDict objectForKey:@"msg"]];
        }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          
                                          [CommonFunction removeActivityIndicator];
                                          if([operation.response statusCode]  == 400 ){
                                              NSLog(@"impo response %@",operation.response);
                                              [CommonFunction alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                          }
                                          else{
                                              
                                              if (wbServiceCount==1) {
                                                  wbServiceCount=0;
                                              }
                                              else
                                              {
                                                  [CommonFunction alertTitle:@"" withMessage:@"Network Error..."];
                                              }
                                          }
                                      }];
}

- (void)blockUserWithID:(NSString *)userID
{
    NSString *url = [NSString stringWithFormat:@"users/blockUser/%@",userID];
    
    NSMutableDictionary *header =[[NSMutableDictionary alloc]initWithObjectsAndKeys:@"application/json",@"Content-Type",[NSUSERDEFAULTS valueForKey:kUSERTOKEN],@"User-Token", nil];
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    
//    __weak typeof(self) weakSelf = self;
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypePost withHttpHeaders:header withServiceName:url withParameters:nil withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"responce dict %@",responseDict);
        
        [CommonFunction removeActivityIndicator];
        if(responseDict==Nil)        {
            [CommonFunction alertTitle:@"" withMessage:@"Server Error"];
        }
        else if([operation.response statusCode]==200)
        {
            isBlocked = YES;
            [CommonFunction showAlertWithTitle:@"User blocked" message:nil onViewController:self withButtonsArray:@[@"Dismiss"] dismissBlock:nil];
            [btnBlockUser setTitle:@"Unblock" forState:UIControlStateNormal];
        }
        else
        {
            [CommonFunction alertTitle:@"" withMessage:[responseDict objectForKey:@"msg"]];
        }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          
                                          [CommonFunction removeActivityIndicator];
                                          if([operation.response statusCode]  == 400 ){
                                              NSLog(@"impo response %@",operation.response);
                                              [CommonFunction alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                          }
                                          else{
                                              
                                              if (wbServiceCount==1) {
                                                  wbServiceCount=0;
                                              }
                                              else
                                              {
                                                  [CommonFunction alertTitle:@"" withMessage:@"Network Error..."];
                                              }
                                          }
                                      }];
}

- (void)unBlockUserWithID:(NSString *)userID
{
    NSString *url = [NSString stringWithFormat:@"users/blockUser/%@",userID];
    
    NSMutableDictionary *header =[[NSMutableDictionary alloc]initWithObjectsAndKeys:@"application/json",@"Content-Type",[NSUSERDEFAULTS valueForKey:kUSERTOKEN],@"User-Token", nil];
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    
//    __weak typeof(self) weakSelf = self;
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypeDelete withHttpHeaders:header withServiceName:url withParameters:nil withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"responce dict %@",responseDict);
        
        [CommonFunction removeActivityIndicator];
        if(responseDict==Nil)        {
            [CommonFunction alertTitle:@"" withMessage:@"Server Error"];
        }
        else if([operation.response statusCode]==200)
        {
            isBlocked = NO;
            [CommonFunction showAlertWithTitle:@"User unblocked" message:nil onViewController:self withButtonsArray:@[@"Dismiss"] dismissBlock:nil];
            [btnBlockUser setTitle:@"Block User" forState:UIControlStateNormal];
        }
        else
        {
            [CommonFunction alertTitle:@"" withMessage:[responseDict objectForKey:@"msg"]];
        }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          [CommonFunction removeActivityIndicator];
                                          if([operation.response statusCode]  == 400 ){
                                              NSLog(@"impo response %@",operation.response);
                                              [CommonFunction alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                          }
                                          else{
                                              
                                              if (wbServiceCount==1) {
                                                  wbServiceCount=0;
                                              }
                                              else
                                              {
                                                  [CommonFunction alertTitle:@"" withMessage:@"Network Error..."];
                                              }
                                          }
                                      }];
}

-(void)getUserProfileAndShouldGetPost:(BOOL)getPost
{
    NSString *url = [NSString stringWithFormat:@"users/%@",_strUserName];
    //http://192.168.0.22:8353/v1/users/{username}
    
    NSMutableDictionary *header =[[NSMutableDictionary alloc]initWithObjectsAndKeys:@"application/json",@"Content-Type",[NSUSERDEFAULTS valueForKey:kUSERTOKEN],@"User-Token", nil];
    
    //    NSMutableDictionary *param=[[NSMutableDictionary alloc]initWithObjectsAndKeys:[CommonFunction trimSpaceInString:txtUsername.text],@"username",[CommonFunction trimSpaceInString:txtEmail.text],@"email",[CommonFunction trimSpaceInString:txtPassword.text],@"password",@"ios",@"device_type",pushDeviceToken,@"device_token", nil];
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    
    __weak typeof(self) weakSelf = self;
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypeGet withHttpHeaders:header withServiceName:url withParameters:nil withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"responce dict %@",responseDict);
        
        [CommonFunction removeActivityIndicator];
        if(responseDict==Nil)        {
            [CommonFunction alertTitle:@"" withMessage:@"Server Error"];
            [refreshControl endRefreshing];
        }
        else if([operation.response statusCode]==200)
        {
            dictData=responseDict[@"userInfo"];
            
            isFollowing = [dictData[@"is_following"] boolValue];
            isRequestAccepted = [dictData[@"request_status"] boolValue];
            isPrivate = NO;
            if ([dictData[@"account_type"]isEqualToString:@"private"])
            {
                isPrivate = YES;
            }
            canViewProfile = [dictData[@"view_profile"] boolValue];
            lblFollowersCount.text=[NSString stringWithFormat:@"%@",[[responseDict valueForKey:@"userInfo"]valueForKey:@"followers_count"]];
            lblFollowingCount.text=[NSString stringWithFormat:@"%@",[[responseDict valueForKey:@"userInfo"]valueForKey:@"following_count"]];
            lblReplugCount.text=[NSString stringWithFormat:@"%@",[[responseDict valueForKey:@"userInfo"]valueForKey:@"replugged_count"]];
            lblGifPostedCOunt.text=[NSString stringWithFormat:@"%@",[[responseDict valueForKey:@"userInfo"]valueForKey:@"posted_gif_count"]];

            NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
            paragraphStyle.lineSpacing = 5.0f;
            //    paragraphStyle.maximumLineHeight = 5.0f;
            
            NSString *string = [NSString stringWithFormat:@"%@ \n %@",[[responseDict valueForKey:@"userInfo"]valueForKey:@"bio"],[[responseDict valueForKey:@"userInfo"]valueForKey:@"address"]];

            NSDictionary *attributtes = @{
                                          NSParagraphStyleAttributeName : paragraphStyle,
                                          };
        lblUserBio.attributedText = [[NSAttributedString alloc] initWithString:string
                                                                             attributes:attributtes];
            
            lblUserBio.textAlignment = NSTextAlignmentCenter;
            
            lblUsername.text=[NSString stringWithFormat:@"%@",[[responseDict valueForKey:@"userInfo"]valueForKey:@"username"]];
            
            NSString *strAvtarImg=[[responseDict valueForKey:@"userInfo"]valueForKey:@"avatar_image"];
            if (strAvtarImg.length>1) {
                
                [imgProfileImg sd_setImageWithURL:[NSURL URLWithString:strAvtarImg] placeholderImage:[UIImage imageNamed:@"default.jpg"] options:0 completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                    if (error)
                    {
                        imgProfileImg.contentMode=UIViewContentModeScaleToFill;
                    }
                    else
                    {
                        if ([[strAvtarImg pathExtension]isEqualToString:@"gif"])
                        {
                            imgProfileImg.contentMode=UIViewContentModeScaleAspectFill;
                        }
                        else
                        {
                            imgProfileImg.contentMode=UIViewContentModeScaleToFill;
                        }
                    }
                }];
                
//                [imgProfileImg sd_setImageWithURL:[NSURL URLWithString:strAvtarImg] placeholderImage:[UIImage imageNamed:@"default.jpg"] options:SDWebImageRefreshCached completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
//                    
//                }];
            }
            else
            {
                [imgProfileImg setImage:[UIImage imageNamed:@"default.jpg"]];
            }
            [imgProfileBgImg sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[[responseDict valueForKey:@"userInfo"]valueForKey:@"background_image"]]]];
            
            NSMutableDictionary *newDict=[[NSMutableDictionary alloc]initWithObjectsAndKeys:[dictData valueForKey:@"id"],@"user_id",[dictData valueForKey:@"account_type"],@"account_type", nil];
            [newDict setValue:[NSNumber numberWithBool:isRequestAccepted] forKey:@"request_status"];
            if (!isFollowing) {
                [followBtn setImage:[UIImage imageNamed:@"OUP_followBtn"] forState:UIControlStateNormal];
                followBtn.tag=0;
                followBtn.hidden = NO;
                labelRequested.hidden = YES;
                
                if (dataManager.followersDataArray) {
                    
                    if ([[dataManager.followersDataArray valueForKey:@"user_id"]containsObject:newDict[@"user_id"]])
                    {
                        NSInteger index = [[dataManager.followersDataArray valueForKey:@"user_id"]indexOfObject:newDict[@"user_id"]];
                        [dataManager.followersDataArray removeObjectAtIndex:index];
                    }
                }
            }
            else
            {
                if (isRequestAccepted)
                {
                    [followBtn setImage:[UIImage imageNamed:@"OUP_unfollowBtn"] forState:UIControlStateNormal];
                    followBtn.tag=1;
                    
                    followBtn.hidden = NO;
                    labelRequested.hidden = YES;
                }
               else
               {
                   followBtn.hidden = YES;
                   labelRequested.hidden = NO;
//                   [followBtn setImage:[UIImage imageNamed:@"pendingBtn"] forState:UIControlStateNormal];
//                   followBtn.tag=0;
               }
                if (dataManager.followersDataArray) {
                    if ([[dataManager.followersDataArray valueForKey:@"user_id"]containsObject:newDict[@"user_id"]])
                    {
                        NSInteger index = [[dataManager.followersDataArray valueForKey:@"user_id"]indexOfObject:newDict[@"user_id"]];
                        [dataManager.followersDataArray removeObjectAtIndex:index];
                    }
                    [dataManager.followersDataArray addObject:newDict];
                }
            }
            strUserId=[[responseDict valueForKey:@"userInfo"]valueForKey:@"id"];
            isBlocked = [dictData[@"is_blocked"] boolValue];
            isPostNotificationEnabled = [dictData[@"post_notifications"] boolValue];
            
//            currentPage=1;
            if (canViewProfile)
            {
                if (!isRequestAccepted && isPrivate)
                {
                    lblMsgError.text=@"No Post Found.";
                    if (refreshControl.isRefreshing)
                    {
                        AudioServicesPlaySystemSound(1109);
                    }
                    [refreshControl endRefreshing];
                    isPageRefreshingGif = NO;
                    isPageRefreshingReplug = NO;
                    currentPage = currentPageReplugd = 1;
                    totalPage=totalPageReplugd = 0;
                    [arrUserPost removeAllObjects];
                    [arrUserPostReplug removeAllObjects];
                    [tblVwData reloadData];
                }
                else
                {
                    self.tableHeaderView.userInteractionEnabled = YES;
                    if (getPost)
                    {
                        [weakSelf getUserPost:strUserId];
                    }
                    else
                    {
                        if (refreshControl.isRefreshing)
                        {
                            AudioServicesPlaySystemSound(1109);
                        }
                        [refreshControl endRefreshing];
                    }
                }
            }
            else
            {
                lblMsgError.text=@"No Post Found.";
//                self.tableHeaderView.userInteractionEnabled = NO;
                if (refreshControl.isRefreshing)
                {
                    AudioServicesPlaySystemSound(1109);
                }
                [refreshControl endRefreshing];
                isPageRefreshingGif = NO;
                isPageRefreshingReplug = NO;
                currentPage = currentPageReplugd = 1;
                totalPage=totalPageReplugd = 0;
                [arrUserPost removeAllObjects];
                [arrUserPostReplug removeAllObjects];
                [tblVwData reloadData];
            }
        }
        else
        {
            [refreshControl endRefreshing];
            [CommonFunction alertTitle:@"" withMessage:[responseDict objectForKey:@"msg"]];
        }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          [refreshControl endRefreshing];
                                          [CommonFunction removeActivityIndicator];
                                          if (error.code == -999) {
                                              return;
                                          }
                                          if([operation.response statusCode]  == 400 ){
                                              NSLog(@"impo response %@",operation.response);
                                              [CommonFunction alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                          }
                                          else{
                                              
                                              if (wbServiceCount==1) {
                                                  wbServiceCount=0;
                                              }
                                              else
                                              {
                                                  [CommonFunction alertTitle:@"" withMessage:@"Network Error..."];
                                              }
                                          }
                                      }];
}

-(void)postFollowersListAPI{
    
    NSString *url = [NSString stringWithFormat:@"users/following"];
    //http://192.168.0.22:8353/v1/users/following
    
    NSMutableDictionary *header =[[NSMutableDictionary alloc]initWithObjectsAndKeys:@"application/json",@"Content-Type",[NSUSERDEFAULTS valueForKey:kUSERTOKEN],@"User-Token", nil];
    

    NSMutableDictionary *dataDict=[[NSMutableDictionary alloc]initWithObjectsAndKeys:dictData[@"id"],@"user_id",dictData[@"account_type"],@"account_type", nil];
    NSMutableArray *arrSelectedIndexPath=[[NSMutableArray alloc]initWithObjects:dataDict,nil];
    
    NSMutableDictionary *param=[[NSMutableDictionary alloc]initWithObjectsAndKeys:arrSelectedIndexPath,@"following", nil];
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypePost withHttpHeaders:header withServiceName:url withParameters:param withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"responce dict %@",responseDict);
        
        [CommonFunction removeActivityIndicator];
        
        if(responseDict==Nil)        {
            [CommonFunction alertTitle:@"" withMessage:@"Server Error"];
            
        }
        else if([operation.response statusCode]==200)
        {
//            [followBtn setImage:[UIImage imageNamed:@"OUP_unfollowBtn"] forState:UIControlStateNormal];
//            followBtn.tag=1;
               followBtn.userInteractionEnabled=YES;
            [self getUserProfileAndShouldGetPost:NO];
        }
        else
        {
            [CommonFunction alertTitle:@"" withMessage:[responseDict objectForKey:@"msg"]];
        }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          if([operation.response statusCode]  == 400 ){
                                              NSLog(@"impo response %@",operation.response);
                                              
                                              [CommonFunction removeActivityIndicator];
                                              [CommonFunction alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                              
                                              
                                          }
                                          else{
                                              
                                              if (wbServiceCount==1) {
                                                  wbServiceCount=0;
                                                  [CommonFunction showActivityIndicatorWithText:@""];
                                                  //      [self loginAPI];
                                              }
                                              else
                                              {
                                                  [CommonFunction removeActivityIndicator];
                                                  [CommonFunction alertTitle:@"" withMessage:@"Network Error..."];
                                              }
                                              
                                          }
                                          
                                      }];
}

-(void)unFollowUserAPI:(NSString *)strUserId;
{
    
    NSString *url = [NSString stringWithFormat:@"users/following/%@",dictData[@"id"]];
    //http://192.168.0.22:8353/v1//users/following/{userId}
    
    NSMutableDictionary *header =[[NSMutableDictionary alloc]initWithObjectsAndKeys:@"application/json",@"Content-Type",[NSUSERDEFAULTS valueForKey:kUSERTOKEN],@"User-Token", nil];
    
    //    NSMutableDictionary *param=[[NSMutableDictionary alloc]initWithObjectsAndKeys:strUserId,@"userId", nil];
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypeDelete withHttpHeaders:header withServiceName:url withParameters:nil withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"responce dict %@",responseDict);
        
        [CommonFunction removeActivityIndicator];
        [CommonFunction removeActivityIndicatorWithWait];
        if(responseDict==Nil)        {
            [CommonFunction alertTitle:@"" withMessage:@"Server Error"];
            
        }
        else if([operation.response statusCode]==200)
        {
//            [followBtn setImage:[UIImage imageNamed:@"OUP_followBtn"] forState:UIControlStateNormal];
            followBtn.tag=0;
            followBtn.userInteractionEnabled=YES;       
            
            [self getUserProfileAndShouldGetPost:NO];
        }
        else
        {
            [CommonFunction alertTitle:@"" withMessage:[responseDict objectForKey:@"msg"]];
        }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          if([operation.response statusCode]  == 400 ){
                                              NSLog(@"impo response %@",operation.response);
                                              
                                              [CommonFunction removeActivityIndicator];
                                              [CommonFunction alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                          }
                                          else{
                                              
                                              if (wbServiceCount==1) {
                                                  wbServiceCount=0;
                                                  [CommonFunction showActivityIndicatorWithText:@""];
                                                  //      [self loginAPI];
                                              }
                                              else
                                              {
                                                  [CommonFunction removeActivityIndicator];
                                                  [CommonFunction alertTitle:@"" withMessage:@"Network Error..."];
                                              }
                                              
                                          }
                                          
                                      }];
}

-(void)getUserPost:(NSString *)strUsrId
{
    [CommonFunction showActivityIndicatorWithText:@""];
    NSString *type = isReplugSelected?@"replug":@"gif";
    NSInteger page = isReplugSelected?currentPageReplugd:currentPage;
    
    NSString *url = [NSString stringWithFormat:@"posts/userPosts/%@/%ld/%@",strUserId,(long)page,type];
    //http://192.168.0.22:8353/v1/posts/userPosts/{userId}/{page}
    
    NSMutableDictionary *header =[[NSMutableDictionary alloc]initWithObjectsAndKeys:@"application/json",@"Content-Type",[NSUSERDEFAULTS valueForKey:kUSERTOKEN],@"User-Token", nil];
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];

    __weak typeof(self) weakSelf = self;
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypeGet withHttpHeaders:header withServiceName:url withParameters:nil withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"responce dict %@",responseDict);
        [tblVwData setTableFooterView:nil];
        [CommonFunction removeActivityIndicator];

        if (refreshControl.isRefreshing)
        {
            AudioServicesPlaySystemSound(1109);
        }
        [refreshControl endRefreshing];
        if(responseDict==Nil)        {
            [CommonFunction alertTitle:@"" withMessage:@"Server Error"];
            if (isReplugSelected)
            {
                if (currentPageReplugd>1)
                {
                    currentPageReplugd--;
                }
                isPageRefreshingReplug = NO;
            }else
            {
                if (currentPage>1)
                {
                    currentPage--;
                }
                isPageRefreshingGif = NO;
            }
        }
        else if([operation.response statusCode]==200)
        {
            NSArray *arrListing = [responseDict objectForKey:@"posts"];
            
            if (arrListing.count > 0)
            {
                lblMsgError.text=@"";
                strMainImgURl = responseDict[@"site_url"];
                if (isReplugSelected)
                {
                    if (currentPageReplugd == 1)
                    {
                        if (arrUserPostReplug.count > 0)
                        {
                            [arrUserPostReplug removeAllObjects];
                        }
                        [arrUserPostReplug addObjectsFromArray:arrListing];
                    }
                    else
                    {
                        [arrUserPostReplug addObjectsFromArray:arrListing];
                        tblVwData.tableFooterView = nil;
                        [actiVtyIndictor stopAnimating];
                    }
                    
                    for (int i = 0 ;i<arrUserPostReplug.count;i++) {
                        
                        NSDictionary *dict = [arrUserPostReplug objectAtIndex:i];
                        
                        NSString *strImgUrl=[dict valueForKey:@"post_image"];
                        NSURL *mainURL=[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",strMainImgURl,strImgUrl]];
                        NSURL *localURL = [weakSelf URLForGiffWithName:[mainURL lastPathComponent]];
                        if ([[NSFileManager defaultManager] fileExistsAtPath:[localURL path]] && localURL) {
                            //                            [_tempDirectUrls addObject:localURL.path];
                            [[AppWebHandler sharedInstance].dictProgressTask setObject:@(1.0) forKey:[mainURL absoluteString]];
                        }
                    }
                    totalPageReplugd=[[responseDict objectForKey:@"total_pages"] integerValue];
                    totalPostReplugd=[[responseDict objectForKey:@"total_posts"] integerValue];
                    [tblVwData reloadData];
                    isPageRefreshingReplug = NO;
                }
                else
                {
                    if (currentPage == 1)
                    {
                        if (arrUserPost.count > 0)
                        {
                            [arrUserPost removeAllObjects];
                        }
                        [arrUserPost addObjectsFromArray:arrListing];
                    }
                    else
                    {
                        [arrUserPost addObjectsFromArray:arrListing];
                    }
                    
                    for (int i = 0 ;i<arrUserPost.count;i++) {
                        
                        NSDictionary *dict = [arrUserPost objectAtIndex:i];
                        
                        NSString *strImgUrl=[dict valueForKey:@"post_image"];
                        NSURL *mainURL=[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",strMainImgURl,strImgUrl]];
                        NSURL *localURL = [weakSelf URLForGiffWithName:[mainURL lastPathComponent]];
                        if ([[NSFileManager defaultManager] fileExistsAtPath:[localURL path]] && localURL) {
                            [[AppWebHandler sharedInstance].dictProgressTask setObject:@(1.0) forKey:[mainURL absoluteString]];
                        }
                    }
                    totalPage=[[responseDict objectForKey:@"total_pages"] integerValue];
                    totalPost=[[responseDict objectForKey:@"total_posts"] integerValue];
                    [tblVwData reloadData];
                    isPageRefreshingGif = NO;
                }
            }
            else
            {
                lblMsgError.text=@"No Post Found.";
            }
            CGRect visibleRect = (CGRect){.origin = tblVwData.contentOffset, .size = tblVwData.bounds.size};
            CGPoint visiblePoint = CGPointMake(CGRectGetMidX(visibleRect), CGRectGetMidY(visibleRect));
            visibleIndexPath = [tblVwData indexPathForRowAtPoint:visiblePoint];
            if (visibleIndexPath)
            {
                btnSetting.tag=visibleIndexPath.row;
            }
        }
        else
        {
            if (isReplugSelected)
            {
                if (currentPageReplugd>1)
                {
                    currentPageReplugd--;
                }
                isPageRefreshingReplug = NO;
            }else
            {
                if (currentPage>1)
                {
                    currentPage--;
                }
                isPageRefreshingGif = NO;
            }
            [CommonFunction alertTitle:@"" withMessage:[responseDict objectForKey:@"msg"]];
        }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          
                                          if (isReplugSelected)
                                          {
                                              if (currentPageReplugd>1)
                                              {
                                                  currentPageReplugd--;
                                              }
                                              isPageRefreshingReplug = NO;
                                          }else
                                          {
                                              if (currentPage>1)
                                              {
                                                  currentPage--;
                                              }
                                              isPageRefreshingGif = NO;
                                          }
                                          [CommonFunction removeActivityIndicator];
                                          [refreshControl endRefreshing];
                                          
                                          if (error.code == -999) {
                                              return;
                                          }
                                          
                                          if([operation.response statusCode]  == 400 ){
                                              NSLog(@"impo response %@",operation.response);
                                              [CommonFunction alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                          }
                                          else{
                                              
                                              if (wbServiceCount==1) {
                                                  wbServiceCount=0;
                                              }
                                              else
                                              {
                                                  [CommonFunction alertTitle:@"" withMessage:@"Network Error..."];
                                              }
                                              
                                          }
                                          
                                      }];
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)reportActionSheetClickedWithButton:(UIButton *)sender {
    
//    type of the report (inappropriate_content,spam_account,harmful_behavior,other)
    NSString *reportType = nil;
    switch (sender.tag) {
        case 0:
        {
            [self clickOnTapGesture];
        }
            break;
        case 1:
        {
            reportType = @"other";
        }
            break;
        case 2:
        {
            reportType = @"harmful_behavior";
        }
            break;
        case 3:
        {
            reportType = @"spam_account";
        }
            break;
        case 4:
        {
            reportType = @"inappropriate_content";
        }
            break;
            
        default:
            break;
    }
    if (reportType) {
        vwReportSheet.userInteractionEnabled=NO;
        NSDictionary *params = @{@"reported_user_id":strUserId, @"report_type":reportType, @"description":@""};
        [self reportUserWithDictionary:params];
    }
}

- (void)reportUserWithDictionary:(NSDictionary *)params
{
    [CommonFunction showActivityIndicatorWithText:@""];
    
    NSString *url = [NSString stringWithFormat:@"users/reportUser"];
    
    NSMutableDictionary *header =[[NSMutableDictionary alloc]initWithObjectsAndKeys:@"application/json",@"Content-Type",[NSUSERDEFAULTS valueForKey:kUSERTOKEN],@"User-Token", nil];
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    
    __weak typeof(self) weakSelf = self;
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypePost withHttpHeaders:header withServiceName:url withParameters:[params mutableCopy] withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"responce dict %@",responseDict);
        
        vwReportSheet.userInteractionEnabled = YES;
        [CommonFunction removeActivityIndicator];
        if(responseDict==Nil){
            [CommonFunction alertTitle:@"" withMessage:@"Server Error"];
        }
        else if([operation.response statusCode]==200)
        {
            [weakSelf clickOnTapGesture];
            
            // Delay execution of my block for 10 seconds.
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.3 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                popUpImageView.alpha=0.0;
                popUpImageView.layer.cornerRadius = 30*SCREEN_XScale;
                popUpImageView.hidden = NO;
                popUpImageView.transform = CGAffineTransformMakeScale(0.85, 0.85);
                [UIView animateWithDuration:0.25 delay:0 usingSpringWithDamping:0.8 initialSpringVelocity:0.0 options:7<<15 animations:^{
                    popUpImageView.alpha=1.0f;
                    popUpImageView.transform = CGAffineTransformIdentity;
                } completion:^(BOOL finished) {
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 2 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                        popUpImageView.hidden = YES;
                    });
                }];
            });
        
//            [CommonFunction showAlertWithTitle:@"Thank you for letting us know." message:nil onViewController:self withButtonsArray:@[@"Dismiss"] dismissBlock:nil];
        }
        else
        {
            [CommonFunction alertTitle:@"" withMessage:[responseDict objectForKey:@"msg"]];
        }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          vwReportSheet.userInteractionEnabled = YES;
                                          [CommonFunction removeActivityIndicator];
                                          if([operation.response statusCode]  == 400 ){
                                              NSLog(@"impo response %@",operation.response);
                                              [CommonFunction alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                          }
                                          else{
                                              
                                              if (wbServiceCount==1) {
                                                  wbServiceCount=0;
                                              }
                                              else
                                              {
                                                  [CommonFunction alertTitle:@"" withMessage:@"Network Error..."];
                                              }
                                          }
                                      }];
}

- (IBAction)onInfoSliderView:(id)sender {
    
    
    BOOL flag = NO;
    if (isReplugSelected && arrUserPostReplug.count>0)
    {
        flag = YES;
    }
    else if (!isReplugSelected && arrUserPost.count>0)
    {
        flag = YES;
    }
    if (flag)
    {
        CGRect visibleRect = (CGRect){.origin = tblVwData.contentOffset, .size = tblVwData.bounds.size};
        CGPoint visiblePoint = CGPointMake(CGRectGetMidX(visibleRect), CGRectGetMidY(visibleRect));
        visibleIndexPath = [tblVwData indexPathForRowAtPoint:visiblePoint];
        
        btnInfoView.userInteractionEnabled=NO;
        [tblVwData setContentOffset:tblVwData.contentOffset animated:NO];
        SliderMenuVC *smvc=[[SliderMenuVC alloc]init];
        smvc.index = visibleIndexPath.row;
        if (isReplugSelected)
        {
            smvc.dictPostsData=arrUserPostReplug[visibleIndexPath.row];
            smvc.strPostID=arrUserPostReplug[visibleIndexPath.row][@"id"];
            smvc.gifCount = [arrUserPostReplug[visibleIndexPath.row][@"view_count"]integerValue];
        }
        else
        {
            smvc.gifCount = [arrUserPost[visibleIndexPath.row][@"view_count"]integerValue];
            smvc.dictPostsData=arrUserPost[visibleIndexPath.row];
            smvc.strPostID=arrUserPost[visibleIndexPath.row][@"id"];
        }
        smvc.strUsername = dictData[@"username"];
        smvc.view.tag = 1001;
        
        smvc.view.frame=CGRectMake([UIScreen mainScreen].bounds.size.width, 0.0f, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height);
        CGRect basketTopFrame = smvc.view.frame;
        basketTopFrame.origin.x = 0;
        [APPDELEGATE.window.rootViewController.view addSubview:smvc.view];
        [APPDELEGATE.window.rootViewController addChildViewController:smvc];
        
        [UIView animateWithDuration:0.25 delay:0.0 usingSpringWithDamping:1.0 initialSpringVelocity:0.0 options:UIViewAnimationOptionCurveLinear animations:^{
            smvc.view.frame = basketTopFrame;
        } completion:^(BOOL finished) {
            btnInfoView.userInteractionEnabled=YES;
        }];
    }
}


#pragma mark -
#pragma mark Get Post Details
- (void)getGifLatestDetailsWithID:(NSString *)postId
{
    NSString *url = [NSString stringWithFormat:@"posts/%@",postId];
    
    NSMutableDictionary *header =[[NSMutableDictionary alloc]initWithObjectsAndKeys:@"application/json",@"Content-Type",[NSUSERDEFAULTS valueForKey:kUSERTOKEN],@"User-Token", nil];
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypeGet withHttpHeaders:header withServiceName:url withParameters:nil withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"--responce dict %@",responseDict);
        
        if(responseDict==Nil)        {
//            [CommonFunction alertTitle:@"" withMessage:@"Server Error"];
        }
        else if([operation.response statusCode]==200)
        {
            BOOL flag = NO;
            for (NSInteger i=0; i<arrUserPost.count; i++)
            {
                NSMutableDictionary *dict = arrUserPost[i];
                if ([dict[@"id"] isEqualToString:postId])
                {
                    NSInteger count = [responseDict[@"post"][@"comments_count"] integerValue];
                    BOOL isComent = [responseDict[@"post"][@"is_comment"] boolValue];
                    [arrUserPost[i] setObject:[NSNumber numberWithInteger:count] forKey:@"comments_count"];
                    [arrUserPost[i] setObject:[NSNumber numberWithBool:isComent] forKey:@"is_comment"];
                    flag = YES;
                }
                if (flag) {
                    break;
                }
            }
            
            flag = NO;
            for (NSInteger i=0; i<arrUserPostReplug.count; i++)
            {
                NSMutableDictionary *dict = arrUserPostReplug[i];
                if ([dict[@"id"] isEqualToString:postId])
                {
                    NSInteger count = [responseDict[@"post"][@"comments_count"] integerValue];
                    BOOL isComent = [responseDict[@"post"][@"is_comment"] boolValue];
                    [arrUserPostReplug[i] setObject:[NSNumber numberWithInteger:count] forKey:@"comments_count"];
                    [arrUserPostReplug[i] setObject:[NSNumber numberWithBool:isComent] forKey:@"is_comment"];
                    flag = YES;
                }
                if (flag) {
                    break;
                }
            }
            
            
            flag = NO;
            for (NSInteger i=0; i<dataManager.globalUserFeedArray.count; i++)
            {
                NSMutableDictionary *dict = dataManager.globalUserFeedArray[i];
                if ([dict[@"post_id"] isEqualToString:postId])
                {
                    NSInteger count = [responseDict[@"post"][@"comments_count"] integerValue];
                    BOOL isComent = [responseDict[@"post"][@"is_comment"] boolValue];
                    [dataManager.globalUserFeedArray[i] setObject:[NSNumber numberWithInteger:count] forKey:@"comments_count"];
                    [dataManager.globalUserFeedArray[i] setObject:[NSNumber numberWithBool:isComent] forKey:@"is_comment"];
                    flag = YES;
                }
                if (flag) {
                    break;
                }
            }
            
            flag = NO;
            for (NSInteger i=0; i<dataManager.globalUserReplugArray.count; i++)
            {
                NSMutableDictionary *dict = dataManager.globalUserReplugArray[i];
                if ([dict[@"post_id"] isEqualToString:postId])
                {
                    NSInteger count = [responseDict[@"post"][@"comments_count"] integerValue];
                    BOOL isComent = [responseDict[@"post"][@"is_comment"] boolValue];
                    [dataManager.globalUserReplugArray[i] setObject:[NSNumber numberWithInteger:count] forKey:@"comments_count"];
                    [dataManager.globalUserReplugArray[i] setObject:[NSNumber numberWithBool:isComent] forKey:@"is_comment"];
                    flag = YES;
                }
                if (flag) {
                    break;
                }
            }
            
            flag = NO;
            for (NSInteger i=0; i<dataManager.globalHomeFeedArray.count; i++)
            {
                NSMutableDictionary *dict = dataManager.globalHomeFeedArray[i];
                if ([dict[@"post_id"] isEqualToString:postId])
                {
                    NSInteger count = [responseDict[@"post"][@"comments_count"] integerValue];
                    BOOL isComent = [responseDict[@"post"][@"is_comment"] boolValue];
                    [dataManager.globalHomeFeedArray[i] setObject:[NSNumber numberWithInteger:count] forKey:@"comments_count"];
                    [dataManager.globalHomeFeedArray[i] setObject:[NSNumber numberWithBool:isComent] forKey:@"is_comment"];
                    flag = YES;
                }
                if (flag) {
                    break;
                }
            }
            
            
            [tblVwData reloadData];
        }
        else
        {
//            [CommonFunction alertTitle:@"" withMessage:[responseDict objectForKey:@"msg"]];
        }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          if([operation.response statusCode]  == 400 ){
                                              NSLog(@"impo response %@",operation.response);
                                              
//                                              [CommonFunction alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                          }
                                          else{
                                              
                                              if (wbServiceCount==1) {
                                                  wbServiceCount=0;
                                              }
                                              else
                                              {
//                                                  [CommonFunction alertTitle:@"" withMessage:@"Network Error..."];
                                              }
                                          }
                                      }];
}


#pragma mark -
#pragma mark Handle UserName Tap
- (void)customTableViewCellDidTapOnUserHandle:(NSString *)userHandle
{
    NSString *loggedInUser = [NSUSERDEFAULTS objectForKey:kUSERID];
    if ([loggedInUser isEqualToString:userHandle])
    {
        
    }
    else
    {
        OtherUserProfileVC *ouVC=[[OtherUserProfileVC alloc]init];
        ouVC.hidesBottomBarWhenPushed=YES;
        ouVC.strUserName=userHandle;
        [self.navigationController pushViewController:ouVC animated:YES];
    }
}

#pragma mark -
#pragma mark Handle HashTag Tap
- (void)customTableViewCellDidTapHashTagHandle:(NSString *)hashtagHandle
{
    NSString *categoryName = [NSString stringWithFormat:@"#%@",hashtagHandle];
    NSString *categoryTag = hashtagHandle;
    CategoryDetailVC *categorydetailVC = [[CategoryDetailVC alloc]initWithNibName:NSStringFromClass([CategoryDetailVC class]) bundle:nil];
    categorydetailVC.hidesBottomBarWhenPushed=YES;
    categorydetailVC.categoryTag = categoryTag;
    categorydetailVC.categoryName = categoryName;
    [self.navigationController pushViewController:categorydetailVC animated:YES];
}

#pragma mark -
#pragma mark Save Gif
- (void)saveGIFToGallery:(NSNumber *)indexNumber
{
    UIAlertController *controller = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *actionCancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
    
    UIAlertAction *action = [UIAlertAction actionWithTitle:@"Save GIF" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        NSString *imgGifUrl=isReplugSelected?arrUserPostReplug[[indexNumber integerValue]][@"post_image"]:arrUserPost[[indexNumber integerValue]][@"post_image"];
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
            NSURL *localURL = [self URLForGiffWithName:[imgGifUrl lastPathComponent]];
            
            if ([[NSFileManager defaultManager] fileExistsAtPath:[localURL path]]) {
                
                NSData *animatedImageData = [[NSFileManager defaultManager] contentsAtPath:localURL.path];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    
                    if ([[[UIDevice currentDevice]systemVersion]floatValue]<9)
                    {
                        ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
                        [library writeImageDataToSavedPhotosAlbum:animatedImageData metadata:nil completionBlock:^(NSURL *assetURL, NSError *error) {
                            dispatch_async(dispatch_get_main_queue(), ^{
                                [CommonFunction removeActivityIndicator];
                                if (!error)
                                {
                                    [CommonFunction showAlertWithTitle:@"Gif Saved" message:@"Gif has been saved to Gallery" onViewController:self useAsDelegate:NO dismissBlock:nil];
                                }
                                else
                                {
                                    [CommonFunction showAlertWithTitle:@"Error" message:error.localizedDescription onViewController:self useAsDelegate:NO dismissBlock:nil];
                                }
                            });
                        }];
                    }
                    else
                    {
                        [[PHPhotoLibrary sharedPhotoLibrary] performChanges:^{
                            [[PHAssetCreationRequest creationRequestForAsset]addResourceWithType:PHAssetResourceTypePhoto data:animatedImageData options:nil];
                        } completionHandler:^(BOOL success, NSError *error) {
                            
                            dispatch_async(dispatch_get_main_queue(), ^{
                                [CommonFunction removeActivityIndicator];
                                if (success) {
                                    [CommonFunction showAlertWithTitle:@"Gif Saved" message:@"Your Gif has been saved to Gallery" onViewController:self useAsDelegate:NO dismissBlock:nil];
                                }
                                else {
                                    [CommonFunction showAlertWithTitle:@"Error" message:error.localizedDescription onViewController:self useAsDelegate:NO dismissBlock:nil];
                                }
                            });
                        }];
                    }
                });
            }
        });
    }];
    
    [controller addAction:actionCancel];
    [controller addAction:action];
    
    [self presentViewController:controller animated:YES completion:nil];
}

- (void)onSaveGifAction:(UILongPressGestureRecognizer *)likeTap
{
    if (likeTap.state == UIGestureRecognizerStateBegan)
    {
        if ([PHPhotoLibrary authorizationStatus] == PHAuthorizationStatusDenied || [PHPhotoLibrary authorizationStatus] == PHAuthorizationStatusRestricted)
        {
            //            [self showPhotoAccessAlert];
            PhotoPickerController *photoPicker = [[PhotoPickerController alloc]initWithNibName:NSStringFromClass([PhotoPickerController class]) bundle:nil];
            [self.navigationController presentViewController:photoPicker animated:YES completion:nil];
        }
        else
        {
            NSInteger index = likeTap.view.tag;
            
            [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status) {
                switch (status) {
                    case PHAuthorizationStatusAuthorized:
                    {
                        [self performSelectorOnMainThread:@selector(saveGIFToGallery:) withObject:[NSNumber numberWithInteger:index] waitUntilDone:NO];
                    }
                        break;
                        
                    default:
                        break;
                }
            }];
            
        }
    }
}

@end


