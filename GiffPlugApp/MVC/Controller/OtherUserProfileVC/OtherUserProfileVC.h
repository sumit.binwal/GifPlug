//
//  OtherUserProfileVC.h
//  GiffPlugApp
//
//  Created by Sumit Sharma on 22/02/16.
//  Copyright © 2016 Kshitij Godara. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OtherUserProfileVC : UIViewController<NSURLSessionDelegate,NSURLSessionDownloadDelegate>
@property (nonatomic,strong)NSString *strUserName;
@property (nonatomic, strong) NSURLSession *session;
@property (strong, nonatomic) NSMutableDictionary *progressBuffer;
@property (strong,nonatomic) NSMutableArray *tempDirectUrls;
@property (strong,nonatomic) NSMutableDictionary *imageBuffer;
@property (strong,nonatomic)NSOperationQueue *imageLoadingOperationQueue;
@end
