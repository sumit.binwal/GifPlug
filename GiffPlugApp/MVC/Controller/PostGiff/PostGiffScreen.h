//
//  PostGiffScreen.h
//  GiffPlugApp
//
//  Created by Kshitij Godara on 05/02/16.
//  Copyright © 2016 Kshitij Godara. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PostGiffScreen : UIViewController<UITextViewDelegate>

- (IBAction)postBtnPressed:(id)sender;
- (IBAction)shareGiffActions:(id)sender;

//@property (weak, nonatomic) IBOutlet UITextView *captionTextView;
//@property (weak, nonatomic) IBOutlet NSLayoutConstraint *textViewHeightCons;
//@property (weak, nonatomic) IBOutlet NSLayoutConstraint *textVBtmSpaceCons;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *postBtnBtmCons;
- (IBAction)backBtnPressed:(id)sender;
@property (weak, nonatomic) IBOutlet UIImageView *giffImageView;
@property (weak, nonatomic) IBOutlet UIButton *postBtn;
@property (weak, nonatomic) IBOutlet UIView *shareBtnContainer;

@property (strong,nonatomic)UILabel *placeHolderLabel;

@property (nonatomic) BOOL isGifFromGallery;


@property (strong,nonatomic)NSMutableArray *giffImagePartsArray;
@property (assign,nonatomic)float animationDuration;

@end


@interface SampleLayer : CAShapeLayer



@end
