//
//  HomeFeedVC.m
//  GiffPlugApp
//
//  Created by Kshitij Godara on 08/01/16.
//  Copyright © 2016 Kshitij Godara. All rights reserved.
//

#import "HomeFeedVC.h"
#import "AppDataManager.h"
#import "HomePostedGifCustomeCell.h"
#import "OtherUserProfileVC.h"
#import "CommentVC.h"
#import "SliderMenuVC.h"
#import "AFURLSessionManager.h"
#import "UIImage+Resize.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import "PhotoPickerController.h"
#import "CategoryDetailVC.h"

#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKShareKit/FBSDKShareKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <Social/Social.h>
#import "CEMovieMaker.h"
#import "PHAsset+Utility.h"


@interface HomeFeedVC ()<UIGestureRecognizerDelegate,UITableViewDelegate,UITableViewDataSource,UpdateComments,UserHandleDelegate>
{
    IBOutlet UIView *vwNavigation;
    IBOutlet UILabel *lblNameUserName;
    IBOutlet UIButton *btnProfileImg;
    NSInteger currentPage, totalPage,totalPost;
    BOOL isBlockListDataFetching;
    BOOL isSearching;
    IBOutlet UIButton *infBtn;
    IBOutlet UILabel *lblError;
    NSString *strBaseUrl;
    NSMutableArray *arrComments;
    NSMutableArray *arrLikes;
    IBOutlet UIView *vwFooterVw;
    
    IBOutlet UIImageView *imgFooterCircle;
    
    UIViewController *currentViewController;
    UIView *SliderVw;
    UIRefreshControl *refreshControl;
    
    NSIndexPath *visibleIndexPath, *prevIndexPath;
    
    AppDataManager *appDataManager;
    
    BOOL isPageRefreshing;
    
    NSTimer *timerGifCount;
}

@property (strong, nonatomic) IBOutlet UIView *uploadProgressView;
@property (weak, nonatomic) IBOutlet UIImageView *upLoadImageView;
@property (weak, nonatomic) IBOutlet UITableView *tableHomeFeed;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *progressBarHeightConstraint;
@property (weak, nonatomic) IBOutlet UIProgressView *uploadProgressBar;
@property (weak, nonatomic) IBOutlet UILabel *labelPercentage;

- (void)uploadGifToServer:(NSNotification *)notification;
@end

@implementation HomeFeedVC
@synthesize infBtn;
#pragma mark - View Life Cycle

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"uploadgif" object:nil];
    NSLog(@"dealloced called for HOME FEED VC...................");
    //    self.uploadProgressView=nil;
    //    infBtn = nil;
    //    [arrFeedData removeAllObjects],arrFeedData = nil;
    //    [self.progressBuffer removeAllObjects],self.progressBuffer = nil;
    //    self.session = nil;
    //    [self.tempDirectUrls removeAllObjects],self.tempDirectUrls = nil;
    //    [self.imageBuffer removeAllObjects],self.imageBuffer = nil;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[AppWebHandler sharedInstance]addDelegate:self];
    appDataManager = [AppDataManager sharedInstance];
    
    
    //Kshitij Work
    
    
    isPageRefreshing = YES;
    //    _tempDirectUrls = [[NSMutableArray alloc] init];
    
    // Initialize Session
    //    [self setSession:[self backgroundSession]];
    
    // Initialize Progress Buffer
    //    [self setProgressBuffer:[NSMutableDictionary dictionary]];
    //    [self setImageBuffer:[NSMutableDictionary dictionary]];
    
    //------------------------------------------------------
    
    
    [self.tableHomeFeed registerNib:[UINib nibWithNibName:NSStringFromClass([HomePostedGifCustomeCell class]) bundle:nil] forCellReuseIdentifier:@"homeFeedCell"];
    
    
    //    arrFeedData=[[NSMutableArray alloc]init];
    
    CGFloat fontSizeUsername = lblNameUserName.font.pointSize;
    
    [lblNameUserName setFont:[UIFont fontWithName:lblNameUserName.font.fontName size:fontSizeUsername*SCREEN_XScale]];
    
    btnProfileImg.layer.cornerRadius=btnProfileImg.frame.size.width*SCREEN_XScale/2;
    btnProfileImg.clipsToBounds=YES;
    
    
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"uploadgif" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(uploadGifToServer:) name:@"uploadgif" object:nil];
    
    self.edgesForExtendedLayout = UIRectEdgeNone;
    
    refreshControl = [[UIRefreshControl alloc] initWithFrame:CGRectMake(0, 150, 320.0f, 30.0f)];
    refreshControl.backgroundColor=[UIColor colorWithRed:33.0f/255.0f green:31.0f/255.0f blue:31.0f/255.0f alpha:1];
    refreshControl.tintColor=[UIColor whiteColor];
    
    //    NSAttributedString *title = [[NSAttributedString alloc] initWithString:@"Please Wait"
    //                                                                attributes: @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    //    refreshControl.attributedTitle = [[NSAttributedString alloc]initWithAttributedString:title];
    
    
    [refreshControl addTarget:self action:@selector(refresh:) forControlEvents:UIControlEventValueChanged];
    [self.tableHomeFeed setAutoresizingMask:(UIViewAutoresizingFlexibleRightMargin|UIViewAutoresizingFlexibleLeftMargin)];
    
    [self.tableHomeFeed addSubview:refreshControl];
    
    
    UISwipeGestureRecognizer *swipeGesture=[[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swipeGestureClicked)];
    swipeGesture.direction=UISwipeGestureRecognizerDirectionLeft;
    swipeGesture.delegate=self;
    [self.view addGestureRecognizer:swipeGesture];
    
    
    UITapGestureRecognizer *nameTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(usernameClicked:)];
    nameTap.numberOfTapsRequired=1;
    lblNameUserName.userInteractionEnabled = YES;
    [lblNameUserName addGestureRecognizer:nameTap];
    lblNameUserName.tag = 0;
    
    
    currentPage = 1;
    totalPage = 0;
    if ([CommonFunction reachabiltyCheck]) {
        [CommonFunction showActivityIndicatorWithText:@""];
        [self getPostedFeed];
    }
    else
    {
        [CommonFunction alertTitle:@"" withMessage:@"Please check your network connection."];
        [refreshControl endRefreshing];
    }
    //    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(onLogoutFunction) name:@"logout" object:nil];
    
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = YES;
        self.navigationController.interactivePopGestureRecognizer.delegate = self;
    }
}



//- (void)onLogoutFunction
//{
////    [[self session] invalidateAndCancel];
////    self.session = nil;
//}


-(void)swipeGestureClicked
{
    [self infoBtnClicked:nil];
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldBeRequiredToFailByGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    return NO;
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    NSLog(@"String Test : %@",_strTest);
    infBtn.tag=0;
    
    [self.tableHomeFeed reloadData];
    
    if (appDataManager.globalHomeFeedArray.count == 0)
    {
        lblError.text=@"No Feeds Found.";
    }
    
    
    if (appDataManager.globalHomeFeedArray.count>0) {
        
        CGRect visibleRect = (CGRect){.origin = self.tableHomeFeed.contentOffset, .size = self.tableHomeFeed.bounds.size};
        CGPoint visiblePoint = CGPointMake(CGRectGetMidX(visibleRect), CGRectGetMidY(visibleRect));
        visibleIndexPath = [self.tableHomeFeed indexPathForRowAtPoint:visiblePoint];
        
        NSURL *imgUrl=[NSURL URLWithString:appDataManager.globalHomeFeedArray[visibleIndexPath.row][@"avatar_image"]];
        
        //        if ([imgUrl.absoluteString.pathExtension isEqualToString:@"gif"])
        //        {
        //            btnProfileImg.contentMode = UIViewContentModeScaleAspectFill;
        //        }
        //        else
        //        {
        //            btnProfileImg.contentMode = UIViewContentModeScaleToFill;
        //        }
        
        [btnProfileImg sd_setImageWithURL:imgUrl forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"default.jpg"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            if (error)
            {
                btnProfileImg.contentMode=UIViewContentModeScaleToFill;
                btnProfileImg.imageView.contentMode=UIViewContentModeScaleToFill;
            }
            else
            {
                if ([[imgUrl.absoluteString pathExtension]isEqualToString:@"gif"])
                {
                    btnProfileImg.contentMode=UIViewContentModeScaleAspectFill;
                    btnProfileImg.imageView.contentMode=UIViewContentModeScaleAspectFill;
                }
                else
                {
                    btnProfileImg.contentMode=UIViewContentModeScaleToFill;
                    btnProfileImg.imageView.contentMode=UIViewContentModeScaleToFill;
                }
            }
        }];
        
        btnProfileImg.tag=visibleIndexPath.row;
        lblNameUserName.text=[[appDataManager.globalHomeFeedArray objectAtIndex:visibleIndexPath.row]objectForKey:@"username"];
        lblNameUserName.tag = visibleIndexPath.row;
        
        infBtn.tag=visibleIndexPath.row;
        infBtn.hidden = NO;
    }
    else
    {
        lblNameUserName.tag = -1;
        lblNameUserName.text = @"";
        [btnProfileImg setImage:nil forState:UIControlStateNormal];
        infBtn.hidden = YES;
    }
    
    [[UIApplication sharedApplication]setStatusBarHidden:NO withAnimation:UIStatusBarAnimationFade];
    [[UIApplication sharedApplication]setStatusBarStyle:UIStatusBarStyleLightContent];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [self.navigationController.navigationBar setHidden:YES];
    [timerGifCount invalidate],timerGifCount=nil;
    prevIndexPath = visibleIndexPath = nil;
    [super viewWillDisappear:animated];
}

- (void)refresh:(UIRefreshControl *)refreshControls
{
    currentPage=1;
    totalPage=0;
    if ([CommonFunction reachabiltyCheck]) {
        [CommonFunction showActivityIndicatorWithText:@""];
        [self getPostedFeed];
    }
    else
    {
        [CommonFunction alertTitle:@"" withMessage:@"Please check your network connection."];
        [refreshControl endRefreshing];
    }
    
}

#pragma mark - IBACtion Button Clicked




- (IBAction)infoBtnClicked:(UIButton *)sender {
    if (appDataManager.globalHomeFeedArray.count>0) {
        infBtn.userInteractionEnabled=NO;
        [self.tableHomeFeed setContentOffset:self.tableHomeFeed.contentOffset animated:NO];
        SliderMenuVC *smvc=[[SliderMenuVC alloc]init];
        smvc.gifCount = [appDataManager.globalHomeFeedArray[visibleIndexPath.row][@"view_count"]integerValue];
        smvc.index = visibleIndexPath.row;
        smvc.dictPostsData=appDataManager.globalHomeFeedArray[visibleIndexPath.row];
        smvc.strPostID=appDataManager.globalHomeFeedArray[visibleIndexPath.row][@"post_id"];
        smvc.view.tag = 1001;
        
        NSLog(@"%@",appDataManager.globalHomeFeedArray[visibleIndexPath.row]);
        smvc.view.frame=CGRectMake([UIScreen mainScreen].bounds.size.width, 0.0f, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height);
        CGRect basketTopFrame = smvc.view.frame;
        basketTopFrame.origin.x = 0;
        //    basketTopFrame.origin.y=20;
        [APPDELEGATE.window.rootViewController.view addSubview:smvc.view];
        [APPDELEGATE.window.rootViewController addChildViewController:smvc];
        
        [UIView animateWithDuration:0.25 delay:0.0 usingSpringWithDamping:1.0 initialSpringVelocity:0.0 options:UIViewAnimationOptionCurveLinear animations:^{
            smvc.view.frame = basketTopFrame;
        } completion:^(BOOL finished) {
            infBtn.userInteractionEnabled=YES;
        }];
        //        [UIView animateWithDuration:0.3 delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{ smvc.view.frame = basketTopFrame; } completion:^(BOOL finished){
        //            infBtn.userInteractionEnabled=YES;
        //
        //        }];
        
    }
}

- (void)transitionFromViewController:(UIViewController *)fromViewController toViewController:(UIViewController *)toViewController duration:(NSTimeInterval)duration animations:(void (^)(void))animations completion:(void (^)(BOOL))completion
{
    currentViewController = toViewController;
    // Put any auto- and manual-resizing handling code here
    
    [UIView animateWithDuration:duration animations:animations completion:completion];
    
    [fromViewController.view removeFromSuperview];
}


#pragma mark -
#pragma mark Save Gif
- (void)saveGIFToGallery:(NSNumber *)indexNumber
{
    UIAlertController *controller = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *actionCancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
    
    UIAlertAction *action = [UIAlertAction actionWithTitle:@"Save GIF" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        NSString *imgGifUrl=appDataManager.globalHomeFeedArray[[indexNumber integerValue]][@"post_image"];
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
            NSURL *localURL = [self URLForGiffWithName:[imgGifUrl lastPathComponent]];
            
            if ([[NSFileManager defaultManager] fileExistsAtPath:[localURL path]]) {
                
                NSData *animatedImageData = [[NSFileManager defaultManager] contentsAtPath:localURL.path];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    
                    if ([[[UIDevice currentDevice]systemVersion]floatValue]<9)
                    {
                        ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
                        [library writeImageDataToSavedPhotosAlbum:animatedImageData metadata:nil completionBlock:^(NSURL *assetURL, NSError *error) {
                            dispatch_async(dispatch_get_main_queue(), ^{
                                [CommonFunction removeActivityIndicator];
                                if (!error)
                                {
                                    [CommonFunction showAlertWithTitle:@"Gif Saved" message:@"Gif has been saved to Gallery" onViewController:self useAsDelegate:NO dismissBlock:nil];
                                }
                                else
                                {
                                    [CommonFunction showAlertWithTitle:@"Error" message:error.localizedDescription onViewController:self useAsDelegate:NO dismissBlock:nil];
                                }
                            });
                        }];
                    }
                    else
                    {
                        [[PHPhotoLibrary sharedPhotoLibrary] performChanges:^{
                            [[PHAssetCreationRequest creationRequestForAsset]addResourceWithType:PHAssetResourceTypePhoto data:animatedImageData options:nil];
                        } completionHandler:^(BOOL success, NSError *error) {
                            
                            dispatch_async(dispatch_get_main_queue(), ^{
                                [CommonFunction removeActivityIndicator];
                                if (success) {
                                    [CommonFunction showAlertWithTitle:@"Gif Saved" message:@"Your Gif has been saved to Gallery" onViewController:self useAsDelegate:NO dismissBlock:nil];
                                }
                                else {
                                    [CommonFunction showAlertWithTitle:@"Error" message:error.localizedDescription onViewController:self useAsDelegate:NO dismissBlock:nil];
                                }
                            });
                        }];
                    }
                });
            }
        });
    }];
    
    [controller addAction:actionCancel];
    [controller addAction:action];
    
    [self presentViewController:controller animated:YES completion:nil];
}

- (void)onSaveGifAction:(UILongPressGestureRecognizer *)likeTap
{
    if (likeTap.state == UIGestureRecognizerStateBegan)
    {
        if ([PHPhotoLibrary authorizationStatus] == PHAuthorizationStatusDenied || [PHPhotoLibrary authorizationStatus] == PHAuthorizationStatusRestricted)
        {
            //            [self showPhotoAccessAlert];
            PhotoPickerController *photoPicker = [[PhotoPickerController alloc]initWithNibName:NSStringFromClass([PhotoPickerController class]) bundle:nil];
            [self.navigationController presentViewController:photoPicker animated:YES completion:nil];
        }
        else
        {
            NSInteger index = likeTap.view.tag;
            
            [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status) {
                switch (status) {
                    case PHAuthorizationStatusAuthorized:
                    {
                        [self performSelectorOnMainThread:@selector(saveGIFToGallery:) withObject:[NSNumber numberWithInteger:index] waitUntilDone:NO];
                    }
                        break;
                        
                    default:
                        break;
                }
            }];
            
        }
    }
}

- (void)onDoubleTapLikeAction:(UITapGestureRecognizer *)likeTap
{
    NSInteger index = likeTap.view.tag;
    
    HomePostedGifCustomeCell *cellView = (HomePostedGifCustomeCell*) [self.tableHomeFeed cellForRowAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0]];
    
    if (cellView.isHeartAnimationActive)
    {
        return;
    }
    
    NSNumber *isLike=appDataManager.globalHomeFeedArray[index][@"is_like"];
    NSLog(@"%@",isLike);
    [cellView performHeartAnimation];
    
    if (![isLike boolValue])
    {
        NSInteger likeCount = [appDataManager.globalHomeFeedArray[index][@"likes_count"] integerValue];
        
        if (cellView.imgLikeUnlikeImg.tag==0) {
            
            [cellView.imgLikeUnlikeImg setImage:[UIImage imageNamed:@"PG_LikeBtn"]];
            cellView.imgLikeUnlikeImg.tag=1;
            
            [CommonFunction showActivityIndicatorWithText:nil];
            likeCount += 1;
            
            cellView.lblLikeCount.text=[NSString stringWithFormat:@"%ld",(long)likeCount];
            
            [appDataManager.globalHomeFeedArray[index] setValue:[NSNumber numberWithInteger:likeCount] forKey:@"likes_count"];
            [appDataManager.globalHomeFeedArray[index] setValue:@1 forKey:@"is_like"];
            
            NSString *postID = appDataManager.globalHomeFeedArray[index][@"post_id"];
            
            for (NSInteger i=0; i<appDataManager.globalUserFeedArray.count; i++)
            {
                NSString *userPostID = appDataManager.globalUserFeedArray[i][@"post_id"];
                if ([userPostID isEqualToString:postID])
                {
                    [appDataManager.globalUserFeedArray[i] setValue:[NSNumber numberWithInteger:likeCount] forKey:@"likes_count"];
                    [appDataManager.globalUserFeedArray[i] setValue:@1 forKey:@"is_like"];
                }
            }
            for (NSInteger i=0; i<appDataManager.globalUserReplugArray.count; i++)
            {
                NSString *userPostID = appDataManager.globalUserReplugArray[i][@"post_id"];
                if ([userPostID isEqualToString:postID])
                {
                    [appDataManager.globalUserReplugArray[i] setValue:[NSNumber numberWithInteger:likeCount] forKey:@"likes_count"];
                    [appDataManager.globalUserReplugArray[i] setValue:@1 forKey:@"is_like"];
                }
            }
            [self likeAPost:postID cell:cellView indexpath:index];
        }
    }
}

//Posted Gif Cell IbAction Methods
-(IBAction)likeBtnClicked:(UIButton *)sender
{
    if (!isInternetAvailabel) {
        return;
    }
    NSLog(@"%ld",(long)sender.tag);
    HomePostedGifCustomeCell *cellView = (HomePostedGifCustomeCell*) [self.tableHomeFeed cellForRowAtIndexPath:[NSIndexPath indexPathForRow:sender.tag inSection:0]];
    
    NSInteger likeCount = [appDataManager.globalHomeFeedArray[sender.tag][@"likes_count"] integerValue];
    
    if (cellView.imgLikeUnlikeImg.tag==0) {
        
        [cellView.imgLikeUnlikeImg setImage:[UIImage imageNamed:@"PG_LikeBtn"]];
        cellView.imgLikeUnlikeImg.tag=1;
        
        [CommonFunction showActivityIndicatorWithText:nil];
        likeCount += 1;
        
        cellView.lblLikeCount.text=[NSString stringWithFormat:@"%ld",(long)likeCount];
        
        [appDataManager.globalHomeFeedArray[sender.tag] setValue:[NSNumber numberWithInteger:likeCount] forKey:@"likes_count"];
        [appDataManager.globalHomeFeedArray[sender.tag] setValue:@1 forKey:@"is_like"];
        
        NSString *postID = appDataManager.globalHomeFeedArray[sender.tag][@"post_id"];
        
        
        for (NSInteger i=0; i<appDataManager.globalUserFeedArray.count; i++)
        {
            NSString *userPostID = appDataManager.globalUserFeedArray[i][@"post_id"];
            if ([userPostID isEqualToString:postID])
            {
                [appDataManager.globalUserFeedArray[i] setValue:[NSNumber numberWithInteger:likeCount] forKey:@"likes_count"];
                [appDataManager.globalUserFeedArray[i] setValue:@1 forKey:@"is_like"];
            }
        }
        for (NSInteger i=0; i<appDataManager.globalUserReplugArray.count; i++)
        {
            NSString *userPostID = appDataManager.globalUserReplugArray[i][@"post_id"];
            if ([userPostID isEqualToString:postID])
            {
                [appDataManager.globalUserReplugArray[i] setValue:[NSNumber numberWithInteger:likeCount] forKey:@"likes_count"];
                [appDataManager.globalUserReplugArray[i] setValue:@1 forKey:@"is_like"];
            }
        }
        
        [self likeAPost:postID cell:cellView indexpath:sender.tag];
    }
    else
    {
        [CommonFunction showActivityIndicatorWithText:nil];
        
        [cellView.imgLikeUnlikeImg setImage:[UIImage imageNamed:@"HF_likeUnfillIcon"]];
        cellView.imgLikeUnlikeImg.tag=0;
        
        likeCount -= 1;
        cellView.lblLikeCount.text=[NSString stringWithFormat:@"%ld",(long)likeCount];
        [appDataManager.globalHomeFeedArray[sender.tag] setValue:[NSNumber numberWithInteger:likeCount] forKey:@"likes_count"];
        [appDataManager.globalHomeFeedArray[sender.tag] setValue:@0 forKey:@"is_like"];
        
        
        NSString *postID = appDataManager.globalHomeFeedArray[sender.tag][@"post_id"];
        
        
        for (NSInteger i=0; i<appDataManager.globalUserFeedArray.count; i++)
        {
            NSString *userPostID = appDataManager.globalUserFeedArray[i][@"post_id"];
            if ([userPostID isEqualToString:postID])
            {
                [appDataManager.globalUserFeedArray[i] setValue:[NSNumber numberWithInteger:likeCount] forKey:@"likes_count"];
                [appDataManager.globalUserFeedArray[i] setValue:@0 forKey:@"is_like"];
            }
        }
        for (NSInteger i=0; i<appDataManager.globalUserReplugArray.count; i++)
        {
            NSString *userPostID = appDataManager.globalUserReplugArray[i][@"post_id"];
            if ([userPostID isEqualToString:postID])
            {
                [appDataManager.globalUserReplugArray[i] setValue:[NSNumber numberWithInteger:likeCount] forKey:@"likes_count"];
                [appDataManager.globalUserReplugArray[i] setValue:@0 forKey:@"is_like"];
            }
        }
        
        [self unLikeAPost:postID cell:cellView indexpath:sender.tag];
    }
}
-(IBAction)commentBtnClickedOnCell:(UIButton *)sender
{
    CommentVC *lvc=[[CommentVC alloc]init];
    lvc.commentDelegate=self;
    lvc.hidesBottomBarWhenPushed = YES;
    lvc.index = sender.tag;
    lvc.strUsername = appDataManager.globalHomeFeedArray[sender.tag][@"username"];
    lvc.strPostId=appDataManager.globalHomeFeedArray[sender.tag][@"post_id"];
    lvc.strLikeCount=[NSString stringWithFormat:@"%@",appDataManager.globalHomeFeedArray[sender.tag][@"comments_count"]];
    lvc.hidesBottomBarWhenPushed=YES;
    [self.navigationController pushViewController:lvc animated:YES];
}
-(IBAction)rePlugBtnClicked:(UIButton *)sender
{
    if (!isInternetAvailabel) {
        return;
    }
    NSInteger p=sender.tag;
    int x=(int)p;
    NSIndexPath *path = [NSIndexPath indexPathForRow:x inSection:0];
    HomePostedGifCustomeCell *cell = [self.tableHomeFeed cellForRowAtIndexPath:path];
    __weak typeof(cell) weakCell =cell;
    [UIView animateWithDuration:1.0
                     animations:^
     {
         weakCell.imgReplug.transform = CGAffineTransformMakeRotation(M_PI);
         weakCell.imgReplug.transform = CGAffineTransformMakeRotation(0);
     }];
    
    NSInteger replugCount = [appDataManager.globalHomeFeedArray[sender.tag][@"replugged_count"] integerValue];
    replugCount += 1;
    cell.lblReplugCount.text=[NSString stringWithFormat:@"%ld",(long)replugCount];
    [appDataManager.globalHomeFeedArray[sender.tag] setValue:[NSNumber numberWithInteger:replugCount] forKey:@"replugged_count"];
    [appDataManager.globalHomeFeedArray[sender.tag] setValue:@1 forKey:@"is_replug"];
    
    [CommonFunction showActivityIndicatorWithText:nil];
    [self replugAPost:[NSString stringWithFormat:@"%@",appDataManager.globalHomeFeedArray[sender.tag][@"post_id"]]labelName:cell.lblReplugCount indexpath:sender.tag ];
    
}
-(IBAction)menuBtnClicked:(UIButton *)sender
{
//    NSString *string = @"sd";
    NSURL *URL = [NSURL URLWithString:appDataManager.globalHomeFeedArray[sender.tag][@"watermark_image"]];
    //    [sender setImage:[UIImage imageNamed:@"PG_menu_Red"] forState:UIControlStateNormal];
    
    //    [moreButton setImage:[UIImage imageNamed:@"PG_menu"] forState:UIControlStateNormal];
    
    //    UIImage *img=[UIImage imageNamed:@"GCS_bgImg"];
    
    UIActivityViewController *activityViewController =
    [[UIActivityViewController alloc] initWithActivityItems:@[URL]
                                      applicationActivities:nil];
    
    NSArray *excludedActivities = @[
                                    UIActivityTypeAirDrop,
                                    UIActivityTypePostToWeibo,
                                    UIActivityTypePrint,
                                    UIActivityTypeAssignToContact,
                                    ];
    activityViewController.excludedActivityTypes = excludedActivities;
    
    // Present the controller
    [self presentViewController:activityViewController animated:YES completion:nil];
    
}


- (IBAction)profileImgBtnClicked:(UIButton *)sender {
    
    if (appDataManager.globalHomeFeedArray.count>0) {
        if ([[appDataManager.globalHomeFeedArray[sender.tag][@"is_my_profile"]stringValue]isEqualToString:@"0"])
        {
            OtherUserProfileVC *ouVC=[[OtherUserProfileVC alloc]init];
            ouVC.hidesBottomBarWhenPushed=YES;
            ouVC.strUserName=appDataManager.globalHomeFeedArray[sender.tag][@"username"];
            [self.navigationController pushViewController:ouVC animated:YES];
        }
    }
}

- (void)usernameClicked:(UITapGestureRecognizer *)sender {
    
    if (sender.state == UIGestureRecognizerStateEnded)
    {
        if (appDataManager.globalHomeFeedArray.count>0) {
            if ([[appDataManager.globalHomeFeedArray[sender.view.tag][@"is_my_profile"]stringValue]isEqualToString:@"0"])
            {
                OtherUserProfileVC *ouVC=[[OtherUserProfileVC alloc]init];
                ouVC.hidesBottomBarWhenPushed=YES;
                ouVC.strUserName=appDataManager.globalHomeFeedArray[sender.view.tag][@"username"];
                [self.navigationController pushViewController:ouVC animated:YES];
            }
        }
    }
}


- (void) stopAnimatingFooter{
    //add the data
    
    if (totalPage <= currentPage)
    {
        
        if (isBlockListDataFetching == NO)
        {
            //hide the header
            self.tableHomeFeed.tableFooterView = nil;
            return;
        }
    }
    else
    {
        
        
        if (isBlockListDataFetching == NO)
        {
            
            currentPage++;
            
            [self performSelectorOnMainThread:@selector(getPostedFeed) withObject:nil waitUntilDone:YES];
        }
    }
}

#pragma mark - UIScrollViewDelegate Method

-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (appDataManager.globalHomeFeedArray.count>0) {
        
        CGRect visibleRect = (CGRect){.origin = self.tableHomeFeed.contentOffset, .size = self.tableHomeFeed.bounds.size};
        CGPoint visiblePoint = CGPointMake(CGRectGetMidX(visibleRect), CGRectGetMidY(visibleRect));
        visibleIndexPath = [self.tableHomeFeed indexPathForRowAtPoint:visiblePoint];
        
        if (!prevIndexPath)
        {
            prevIndexPath = visibleIndexPath;
            timerGifCount = [NSTimer scheduledTimerWithTimeInterval:2.0f target:self selector:@selector(fireGifCountApi) userInfo:nil repeats:NO];
        }
        
        NSURL *imgUrl=[NSURL URLWithString:appDataManager.globalHomeFeedArray[visibleIndexPath.row][@"avatar_image"]];
        
        [btnProfileImg sd_setImageWithURL:imgUrl forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"default.jpg"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            if (error)
            {
                btnProfileImg.contentMode=UIViewContentModeScaleToFill;
                btnProfileImg.imageView.contentMode=UIViewContentModeScaleToFill;
            }
            else
            {
                if ([[imgUrl.absoluteString pathExtension]isEqualToString:@"gif"])
                {
                    btnProfileImg.contentMode=UIViewContentModeScaleAspectFill;
                    btnProfileImg.imageView.contentMode=UIViewContentModeScaleAspectFill;
                }
                else
                {
                    btnProfileImg.contentMode=UIViewContentModeScaleToFill;
                    btnProfileImg.imageView.contentMode=UIViewContentModeScaleToFill;
                }
            }
        }];
        
        btnProfileImg.tag=visibleIndexPath.row;
        lblNameUserName.text=[[appDataManager.globalHomeFeedArray objectAtIndex:visibleIndexPath.row]objectForKey:@"username"];
        lblNameUserName.tag = visibleIndexPath.row;
        
        infBtn.tag=visibleIndexPath.row;
        infBtn.hidden = NO;
        
        // For Gif Count......................................................
        if (prevIndexPath != visibleIndexPath)
        {
            if (timerGifCount.isValid || timerGifCount)
            {
                [timerGifCount invalidate],timerGifCount=nil;
            }
            prevIndexPath = visibleIndexPath;
            timerGifCount = [NSTimer scheduledTimerWithTimeInterval:2.0f target:self selector:@selector(fireGifCountApi) userInfo:nil repeats:NO];
        }
    }
    else
    {
        [timerGifCount invalidate],timerGifCount=nil;
        prevIndexPath = visibleIndexPath = nil;
        lblNameUserName.text = @"";
        lblNameUserName.tag = -1;
        [btnProfileImg setImage:nil forState:UIControlStateNormal];
        infBtn.hidden = YES;
    }
    
    if (([self.tableHomeFeed contentOffset].y + self.tableHomeFeed.frame.size.height) >= [self.tableHomeFeed contentSize].height)
    {
        NSLog(@"scrolled to bottom");
        if (!isPageRefreshing) {
            
            if(totalPage <= currentPage)
            {
                return;
            }
            else
            {
                currentPage++;
                [self animateFooterView];
                [self.tableHomeFeed setTableFooterView:vwFooterVw];
            }
            
            isPageRefreshing = YES;
            [self getPostedFeed];
        }
    }
}

#pragma mark -
#pragma mark Gif Count Update
- (void)fireGifCountApi
{
    //    HomePostedGifCustomeCell *cell = [self.tableHomeFeed cellForRowAtIndexPath:prevIndexPath];
    
    if (!prevIndexPath) {
        return;
    }
    
    NSIndexPath *indexPath = prevIndexPath;
    
    NSString *postid = appDataManager.globalHomeFeedArray[indexPath.row][@"post_id"];
    
    BOOL isViewed = [appDataManager.globalHomeFeedArray[indexPath.row][@"is_view"]boolValue];;
    
    if (isViewed) {
        return;
    }
    
    NSString *url = [NSString stringWithFormat:@"posts/views/%@",postid];
    
    NSMutableDictionary *header =[[NSMutableDictionary alloc]initWithObjectsAndKeys:@"application/json",@"Content-Type",[NSUSERDEFAULTS valueForKey:kUSERTOKEN],@"User-Token", nil];
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypePost withHttpHeaders:header withServiceName:url withParameters:nil withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"--responce dict %@",responseDict);
        if(responseDict==Nil){
        }
        else if([operation.response statusCode]==200)
        {
            if (!prevIndexPath) {
                return;
            }
            
            if ([responseDict[@"success"] boolValue])
            {
                NSInteger gifCount = [appDataManager.globalHomeFeedArray[indexPath.row][@"view_count"]integerValue];
                gifCount +=1;
                [appDataManager.globalHomeFeedArray[indexPath.row] setValue:[NSNumber numberWithInteger:gifCount] forKey:@"view_count"];
                [appDataManager.globalHomeFeedArray[indexPath.row] setValue:[NSNumber numberWithBool:YES] forKey:@"is_view"];
                NSString *gifID = appDataManager.globalHomeFeedArray[indexPath.row][@"post_id"];
                for (NSInteger i=0; i<appDataManager.globalUserFeedArray.count; i++)
                {
                    NSString *postID = appDataManager.globalUserFeedArray[i][@"post_id"];
                    
                    if ([gifID isEqualToString:postID])
                    {
                        NSInteger gifCount = [appDataManager.globalUserFeedArray[i][@"view_count"]integerValue];
                        gifCount +=1;
                        [appDataManager.globalUserFeedArray[i] setValue:[NSNumber numberWithInteger:gifCount] forKey:@"view_count"];
                        [appDataManager.globalUserFeedArray[i] setValue:[NSNumber numberWithBool:YES] forKey:@"is_view"];
                    }
                }
            }
        }
        else
        {
        }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                      }];
}

#pragma mark - Navigation Propertites

-(void)setNavigationProperties
{
    [CommonFunction hideNavigationBarFromController:self];
}


#pragma mark - Memory Managment

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)uploadGifToServer:(NSNotification *)notification
{
    
    NSDictionary *dict = notification.object;
    
    BOOL isFromGallery = [dict[@"isFromGallery"] boolValue];
    NSData *gifData = nil;
    NSURL *gifUrl = nil;
    
    if (isFromGallery)
    {
        gifData = [NSUSERDEFAULTS objectForKey:@"selectedGif"];
    }
    else
    {
        gifUrl = dict[@"url"];
        if (gifUrl)
        {
            gifData = [NSData dataWithContentsOfURL:gifUrl];
            if (!gifData)
            {
                [CommonFunction showAlertWithTitle:@"Error" message:@"There was some error while posting gif, try later" onViewController:self useAsDelegate:NO dismissBlock:nil];
                return;
            }
        }
        else
        {
            [CommonFunction showAlertWithTitle:@"Error" message:@"There was some error while posting gif, try later" onViewController:self useAsDelegate:NO dismissBlock:nil];
            return;
        }
    }
    //    CGRect frame = self.uploadProgressView.frame;
    //    frame.size.height = 50.0f*SCREEN_YScale;
    //    frame.origin.y=frame.origin.y-2;
    //    self.uploadProgressView.frame = frame;
    //
    self.tableHomeFeed.tableHeaderView = self.uploadProgressView;
    //
    //    self.uploadProgressBar.progress = 0.0f;
    //
    //    self.progressBarHeightConstraint.constant = 50;
    
    UIImage *imagetoresize = [UIImage imageWithData:gifData];
    self.upLoadImageView.image = [imagetoresize resizedImageToFitInSize:CGSizeMake(imagetoresize.size.width/3, imagetoresize.size.height/3) scaleIfSmaller:NO];
    
    self.labelPercentage.text = @"UPLOADING 0%";
    [CommonFunction showActivityIndicatorWithText:@""];
    [self uploadImageBySessionUserMethod:@"" withParameters:@{@"caption":dict[@"caption"], @"shareType":dict[@"shareType"]} andImageData:gifData withCompletion:^(NSArray *data) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            NSLog(@"%@",data);
        });
        
    } WithFailure:^(NSString *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            NSLog(@"%@",error);
        });
    }];
}


- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 461*SCREEN_YScale;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 461*SCREEN_YScale;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return appDataManager.globalHomeFeedArray.count;
    
}



- (HomePostedGifCustomeCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    HomePostedGifCustomeCell *cell = [tableView dequeueReusableCellWithIdentifier:@"homeFeedCell"];
    cell.delegate=nil;
    cell.delegate=self;
    return cell;
}


- (void)tableView:(UITableView *)tableView willDisplayCell:(HomePostedGifCustomeCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    
    
    [cell.btnLike addTarget:self action:@selector(likeBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
    [cell.btnComment addTarget:self action:@selector(commentBtnClickedOnCell:) forControlEvents:UIControlEventTouchUpInside];
    [cell.btnReplug addTarget:self action:@selector(rePlugBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
    [cell.btnMoreClicked addTarget:self action:@selector(menuBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
    cell.btnLike.tag=indexPath.row;
    cell.btnComment.tag=indexPath.row;
    cell.btnReplug.tag=indexPath.row;
    cell.imgVwGif.tag = indexPath.row;
    cell.btnMoreClicked.tag = indexPath.row;
    
    for (NSInteger i=0; i<cell.imgVwGif.gestureRecognizers.count; i++)
    {
        UIGestureRecognizer *imgLikeTap = cell.imgVwGif.gestureRecognizers[i];
        [cell.imgVwGif removeGestureRecognizer:imgLikeTap];
    }
    UITapGestureRecognizer *imgLikeTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onDoubleTapLikeAction:)];
    imgLikeTap.numberOfTapsRequired=2;
    [cell.imgVwGif addGestureRecognizer:imgLikeTap];
    
    UILongPressGestureRecognizer *imgsavepress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(onSaveGifAction:)];
    [cell.imgVwGif addGestureRecognizer:imgsavepress];
    
    
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.lineSpacing = 5.0f;
    paragraphStyle.lineBreakMode = NSLineBreakByTruncatingTail;
    //    paragraphStyle.maximumLineHeight = 5.0f;
    
    NSString *string = [NSString stringWithFormat:@"%@",appDataManager.globalHomeFeedArray[indexPath.row][@"caption"]];
    NSDictionary *attributtes = @{
                                  NSParagraphStyleAttributeName : paragraphStyle,
                                  NSForegroundColorAttributeName : [UIColor whiteColor],
                                  NSFontAttributeName : [UIFont fontWithName:cell.lblCaption.font.fontName size:14.0f]
                                  };
    cell.lblCaption.attributedText = [[NSAttributedString alloc] initWithString:string
                                                                     attributes:attributtes];
    
    
    // cell.lblCaption.text=[NSString stringWithFormat:@"%@",arrFeedData[indexPath.row][@"caption"]];
    
    
    
    if ([[appDataManager.globalHomeFeedArray[indexPath.row][@"is_like"]stringValue] isEqualToString:@"0"]) {
        [cell.imgLikeUnlikeImg setImage:[UIImage imageNamed:@"HF_likeUnfillIcon"]];
        cell.imgLikeUnlikeImg.tag=0;
    }
    else
    {
        [cell.imgLikeUnlikeImg setImage:[UIImage imageNamed:@"PG_LikeBtn"]];
        cell.imgLikeUnlikeImg.tag=1;
    }
    
    [[appDataManager.globalHomeFeedArray[indexPath.row][@"is_comment"]stringValue] isEqualToString:@"0"]
    ?[cell.imgCommentFillUnFill setImage:[UIImage imageNamed:@"PG_Comment"]]
    :[cell.imgCommentFillUnFill setImage:[UIImage imageNamed:@"PG_CommentSelected"]];
    
    
    cell.lblCommentCount.text=[NSString stringWithFormat:@"%@",[[appDataManager.globalHomeFeedArray objectAtIndex:indexPath.row]objectForKey:@"comments_count"]];
    cell.lblLikeCount.text=[NSString stringWithFormat:@"%@",[[appDataManager.globalHomeFeedArray objectAtIndex:indexPath.row]objectForKey:@"likes_count"]];
    cell.lblReplugCount.text=[NSString stringWithFormat:@"%@",[[appDataManager.globalHomeFeedArray objectAtIndex:indexPath.row]objectForKey:@"replugged_count"]];
    
    cell.tag = indexPath.row;
    
    NSString *imgGifUrl=appDataManager.globalHomeFeedArray[indexPath.row][@"post_image"];
    
    NSURL *url1 = [NSURL URLWithString:imgGifUrl];
    
    NSNumber *progress = [[AppWebHandler sharedInstance].dictProgressTask objectForKey:[url1 absoluteString]];
    //    NSNumber *progress = [self.progressBuffer objectForKey:[url1 absoluteString]];
    if (!progress) progress = @(0.0);
    
    [cell setProgress:[progress floatValue]];
    
    if ([progress intValue]==1) {
        
        [cell.progresView setHidden:true];
        [cell.imgVwGif setHidden:false];
    }
    else
    {
        [cell.progresView setHidden:false];
        [cell.imgVwGif setHidden:true];
    }
    
    if (![[AppWebHandler sharedInstance].dictProgressTask objectForKey:[url1 absoluteString]]) {
        
        // Download Giff with URL Item
        [self downloadGifFromUrl:url1];
    }
    else
    {
        // Update Progress Buffer
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
            NSURL *localURL = [self URLForGiffWithName:[imgGifUrl lastPathComponent]];
            
            if ([[NSFileManager defaultManager] fileExistsAtPath:[localURL path]]) {
                
                NSData *animatedImageData = [[NSFileManager defaultManager] contentsAtPath:localURL.path];
                FLAnimatedImage *animatedImage = [FLAnimatedImage animatedImageWithGIFData:animatedImageData generateThumbnail:NO];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    [cell.progresView setHidden:true];
                    [cell.imgVwGif setHidden:false];
                    [cell.imgVwGif setAnimatedImage:nil];
                    [cell.imgVwGif setAnimatedImage:animatedImage];
                });
            }
        });
    }
    [cell handleCaptionBar];

}

- (void)tableView:(UITableView *)tableView didEndDisplayingCell:(HomePostedGifCustomeCell *)cell forRowAtIndexPath:(NSIndexPath*)indexPath
{
    if (cell) {
        [cell resetCaptionBar];
    }
}


//- (void)configureCell:(HomePostedGifCustomeCell *)cell withGifUrl:(NSURL *)gifUrl
//{
//    [[cell serialQueue]cancelAllOperations];
//
//    __weak typeof(cell) weakCell = cell;
//
//    NSBlockOperation *operation = [NSBlockOperation blockOperationWithBlock:^{
//
//        NSData *animatedImageData = [[NSFileManager defaultManager] contentsAtPath:gifUrl.path];
//        FLAnimatedImage *animatedImage = [FLAnimatedImage animatedImageWithGIFData:animatedImageData];
//
//        dispatch_async(dispatch_get_main_queue(), ^{
//            [weakCell.imgVwGif setAnimatedImage:nil];
//            [weakCell.imgVwGif setAnimatedImage:animatedImage];
//        });
//    }];
//    [[cell serialQueue]addOperation:operation];
//}

#pragma mark - FooterView Animating
-(void)animateFooterView
{
    
    [UIView animateWithDuration:1.0f delay:0.0f options:UIViewAnimationOptionRepeat|UIViewAnimationOptionCurveLinear
                     animations:^{
                         [imgFooterCircle setTransform:CGAffineTransformRotate([imgFooterCircle transform], M_PI-0.00001f)];
                     } completion:nil];
    
}



#pragma mark - WebServiceAPI



//-(void)uploadImageBySessionUserMethod:(NSString *)strMethodName withParameters:(NSDictionary*)dictParameter andImageData:(NSData *)imageData withCompletion:(void (^)(NSArray *data))completion WithFailure:(void (^)(NSString *error))failure
//{
//    NSString *strUrl = [NSString stringWithFormat:@"%@posts",serverURL];
//    // NSString *strUrl=[NSString stringWithFormat:@"%@%@"serverURL,strMethodName];
//    NSURL *url = [NSURL URLWithString:strUrl];
//
////    NSMutableDictionary *param=[[NSMutableDictionary alloc]init];
////
////    if ([NSUSERDEFAULTS valueForKey:@"token"])
////    {
////        [param setValue:[NSUSERDEFAULTS valueForKey:@"token"] forKey:@"User-Token"];
////    }
//
//
//    // Initialize Session Configuration
//    NSURLSessionConfiguration *sessionConfiguration = [NSURLSessionConfiguration backgroundSessionConfigurationWithIdentifier:@"upload.gif"];
//
//    // Initialize Session Manager
//    AFURLSessionManager *newManager = [[AFURLSessionManager alloc] initWithSessionConfiguration:sessionConfiguration];
//
//    // Configure Manager
//    [newManager setResponseSerializer:[AFJSONResponseSerializer serializer]];
//
//    // Send Request
//    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
//    [request addValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
//    [request addValue:[NSUSERDEFAULTS valueForKey:@"token"] forHTTPHeaderField:@"User-Token"];
//
//
//    __block NSProgress *progresCounter;
//
//    __weak typeof(self) weakSelf = self;
//
//    NSURLSessionUploadTask *uploadTask=[newManager uploadTaskWithRequest:request fromData:imageData progress:&progresCounter completionHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
//
//        [progresCounter removeObserver:self forKeyPath:@"fractionCompleted"];
//        if (error)
//        {
//            weakSelf.tableHomeFeed.tableHeaderView = nil;
//            [CommonFunction removeActivityIndicator];
//            [[[UIAlertView alloc]initWithTitle:@"Giffplug Error" message:error.localizedDescription delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil]show];
//        }
//        else
//        {
//            weakSelf.tableHomeFeed.tableHeaderView = nil;
//            [CommonFunction removeActivityIndicator];
//            if (responseObject != nil)
//            {
//                NSLog(@"%@",responseObject);
//
//                if ([responseObject[@"msg"] isEqualToString:@"Post added successfully."])
//                {
//                    //                                              currentPage=1;
//                    //                                              totalPage=0;
//                    [CommonFunction showActivityIndicatorWithText:@""];
//                    //                                              [weakSelf getPostedFeed];
//
//                    if (appDataManager.globalHomeFeedArray.count>0)
//                    {
//                        [appDataManager.globalHomeFeedArray insertObject:[responseObject[@"postData"] mutableCopy] atIndex:0];
//                    }
//                    else
//                    {
//                        [appDataManager.globalHomeFeedArray addObject:[responseObject[@"postData"] mutableCopy]];
//                    }
//                    if (appDataManager.globalUserFeedArray.count>0)
//                    {
//                        [appDataManager.globalUserFeedArray insertObject:[responseObject[@"postData"] mutableCopy] atIndex:0];
//                    }
//                    [_tableHomeFeed insertRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:0 inSection:0]] withRowAnimation:UITableViewRowAnimationTop];
//                    [_tableHomeFeed setContentOffset:CGPointZero animated:NO];
//                }
//                else
//                {
//                    [[[UIAlertView alloc]initWithTitle:@"Giffplug Error" message:@"Giff could not be posted due to some error, try again!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil]show];
//                }
//            }
//        }
//    }];
//
//    [uploadTask resume];
//
//    // Observe fractionCompleted using KVO
//    [progresCounter addObserver:self
//               forKeyPath:@"fractionCompleted"
//                  options:NSKeyValueObservingOptionNew
//                  context:NULL];
//}
//
//- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
//{
//    [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
//
//    if ([keyPath isEqualToString:@"fractionCompleted"] && [object isKindOfClass:[NSProgress class]]) {
//        NSProgress *progress = (NSProgress *)object;
//        NSLog(@"Progress is %f", progress.fractionCompleted);
//
////        CGFloat progressValue=(float)totalBytesWritten/(float)totalBytesExpectedToWrite;
////        self.uploadProgressBar.progress = progressValue;
////        NSString *percentString = [NSString stringWithFormat:@"UPLOADING %ld",lround(progressValue*100)];
////        self.labelPercentage.text = [percentString stringByAppendingString:@"%"];
//    }
//}

-(void)uploadImageBySessionUserMethod:(NSString *)strMethodName withParameters:(NSDictionary*)dictParameter andImageData:(NSData *)imageData withCompletion:(void (^)(NSArray *data))completion WithFailure:(void (^)(NSString *error))failure
{
    isGifUploadedInProcess = YES;
    
    NSString *shareType = dictParameter[@"shareType"];
    NSString *caption = dictParameter[@"caption"];
    NSString *strUrl = [NSString stringWithFormat:@"%@posts",serverURL];
    // NSString *strUrl=[NSString stringWithFormat:@"%@%@"serverURL,strMethodName];
    NSURL *url = [NSURL URLWithString:strUrl];
    NSData *gifData = imageData;
    
    NSMutableDictionary *param=[[NSMutableDictionary alloc]init];
    
    if ([NSUSERDEFAULTS valueForKey:@"token"])
    {
        [param setValue:[NSUSERDEFAULTS valueForKey:@"token"] forKey:@"User-Token"];
    }
    
    
    
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:url];
    
    [manager.requestSerializer setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [manager.requestSerializer setValue:[NSUSERDEFAULTS valueForKey:@"token"]  forHTTPHeaderField:@"User-Token"];
    
    
    __weak typeof(self) weakSelf = self;
    AFHTTPRequestOperation *op = [manager POST:@"" parameters:dictParameter constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        
        NSString *fileImg=[NSString stringWithFormat:@"giffplug.gif"];
        NSString *parameterNm=[NSString stringWithFormat:@"post_image"];
        
        [formData appendPartWithFileData:gifData name:parameterNm fileName:fileImg mimeType:@"image/gif"];
        
        NSString *fileImg2=[NSString stringWithFormat:@"watermark.jpeg"];
        NSString *parameterNm2=[NSString stringWithFormat:@"watermark_image"];
        NSData *watermarkData = [CommonFunction getWatermarkImageData];
        [formData appendPartWithFileData:watermarkData name:parameterNm2 fileName:fileImg2 mimeType:@"image/jpeg"];
        
    } success:^(AFHTTPRequestOperation *operation, id responseObject)
                                  {
                                      isGifUploadedInProcess = NO;
//                                      weakSelf.tableHomeFeed.tableHeaderView = nil;
                                      [CommonFunction removeActivityIndicator];
                                      if (responseObject != nil)
                                      {
                                          if ([operation.response statusCode] == 200)
                                          {
                                              NSLog(@"%@",responseObject);
                                              
                                              //                                              currentPage=1;
                                              //                                              totalPage=0;
                                              //                                              [weakSelf getPostedFeed];
                                              
                                              if (appDataManager.globalHomeFeedArray.count>0)
                                              {
                                                  [appDataManager.globalHomeFeedArray insertObject:[responseObject[@"postData"] mutableCopy] atIndex:0];
                                              }
                                              else
                                              {
                                                  [appDataManager.globalHomeFeedArray addObject:[responseObject[@"postData"] mutableCopy]];
                                              }
                                              if (appDataManager.globalUserFeedArray.count>0)
                                              {
                                                  [appDataManager.globalUserFeedArray insertObject:[responseObject[@"postData"] mutableCopy] atIndex:0];
                                              }
                                              else
                                              {
                                                  [appDataManager.globalUserFeedArray addObject:[responseObject[@"postData"] mutableCopy]];
                                              }
                                              [self.tableHomeFeed beginUpdates];
                                              [self.tableHomeFeed insertRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:0 inSection:0]] withRowAnimation:UITableViewRowAnimationTop];
                                              [self.tableHomeFeed endUpdates];
                                              [self.tableHomeFeed setContentOffset:CGPointZero animated:NO];
                                              
                                              lblError.text=@"";
                                              
                                              if (responseObject[@"postData"][@"avatar_image"])
                                              {
                                                  NSString *imgStr = responseObject[@"postData"][@"avatar_image"];
                                                  if (imgStr.length>0)
                                                  {
                                                      NSString *username = responseObject[@"postData"][@"username"];
                                                      for (NSInteger i=0; i<appDataManager.globalHomeFeedArray.count; i++)
                                                      {
                                                          NSString *tempUsername = appDataManager.globalHomeFeedArray[i][@"username"];
                                                          NSString *tempImageStr = appDataManager.globalHomeFeedArray[i][@"avatar_image"];
                                                          if ([tempUsername isEqualToString:username] && ![imgStr isEqualToString:tempImageStr]) {
                                                              [appDataManager.globalHomeFeedArray[i] setValue:imgStr forKey:@"avatar_image"];
                                                          }
                                                      }
                                                  }
                                              }
                                              
                                              if (appDataManager.globalHomeFeedArray.count >0) {
                                                  
                                                  CGRect visibleRect = (CGRect){.origin = self.tableHomeFeed.contentOffset, .size = self.tableHomeFeed.bounds.size};
                                                  CGPoint visiblePoint = CGPointMake(CGRectGetMidX(visibleRect), CGRectGetMidY(visibleRect));
                                                  visibleIndexPath = [self.tableHomeFeed indexPathForRowAtPoint:visiblePoint];
                                                  
                                                  NSURL *imgUrl=[NSURL URLWithString:appDataManager.globalHomeFeedArray[visibleIndexPath.row][@"avatar_image"]];
                                                  
                                                  
                                                  //                                                  if ([imgUrl.absoluteString.pathExtension isEqualToString:@"gif"])
                                                  //                                                  {
                                                  //                                                      btnProfileImg.contentMode = UIViewContentModeScaleAspectFill;
                                                  //                                                  }
                                                  //                                                  else
                                                  //                                                  {
                                                  //                                                      btnProfileImg.contentMode = UIViewContentModeScaleToFill;
                                                  //                                                  }
                                                  
                                                  [btnProfileImg sd_setImageWithURL:imgUrl forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"default.jpg"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                                      if (error)
                                                      {
                                                          btnProfileImg.contentMode=UIViewContentModeScaleToFill;
                                                          btnProfileImg.imageView.contentMode=UIViewContentModeScaleToFill;
                                                      }
                                                      else
                                                      {
                                                          if ([[imgUrl.absoluteString pathExtension]isEqualToString:@"gif"])
                                                          {
                                                              btnProfileImg.contentMode=UIViewContentModeScaleAspectFill;
                                                              btnProfileImg.imageView.contentMode=UIViewContentModeScaleAspectFill;
                                                          }
                                                          else
                                                          {
                                                              btnProfileImg.contentMode=UIViewContentModeScaleToFill;
                                                              btnProfileImg.imageView.contentMode=UIViewContentModeScaleToFill;
                                                          }
                                                      }
                                                  }];
                                                  
                                                  btnProfileImg.tag=visibleIndexPath.row;
                                                  lblNameUserName.text=[[appDataManager.globalHomeFeedArray objectAtIndex:visibleIndexPath.row]objectForKey:@"username"];
                                                  lblNameUserName.tag = visibleIndexPath.row;
                                                  infBtn.tag=visibleIndexPath.row;
                                                  infBtn.hidden = NO;
                                                  if ([shareType isEqualToString:@"TWITTER"])
                                                  {
                                                      weakSelf.labelPercentage.text = @"Uploading to Twitter...";
                                                  }
                                                  else
                                                  {
                                                      weakSelf.tableHomeFeed.tableHeaderView = nil;
                                                  }
                                                  [weakSelf handleSocialShareForType:shareType caption:caption gifData:gifData andURL:[NSURL URLWithString:responseObject[@"postData"][@"watermark_image"]]];
                                              }
                                          }
                                          else
                                          {
                                              weakSelf.tableHomeFeed.tableHeaderView = nil;
                                              [[[UIAlertView alloc]initWithTitle:@"Giffplug Error" message:@"Giff could not be posted due to some error, try again!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil]show];
                                          }
                                      }
                                      else
                                      {
                                          weakSelf.tableHomeFeed.tableHeaderView = nil;
                                      }
                                  } failure:^(AFHTTPRequestOperation *operation, NSError *error)
                                  {
                                      isGifUploadedInProcess = NO;
                                      weakSelf.tableHomeFeed.tableHeaderView = nil;
                                      [CommonFunction removeActivityIndicator];
                                      NSLog(@"Error: %@ ***** %@", operation.responseString, error);
                                      
                                      [[[UIAlertView alloc]initWithTitle:@"Giffplug Error" message:error.localizedDescription delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil]show];
                                  }];
    
    [op setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
        CGFloat progressValue=(float)totalBytesWritten/(float)totalBytesExpectedToWrite;
        weakSelf.uploadProgressBar.progress = progressValue;
        NSString *percentString = [NSString stringWithFormat:@"UPLOADING %ld",lround(progressValue*100)];
        weakSelf.labelPercentage.text = [percentString stringByAppendingString:@"%"];
    }];
    
    [op start];
    
    
}

- (void)handleSocialShareForType:(NSString *)shareType caption:(NSString *)caption gifData:(NSData *)gifData andURL:(NSURL *)shareURL
{
    if ([shareType isEqualToString:@"FACEBOOK"])
    {
        if ([FBSDKAccessToken currentAccessToken])
        {
            FBSDKShareLinkContent *content = [[FBSDKShareLinkContent alloc]init];
            content.contentURL = shareURL;
            content.contentTitle = @"GifPlug";
            content.contentDescription = caption;
            [FBSDKShareDialog showFromViewController:self
                                         withContent:content
                                            delegate:nil];
        }
        else
        {
            FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
            [login logInWithPublishPermissions:@[@"publish_actions"] fromViewController:self handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
                if (result.token) {
                    FBSDKShareLinkContent *content = [[FBSDKShareLinkContent alloc]init];
                    content.contentURL = shareURL;
                    
                    [FBSDKShareDialog showFromViewController:self
                                                 withContent:content
                                                    delegate:nil];
                }
            }];
        }
    }
    else if ([shareType isEqualToString:@"TWITTER"])
    {
        __weak typeof(self) weakSelf = self;

//        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{

            NSMutableArray *gifArray = [NSMutableArray array];
            NSDictionary *dict = [CommonFunction animatedGIFWithData:gifData];
            [gifArray removeAllObjects];
            [gifArray addObjectsFromArray:[dict[@"images"]mutableCopy]];
            CGFloat _animationDuration = [dict[@"duration"] floatValue];
            
            NSURL *url=[CommonFunction createGiffFromImageArray:gifArray andDelayTime:_animationDuration/gifArray.count isProfileUpload:NO];
            
            NSData *newGifData = [NSData dataWithContentsOfURL:url];
            
//            dispatch_async(dispatch_get_main_queue(), ^{
                [self uploadGifToTwitterWithData:newGifData andCaption:caption];
//            });
//        });
    }
    else if ([shareType isEqualToString:@"INSTAGRAM"])
    {
        [self uploadVideoToInstagramData:gifData andCaption:caption];
    }
}

- (void)uploadVideoToInstagramData:(NSData *)gifData andCaption:(NSString *)caption
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        NSMutableArray *gifArray = [NSMutableArray array];
        NSDictionary *dict = [CommonFunction animatedGIFWithData:gifData];
        [gifArray removeAllObjects];
        [gifArray addObjectsFromArray:[dict[@"images"]mutableCopy]];
        CGFloat _animationDuration = [dict[@"duration"] floatValue];
        
        NSDictionary *settings = [CEMovieMaker videoSettingsWithCodec:AVVideoCodecH264 withWidth:704 andHeight:1264];
        CEMovieMaker *maker = [[CEMovieMaker alloc]initWithSettings:settings andDelayTime:_animationDuration];
        NSMutableArray *array = [NSMutableArray array];
        for (NSInteger i=0; i<gifArray.count ; i++)
        {
            if ([gifArray[i] isKindOfClass:[NSDictionary class]])
            {
                NSDictionary *dict = gifArray[i];
                
                UIImage *image = dict[@"image"];
                
                [array addObject:[image resizedImageToFitInSize:CGSizeMake(704, 1264) scaleIfSmaller:NO]];
            }
            else
            {
                UIImage *image = gifArray[i];
                
                [array addObject:[image resizedImageToFitInSize:CGSizeMake(704, 1264) scaleIfSmaller:YES]];
            }
        }
        
        __weak typeof(self) weakSelf = self;
        [maker createMovieFromImages:array withCompletion:^(NSURL *fileURL) {
            if ([[[UIDevice currentDevice]systemVersion]floatValue]<9)
            {
                ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
                [library writeVideoAtPathToSavedPhotosAlbum:fileURL completionBlock:^(NSURL *assetURL, NSError *error) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [[NSFileManager defaultManager]removeItemAtURL:fileURL error:NULL];
                        [weakSelf uploadVideoToInstagramWithURL:assetURL.absoluteString andCaption:caption];
                    });
                }];
            }
            else
            {
                [PHAsset saveVideoAtURL:fileURL location:nil completionBlock:^(PHAsset *asset, BOOL success) {
                    [[PHImageManager defaultManager]requestAVAssetForVideo:asset options:nil resultHandler:^(AVAsset * _Nullable asset, AVAudioMix * _Nullable audioMix, NSDictionary * _Nullable info) {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [[NSFileManager defaultManager]removeItemAtURL:fileURL error:NULL];
                            NSArray *arr=[info[@"PHImageFileSandboxExtensionTokenKey"] componentsSeparatedByString:@";/"];
                            NSArray *arr1 = [[arr lastObject] componentsSeparatedByString:@"\""];
                            NSString *videoURL = [NSString stringWithFormat:@"%@",[arr1 firstObject]];
                            [weakSelf uploadVideoToInstagramWithURL:videoURL andCaption:caption];
                        });
                    }];
                }];
            }
        }];
        
    });
}
- (void)uploadVideoToInstagramWithURL:(NSString *)fileURL  andCaption:(NSString *)caption
{
    NSURL *instaURL = [NSURL URLWithString:[NSString stringWithFormat:@"instagram://library?AssetPath=%@",[fileURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]];
    NSLog(@"********* Instagram url : %@",instaURL);
    if ([[UIApplication sharedApplication]canOpenURL:instaURL])
    {
        NSLog(@"********* OPENED ************");

        [[UIApplication sharedApplication]openURL:instaURL];
    }
    else
    {
        [CommonFunction showAlertWithTitle:@"GifPlug" message:@"You need to install instagram app for sharing." onViewController:self withButtonsArray:@[@"OK"] dismissBlock:nil];
    }
}

-(void)uploadGifToTwitterWithData:(NSData *)gifData andCaption:(NSString *)caption {
    
    ACAccountStore *account = [[ACAccountStore alloc] init];
    ACAccountType *accountType = [account accountTypeWithAccountTypeIdentifier:
                                  ACAccountTypeIdentifierTwitter];
    
    __weak typeof(self) weakSelf = self;
    
    [account requestAccessToAccountsWithType:accountType options:nil
                                  completion:^(BOOL granted, NSError *error)
     {
         dispatch_async(dispatch_get_main_queue(), ^{
             if (granted == YES)
             {
                 weakSelf.tableHomeFeed.tableHeaderView = nil;
                 
                 NSArray *arrayOfAccounts = [account
                                             accountsWithAccountType:accountType];
                 
                 if ([arrayOfAccounts count] > 0)
                 {
                     ACAccount *twitterAccount =
                     [arrayOfAccounts lastObject];
                     
                     NSURL *requestURL = [NSURL URLWithString:@"https://upload.twitter.com/1.1/media/upload.json"];
                     
                     SLRequest *postRequest = [SLRequest
                                               requestForServiceType:SLServiceTypeTwitter
                                               requestMethod:SLRequestMethodPOST
                                               URL:requestURL parameters:nil];
                     
                     postRequest.account = twitterAccount;
                     
                     [postRequest addMultipartData:gifData
                                          withName:@"media"
                                              type:@"image/gif"
                                          filename:@"gifplug.gif"];
                     [postRequest
                      performRequestWithHandler:^(NSData *responseData,
                                                  NSHTTPURLResponse *urlResponse, NSError *error)
                      {
                          if (error)
                          {
                              [CommonFunction removeActivityIndicator];
                              //                          [CommonFunction showAlertWithTitle:@"Posting Failed" message:error.localizedDescription onViewController:self useAsDelegate:NO dismissBlock:nil];
                          }
                          else
                          {
                              NSDictionary *json = [NSJSONSerialization JSONObjectWithData:responseData options:0 error:nil];
                              
                              NSString *mediaID = [json objectForKey:@"media_id_string"];
                              
                              if (mediaID!=nil)
                              {
                                  NSURL *requestURL2 = [NSURL URLWithString:@"https://api.twitter.com/1.1/statuses/update.json"];
                                  NSDictionary *message2 = @{@"status": caption,
                                                             @"media_ids": mediaID };
                                  
                                  SLRequest *postRequest2 = [SLRequest
                                                             requestForServiceType:SLServiceTypeTwitter
                                                             requestMethod:SLRequestMethodPOST
                                                             URL:requestURL2 parameters:message2];
                                  postRequest2.account = twitterAccount;
                                  
                                  [postRequest2
                                   performRequestWithHandler:^(NSData *responseData,
                                                               NSHTTPURLResponse *urlResponse, NSError *error)
                                   {
                                       [CommonFunction removeActivityIndicator];
                                       // DONE!!!
                                       //                                       NSDictionary *resultJson = [NSJSONSerialization JSONObjectWithData:responseData options:0 error:nil];
                                   }];
                              }
                              else
                              {
                                  [CommonFunction removeActivityIndicator];
                                  [CommonFunction showAlertWithTitle:@"Posting Failed" message:@"Due to some reason Gif posting failed, try again later!" onViewController:self useAsDelegate:NO dismissBlock:nil];
                              }
                              
                          }
                      }];
                 }
                 else
                 {
                     [CommonFunction removeActivityIndicator];
                     [CommonFunction showAlertWithTitle:@"No Twiiter Accounts" message:@"There is no twitter accounts linked to your device, you can link one by going to Settings -> Twitter." onViewController:self useAsDelegate:NO dismissBlock:nil];
                 }
             }
             else
             {
                 weakSelf.tableHomeFeed.tableHeaderView = nil;
                 [CommonFunction removeActivityIndicator];
                 [CommonFunction showAlertWithTitle:@"Access Denied" message:@"When asked you denied the acces to twitter accounts, please enable access manually by going to settings." onViewController:self useAsDelegate:NO dismissBlock:nil];
             }
         });
     }];
}

-(void)getPostedFeed
{
    NSString *url = [NSString stringWithFormat:@"posts/list/%ld",(long)currentPage];
    //http://192.168.0.22:8353/v1/posts/list/{page}
    
    NSMutableDictionary *header =[[NSMutableDictionary alloc]initWithObjectsAndKeys:@"application/json",@"Content-Type",[NSUSERDEFAULTS valueForKey:kUSERTOKEN],@"User-Token", nil];
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    
    __weak typeof(self) weakSelf = self;
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypeGet withHttpHeaders:header withServiceName:url withParameters:nil withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"--responce dict %@",responseDict);
        
        [self.tableHomeFeed setTableFooterView:nil];
        [CommonFunction removeActivityIndicator];
        
        if (refreshControl.isRefreshing)
        {
            AudioServicesPlaySystemSound(1109);
        }
        
        [refreshControl endRefreshing];
        if(responseDict==Nil)        {
            [CommonFunction alertTitle:@"" withMessage:@"Server Error"];
            if (currentPage>1)
            {
                currentPage--;
            }
            isPageRefreshing = NO;
        }
        else if([operation.response statusCode]==200)
        {
            NSArray *arrListing = [responseDict objectForKey:@"posts"];
            
            //            if (arrListing.count > 0)
            //            {
            if (currentPage == 1)
            {
                if (appDataManager.globalHomeFeedArray.count > 0)
                {
                    [appDataManager.globalHomeFeedArray removeAllObjects];
                }
                //                    if (appDataManager.globalHomeFeedArray == nil)
                //                    {
                //                        appDataManager.globalHomeFeedArray = [NSMutableArray array];
                //                    }
                [appDataManager.globalHomeFeedArray addObjectsFromArray:arrListing];
            }
            else
            {
                [appDataManager.globalHomeFeedArray addObjectsFromArray:arrListing];
            }
            
            if (arrListing.count > 0) {
                totalPage=[[responseDict objectForKey:@"total_pages"] integerValue];
                totalPost=[[responseDict objectForKey:@"total_posts"] integerValue];
                if(currentPage==1 && appDataManager.globalHomeFeedArray.count>0)
                {
                    NSURL *imgUrl=[NSURL URLWithString:appDataManager.globalHomeFeedArray[0][@"avatar_image"]];
                    
                    //                    if ([imgUrl.absoluteString.pathExtension isEqualToString:@"gif"])
                    //                    {
                    //                        btnProfileImg.contentMode = UIViewContentModeScaleAspectFill;
                    //                    }
                    //                    else
                    //                    {
                    //                        btnProfileImg.contentMode = UIViewContentModeScaleToFill;
                    //                    }
                    
                    [btnProfileImg sd_setImageWithURL:imgUrl forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"default.jpg"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                        if (error)
                        {
                            btnProfileImg.contentMode=UIViewContentModeScaleToFill;
                            btnProfileImg.imageView.contentMode=UIViewContentModeScaleToFill;
                        }
                        else
                        {
                            if ([[imgUrl.absoluteString pathExtension]isEqualToString:@"gif"])
                            {
                                btnProfileImg.contentMode=UIViewContentModeScaleAspectFill;
                                btnProfileImg.imageView.contentMode=UIViewContentModeScaleAspectFill;
                            }
                            else
                            {
                                btnProfileImg.contentMode=UIViewContentModeScaleToFill;
                                btnProfileImg.imageView.contentMode=UIViewContentModeScaleToFill;
                            }
                        }
                    }];
                    
                    btnProfileImg.tag=0;
                    lblNameUserName.text=[[appDataManager.globalHomeFeedArray objectAtIndex:0]objectForKey:@"username"];
                    lblNameUserName.tag = visibleIndexPath.row;
                    infBtn.tag=0;
                    infBtn.hidden = NO;
                }
                
                // strMainImgURl=[responseDict objectForKey:@"site_url"];
                if (!(appDataManager.globalHomeFeedArray.count>0)) {
                    lblError.text=@"No Feeds Found.";
                }
                else
                {
                    lblError.text=@"";
                    //
                    //                    _tempDirectUrls = [[NSMutableArray alloc] init];
                    for (int i = 0 ;i<appDataManager.globalHomeFeedArray.count;i++) {
                        
                        NSDictionary *dict = [appDataManager.globalHomeFeedArray objectAtIndex:i];
                        
                        NSString *strImgUrl=[dict valueForKey:@"post_image"];
                        NSURL *mainURL=[NSURL URLWithString:[NSString stringWithFormat:@"%@",strImgUrl]];
                        
                        NSURL *localURL = [self URLForGiffWithName:[mainURL lastPathComponent]];
                        
                        if ([[NSFileManager defaultManager] fileExistsAtPath:[localURL path]] && localURL) {
                            
                            //                            [_tempDirectUrls addObject:localURL.path];
                            [[AppWebHandler sharedInstance].dictProgressTask setObject:@(1.0) forKey:[mainURL absoluteString]];
                        }
                    }
                }
            }
            //            }
            totalPost=[[responseDict objectForKey:@"total_posts"] integerValue];
            if (totalPage<=0)
            {
                lblError.text=@"No Feeds Found.";
            }
            else
            {
                lblError.text=@"";
            }
            [self.tableHomeFeed reloadData];
            isPageRefreshing = NO;
        }
        else
        {
            [CommonFunction alertTitle:@"" withMessage:[responseDict objectForKey:@"msg"]];
            if (currentPage>1)
            {
                currentPage--;
            }
            isPageRefreshing = NO;
        }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          
                                          
                                          [self.tableHomeFeed setTableFooterView:nil];
                                          [CommonFunction removeActivityIndicator];
                                          if (currentPage>1)
                                          {
                                              currentPage--;
                                          }
                                          isPageRefreshing = NO;
                                          [refreshControl endRefreshing];
                                          if([operation.response statusCode]  == 400 ){
                                              [refreshControl endRefreshing];
                                              NSLog(@"impo response %@",operation.response);
                                              
                                              [CommonFunction removeActivityIndicator];
                                              [CommonFunction alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                          }
                                          else{
                                              
                                              if (wbServiceCount==1) {
                                                  wbServiceCount=0;
                                                  [CommonFunction showActivityIndicatorWithText:@""];
                                                  [weakSelf getPostedFeed];
                                              }
                                              else
                                              {
                                                  [CommonFunction removeActivityIndicator];
                                                  [CommonFunction alertTitle:@"" withMessage:@"Network Error..."];
                                              }
                                          }
                                      }];
}

-(void)likeAPost:(NSString *)postID cell:(HomePostedGifCustomeCell *)cell indexpath:(NSInteger)arrindex
{
    NSString *url = [NSString stringWithFormat:@"posts/like/%@",postID];
    //http://192.168.0.22:8353/v1/posts/like/{postId}
    
    //    for (AFHTTPRequestOperation *operation in [AFHTTPRequestOperationManager manager].operationQueue.operations)
    //    {
    //        NSString *path = operation.request.URL.path;
    //        if ([path containsString:url])
    //        {
    //            [operation cancel];
    //        }
    //    }
    
    NSMutableDictionary *header =[[NSMutableDictionary alloc]initWithObjectsAndKeys:@"application/json",@"Content-Type",[NSUSERDEFAULTS valueForKey:kUSERTOKEN],@"User-Token", nil];
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    
    __weak typeof(cell) weakCell = cell;
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypePost withHttpHeaders:header withServiceName:url withParameters:nil withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"--responce dict %@",responseDict);
        
        [CommonFunction removeActivityIndicator];
        [refreshControl endRefreshing];
        if(responseDict==Nil)        {
            [CommonFunction alertTitle:@"" withMessage:@"Server Error"];
            
            NSInteger likeCount = [appDataManager.globalHomeFeedArray[arrindex][@"likes_count"] integerValue];
            
            [weakCell.imgLikeUnlikeImg setImage:[UIImage imageNamed:@"HF_likeUnfillIcon"]];
            weakCell.imgLikeUnlikeImg.tag=0;
            
            likeCount -= 1;
            weakCell.lblLikeCount.text=[NSString stringWithFormat:@"%ld",(long)likeCount];
            [appDataManager.globalHomeFeedArray[arrindex] setValue:[NSNumber numberWithInteger:likeCount] forKey:@"likes_count"];
            [appDataManager.globalHomeFeedArray[arrindex] setValue:@0 forKey:@"is_like"];
            
            NSString *postidstr = appDataManager.globalHomeFeedArray[arrindex][@"post_id"];
            for (NSInteger i=0; i<appDataManager.globalUserFeedArray.count; i++)
            {
                NSString *userPostID = appDataManager.globalUserFeedArray[i][@"post_id"];
                if ([userPostID isEqualToString:postidstr])
                {
                    [appDataManager.globalUserFeedArray[i] setValue:[NSNumber numberWithInteger:likeCount] forKey:@"likes_count"];
                    [appDataManager.globalUserFeedArray[i] setValue:@0 forKey:@"is_like"];
                }
            }
            for (NSInteger i=0; i<appDataManager.globalUserReplugArray.count; i++)
            {
                NSString *userPostID = appDataManager.globalUserReplugArray[i][@"post_id"];
                if ([userPostID isEqualToString:postID])
                {
                    [appDataManager.globalUserReplugArray[i] setValue:[NSNumber numberWithInteger:likeCount] forKey:@"likes_count"];
                    [appDataManager.globalUserReplugArray[i] setValue:@0 forKey:@"is_like"];
                }
            }
        }
        else if([operation.response statusCode]==200)
        {
            
        }
        else
        {
            [CommonFunction alertTitle:@"" withMessage:[responseDict objectForKey:@"msg"]];
            NSInteger likeCount = [appDataManager.globalHomeFeedArray[arrindex][@"likes_count"] integerValue];
            
            [weakCell.imgLikeUnlikeImg setImage:[UIImage imageNamed:@"HF_likeUnfillIcon"]];
            weakCell.imgLikeUnlikeImg.tag=0;
            
            likeCount -= 1;
            weakCell.lblLikeCount.text=[NSString stringWithFormat:@"%ld",(long)likeCount];
            [appDataManager.globalHomeFeedArray[arrindex] setValue:[NSNumber numberWithInteger:likeCount] forKey:@"likes_count"];
            [appDataManager.globalHomeFeedArray[arrindex] setValue:@0 forKey:@"is_like"];
            
            NSString *postidstr = appDataManager.globalHomeFeedArray[arrindex][@"post_id"];
            for (NSInteger i=0; i<appDataManager.globalUserFeedArray.count; i++)
            {
                NSString *userPostID = appDataManager.globalUserFeedArray[i][@"post_id"];
                if ([userPostID isEqualToString:postidstr])
                {
                    [appDataManager.globalUserFeedArray[i] setValue:[NSNumber numberWithInteger:likeCount] forKey:@"likes_count"];
                    [appDataManager.globalUserFeedArray[i] setValue:@0 forKey:@"is_like"];
                }
            }
            for (NSInteger i=0; i<appDataManager.globalUserReplugArray.count; i++)
            {
                NSString *userPostID = appDataManager.globalUserReplugArray[i][@"post_id"];
                if ([userPostID isEqualToString:postID])
                {
                    [appDataManager.globalUserReplugArray[i] setValue:[NSNumber numberWithInteger:likeCount] forKey:@"likes_count"];
                    [appDataManager.globalUserReplugArray[i] setValue:@0 forKey:@"is_like"];
                }
            }
        }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          
                                          NSInteger likeCount = [appDataManager.globalHomeFeedArray[arrindex][@"likes_count"] integerValue];
                                          
                                          [weakCell.imgLikeUnlikeImg setImage:[UIImage imageNamed:@"HF_likeUnfillIcon"]];
                                          weakCell.imgLikeUnlikeImg.tag=0;
                                          
                                          likeCount -= 1;
                                          weakCell.lblLikeCount.text=[NSString stringWithFormat:@"%ld",(long)likeCount];
                                          [appDataManager.globalHomeFeedArray[arrindex] setValue:[NSNumber numberWithInteger:likeCount] forKey:@"likes_count"];
                                          [appDataManager.globalHomeFeedArray[arrindex] setValue:@0 forKey:@"is_like"];
                                          
                                          
                                          NSString *postidstr = appDataManager.globalHomeFeedArray[arrindex][@"post_id"];
                                          for (NSInteger i=0; i<appDataManager.globalUserFeedArray.count; i++)
                                          {
                                              NSString *userPostID = appDataManager.globalUserFeedArray[i][@"post_id"];
                                              if ([userPostID isEqualToString:postidstr])
                                              {
                                                  [appDataManager.globalUserFeedArray[i] setValue:[NSNumber numberWithInteger:likeCount] forKey:@"likes_count"];
                                                  [appDataManager.globalUserFeedArray[i] setValue:@0 forKey:@"is_like"];
                                              }
                                          }
                                          for (NSInteger i=0; i<appDataManager.globalUserReplugArray.count; i++)
                                          {
                                              NSString *userPostID = appDataManager.globalUserReplugArray[i][@"post_id"];
                                              if ([userPostID isEqualToString:postID])
                                              {
                                                  [appDataManager.globalUserReplugArray[i] setValue:[NSNumber numberWithInteger:likeCount] forKey:@"likes_count"];
                                                  [appDataManager.globalUserReplugArray[i] setValue:@0 forKey:@"is_like"];
                                              }
                                          }
                                          
                                          if([operation.response statusCode]  == 400 ){
                                              NSLog(@"impo response %@",operation.response);
                                              [refreshControl endRefreshing];
                                              [CommonFunction removeActivityIndicator];
                                              APPDELEGATE.window.userInteractionEnabled=NO;
                                              [CommonFunction alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                          }
                                          else{
                                              
                                              if (wbServiceCount==1) {
                                                  wbServiceCount=0;
                                                  //  [CommonFunction showActivityIndicatorWithText:@""];
                                                  
                                              }
                                              else
                                              {
                                                  [CommonFunction removeActivityIndicator];
                                                  [CommonFunction alertTitle:@"" withMessage:@"Network Error..."];
                                              }
                                              
                                          }
                                          
                                      }];
}

-(void)replugAPost:(NSString *)postID labelName:(UILabel *)lbl indexpath:(NSInteger)arrindex
{
    NSString *url = [NSString stringWithFormat:@"posts/replug"];
    //http://192.168.0.22:8353/v1//posts/replug
    
    NSMutableDictionary *header =[[NSMutableDictionary alloc]initWithObjectsAndKeys:@"application/json",@"Content-Type",[NSUSERDEFAULTS valueForKey:kUSERTOKEN],@"User-Token", nil];
    
    NSMutableDictionary *param =[[NSMutableDictionary alloc]initWithObjectsAndKeys:postID,@"postId", nil];
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypePost withHttpHeaders:header withServiceName:url withParameters:param withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"--responce dict %@",responseDict);
        
        [CommonFunction removeActivityIndicator];
        if(responseDict==Nil)        {
            [CommonFunction alertTitle:@"" withMessage:@"Server Error"];
            NSInteger replugCount = [appDataManager.globalHomeFeedArray[arrindex][@"replugged_count"] integerValue];
            replugCount -= 1;
            lbl.text=[NSString stringWithFormat:@"%ld",(long)replugCount];
            [appDataManager.globalHomeFeedArray[arrindex] setValue:[NSNumber numberWithInteger:replugCount] forKey:@"replugged_count"];
            if (replugCount>0)
            {
                [appDataManager.globalHomeFeedArray[arrindex] setValue:@1 forKey:@"is_replug"];
            }
            else
            {
                [appDataManager.globalHomeFeedArray[arrindex] setValue:@0 forKey:@"is_replug"];
            }
        }
        else if([operation.response statusCode]==200)
        {
            //            if (appDataManager.globalHomeFeedArray.count>0)
            //            {
            //                [appDataManager.globalHomeFeedArray insertObject:[responseDict[@"postData"] mutableCopy] atIndex:0];
            //            }
            //            else
            //            {
            //                [appDataManager.globalHomeFeedArray addObject:[responseDict[@"postData"] mutableCopy]];
            //            }
            if (appDataManager.globalUserReplugArray.count>0)
            {
                [appDataManager.globalUserReplugArray insertObject:[responseDict[@"postData"] mutableCopy] atIndex:0];
            }
            else
            {
                [appDataManager.globalUserReplugArray addObject:[responseDict[@"postData"] mutableCopy]];
            }
            //            [self.tableHomeFeed beginUpdates];
            //            [self.tableHomeFeed insertRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:0 inSection:0]] withRowAnimation:UITableViewRowAnimationTop];
            //            [self.tableHomeFeed endUpdates];
            //            [self.tableHomeFeed setContentOffset:CGPointZero animated:NO];
            
            if (responseDict[@"postData"][@"avatar_image"])
            {
                NSString *imgStr = responseDict[@"postData"][@"avatar_image"];
                if (imgStr.length>0)
                {
                    NSString *username = responseDict[@"postData"][@"username"];
                    for (NSInteger i=0; i<appDataManager.globalHomeFeedArray.count; i++)
                    {
                        NSString *tempUsername = appDataManager.globalHomeFeedArray[i][@"username"];
                        NSString *tempImageStr = appDataManager.globalHomeFeedArray[i][@"avatar_image"];
                        if ([tempUsername isEqualToString:username] && ![imgStr isEqualToString:tempImageStr]) {
                            [appDataManager.globalHomeFeedArray[i] setValue:imgStr forKey:@"avatar_image"];
                        }
                    }
                }
            }
            
            if (appDataManager.globalHomeFeedArray.count >0) {
                
                CGRect visibleRect = (CGRect){.origin = self.tableHomeFeed.contentOffset, .size = self.tableHomeFeed.bounds.size};
                CGPoint visiblePoint = CGPointMake(CGRectGetMidX(visibleRect), CGRectGetMidY(visibleRect));
                visibleIndexPath = [self.tableHomeFeed indexPathForRowAtPoint:visiblePoint];
                
                NSURL *imgUrl=[NSURL URLWithString:appDataManager.globalHomeFeedArray[visibleIndexPath.row][@"avatar_image"]];
                
                //                if ([imgUrl.absoluteString.pathExtension isEqualToString:@"gif"])
                //                {
                //                    btnProfileImg.contentMode = UIViewContentModeScaleAspectFill;
                //                }
                //                else
                //                {
                //                    btnProfileImg.contentMode = UIViewContentModeScaleToFill;
                //                }
                
                [btnProfileImg sd_setImageWithURL:imgUrl forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"default.jpg"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                    if (error)
                    {
                        btnProfileImg.contentMode=UIViewContentModeScaleToFill;
                        btnProfileImg.imageView.contentMode=UIViewContentModeScaleToFill;
                    }
                    else
                    {
                        if ([[imgUrl.absoluteString pathExtension]isEqualToString:@"gif"])
                        {
                            btnProfileImg.contentMode=UIViewContentModeScaleAspectFill;
                            btnProfileImg.imageView.contentMode=UIViewContentModeScaleAspectFill;
                        }
                        else
                        {
                            btnProfileImg.contentMode=UIViewContentModeScaleToFill;
                            btnProfileImg.imageView.contentMode=UIViewContentModeScaleToFill;
                        }
                    }
                }];
                
                btnProfileImg.tag=visibleIndexPath.row;
                lblNameUserName.text=[[appDataManager.globalHomeFeedArray objectAtIndex:visibleIndexPath.row]objectForKey:@"username"];
                lblNameUserName.tag = visibleIndexPath.row;
                infBtn.tag=visibleIndexPath.row;
                infBtn.hidden = NO;
            }
        }
        else
        {
            [CommonFunction alertTitle:@"" withMessage:[responseDict objectForKey:@"msg"]];
            NSInteger replugCount = [appDataManager.globalHomeFeedArray[arrindex][@"replugged_count"] integerValue];
            replugCount -= 1;
            lbl.text=[NSString stringWithFormat:@"%ld",(long)replugCount];
            [appDataManager.globalHomeFeedArray[arrindex] setValue:[NSNumber numberWithInteger:replugCount] forKey:@"replugged_count"];
            if (replugCount>0)
            {
                [appDataManager.globalHomeFeedArray[arrindex] setValue:@1 forKey:@"is_replug"];
            }
            else
            {
                [appDataManager.globalHomeFeedArray[arrindex] setValue:@0 forKey:@"is_replug"];
            }
        }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          
                                          NSInteger replugCount = [appDataManager.globalHomeFeedArray[arrindex][@"replugged_count"] integerValue];
                                          replugCount -= 1;
                                          lbl.text=[NSString stringWithFormat:@"%ld",(long)replugCount];
                                          [appDataManager.globalHomeFeedArray[arrindex] setValue:[NSNumber numberWithInteger:replugCount] forKey:@"replugged_count"];
                                          if (replugCount>0)
                                          {
                                              [appDataManager.globalHomeFeedArray[arrindex] setValue:@1 forKey:@"is_replug"];
                                          }
                                          else
                                          {
                                              [appDataManager.globalHomeFeedArray[arrindex] setValue:@0 forKey:@"is_replug"];
                                          }
                                          
                                          if([operation.response statusCode]  == 400 ){
                                              NSLog(@"impo response %@",operation.response);
                                              
                                              [CommonFunction removeActivityIndicator];
                                              [CommonFunction alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                          }
                                          else{
                                              
                                              if (wbServiceCount==1) {
                                                  wbServiceCount=0;
                                                  //  [CommonFunction showActivityIndicatorWithText:@""];
                                              }
                                              else
                                              {
                                                  [CommonFunction removeActivityIndicator];
                                                  [CommonFunction alertTitle:@"" withMessage:@"Network Error..."];
                                              }
                                          }
                                      }];
}


-(void)unLikeAPost:(NSString *)postID cell:(HomePostedGifCustomeCell *)cell indexpath:(NSInteger)arrindex

{
    NSString *url = [NSString stringWithFormat:@"posts/like/%@",postID];
    //http://192.168.0.22:8353/v1/posts/like/{postId}
    
    //    for (AFHTTPRequestOperation *operation in [AFHTTPRequestOperationManager manager].operationQueue.operations)
    //    {
    //        NSString *path = operation.request.URL.path;
    //        if ([path containsString:url])
    //        {
    //            [operation cancel];
    //        }
    //    }
    
    NSMutableDictionary *header =[[NSMutableDictionary alloc]initWithObjectsAndKeys:@"application/json",@"Content-Type",[NSUSERDEFAULTS valueForKey:kUSERTOKEN],@"User-Token", nil];
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    
    __weak typeof(cell) weakCell = cell;
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypeDelete withHttpHeaders:header withServiceName:url withParameters:nil withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"--responce dict %@",responseDict);
        
        [CommonFunction removeActivityIndicator];
        if(responseDict==Nil)        {
            [CommonFunction alertTitle:@"" withMessage:@"Server Error"];
            NSInteger likeCount = [appDataManager.globalHomeFeedArray[arrindex][@"likes_count"] integerValue];
            
            [weakCell.imgLikeUnlikeImg setImage:[UIImage imageNamed:@"PG_LikeBtn"]];
            weakCell.imgLikeUnlikeImg.tag=1;
            
            likeCount += 1;
            weakCell.lblLikeCount.text=[NSString stringWithFormat:@"%ld",(long)likeCount];
            [appDataManager.globalHomeFeedArray[arrindex] setValue:[NSNumber numberWithInteger:likeCount] forKey:@"likes_count"];
            [appDataManager.globalHomeFeedArray[arrindex] setValue:@1 forKey:@"is_like"];
            
            NSString *postidstr = appDataManager.globalHomeFeedArray[arrindex][@"post_id"];
            for (NSInteger i=0; i<appDataManager.globalUserFeedArray.count; i++)
            {
                NSString *userPostID = appDataManager.globalUserFeedArray[i][@"post_id"];
                if ([userPostID isEqualToString:postidstr])
                {
                    [appDataManager.globalUserFeedArray[i] setValue:[NSNumber numberWithInteger:likeCount] forKey:@"likes_count"];
                    [appDataManager.globalUserFeedArray[i] setValue:@1 forKey:@"is_like"];
                }
            }
            for (NSInteger i=0; i<appDataManager.globalUserReplugArray.count; i++)
            {
                NSString *userPostID = appDataManager.globalUserReplugArray[i][@"post_id"];
                if ([userPostID isEqualToString:postID])
                {
                    [appDataManager.globalUserReplugArray[i] setValue:[NSNumber numberWithInteger:likeCount] forKey:@"likes_count"];
                    [appDataManager.globalUserReplugArray[i] setValue:@1 forKey:@"is_like"];
                }
            }
        }
        else if([operation.response statusCode]==200)
        {
            
        }
        else
        {
            [CommonFunction alertTitle:@"" withMessage:[responseDict objectForKey:@"msg"]];
            NSInteger likeCount = [appDataManager.globalHomeFeedArray[arrindex][@"likes_count"] integerValue];
            
            [weakCell.imgLikeUnlikeImg setImage:[UIImage imageNamed:@"PG_LikeBtn"]];
            weakCell.imgLikeUnlikeImg.tag=1;
            
            likeCount += 1;
            weakCell.lblLikeCount.text=[NSString stringWithFormat:@"%ld",(long)likeCount];
            [appDataManager.globalHomeFeedArray[arrindex] setValue:[NSNumber numberWithInteger:likeCount] forKey:@"likes_count"];
            [appDataManager.globalHomeFeedArray[arrindex] setValue:@1 forKey:@"is_like"];
            
            NSString *postidstr = appDataManager.globalHomeFeedArray[arrindex][@"post_id"];
            for (NSInteger i=0; i<appDataManager.globalUserFeedArray.count; i++)
            {
                NSString *userPostID = appDataManager.globalUserFeedArray[i][@"post_id"];
                if ([userPostID isEqualToString:postidstr])
                {
                    [appDataManager.globalUserFeedArray[i] setValue:[NSNumber numberWithInteger:likeCount] forKey:@"likes_count"];
                    [appDataManager.globalUserFeedArray[i] setValue:@1 forKey:@"is_like"];
                }
            }
            for (NSInteger i=0; i<appDataManager.globalUserReplugArray.count; i++)
            {
                NSString *userPostID = appDataManager.globalUserReplugArray[i][@"post_id"];
                if ([userPostID isEqualToString:postID])
                {
                    [appDataManager.globalUserReplugArray[i] setValue:[NSNumber numberWithInteger:likeCount] forKey:@"likes_count"];
                    [appDataManager.globalUserReplugArray[i] setValue:@1 forKey:@"is_like"];
                }
            }
        }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          
                                          NSInteger likeCount = [appDataManager.globalHomeFeedArray[arrindex][@"likes_count"] integerValue];
                                          
                                          [weakCell.imgLikeUnlikeImg setImage:[UIImage imageNamed:@"PG_LikeBtn"]];
                                          weakCell.imgLikeUnlikeImg.tag=1;
                                          
                                          likeCount += 1;
                                          weakCell.lblLikeCount.text=[NSString stringWithFormat:@"%ld",(long)likeCount];
                                          [appDataManager.globalHomeFeedArray[arrindex] setValue:[NSNumber numberWithInteger:likeCount] forKey:@"likes_count"];
                                          [appDataManager.globalHomeFeedArray[arrindex] setValue:@1 forKey:@"is_like"];
                                          
                                          NSString *postidstr = appDataManager.globalHomeFeedArray[arrindex][@"post_id"];
                                          for (NSInteger i=0; i<appDataManager.globalUserFeedArray.count; i++)
                                          {
                                              NSString *userPostID = appDataManager.globalUserFeedArray[i][@"post_id"];
                                              if ([userPostID isEqualToString:postidstr])
                                              {
                                                  [appDataManager.globalUserFeedArray[i] setValue:[NSNumber numberWithInteger:likeCount] forKey:@"likes_count"];
                                                  [appDataManager.globalUserFeedArray[i] setValue:@1 forKey:@"is_like"];
                                              }
                                          }
                                          for (NSInteger i=0; i<appDataManager.globalUserReplugArray.count; i++)
                                          {
                                              NSString *userPostID = appDataManager.globalUserReplugArray[i][@"post_id"];
                                              if ([userPostID isEqualToString:postID])
                                              {
                                                  [appDataManager.globalUserReplugArray[i] setValue:[NSNumber numberWithInteger:likeCount] forKey:@"likes_count"];
                                                  [appDataManager.globalUserReplugArray[i] setValue:@1 forKey:@"is_like"];
                                              }
                                          }
                                          if([operation.response statusCode]  == 400 ){
                                              NSLog(@"impo response %@",operation.response);
                                              
                                              [CommonFunction removeActivityIndicator];
                                              [CommonFunction alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                          }
                                          else{
                                              
                                              if (wbServiceCount==1) {
                                                  wbServiceCount=0;
                                                  //  [CommonFunction showActivityIndicatorWithText:@""];
                                                  
                                              }
                                              else
                                              {
                                                  [CommonFunction removeActivityIndicator];
                                                  [CommonFunction alertTitle:@"" withMessage:@"Network Error..."];
                                              }
                                              
                                          }
                                          
                                      }];
}


/// Even though NSURLCache *may* cache the results for remote images, it doesn't guarantee it.
/// Cache control headers or internal parts of NSURLCache's implementation may cause these images to become uncache.
/// Here we enfore strict disk caching so we're sure the images stay around.
- (void)loadAnimatedImageWithURL:(NSURL *const)url completion:(void (^)(FLAnimatedImage *animatedImage))completion
{
    NSString *const filename = url.lastPathComponent;
    NSString *const diskPath = [NSHomeDirectory() stringByAppendingPathComponent:filename];
    
    NSData * __block animatedImageData = [[NSFileManager defaultManager] contentsAtPath:diskPath];
    FLAnimatedImage * __block animatedImage = [FLAnimatedImage animatedImageWithGIFData:animatedImageData generateThumbnail:NO];
    
    if (animatedImage) {
        if (completion) {
            completion(animatedImage);
        }
    } else {
        [[[NSURLSession sharedSession] dataTaskWithURL:url completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            animatedImageData = data;
            animatedImage = [FLAnimatedImage animatedImageWithGIFData:animatedImageData generateThumbnail:NO];
            if (animatedImage) {
                if (completion) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        completion(animatedImage);
                    });
                }
                [data writeToFile:diskPath atomically:YES];
            }
        }] resume];
    }
}


#pragma mark - NSURLSession Delegate

- (NSURLSession *)backgroundSession {
    static NSURLSession *session = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        // Session Configuration
        NSURLSessionConfiguration *sessionConfiguration = [NSURLSessionConfiguration backgroundSessionConfigurationWithIdentifier:@"com.mobiletuts.Singlecast.BackgroundSession"];
        
        // Initialize Session
        session = [NSURLSession sessionWithConfiguration:sessionConfiguration delegate:self delegateQueue:nil];
    });
    
    return session;
}

- (void)downloadGifFromUrl:(NSURL *)gifUrl {
    
    [[AppWebHandler sharedInstance]initializeBackgroundSession];
    if (gifUrl) {
        
        [[AppWebHandler sharedInstance]downloadTaskForUrl:gifUrl];
        
        // Schedule Download Task
        //        [[self.session downloadTaskWithURL:gifUrl] resume];
        
        // Update Progress Buffer
        [[AppWebHandler sharedInstance].dictProgressTask setObject:@(0.0) forKey:[gifUrl absoluteString]];
        //        [self.progressBuffer setObject:@(0.0) forKey:[gifUrl absoluteString]];
    }
}




- (void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didResumeAtOffset:(int64_t)fileOffset expectedTotalBytes:(int64_t)expectedTotalBytes {
    NSLog(@"%s", __PRETTY_FUNCTION__);
}


- (void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didWriteData:(int64_t)bytesWritten totalBytesWritten:(int64_t)totalBytesWritten totalBytesExpectedToWrite:(int64_t)totalBytesExpectedToWrite {
    // Calculate Progress
    double progress = (double)totalBytesWritten / (double)totalBytesExpectedToWrite;
    
    // Update Progress Buffer
    NSURL *URL = [[downloadTask originalRequest] URL];
    [[AppWebHandler sharedInstance].dictProgressTask setObject:@(progress) forKey:[URL absoluteString]];
    //    [self.progressBuffer setObject:@(progress) forKey:[URL absoluteString]];
    
    // Update Table View Cell
    HomePostedGifCustomeCell *cell = [self cellForForDownloadTask:downloadTask];
    __weak typeof(cell) weakCell =cell;
    dispatch_async(dispatch_get_main_queue(), ^{
        [weakCell setProgress:progress];
    });
}


- (void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didFinishDownloadingToURL:(NSURL *)location {
    // Write File to Disk
    
    // NSData *imageData = [NSData dataWithContentsOfURL:lo];
    NSData *imageData = [NSData dataWithContentsOfFile:[location path]];
    NSLog(@"location is %@",location.absoluteString);
    
    [self moveFileWithURL:location downloadTask:downloadTask];
    NSURL *URL = [[downloadTask originalRequest] URL];
    
    [[AppWebHandler sharedInstance].dictProgressTask setObject:@(1.0) forKey:[URL absoluteString]];
    
    // Update Table View Cell
    HomePostedGifCustomeCell *cell = [self cellForForDownloadTask:downloadTask];
    
    // Update Progress Buffer
    //    [self.progressBuffer setObject:@(1.0) forKey:[URL absoluteString]];
    
    __weak typeof(cell) weakCell =cell;
    __weak typeof(self) weakSelf = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        FLAnimatedImage *image = [FLAnimatedImage animatedImageWithGIFData:imageData generateThumbnail:NO];
        [weakCell.imgVwGif setAnimatedImage:image];
        [weakCell.progresView setHidden:true];
        [weakCell.imgVwGif setHidden:false];
        if (weakCell)
        {
            [weakSelf.tableHomeFeed reloadData];
        }
    });
    // Invoke Background Completion Handler
    [self invokeBackgroundSessionCompletionHandler];
}


- (void)moveFileWithURL:(NSURL *)URL downloadTask:(NSURLSessionDownloadTask *)downloadTask {
    // Filename
    NSString *fileName = [[[downloadTask originalRequest] URL] lastPathComponent];
    
    // Local URL
    NSURL *localURL = [self URLForGiffWithName:fileName];
    
    NSFileManager *fm = [NSFileManager defaultManager];
    
    if ([fm fileExistsAtPath:[URL path]]) {
        NSError *error = nil;
        [fm moveItemAtURL:URL toURL:localURL error:&error];
        //        [_tempDirectUrls addObject:[localURL path]];
        if (error) {
            NSLog(@"Unable to move temporary file to destination. %@, %@", error, error.userInfo);
        }
    }
}


- (NSURL *)URLForGiffWithName:(NSString *)name {
    if (!name) return nil;
    return [self.gifDirectory URLByAppendingPathComponent:name];
}

- (NSURL *)gifDirectory {
    //    NSURL *documents = [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
    //    NSURL *gifs = [documents URLByAppendingPathComponent:@"Gif"];
    //
    //    NSFileManager *fm = [NSFileManager defaultManager];
    //
    //    if (![fm fileExistsAtPath:[gifs path]]) {
    //        NSError *error = nil;
    //        [fm createDirectoryAtURL:gifs withIntermediateDirectories:YES attributes:nil error:&error];
    //
    //        if (error) {
    //            NSLog(@"Unable to create episodes directory. %@, %@", error, error.userInfo);
    //        }
    //    }
    
    return [AppWebHandler sharedInstance].tempDirectory;
}


- (HomePostedGifCustomeCell *)cellForForDownloadTask:(NSURLSessionDownloadTask *)downloadTask {
    // Helpers
    
    HomePostedGifCustomeCell *cell = nil;
    NSURL *URL = [[downloadTask originalRequest] URL];
    
    for (NSInteger i =0; i<appDataManager.globalHomeFeedArray.count; i++)
    {
        BOOL flag = NO;
        NSDictionary *dict = appDataManager.globalHomeFeedArray[i];
        
        NSString *strImgUrl=[dict valueForKey:@"post_image"];
        NSURL *mainURL=[NSURL URLWithString:[NSString stringWithFormat:@"%@",strImgUrl]];
        //   NSURL *feedItemURL = [self urlForFeedItem:feedItem];
        
        
        @try {
            if ([URL isEqual:mainURL]) {
                NSUInteger index = [appDataManager.globalHomeFeedArray indexOfObject:dict];
                NSIndexPath *indexpath = [NSIndexPath indexPathForRow:index inSection:0];
                if ([[self.tableHomeFeed indexPathsForVisibleRows]containsObject:indexpath]) {
                    cell = (HomePostedGifCustomeCell *)[self.tableHomeFeed cellForRowAtIndexPath:indexpath];
                    flag = YES;
                }
            }
        } @catch (NSException *exception) {
            [self.tableHomeFeed reloadData];
        } @finally {
            if (flag) {
                break;
            }
        }
    }
    
    return cell;
}

- (void)invokeBackgroundSessionCompletionHandler {
    
    [[AppWebHandler sharedInstance].httpSessionBackground getTasksWithCompletionHandler:^(NSArray *dataTasks, NSArray *uploadTasks, NSArray *downloadTasks) {
        NSUInteger count = [dataTasks count] + [uploadTasks count] + [downloadTasks count];
        
        if (!count) {
            AppDelegate *applicationDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
            void (^backgroundSessionCompletionHandler)() = [applicationDelegate backgroundSessionCompletionHandler];
            
            if (backgroundSessionCompletionHandler) {
                [applicationDelegate setBackgroundSessionCompletionHandler:nil];
                backgroundSessionCompletionHandler();
            }
        }
    }];
}


#pragma mark -
#pragma mark Get Post Details
- (void)getGifLatestDetailsWithID:(NSString *)postId
{
    NSString *url = [NSString stringWithFormat:@"posts/%@",postId];
    
    NSMutableDictionary *header =[[NSMutableDictionary alloc]initWithObjectsAndKeys:@"application/json",@"Content-Type",[NSUSERDEFAULTS valueForKey:kUSERTOKEN],@"User-Token", nil];
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypeGet withHttpHeaders:header withServiceName:url withParameters:nil withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"--responce dict %@",responseDict);
        
        if(responseDict==Nil)        {
            //            [CommonFunction alertTitle:@"" withMessage:@"Server Error"];
        }
        else if([operation.response statusCode]==200)
        {
            BOOL flag = NO;
            for (NSInteger i=0; i<appDataManager.globalUserFeedArray.count; i++)
            {
                NSMutableDictionary *dict = appDataManager.globalUserFeedArray[i];
                if ([dict[@"post_id"] isEqualToString:postId])
                {
                    NSInteger count = [responseDict[@"post"][@"comments_count"] integerValue];
                    BOOL isComent = [responseDict[@"post"][@"is_comment"] boolValue];
                    [appDataManager.globalUserFeedArray[i] setObject:[NSNumber numberWithInteger:count] forKey:@"comments_count"];
                    [appDataManager.globalUserFeedArray[i] setObject:[NSNumber numberWithBool:isComent] forKey:@"is_comment"];
                    flag = YES;
                }
                if (flag) {
                    break;
                }
            }
            
            flag = NO;
            for (NSInteger i=0; i<appDataManager.globalUserReplugArray.count; i++)
            {
                NSMutableDictionary *dict = appDataManager.globalUserReplugArray[i];
                if ([dict[@"post_id"] isEqualToString:postId])
                {
                    NSInteger count = [responseDict[@"post"][@"comments_count"] integerValue];
                    BOOL isComent = [responseDict[@"post"][@"is_comment"] boolValue];
                    [appDataManager.globalUserReplugArray[i] setObject:[NSNumber numberWithInteger:count] forKey:@"comments_count"];
                    [appDataManager.globalUserReplugArray[i] setObject:[NSNumber numberWithBool:isComent] forKey:@"is_comment"];
                    flag = YES;
                }
                if (flag) {
                    break;
                }
            }
            
            
            flag = NO;
            for (NSInteger i=0; i<appDataManager.globalHomeFeedArray.count; i++)
            {
                NSMutableDictionary *dict = appDataManager.globalHomeFeedArray[i];
                if ([dict[@"post_id"] isEqualToString:postId])
                {
                    NSInteger count = [responseDict[@"post"][@"comments_count"] integerValue];
                    BOOL isComent = [responseDict[@"post"][@"is_comment"] boolValue];
                    [appDataManager.globalHomeFeedArray[i] setObject:[NSNumber numberWithInteger:count] forKey:@"comments_count"];
                    [appDataManager.globalHomeFeedArray[i] setObject:[NSNumber numberWithBool:isComent] forKey:@"is_comment"];
                    flag = YES;
                }
                if (flag) {
                    break;
                }
            }
            
            [self.tableHomeFeed reloadData];
        }
        else
        {
            //            [CommonFunction alertTitle:@"" withMessage:[responseDict objectForKey:@"msg"]];
        }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          if([operation.response statusCode]  == 400 ){
                                              NSLog(@"impo response %@",operation.response);
                                              
                                              //                                              [CommonFunction alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                          }
                                          else{
                                              
                                              if (wbServiceCount==1) {
                                                  wbServiceCount=0;
                                              }
                                              else
                                              {
                                                  //                                                  [CommonFunction alertTitle:@"" withMessage:@"Network Error..."];
                                              }
                                          }
                                      }];
}

#pragma mark -
#pragma mark Handle UserName Tap
- (void)customTableViewCellDidTapOnUserHandle:(NSString *)userHandle
{
    NSString *loggedInUser = [NSUSERDEFAULTS objectForKey:kUSERID];
    if ([loggedInUser isEqualToString:userHandle])
    {
        
    }
    else
    {
        OtherUserProfileVC *ouVC=[[OtherUserProfileVC alloc]init];
        ouVC.hidesBottomBarWhenPushed=YES;
        ouVC.strUserName=userHandle;
        [self.navigationController pushViewController:ouVC animated:YES];
    }
}

#pragma mark -
#pragma mark Handle HashTag Tap
- (void)customTableViewCellDidTapHashTagHandle:(NSString *)hashtagHandle
{
    NSString *categoryName = [NSString stringWithFormat:@"#%@",hashtagHandle];
    NSString *categoryTag = hashtagHandle;
    CategoryDetailVC *categorydetailVC = [[CategoryDetailVC alloc]initWithNibName:NSStringFromClass([CategoryDetailVC class]) bundle:nil];
    categorydetailVC.hidesBottomBarWhenPushed=YES;
    categorydetailVC.categoryTag = categoryTag;
    categorydetailVC.categoryName = categoryName;
    [self.navigationController pushViewController:categorydetailVC animated:YES];
}

@end
