//
//  HomeFeedVC.h
//  GiffPlugApp
//
//  Created by Kshitij Godara on 08/01/16.
//  Copyright © 2016 Kshitij Godara. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomeFeedVC : UIViewController<NSURLSessionDelegate,NSURLSessionDownloadDelegate>


@property(nonatomic,strong)    IBOutlet UIButton *infBtn;
//@property(nonatomic,strong)NSMutableArray *arrFeedData;
@property (nonatomic, strong) NSURLSession *session;
@property (strong, nonatomic) NSMutableDictionary *progressBuffer;
@property (strong,nonatomic) NSMutableArray *tempDirectUrls;
@property (strong,nonatomic) NSMutableDictionary *imageBuffer;
@property(nonatomic,strong)NSString *strTest;
//@property (strong,nonatomic)NSOperationQueue *imageLoadingOperationQueue;
@end
