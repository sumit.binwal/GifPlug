//
//  ForgotPasswordVC.m
//  GiffPlugApp
//
//  Created by Sumit Sharma on 17/02/16.
//  Copyright © 2016 Kshitij Godara. All rights reserved.
//
#import "FollowUnFollowScreen.h"
#import "GuidedContentScreen.h"
#import "ValidateUserNameVC.h"
#import "SignUpVC.h"
@interface ValidateUserNameVC ()<UIAlertViewDelegate,UITextFieldDelegate>
{
    
    __weak IBOutlet UIView *vwBcgroundVdo;
    __weak IBOutlet UITextField *txtEmailAddress;
        CMTime vdoCurrentTime;
}
@property(strong,nonatomic)AVPlayer *avPlayer;
@property(strong,nonatomic)AVPlayerItem *avPlayerItem;
@property(strong,nonatomic)AVAsset *avAsset;
@property(strong,nonatomic)AVPlayerLayer *avPlayerLayer;
@end

@implementation ValidateUserNameVC
@synthesize dataDict;
- (void)viewDidLoad {
    [super viewDidLoad];
    [txtEmailAddress setValue:[UIColor colorWithRed:255.0f/255.0f green:255.0f/255.0f blue:255.0f/255.0f alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
    wbServiceCount=0;
    
    [self playVideo];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appEnteredForeground) name:UIApplicationWillEnterForegroundNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appEnteredBackground) name:UIApplicationDidEnterBackgroundNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(playerItemDidReachEnd:)
                                                 name:AVPlayerItemDidPlayToEndTimeNotification
                                               object:[self.avPlayer currentItem]];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[UIApplication sharedApplication]setStatusBarHidden:NO withAnimation:UIStatusBarAnimationFade];
    [[UIApplication sharedApplication]setStatusBarStyle:UIStatusBarStyleLightContent];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationWillEnterForegroundNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidEnterBackgroundNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:AVPlayerItemDidPlayToEndTimeNotification object:nil];
    [super viewWillDisappear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Play Video In Av Player Method

-(void)playVideo
{
    NSString *filepath1 = [[NSBundle mainBundle] pathForResource:@"newlogin" ofType:@"mov"];
    NSURL *fileURL1 = [NSURL fileURLWithPath:filepath1];
    
    self.avPlayer = [AVPlayer playerWithURL:fileURL1]; //
    
    self.avPlayerLayer = [AVPlayerLayer layer];
    
    [self.avPlayerLayer setPlayer:self.avPlayer];
    [self.avPlayerLayer setFrame:[UIScreen mainScreen].bounds];
    //[layer setBackgroundColor:[UIColor redColor].CGColor];
    [self.avPlayerLayer setVideoGravity:AVLayerVideoGravityResizeAspectFill];
    
    [vwBcgroundVdo.layer addSublayer:self.avPlayerLayer];
    [self.avPlayer play];
    
    self.avPlayer.actionAtItemEnd = AVPlayerActionAtItemEndNone;
    
    
}
#pragma mark - AvPlayer Background Forground Even Method

-(void) appEnteredForeground {
    // Application Goes In Forground and play video
    [self.avPlayer seekToTime:vdoCurrentTime toleranceBefore:kCMTimeZero toleranceAfter:kCMTimeZero completionHandler:^(BOOL finished) {
        [self.avPlayer play];
    }];
}

-(void) appEnteredBackground {
    // Application Goes In Background
    [self.avPlayer pause];
    AVPlayerItem *currentItem = self.avPlayer.currentItem;
    
    vdoCurrentTime = currentItem.currentTime;
}
- (void)playerItemDidReachEnd:(NSNotification *)notification {
    // video did reach end and repeate video
    AVPlayerItem *p = [notification object];
    [p seekToTime:kCMTimeZero];
}





- (IBAction)backBtnClicked:(id)sender {
txtEmailAddress.text=@"";
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (IBAction)nextBtnClicked:(id)sender {
    [self.view endEditing:YES];
    if ([CommonFunction isValueNotEmpty:txtEmailAddress.text]) {

            if ([CommonFunction reachabiltyCheck]) {
                [CommonFunction showActivityIndicatorWithText:@""];
                [self fbLoginAPI:dataDict];
            }
    }
    else
    {
        [CommonFunction alertTitle:@"" withMessage:@"Please enter username."];
    }
    
}

#pragma mark - UIAlertView Delegate Methd
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag==200) {
        txtEmailAddress.text=@"";
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

#pragma mark - UITextField Delegate method
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField == txtEmailAddress)
    {
        if ([string isEqualToString:@" "] )
        {
            return NO;
        }
    }
    return YES;
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{

    return [txtEmailAddress resignFirstResponder];
    
}

#pragma mark - WebService API
-(void)fbLoginAPI:(NSMutableDictionary *)dataDict
{
    [NSUSERDEFAULTS removeObjectForKey:kLOGINPASSWORD];
    [NSUSERDEFAULTS removeObjectForKey:kLOGINUSERNAME];
    [NSUSERDEFAULTS synchronize];
    NSString *url = [NSString stringWithFormat:@"users/socialSignup"];
    //http://192.168.0.22:8353/v1/users/socialSignup
    NSString *strEmail=[dataDict valueForKey:@"email"];
    if (strEmail.length<1) {
        strEmail=@"";
    }
    NSString *strUsername=txtEmailAddress.text;
    
    NSMutableDictionary *header =[[NSMutableDictionary alloc]initWithObjectsAndKeys:@"application/json",@"Content-Type", nil];
    
    NSMutableDictionary *param=[[NSMutableDictionary alloc]initWithObjectsAndKeys:strUsername,@"username",[dataDict valueForKey:@"id"],@"social_id",[dataDict valueForKey:@"first_name"],@"name",@"facebook",@"social_type",@"ios",@"device_type",appDeviceToken,@"device_token",strEmail,@"email", nil];
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypePost withHttpHeaders:header withServiceName:url withParameters:param withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"responce dict %@",responseDict);
        
        [CommonFunction removeActivityIndicator];
        if(responseDict==Nil)        {
            [CommonFunction alertTitle:@"" withMessage:@"Server Error"];
            
        }
        else if([operation.response statusCode]==200)
            
        {
            //[CommonFunction alertTitle:@"" withMessage:[responseDict objectForKey:@"msg"]];
            
            
            NSLog(@"%@",[NSUSERDEFAULTS objectForKey:kLOGINUSERNAME]);
            
            [NSUSERDEFAULTS setObject:[responseDict valueForKey:@"username"] forKey:kUSERNAME];
            [NSUSERDEFAULTS setObject:[responseDict valueForKey:@"token"] forKey:kUSERTOKEN];
            [NSUSERDEFAULTS setObject:[responseDict objectForKey:@"categories"] forKey:kFOLLOWINGCATEGORY];
            [NSUSERDEFAULTS setObject:[[responseDict valueForKey:@"hasCategory"] stringValue] forKey:kISCATEGORYSUBMITTED];
            [NSUSERDEFAULTS setObject:[[responseDict valueForKey:@"hasFollowing"] stringValue] forKey:kISFOLLOWINGSUBMITTED];
            
            [NSUSERDEFAULTS synchronize];
            NSLog(@"%@",[NSUSERDEFAULTS valueForKey:kLOGINUSERNAME]);
            if ([[NSUSERDEFAULTS valueForKey:kISCATEGORYSUBMITTED] isEqualToString:@"0"]) {
                GuidedContentScreen *gcS=[[GuidedContentScreen alloc]initWithNibName:@"GuidedContentScreen" bundle:nil];
                [self.navigationController pushViewController:gcS animated:YES];
            }
            else if ([[NSUSERDEFAULTS valueForKey:kISFOLLOWINGSUBMITTED] isEqualToString:@"0"])
            {
                FollowUnFollowScreen *fufs=[[FollowUnFollowScreen alloc]initWithNibName:@"FollowUnFollowScreen" bundle:nil];
                NSLog(@"%@",[NSUSERDEFAULTS objectForKey:kFOLLOWINGCATEGORY]);
                [self.navigationController pushViewController:fufs animated:YES];
            }
            else
            {
                [CommonFunction changeRootViewController:YES];
                [[UITabBar appearance] setTintColor:[UIColor colorWithRed:255.0f/255.0f green:33.0f/255.0f blue:87.0f/255.0f alpha:1]];
            }
            
            
        }
        else
        {
//            if([[responseDict objectForKey:@"msg"]isEqualToString:@"EMAIL_ALREADY_EXIST"])
//            {
//                UIAlertController *alert=[UIAlertController alertControllerWithTitle:[NSString stringWithFormat:@"Your Facebook email address, %@ , already has an account with GifPlug",strEmail ]message:@"" preferredStyle:UIAlertControllerStyleAlert];
//                UIAlertAction *newAccount=[UIAlertAction actionWithTitle:@"New Account" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
//                    SignUpVC *suvc=[[SignUpVC alloc]init];
//                    [self.navigationController pushViewController:suvc animated:YES];
//                }];
//                UIAlertAction *lgin=[UIAlertAction actionWithTitle:@"Log in" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
//                    
//                }];
//                [alert addAction:newAccount];
//                [alert addAction:lgin];
//                [self presentViewController:alert animated:YES completion:nil];
//            }
            if([[responseDict objectForKey:@"msg"]isEqualToString:@"USERNAME_ALREADY_EXIST"])
            {
                [CommonFunction showAlertWithTitle:@"GifPlug" message:@"This username is already Exist." onViewController:self useAsDelegate:NO dismissBlock:^{
                
                }];
                
            }
            else
            {
                [CommonFunction alertTitle:@"" withMessage:[responseDict objectForKey:@"msg"]];
            }
            
        }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          if([operation.response statusCode]  == 400 ){
                                              NSLog(@"impo response %@",operation.response);
                                              
                                              [CommonFunction removeActivityIndicator];
                                              [CommonFunction alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                              
                                              
                                          }
                                          else{
                                              
                                              if (wbServiceCount==1) {
                                                  wbServiceCount=0;
                                                  [CommonFunction showActivityIndicatorWithText:@""];
                                                  [self fbLoginAPI:dataDict];
                                              }
                                              else
                                              {
                                                  [CommonFunction removeActivityIndicator];
                                                  [CommonFunction alertTitle:@"" withMessage:@"Network Error..."];
                                              }
                                              
                                          }
                                          
                                      }];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
