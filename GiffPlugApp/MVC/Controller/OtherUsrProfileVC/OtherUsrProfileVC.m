//
//  UserProfileVC.m
//  GiffPlugApp
//
//  Created by Sumit Sharma on 02/02/16.
//  Copyright © 2016 Kshitij Godara. All rights reserved.
//
#import "EditUserProfileVC.h"
#import "OtherUsrProfileVC.h"
#import "LoginVC.h"

@interface OtherUsrProfileVC ()<UIScrollViewDelegate,UITableViewDelegate,UITableViewDataSource>
{
    IBOutlet UILabel *lblFollowersCount;
    IBOutlet UILabel *lblFollowingCount;
    IBOutlet UIImageView *imgProfileImg;
    IBOutlet UIImageView *imgProfileBgImg;
    IBOutlet UILabel *lblUsername;
    IBOutlet UILabel *lblUserBio;
    IBOutlet UILabel *lblGifPostedCOunt;
    IBOutlet UILabel *lblReplugCount;
    __weak IBOutlet UIButton *btnSetting;
    IBOutlet UITableView *tblVwData;
    CGFloat scaleX, scaleY, scaleSixPlusX, scaleSixPlusY;
    NSMutableDictionary *dictData;
    
}
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *settingBtnWidthCnstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *settingBtnTrailingCnstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *settingBtnTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *gifRepluggedCountTopConstrant;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *gifpostedCountTopConstrant;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *headerTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *headerHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *nameTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imgProfileVwCenterContraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imgProfileVwWidthContraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imgProfileVwTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *replugLeadingConstraintFor6Plus;
@property (weak, nonatomic) IBOutlet UIView *tableHeaderView;
@end

@implementation OtherUsrProfileVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUpView];
    // Do any additional setup after loading the view from its nib.
}

-(void)setUpView
{
    imgProfileImg.clipsToBounds=YES;
    imgProfileImg.layer.borderColor=[UIColor colorWithRed:106.0f/255.0f green:106.0f/255.0f blue:106.0f/255.0f alpha:1.0f].CGColor;
    imgProfileImg.layer.borderWidth=1.0f;
    
    scaleX = [[UIScreen mainScreen]bounds].size.width/320;
    
    scaleY = [[UIScreen mainScreen]bounds].size.height/568;
    
    
    scaleSixPlusX = [[UIScreen mainScreen]bounds].size.width/414;
    
    scaleSixPlusY = [[UIScreen mainScreen]bounds].size.height/736;

    self.settingBtnTopConstraint.constant=32*scaleSixPlusY;
    
    imgProfileImg.layer.cornerRadius=imgProfileImg.frame.size.width/2 *scaleX;

    self.headerHeightConstraint.constant = self.headerHeightConstraint.constant * scaleX;
    lblUsername.font = [UIFont fontWithName:lblUsername.font.fontName size:30*scaleX];
    
    if ([UIScreen mainScreen].bounds.size.width>375) {
    _replugLeadingConstraintFor6Plus.constant=20*scaleX;
    }

}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    tblVwData.contentOffset=CGPointZero;
    if ([CommonFunction reachabiltyCheck]) {
        [CommonFunction showActivityIndicatorWithText:@""];
        [self getUserProfile];
    }
    else
    {
        [CommonFunction alertTitle:@"" withMessage:@"Please check your network connection."];
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - IBAction Button Methods
- (IBAction)editProfileBtnClicked:(id)sender {
    
    EditUserProfileVC *eupvc=[[EditUserProfileVC alloc]init];
    eupvc.dictData=dictData;
    [self presentViewController:eupvc animated:YES completion:nil];
}
- (IBAction)settingBtnClicked:(id)sender
{
    
    UIAlertController * actionSheet=   [UIAlertController
                                  alertControllerWithTitle:@"Logout"
                                  message:@"Logout From GifPlug."
                                  preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"Logout"
                         style:UIAlertActionStyleDestructive
                         handler:^(UIAlertAction * action)
                         {
                             
                             UIAlertController * alert=   [UIAlertController
                                                           alertControllerWithTitle:@"Logout"
                                                           message:@"Are you sure you want to logout."
                                                           preferredStyle:UIAlertControllerStyleAlert];

                             UIAlertAction *ok=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                                 
                                 
                                     LoginVC *fufs=[[LoginVC alloc]initWithNibName:@"LoginVC" bundle:nil];
                                     APPDELEGATE.navigationController=[[UINavigationController alloc]initWithRootViewController:fufs];
                                     self.navigationController.navigationBarHidden=YES;
                                     [APPDELEGATE.window.window setRootViewController:self.navigationController];
                                 
                                     [NSUSERDEFAULTS removeObjectForKey:@"previousSelectedVC"];
                                 
                                     [NSUSERDEFAULTS removeObjectForKey:kUSERTOKEN];
                                     [CommonFunction changeRootViewController:NO];
                                 
                             }];;
                             
                             UIAlertAction* simpleCancle = [UIAlertAction
                                                            actionWithTitle:@"Cancel"
                                                            style:UIAlertActionStyleCancel
                                                            handler:^(UIAlertAction * action)
                                                            {
                                                                [actionSheet dismissViewControllerAnimated:YES completion:nil];
                                                                
                                                            }];
                             [alert addAction:ok];
                             [alert addAction:simpleCancle];
                                 [self presentViewController:alert animated:YES completion:nil];

                             
                         }];
    
    
    UIAlertAction* simpleCancle = [UIAlertAction
                                   actionWithTitle:@"Cancel"
                                   style:UIAlertActionStyleCancel
                                   handler:^(UIAlertAction * action)
                                   {
                                       [actionSheet dismissViewControllerAnimated:YES completion:nil];
                                       
                                   }];
    
    [actionSheet addAction:ok];
    [actionSheet addAction:simpleCancle];
    
    [self presentViewController:actionSheet animated:YES completion:nil];

    
    
    
    
    
    
    
    
    

}
- (IBAction)gifRepluggedBtnClicked:(id)sender {
    NSLog(@"Gif Replugged Btn Clicked");
}
- (IBAction)gifPostedBtnClicked:(id)sender {
        NSLog(@"Gif Posted Btn Clicked");
}
- (IBAction)followersBtnClicked:(id)sender {
            NSLog(@"followersBtnClicked Posted Btn Clicked");
}
- (IBAction)followingBtnClicked:(id)sender {
            NSLog(@"followingBtnClicked Posted Btn Clicked");
}


#pragma mark - ScrollView Delegate method
-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    
    NSLog(@"%f",scrollView.contentOffset.y);
    
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    NSLog(@"%f",scrollView.contentOffset.y);
    if (scrollView.contentOffset.y<=0.0f)
    {
        
        [self.tableHeaderView setFrame:CGRectMake(0.0f, 0.0f, 320.0f*scaleX, 262.0f*scaleY)];
        self.headerTopConstraint.constant=0;
        self.tableHeaderView.alpha = 1;
        
       // [lblUsername setFrame:CGRectMake(89.0f*scaleX, 122.0f*scaleY, lblUsername.frame.size.width, lblUsername.frame.size.height)];
        //[btnSetting setFrame:CGRectMake(89.0f*scaleX, 122.0f*scaleY, lblUsername.frame.size.width, lblUsername.frame.size.height)];
        //[btnSetting setFrame:CGRectMake(117*scaleX, 30.0f*scaleY, 86*scaleX, 86*scaleY)];
        self.settingBtnTopConstraint.constant=25*scaleY;
        self.settingBtnTrailingCnstraint.constant=10*scaleX;
       // self.settingBtnWidthCnstraint.constant=23.0f*scaleX;

        [imgProfileImg setFrame:CGRectMake(117*scaleX, 30.0f*scaleY, 86*scaleX, 86*scaleX)];
        self.nameTopConstraint.constant=0/568*scaleX;
        self.imgProfileVwCenterContraint.constant = 0;
//      self.imgProfileVwTopConstraint.constant = MAX(0*scaleX, offsetY);
        self.imgProfileVwWidthContraint.constant=86/320*scaleX;
        
        imgProfileImg.layer.cornerRadius=imgProfileImg.frame.size.width/2 ;
        imgProfileImg.clipsToBounds=YES;
        
        self.gifpostedCountTopConstrant.constant=0;
        self.gifRepluggedCountTopConstrant.constant=0;
        self.imgProfileVwCenterContraint.constant =0;
       
//        self.nameTopConstraint.constant= 0;
        
//        self.imgProfileVwWidthContraint.constant = MAX(0*scaleX, scrollView.contentOffset.x*1.2);
//        
//        self.imgProfileVwCenterContraint.constant = MAX(0*scaleX, scrollView.contentOffset.x);
//        self.imgProfileVwTopConstraint.constant = MAX(0*scaleX, scrollView.contentOffset.y);
//        self.gifpostedCountTopConstrant.constant=MAX(0*scaleX, scrollView.contentOffset.y*2);
//        self.gifRepluggedCountTopConstrant.constant=MAX(0*scaleX, scrollView.contentOffset.y*2);
//        
//        imgProfileImg.layer.cornerRadius=imgProfileImg.frame.size.width/2 *scaleX;

    }
    else
    {

        CGFloat heightRemained=self.tableHeaderView.frame.size.height - 58.0f*scaleX;
        
        CGFloat offsetY = -+scrollView.contentOffset.y;
        
        if (offsetY <= -heightRemained)
        {
            lblUsername.lineBreakMode = NSLineBreakByTruncatingTail;
            self.headerTopConstraint.constant = - heightRemained;
            [UIView transitionWithView:self.tableHeaderView duration:.3 options:UIViewAnimationOptionCurveEaseIn animations:^{
                  self.tableHeaderView.alpha=1.0f;
//                imgProfileImg.frame= CGRectMake(imgProfileImg.frame.origin.x, imgProfileImg.frame.origin.y, btnSetting.frame.size.width, btnSetting.frame.size.height);
//                imgProfileImg.center = CGPointMake(imgProfileImg.center.x, btnSetting.center.y);
            
            } completion:nil];
        
            self.nameTopConstraint.constant= MAX(-100*scaleX, offsetY/1.5);
            self.imgProfileVwCenterContraint.constant = MAX(-140*scaleX, offsetY);
            //[imgProfileImg setFrame:CGRectMake(-80, 0, 44.0f, 0.0)];
            //self.imgProfileVwWidthContraint.constant = MAX(-80*scaleX, offsetY);
            self.imgProfileVwTopConstraint.constant = MAX(-7*scaleX, offsetY);
            //[scrollView setContentOffset:CGPointZero];
            [UIView animateWithDuration:.1 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
                CGRect frame = imgProfileImg.frame;
                //frame.size.height =MIN(23, imgProfileImg.frame.size.height);
                //frame.size.width = MIN(23, imgProfileImg.frame.size.width);
                imgProfileImg.frame= frame;
                imgProfileImg.layer.cornerRadius=imgProfileImg.frame.size.width/2;
            } completion:nil];
            
            
            
        }
        else
        {

            lblUsername.lineBreakMode = NSLineBreakByWordWrapping;
            self.headerTopConstraint.constant = offsetY;
            CGFloat origin = self.tableHeaderView.frame.origin.y;
            CGFloat delta = fabs(origin - (-offsetY));
            //self.settingBtnTopConstraint.constant=MAX(26*scaleY,offsetY );
            self.tableHeaderView.alpha = 1 - delta/self.tableHeaderView.frame.size.height*0.4;
            self.nameTopConstraint.constant= MAX(-100*scaleX, offsetY/1.5);
            self.imgProfileVwWidthContraint.constant = MAX(-60*scaleX, offsetY*1.2);
            self.imgProfileVwCenterContraint.constant = MAX(-140*scaleX, offsetY);
            self.imgProfileVwTopConstraint.constant = MAX(-7*scaleX, offsetY);
            self.gifpostedCountTopConstrant.constant=MAX(-99*scaleX, offsetY*2);
            self.gifRepluggedCountTopConstrant.constant=MAX(-99*scaleX, offsetY*2);
            imgProfileImg.layer.cornerRadius=imgProfileImg.frame.size.width/2;
        }
    }

}

#pragma mark - UITableView Delegate Method

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 30;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (!cell)
    {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
   // cell.textLabel.text = @"No GIF Posted yet.";
    cell.backgroundColor=[UIColor clearColor];
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 30.0f;
}


#pragma mark - WebService API
-(void)getUserProfile
{
    
    NSString *url = [NSString stringWithFormat:@"users/%@",[NSUSERDEFAULTS valueForKey:kUSERNAME]];
    //http://192.168.0.22:8353/v1/users/{username}
    
    NSMutableDictionary *header =[[NSMutableDictionary alloc]initWithObjectsAndKeys:@"application/json",@"Content-Type",[NSUSERDEFAULTS valueForKey:kUSERTOKEN],@"User-Token", nil];
    
    //    NSMutableDictionary *param=[[NSMutableDictionary alloc]initWithObjectsAndKeys:[CommonFunction trimSpaceInString:txtUsername.text],@"username",[CommonFunction trimSpaceInString:txtEmail.text],@"email",[CommonFunction trimSpaceInString:txtPassword.text],@"password",@"ios",@"device_type",pushDeviceToken,@"device_token", nil];
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypeGet withHttpHeaders:header withServiceName:url withParameters:nil withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"responce dict %@",responseDict);
        
        [CommonFunction removeActivityIndicator];
        if(responseDict==Nil)        {
            [CommonFunction alertTitle:@"" withMessage:@"Server Error"];
            
        }
        else if([operation.response statusCode]==200)
            
        {
            dictData=responseDict[@"userInfo"];
            lblFollowersCount.text=[NSString stringWithFormat:@"%@",[[responseDict valueForKey:@"userInfo"]valueForKey:@"followers_count"]];
            lblFollowingCount.text=[NSString stringWithFormat:@"%@",[[responseDict valueForKey:@"userInfo"]valueForKey:@"following_count"]];
            lblReplugCount.text=[NSString stringWithFormat:@"%@",[[responseDict valueForKey:@"userInfo"]valueForKey:@"replugged_count"]];
            lblGifPostedCOunt.text=[NSString stringWithFormat:@"%@",[[responseDict valueForKey:@"userInfo"]valueForKey:@"posted_gif_count"]];
            lblUserBio.text=[NSString stringWithFormat:@"%@ \n %@",[[responseDict valueForKey:@"userInfo"]valueForKey:@"bio"],[[responseDict valueForKey:@"userInfo"]valueForKey:@"address"]];

            
            lblUsername.text=[NSString stringWithFormat:@"%@",[[responseDict valueForKey:@"userInfo"]valueForKey:@"username"]].capitalizedString;
            
            NSString *strAvtarImg=[[responseDict valueForKey:@"userInfo"]valueForKey:@"avatar_image"];
            if (strAvtarImg.length>1) {
                
                
                [imgProfileImg sd_setImageWithURL:[NSURL URLWithString:strAvtarImg] placeholderImage:[UIImage imageNamed:@"default.jpg"] options:SDWebImageRefreshCached completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                    
                    
                }];
                
                
                
                [imgProfileImg setImageWithURL:[NSURL URLWithString:strAvtarImg] usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
            }
            else
            {
                [imgProfileImg setImage:[UIImage imageNamed:@"default.jpg"]];
            }
            
            

            [imgProfileBgImg setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[[responseDict valueForKey:@"userInfo"]valueForKey:@"background_image"]]] usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
            
        }
        else
        {
            [CommonFunction alertTitle:@"" withMessage:[responseDict objectForKey:@"msg"]];
        }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          if([operation.response statusCode]  == 400 ){
                                              NSLog(@"impo response %@",operation.response);
                                              
                                              [CommonFunction removeActivityIndicator];
                                              [CommonFunction alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                          }
                                          else{
                                              
                                              if (wbServiceCount==1) {
                                                  wbServiceCount=0;
                                                  [CommonFunction showActivityIndicatorWithText:@""];
                                                  [self getUserProfile];
                                              }
                                              else
                                              {
                                                  [CommonFunction removeActivityIndicator];
                                                  [CommonFunction alertTitle:@"" withMessage:@"Network Error..."];
                                              }
                                              
                                          }
                                          
                                      }];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
