//
//  SearchVC.m
//  GiffPlugApp
//
//  Created by Kshitij Godara on 08/01/16.
//  Copyright © 2016 Kshitij Godara. All rights reserved.
//

#import "SearchVC.h"
#import "RecentSearchCell.h"
#import "SearchCategoryView.h"
#import "AfterSearchView.h"
#import "OtherUserProfileVC.h"
#import "CategoryDetailVC.h"
#import "SinglePostVC.h"
#import "KIHTTPRequestOperationManager.h"

typedef enum : NSUInteger {
    ACTIVE_MODE_SEARCH,
    ACTIVE_MODE_AFTER_SEARCH,
    ACTIVE_MODE_CATEGORY,
} ACTIVEMODE;
@interface SearchVC ()<UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate,AfterSearchDelegate,CategorySearchDelegate,UIGestureRecognizerDelegate>
{
    CGFloat originalSearchHeightConstraint;
    CGRect originalCategoryFrame, originalAfterSearchFrame;
    ACTIVEMODE activeMode;
}
#pragma mark -
#pragma mark PROPERTIES
// <-------------- Properties --------------> //
@property (strong, nonatomic) IBOutlet SearchCategoryView *categoryView;
@property (strong, nonatomic) IBOutlet AfterSearchView *afterSearchView;
@property (weak, nonatomic) IBOutlet UITextField *searchTextField;
@property (weak, nonatomic) IBOutlet UIButton *closeSearchButton;
@property (weak, nonatomic) IBOutlet UIButton *categoryButton;
@property (weak, nonatomic) IBOutlet UIButton *searchButton;
@property (weak, nonatomic) IBOutlet UILabel *labelRecentSearch;
@property (weak, nonatomic) IBOutlet UITableView *tableRecentSearch;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *searchHeightConstraint;
#pragma mark -
#pragma mark IBACTIONS
// <-------------- IBACTIONS --------------> //
- (IBAction)onCloseSearchAction:(id)sender;
- (IBAction)onCategorySelectAction:(id)sender;
- (IBAction)onSearchSelectAction:(id)sender;
#pragma mark -
#pragma mark USER DEFINED FUNCTIONS
- (void)getSearchDataWithKeyword:(NSString *)keyword;
- (void)onAfterSearchView;
@end

@implementation SearchVC

#pragma mark -
#pragma mark View Life Cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
    
    activeMode = ACTIVE_MODE_SEARCH;
    [self.tableRecentSearch registerNib:[UINib nibWithNibName:NSStringFromClass([RecentSearchCell class]) bundle:nil] forCellReuseIdentifier:kCellIdentifier];
    self.tableRecentSearch.tableFooterView = [UIView new];
    
     originalSearchHeightConstraint=self.searchHeightConstraint.constant * SCREEN_XScale;
    self.searchHeightConstraint.constant = originalSearchHeightConstraint;
//    originalCategoryFrame = self.categoryView.frame;
    originalCategoryFrame = self.categoryView.frame;
    originalAfterSearchFrame = self.afterSearchView.frame;
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onSelectSearch)];
    tap.numberOfTapsRequired=1;
    tap.numberOfTouchesRequired=1;
    [[self.view viewWithTag:2000] addGestureRecognizer:tap];
    
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"getSearchData" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(getResultsFromServerUsingParameters:) name:@"getSearchData" object:nil];
    
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = YES;
        self.navigationController.interactivePopGestureRecognizer.delegate = self;
    }
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldBeRequiredToFailByGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    return YES;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (self.categoryView)
    {
        [self.categoryView getCategoryList];
    }
}
- (void)dealloc
{
    NSLog(@"dealloced called for SEARCH VC...................");
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)onSelectSearch
{
    [self.searchTextField becomeFirstResponder];
}

#pragma mark -
#pragma mark On Close Action
- (IBAction)onCloseSearchAction:(id)sender {
    self.searchTextField.text = @"";
    [self.searchTextField resignFirstResponder];
}

#pragma mark -
#pragma mark On Category Active
- (IBAction)onCategorySelectAction:(id)sender {
    
    if (activeMode == ACTIVE_MODE_CATEGORY)
    {
        return;
    }
    
    NSString *url = [NSString stringWithFormat:@"posts/findByKeywords"];
    
    [[KIHTTPRequestOperationManager manager]cancelAllHTTPOperationsWithPath:url];
    
    [self onCloseSearchAction:nil];
    
    [self.view layoutIfNeeded];
    
    [self.categoryButton setImage:[UIImage imageNamed:@"caegoryIconActive"] forState:UIControlStateNormal];
    
    [self.searchButton setImage:[UIImage imageNamed:@"searchIconBig"] forState:UIControlStateNormal];
    
    self.searchHeightConstraint.constant = 0;
    
    self.categoryView.alpha = 0;
//    self.categoryView.frame = CGRectMake(0, 136.0f, self.view.frame.size.width, self.view.frame.size.height - 136.0f);
    [self.view addSubview:self.categoryView];
    
    if (activeMode == ACTIVE_MODE_AFTER_SEARCH)
    {
        self.afterSearchView.alpha = 0.0f;
        self.afterSearchView.frame = originalAfterSearchFrame;
        
        [UIView animateWithDuration:0.25 delay:0 usingSpringWithDamping:1.0 initialSpringVelocity:0 options:UIViewAnimationOptionCurveLinear animations:^{
            [self.view layoutIfNeeded];
            self.categoryView.alpha = 1.0f;
            self.categoryView.frame = CGRectMake(0, self.labelRecentSearch.frame.origin.y, self.view.frame.size.width, self.view.frame.size.height - self.labelRecentSearch.frame.origin.y);
        } completion:^(BOOL finished) {
            [self.afterSearchView resetView];
            [self.afterSearchView removeFromSuperview];
        }];
//        [UIView animateWithDuration:0.25 delay:0 options:7 animations:^{
//            [self.view layoutIfNeeded];
//            self.categoryView.alpha = 1.0f;
//            self.categoryView.frame = CGRectMake(0, self.labelRecentSearch.frame.origin.y, self.view.frame.size.width, self.view.frame.size.height - self.labelRecentSearch.frame.origin.y);
//        } completion:^(BOOL finished) {
//            [self.afterSearchView resetView];
//            [self.afterSearchView removeFromSuperview];
//        }];
    }
    else
    {
        self.tableRecentSearch.hidden = YES;
        self.labelRecentSearch.hidden = YES;
        
        [UIView animateWithDuration:0.25 delay:0 usingSpringWithDamping:1.0 initialSpringVelocity:0 options:UIViewAnimationOptionCurveLinear animations:^{
            [self.view layoutIfNeeded];
            self.categoryView.alpha = 1.0f;
            self.categoryView.frame = CGRectMake(0, self.labelRecentSearch.frame.origin.y, self.view.frame.size.width, self.view.frame.size.height - self.labelRecentSearch.frame.origin.y);
        } completion:^(BOOL finished) {
        }];
        
//        [UIView animateWithDuration:0.25 delay:0 options:7 animations:^{
//            [self.view layoutIfNeeded];
//            self.categoryView.alpha = 1.0f;
//            self.categoryView.frame = CGRectMake(0, self.labelRecentSearch.frame.origin.y, self.view.frame.size.width, self.view.frame.size.height - self.labelRecentSearch.frame.origin.y);
//        } completion:^(BOOL finished) {
//            
//        }];
    }
    self.categoryView.delegate = nil;
    self.categoryView.delegate=self;
    activeMode = ACTIVE_MODE_CATEGORY;
}

#pragma mark -
#pragma mark On Search Active
- (IBAction)onSearchSelectAction:(id)sender {
    if (activeMode == ACTIVE_MODE_SEARCH)
    {
        return;
    }
    
    
    NSString *url = [NSString stringWithFormat:@"posts/findByKeywords"];
    
    [[KIHTTPRequestOperationManager manager]cancelAllHTTPOperationsWithPath:url];
    
    [self.view layoutIfNeeded];
    
    [self onCloseSearchAction:nil];
    
    [self.categoryButton setImage:[UIImage imageNamed:@"categoryIcon"] forState:UIControlStateNormal];
    
    [self.searchButton setImage:[UIImage imageNamed:@"searchIconBigActive"] forState:UIControlStateNormal];
    
    
    self.searchHeightConstraint.constant = originalSearchHeightConstraint;
    
    self.tableRecentSearch.alpha = 0.0f;
    self.labelRecentSearch.alpha = 0.0f;
    self.tableRecentSearch.hidden = NO;
    self.labelRecentSearch.hidden = NO;
    
    if (activeMode == ACTIVE_MODE_AFTER_SEARCH)
    {
        self.afterSearchView.alpha = 0.0f;
        self.afterSearchView.frame = originalAfterSearchFrame;
        
        
        [UIView animateWithDuration:0.25 delay:0 usingSpringWithDamping:1.0 initialSpringVelocity:0 options:UIViewAnimationOptionCurveLinear animations:^{
            [self.view layoutIfNeeded];
            self.tableRecentSearch.alpha = 1.0f;
            self.labelRecentSearch.alpha = 1.0f;
        } completion:^(BOOL finished) {
            [self.afterSearchView resetView];
            [self.afterSearchView removeFromSuperview];
        }];
        
//        [UIView animateWithDuration:0.25 delay:0 options:7 animations:^{
//            [self.view layoutIfNeeded];
//            self.tableRecentSearch.alpha = 1.0f;
//            self.labelRecentSearch.alpha = 1.0f;
//        } completion:^(BOOL finished) {
//            [self.afterSearchView resetView];
//            [self.afterSearchView removeFromSuperview];
//        }];
    }
    else
    {
        self.categoryView.alpha = 0.0f;
        self.categoryView.frame = originalCategoryFrame;
        
        [UIView animateWithDuration:0.25 delay:0 usingSpringWithDamping:1.0 initialSpringVelocity:0 options:UIViewAnimationOptionCurveLinear animations:^{
            [self.view layoutIfNeeded];
            self.tableRecentSearch.alpha = 1.0f;
            self.labelRecentSearch.alpha = 1.0f;
        } completion:^(BOOL finished) {
            [self.categoryView removeFromSuperview];
        }];
        
//        [UIView animateWithDuration:0.25 delay:0 options:7 animations:^{
//            [self.view layoutIfNeeded];
//            self.tableRecentSearch.alpha = 1.0f;
//            self.labelRecentSearch.alpha = 1.0f;
//        } completion:^(BOOL finished) {
//            [self.categoryView removeFromSuperview];
//        }];
    }
    activeMode = ACTIVE_MODE_SEARCH;
}

#pragma mark -
#pragma mark UITableView Delegate & Datasource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSArray *arr = [NSUSERDEFAULTS objectForKey:@"recentPostArray"];
    if (!arr)
    {
        return 0;
    }
    arr = [[arr reverseObjectEnumerator]allObjects];
    return arr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSArray *arr = [NSUSERDEFAULTS objectForKey:@"recentPostArray"];
    arr = [[arr reverseObjectEnumerator]allObjects];
    RecentSearchCell *cell = [tableView dequeueReusableCellWithIdentifier:kCellIdentifier];
    [cell updateNameWithText:arr[indexPath.row]];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [self.view endEditing:YES];
    RecentSearchCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    NSString *searchText = [cell getRecentSearchText];
    self.searchTextField.text = searchText;
    [self getSearchDataWithKeyword:searchText];
}

#pragma mark -
#pragma mark UITextField Delegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    [self getSearchDataWithKeyword:[textField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]];
    return YES;
}


#pragma mark -
#pragma mark Search Api
- (void)getSearchDataWithKeyword:(NSString *)keyword
{
    if (keyword.length>0)
    {
        NSMutableArray *recentSearchArray = [[NSUSERDEFAULTS objectForKey:@"recentPostArray"]mutableCopy];
        if (recentSearchArray)
        {
            if ([recentSearchArray containsObject:keyword])
            {
                
            }
            else
            {
                if (recentSearchArray.count==5)
                {
                    [recentSearchArray removeObjectAtIndex:0];
                }
                [recentSearchArray addObject:keyword];
            }
        }
        else
        {
            recentSearchArray = [NSMutableArray array];
            [recentSearchArray addObject:keyword];
        }
        [NSUSERDEFAULTS setObject:recentSearchArray forKey:@"recentPostArray"];
        NSDictionary *paramDict = @{@"page":@1, @"keyword":keyword};
        self.afterSearchView.searchText = keyword;
        [self getResultsFromServerUsingParameters:paramDict];
    }
    [self.tableRecentSearch reloadData];
}


- (void)getResultsFromServerUsingParameters:(NSDictionary *)paramDict
{
    NSString *url = [NSString stringWithFormat:@"posts/findByKeywords"];
    
    [[KIHTTPRequestOperationManager manager]cancelAllHTTPOperationsWithPath:url];
    
    NSMutableDictionary *header =[[NSMutableDictionary alloc]initWithObjectsAndKeys:@"application/json",@"Content-Type",[NSUSERDEFAULTS valueForKey:kUSERTOKEN],@"User-Token", nil];
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    
    [CommonFunction showActivityIndicatorWithText:@""];
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypePost withHttpHeaders:header withServiceName:url withParameters:[paramDict mutableCopy] withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"--responce dict %@",responseDict);
        
        [CommonFunction removeActivityIndicator];
        if(responseDict==Nil){
            [CommonFunction alertTitle:@"" withMessage:@"Server Error"];
        }
        else if([operation.response statusCode]==200)
        {
            self.afterSearchView.isPageRefreshingRecent=NO;
            [self.afterSearchView setSearchDataDictionary:responseDict];
            if (activeMode == ACTIVE_MODE_AFTER_SEARCH)
            {
                
            }else
            {
                [self onAfterSearchView];
            }
            activeMode = ACTIVE_MODE_AFTER_SEARCH;
        }
        else
        {
            [CommonFunction alertTitle:@"" withMessage:[responseDict objectForKey:@"msg"]];
        }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          [CommonFunction removeActivityIndicator];
                                          if (error.code == -999) {
                                              
                                          }
                                          else
                                          {
                                              [CommonFunction alertTitle:@"" withMessage:error.localizedDescription];
                                          }
                                    }];
}

#pragma mark -
#pragma mark Show After Search View
- (void)onAfterSearchView
{
    [self.view layoutIfNeeded];
    self.afterSearchView.alpha = 0;
    //    self.categoryView.frame = CGRectMake(0, 136.0f, self.view.frame.size.width, self.view.frame.size.height - 136.0f);
    [self.view addSubview:self.afterSearchView];
    self.afterSearchView.delegate = nil;
    self.afterSearchView.delegate=self;
    self.tableRecentSearch.hidden = YES;
    self.labelRecentSearch.hidden = YES;
    [UIView animateWithDuration:0.25 delay:0 options:7 animations:^{
//        [self.view layoutIfNeeded];
        self.afterSearchView.alpha = 1.0f;
        self.afterSearchView.frame = CGRectMake(0, self.labelRecentSearch.frame.origin.y, self.view.frame.size.width, self.view.frame.size.height - self.labelRecentSearch.frame.origin.y);
    } completion:^(BOOL finished) {
        
    }];
}

#pragma mark -
#pragma mark AfterSearch Delegates
- (void)afterSearchViewDidSelectedUserWithName:(NSString *)username
{
    NSString *loggedinUser = [NSUSERDEFAULTS objectForKey:kUSERID];
    if ([username isEqualToString:loggedinUser])
    {
        [APPDELEGATE goToUserProfile];
    }
    else
    {
        OtherUserProfileVC *oupvc=[[OtherUserProfileVC alloc]initWithNibName:@"OtherUserProfileVC" bundle:nil];
        oupvc.strUserName=username;
        oupvc.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:oupvc animated:YES];
    }
}
- (void)afterSearchViewDidSelectedGifID:(NSString *)postID
{
    SinglePostVC *postvc = [[SinglePostVC alloc]initWithNibName:NSStringFromClass([SinglePostVC class]) bundle:nil];
    postvc.postID = postID;
    postvc.hidesBottomBarWhenPushed=YES;
    [self.navigationController pushViewController:postvc animated:YES];
}

#pragma mark -
#pragma mark CategorySearch Delegate
- (void)categorySearchViewDidSelectedCategoryWithName:(NSString *)categoryName andCategoryTag:(NSString *)categoryTag
{
    CategoryDetailVC *categorydetailVC = [[CategoryDetailVC alloc]initWithNibName:NSStringFromClass([CategoryDetailVC class]) bundle:nil];
    categorydetailVC.categoryTag = categoryTag;
    categorydetailVC.categoryName = categoryName;
    [self.navigationController pushViewController:categorydetailVC animated:YES];
}

@end
