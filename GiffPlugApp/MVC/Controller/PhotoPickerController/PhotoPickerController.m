//
//  PhotoPickerController.m
//  GiffPlugApp
//
//  Created by Santosh on 07/03/16.
//  Copyright © 2016 Kshitij Godara. All rights reserved.
//

#import "PhotoPickerController.h"
#import "ImageviewerCell.h"
#import "ViewFullImageVC.h"
#import "VideoviewerCell.h"
#import "UICollectionView+Convenience.h"

@interface PhotoPickerController ()<UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,PHPhotoLibraryChangeObserver,UIVideoEditorControllerDelegate,UINavigationControllerDelegate>
{
    NSMutableArray *imageDataArray;
    NSInteger totalGifCounter;
    NSMutableArray *requiredAssetsArray;
    BOOL isViewLoaded;
}

@property (nonatomic, strong) PHCachingImageManager *imageCachingManager;
@property (nonatomic, strong) PHFetchResult *result;
@property CGRect previousPreheatRect;

@property (weak, nonatomic) IBOutlet UICollectionView *collectionViewImages;
@property (weak, nonatomic) IBOutlet UIView *errorView;
- (IBAction)onCloseAction:(id)sender;
- (IBAction)onEnableLibraryAccess:(id)sender;

@end

@implementation PhotoPickerController


- (void)dealloc {
    [[PHPhotoLibrary sharedPhotoLibrary] unregisterChangeObserver:self];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    isViewLoaded = YES;
    // Do any additional setup after loading the view from its nib.
    
    if ([PHPhotoLibrary authorizationStatus] == PHAuthorizationStatusDenied || [PHPhotoLibrary authorizationStatus] == PHAuthorizationStatusRestricted)
    {
        self.errorView.hidden = NO;
    }
    else
    {
        [self initialSetup];
        
        [self getAllPhotosFromCamera];
    }
//    [self requestAuthenticationForPhotos];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self updateCachedAssets];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    
    [[UIApplication sharedApplication]setStatusBarHidden:NO withAnimation:UIStatusBarAnimationFade];
    [[UIApplication sharedApplication]setStatusBarStyle:UIStatusBarStyleDefault];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Initial View Setup
- (void)initialSetup
{
    if (self.type == 0)
    {
        [self.collectionViewImages registerNib:[UINib nibWithNibName:NSStringFromClass([ImageviewerCell class]) bundle:nil] forCellWithReuseIdentifier:@"gifCell"];
    }
    else
    {
        [self.collectionViewImages registerNib:[UINib nibWithNibName:NSStringFromClass([VideoviewerCell class]) bundle:nil] forCellWithReuseIdentifier:@"videoCell"];

    }
    
    totalGifCounter = 0;
    self.collectionViewImages.delegate=self;
    self.collectionViewImages.dataSource=self;
    
    self.imageCachingManager = [[PHCachingImageManager alloc]init];
    
    [self resetCachedAssets];
    
//    [[PHPhotoLibrary sharedPhotoLibrary] registerChangeObserver:self];

    
//    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"kDismissPhotosView" object:nil];
//    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(dismissViewOnSuccess) name:@"kDismissPhotosView" object:nil];

}

#pragma mark - PHPhotoLibrary Authentication
- (void)requestAuthenticationForPhotos
{
    [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status) {
        switch (status) {
            case PHAuthorizationStatusAuthorized:
            {
                [self getAllPhotosFromCamera];
            }
                break;
            case PHAuthorizationStatusDenied:
            {
                
            }
            case PHAuthorizationStatusRestricted:
            {
                UILabel *label = [self.view viewWithTag:301];
                label.hidden = NO;
                label.text = @"Access Denied By User!";
            }
                break;
                
            default:
                break;
        }
    }];
}

#pragma mark - Fetch Assets
-(void)getAllPhotosFromCamera
{
    imageDataArray = [[NSMutableArray alloc]init];
    
    [self fetchAndSortAssets];
}

- (void)fetchAndSortAssets
{
    // Asset fetch options...
    PHFetchOptions *allPhotosOptions = [[PHFetchOptions alloc] init];
    // creating sort descriptors for sorting based on creationdate of asset...
    allPhotosOptions.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"creationDate" ascending:NO]];
    
    if (self.type == 0)
    {
        // Fetching total assets of type image...
        self.result = [PHAsset fetchAssetsWithMediaType:PHAssetMediaTypeImage options:allPhotosOptions];
        
        if (requiredAssetsArray)
        {
            [requiredAssetsArray removeAllObjects],requiredAssetsArray=nil;
        }
        requiredAssetsArray= [[NSMutableArray alloc]init];
        
        // Looping through the result and fetching only GIF...
        [self.result enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            PHAsset *asset = obj;
            NSString *dataUTI = [asset valueForKey: @"uniformTypeIdentifier"];
            if ([dataUTI isEqualToString:@"com.compuserve.gif"])
            {
                [self.view viewWithTag:301].hidden = YES;
                totalGifCounter = totalGifCounter + 1;
                [requiredAssetsArray addObject:asset];
            }
        }];
    }
    else
    {
        
//        allPhotosOptions.predicate = [NSPredicate predicateWithFormat:@"mediaSubtype != %d", PHAssetMediaSubtypeVideoHighFrameRate];
        // Fetching total assets of type image...
        self.result = [PHAsset fetchAssetsWithMediaType:PHAssetMediaTypeVideo options:allPhotosOptions];
        
        if (requiredAssetsArray)
        {
            [requiredAssetsArray removeAllObjects],requiredAssetsArray=nil;
        }
        requiredAssetsArray= [[NSMutableArray alloc]init];
        
        // Looping through the result and fetching only GIF...
        [self.result enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            PHAsset *asset = obj;
//            NSString *dataUTI = [asset valueForKey: @"uniformTypeIdentifier"];
//            if ([dataUTI isEqualToString:@"com.compuserve.gif"])
//            {
                [self.view viewWithTag:301].hidden = YES;
                totalGifCounter = totalGifCounter + 1;
                [requiredAssetsArray addObject:asset];
//            }
        }];
    }

    // If no GIF found, no go further and return
    if (totalGifCounter == 0)
    {
        [self.view viewWithTag:301].hidden = NO;
        return;
    }
}

#pragma mark - UICollectionView Delegate & Datasource
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(100*SCREEN_XScale, 100*SCREEN_XScale);
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return requiredAssetsArray.count;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 1.0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 1.0;
}

// Layout: Set Edges
- (UIEdgeInsets)collectionView:
(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    // return UIEdgeInsetsMake(0,8,0,8);  // top, left, bottom, right
    return UIEdgeInsetsMake(5,5,5,5);  // top, left, bottom, right
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *cellIdentifier = (self.type == 1)?@"videoCell":@"gifCell";
    if (self.type == 0)
    {
        ImageviewerCell *imgCell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
        if (imgCell)
        {
            PHAsset *asset = requiredAssetsArray[indexPath.row];
            [self.imageCachingManager requestImageForAsset:asset
                                                targetSize:CGSizeMake(200, 200)
                                               contentMode:PHImageContentModeAspectFill
                                                   options:nil
                                             resultHandler:^(UIImage *result, NSDictionary *info) {
                                                 [(UIActivityIndicatorView *)[[imgCell contentView]viewWithTag:111] stopAnimating];
                                                 [imgCell.gifImageView setImage:result];
                                             }];
            
            [self.imageCachingManager requestImageDataForAsset:asset options:nil resultHandler:^(NSData * _Nullable imageData, NSString * _Nullable dataUTI, UIImageOrientation orientation, NSDictionary * _Nullable info) {
                NSLog(@"got the data............");
                imgCell.imageData = imageData;
            }];
        }
        return imgCell;
    }
    else
    {
        VideoviewerCell *imgCell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
        
        if (imgCell)
        {
            PHAsset *asset = requiredAssetsArray[indexPath.row];
            if (self.type == 1)
            {
                imgCell.timeLabel.text = [NSString stringWithFormat:@"%.02f",asset.duration];
            }
            [self.imageCachingManager requestImageForAsset:asset
                                                targetSize:CGSizeMake(200, 200)
                                               contentMode:PHImageContentModeAspectFill
                                                   options:nil
                                             resultHandler:^(UIImage *result, NSDictionary *info) {
                                                 [(UIActivityIndicatorView *)[[imgCell contentView]viewWithTag:111] stopAnimating];
                                                 [imgCell.gifImageView setImage:result];
                                             }];
            
          
            [self.imageCachingManager requestAVAssetForVideo:asset options:nil resultHandler:^(AVAsset * _Nullable asset, AVAudioMix * _Nullable audioMix, NSDictionary * _Nullable info) {
                NSArray *arr=[info[@"PHImageFileSandboxExtensionTokenKey"] componentsSeparatedByString:@";/"];
                NSArray *arr1 = [[arr lastObject] componentsSeparatedByString:@"\""];
                imgCell.fileUrl = [NSString stringWithFormat:@"%@",[arr1 firstObject]];
            }];
        }
        return imgCell;
    }
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    [collectionView deselectItemAtIndexPath:indexPath animated:YES];

    if (self.type == 0)
    {
        ImageviewerCell *cell = (ImageviewerCell *)[collectionView cellForItemAtIndexPath:indexPath];
        ViewFullImageVC *fullImageVC = [[ViewFullImageVC alloc]initWithNibName:NSStringFromClass([ViewFullImageVC class]) bundle:nil];
        
        NSData *data =cell.imageData;
        
        fullImageVC.gifImageData = data;
        [self presentViewController:fullImageVC animated:YES completion:nil];
    }
    else
    {
        VideoviewerCell *cell = (VideoviewerCell *)[collectionView cellForItemAtIndexPath:indexPath];
        UIVideoEditorController* videoEditor = [[UIVideoEditorController alloc] init];
        videoEditor.view.tag = 1000;
//        videoEditor.videoQuality = UIImagePickerControllerQualityTypeIFrame1280x720;
        videoEditor.delegate = self;
        NSString* videoPath = [[NSBundle mainBundle] pathForResource:@"test" ofType:@"MOV"];
        if ( [UIVideoEditorController canEditVideoAtPath:cell.fileUrl] ) {
            videoEditor.videoPath = cell.fileUrl;
            videoEditor.videoMaximumDuration = 5.0;
            [self presentViewController:videoEditor animated:YES completion:nil];
        } else {
            NSLog( @"can't edit video at %@", videoPath );
        }
    }
}

#pragma mark - Video Editor Delegate

-(void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated
{
    // add done button to right side of nav bar
    
    for (UIView *view in navigationController.navigationBar.subviews)
    {
        if ([view isKindOfClass:[UIButton class]])
        {
            UIButton *btn = (UIButton *)view;
            
            if ([btn.titleLabel.text isEqualToString:@"Cancel"]) {
                [btn setTintColor:[UIColor colorWithRed:228/255.0f green:15/255.0f blue:73/255.0f alpha:1.0f]];
                [btn setImage:[UIImage imageNamed:@"CancelBtn"] forState:UIControlStateNormal];
                [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                [btn setTitle:@"" forState:UIControlStateNormal];
            }
            else
            {
                [btn setTintColor:[UIColor colorWithRed:228/255.0f green:15/255.0f blue:73/255.0f alpha:1.0f]];
                [btn setImage:[UIImage imageNamed:@"nextBtn"] forState:UIControlStateNormal];
                [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                [btn setTitle:@"" forState:UIControlStateNormal];
            }
            
        }
    }
    
    [viewController.navigationItem setTitle:@"TRIM"];
}

- (void)videoEditorController:(UIVideoEditorController *)editor didSaveEditedVideoToPath:(NSString *)editedVideoPath
{
    // edited video is saved to a path in app's temporary directory
    [self moveFileToNewLoction:editedVideoPath];
    
}
- (void)videoEditorController:(UIVideoEditorController *)editor didFailWithError:(NSError *)error
{
    
}
- (void)videoEditorControllerDidCancel:(UIVideoEditorController *)editor
{
    [editor dismissViewControllerAnimated:YES completion:nil];
    
}

-(void)moveFileToNewLoction:(NSString *)fromPath
{
    //NSString *filePath = [outputUrl path];
    NSString *pathExtension = [fromPath pathExtension] ;
    if ([pathExtension length] > 0)
    {
        NSFileManager *fileManageer = [NSFileManager defaultManager];
        
        NSError *error;
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES); //create an array and store result of our search for the documents directory in it
        
        NSString *documentsDirectory = [paths objectAtIndex:0]; //create NSString object, that holds our exact path to the documents directory
        
        NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"/%@",kAPPFOLDERNAME]];
        
        if (![fileManageer fileExistsAtPath:dataPath])
        {
            [fileManageer createDirectoryAtPath:dataPath withIntermediateDirectories:NO attributes:nil error:&error];
        }
        NSString *folderPath = [dataPath stringByAppendingPathComponent:[NSString stringWithFormat:@"/VideoGiff"]];
        if (![[NSFileManager defaultManager] fileExistsAtPath:folderPath])
        {
            [[NSFileManager defaultManager] createDirectoryAtPath:folderPath withIntermediateDirectories:NO attributes:nil error:&error];
            
        }
        
        
        NSString *localFilePath = [folderPath stringByAppendingPathComponent:[NSString stringWithFormat:@"trimmedVideo.mov"]];
        
        // Method last path component is used here, so that each video saved will get different name.
        
        BOOL res = [fileManageer moveItemAtPath:fromPath toPath:localFilePath error:&error] ;
        
        if (!res)
        {
            NSLog(@"%@", [error localizedDescription]) ;
        }
        else
        {
            NSLog(@"File saved at : %@",localFilePath);
            //[self getVideoFrames];
            
//            __weak typeof(self) weakSelf = self;
            
            [NSGIF optimalGIFfromURL:[NSURL fileURLWithPath:localFilePath] loopCount:0 completion:^(NSURL *GifURL) {
                NSData *data = [NSData dataWithContentsOfURL:GifURL];
                [NSUSERDEFAULTS setObject:data forKey:@"selectedGif"];
                [[NSNotificationCenter defaultCenter]postNotificationName:@"kGoToPostScreen" object:nil];
            }];
        }
    }
}

#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    // Update cached assets for the new visible area.
    [self updateCachedAssets];
}


#pragma mark - PHPhotoLibraryChangeObserver
- (void)photoLibraryDidChange:(PHChange *)changeInstance
{
    
}

#pragma mark - Asset Caching

- (void)resetCachedAssets {
    [self.imageCachingManager stopCachingImagesForAllAssets];
    self.previousPreheatRect = CGRectZero;
}

- (void)updateCachedAssets {
    BOOL isViewVisible = [self isViewLoaded] && [[self view] window] != nil;
    if (!isViewVisible) { return; }
    
    // The preheat window is twice the height of the visible rect.
    CGRect preheatRect = self.collectionViewImages.bounds;
    preheatRect = CGRectInset(preheatRect, 0.0f, -0.5f * CGRectGetHeight(preheatRect));
    
    /*
     Check if the collection view is showing an area that is significantly
     different to the last preheated area.
     */
    CGFloat delta = ABS(CGRectGetMidY(preheatRect) - CGRectGetMidY(self.previousPreheatRect));
    if (delta > CGRectGetHeight(self.collectionViewImages.bounds) / 3.0f) {
        
        // Compute the assets to start caching and to stop caching.
        NSMutableArray *addedIndexPaths = [NSMutableArray array];
        NSMutableArray *removedIndexPaths = [NSMutableArray array];
        
        [self computeDifferenceBetweenRect:self.previousPreheatRect andRect:preheatRect removedHandler:^(CGRect removedRect) {
            NSArray *indexPaths = [self.collectionViewImages aapl_indexPathsForElementsInRect:removedRect];
            [removedIndexPaths addObjectsFromArray:indexPaths];
        } addedHandler:^(CGRect addedRect) {
            NSArray *indexPaths = [self.collectionViewImages aapl_indexPathsForElementsInRect:addedRect];
            [addedIndexPaths addObjectsFromArray:indexPaths];
        }];
        
        NSArray *assetsToStartCaching = [self assetsAtIndexPaths:addedIndexPaths];
        NSArray *assetsToStopCaching = [self assetsAtIndexPaths:removedIndexPaths];
        
        // Update the assets the PHCachingImageManager is caching.
        [self.imageCachingManager startCachingImagesForAssets:assetsToStartCaching
                                                   targetSize:CGSizeMake(200, 200)
                                                  contentMode:PHImageContentModeAspectFill
                                                      options:nil];
        [self.imageCachingManager stopCachingImagesForAssets:assetsToStopCaching
                                                  targetSize:CGSizeMake(200, 200)
                                                 contentMode:PHImageContentModeAspectFill
                                                     options:nil];
        
        // Store the preheat rect to compare against in the future.
        self.previousPreheatRect = preheatRect;
    }
}

- (void)computeDifferenceBetweenRect:(CGRect)oldRect andRect:(CGRect)newRect removedHandler:(void (^)(CGRect removedRect))removedHandler addedHandler:(void (^)(CGRect addedRect))addedHandler {
    if (CGRectIntersectsRect(newRect, oldRect)) {
        CGFloat oldMaxY = CGRectGetMaxY(oldRect);
        CGFloat oldMinY = CGRectGetMinY(oldRect);
        CGFloat newMaxY = CGRectGetMaxY(newRect);
        CGFloat newMinY = CGRectGetMinY(newRect);
        
        if (newMaxY > oldMaxY) {
            CGRect rectToAdd = CGRectMake(newRect.origin.x, oldMaxY, newRect.size.width, (newMaxY - oldMaxY));
            addedHandler(rectToAdd);
        }
        
        if (oldMinY > newMinY) {
            CGRect rectToAdd = CGRectMake(newRect.origin.x, newMinY, newRect.size.width, (oldMinY - newMinY));
            addedHandler(rectToAdd);
        }
        
        if (newMaxY < oldMaxY) {
            CGRect rectToRemove = CGRectMake(newRect.origin.x, newMaxY, newRect.size.width, (oldMaxY - newMaxY));
            removedHandler(rectToRemove);
        }
        
        if (oldMinY < newMinY) {
            CGRect rectToRemove = CGRectMake(newRect.origin.x, oldMinY, newRect.size.width, (newMinY - oldMinY));
            removedHandler(rectToRemove);
        }
    } else {
        addedHandler(newRect);
        removedHandler(oldRect);
    }
}

- (NSArray *)assetsAtIndexPaths:(NSArray *)indexPaths {
    if (indexPaths.count == 0) { return nil; }
    
    NSMutableArray *assets = [NSMutableArray arrayWithCapacity:indexPaths.count];
    for (NSIndexPath *indexPath in indexPaths) {
        PHAsset *asset = requiredAssetsArray[indexPath.row];
        [assets addObject:asset];
    }
    return assets;
}


#pragma mark -
#pragma mark On Close Action
- (IBAction)onCloseAction:(id)sender {
    self.collectionViewImages.delegate= nil;
    self.collectionViewImages.dataSource=nil;
    [imageDataArray removeAllObjects],imageDataArray = nil;
    [requiredAssetsArray removeAllObjects],requiredAssetsArray = nil;
    totalGifCounter = 0;
    self.collectionViewImages = nil;
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)onEnableLibraryAccess:(id)sender {
    NSURL *appSettings = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
    [[UIApplication sharedApplication] openURL:appSettings];
}

@end
