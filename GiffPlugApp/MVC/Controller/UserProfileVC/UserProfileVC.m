//
//  UserProfileVC.m
//  GiffPlugApp
//
//  Created by Sumit Sharma on 02/02/16.
//  Copyright © 2016 Kshitij Godara. All rights reserved.
//

#import "EditUserProfileVC.h"
#import "UserProfileVC.h"
#import "LoginVC.h"
#import "PostedGifsCustomeCell.h"
#import "ProgressBarInfo.h"
#import "LikeVC.h"
#import "CommentVC.h"
#import "ReplugVC.h"
#import "AppDataManager.h"

#import "FollowerFollowingVC.h"

#import "SliderMenuVC.h"

#import "SettingsVC.h"

#import "OtherUserProfileVC.h"

#import "CategoryDetailVC.h"
#import "PhotoPickerController.h"
#import <AssetsLibrary/AssetsLibrary.h>


@interface UserProfileVC ()<UIScrollViewDelegate,UIGestureRecognizerDelegate,UpdateComments,UserHandleDelegate>
{
    IBOutlet NSLayoutConstraint *tblVwTopCnstraint;
    IBOutlet UIView *vwBg;
    NSInteger currentPage, totalPage,totalPost;
    NSInteger currentPageReplugd, totalPageReplugd,totalPostReplugd;
    IBOutlet UIActivityIndicatorView *actiVtyIndictor;
    IBOutlet UIView *vwFoterVw;
    IBOutlet UILabel *lblFollowersCount;
    IBOutlet UILabel *lblFollowingCount;
    IBOutlet UILabel *lblUsername;
    IBOutlet UILabel *lblUserBio;
    IBOutlet UILabel *lblGifPostedCOunt;
    IBOutlet UILabel *lblReplugCount;
    IBOutlet UILabel *lblLocation;
    
    IBOutlet UIImageView *imgProfileImg;
    IBOutlet UIImageView *imgProfileBgImg;

    IBOutlet UIImageView *replugCircle;
    IBOutlet UIView *vwGifPosted;
    IBOutlet UIView *vwGifReplug;
    
    IBOutlet UIButton *btnSetting;
    
    __weak IBOutlet UIButton *btnViewInfo;
    NSArray *arrVisibleCell;
    
    
    CGFloat scaleX, scaleY, scaleSixPlusX, scaleSixPlusY;
    
    NSMutableDictionary *dictData;
    
//    NSMutableArray *arrMyPost;
    
//    NSString *strMainImgURl;
    
    int selectedIndexpath;
    UIRefreshControl *refreshControl;
    
    
    IBOutlet UILabel *lblMsgError;
    IBOutlet UIButton *btnDelete;
    IBOutlet UIButton *btnShare;
    IBOutlet UIButton *btnCancel;
    IBOutlet UIView *vwCstmeActionSheet;
    BOOL isReplugSelected;
    AppDataManager *dataManager;
    
    BOOL isPageRefreshingGif, isPageRefreshingReplug;
    
    UIButton *moreButton;
    
    NSIndexPath *visibleIndexPath ,*prevIndexPath;
    
    NSTimer *timerGifCount;
    
    BOOL isViewLoaded;
}

- (IBAction)onViewInfoSlider:(UIButton *)sender;

@property (weak, nonatomic) IBOutlet UITableView *tblVwData;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *lblErrorTpCnstraint;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *settingBtnWidthCnstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *settingBtnTrailingCnstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *settingBtnTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *gifRepluggedCountTopConstrant;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *gifpostedCountTopConstrant;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *headerTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *headerHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *nameTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imgProfileVwCenterContraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imgProfileVwWidthContraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imgProfileVwTopConstraint;
@property (weak, nonatomic) IBOutlet UIImageView *gifImageIcon;
@property (weak, nonatomic) IBOutlet UIImageView *replugImageIcon;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *replugLeadingConstraintFor6Plus;
@property (weak, nonatomic) IBOutlet UIView *tableHeaderView;
@end

@implementation UserProfileVC

- (void)dealloc
{
    NSLog(@"dealloced called for USER PROFILE VC...................");
}

- (void)viewDidLoad {
    [super viewDidLoad];
    

    isViewLoaded = YES;
    [[AppWebHandler sharedInstance]addDelegate:self];
    dataManager = [AppDataManager sharedInstance];
    
    [self.tblVwData registerNib:[UINib nibWithNibName:NSStringFromClass([PostedGifsCustomeCell class]) bundle:nil] forCellReuseIdentifier:@"PostedGifsCustomeCell"];
    //------------------------------------------------------
    [self setUpView];
    
    isReplugSelected = NO;
    isPageRefreshingGif = NO;
    isPageRefreshingReplug = NO;
    
    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.tblVwData.rowHeight = UITableViewAutomaticDimension;
    self.headerHeightConstraint.constant=23.0f*SCREEN_XScale;
    [self.tblVwData setTableHeaderView:_tableHeaderView];
    
    
    currentPage = 1;
    currentPageReplugd = 1;
    totalPage=0;
    totalPageReplugd =0;
    [[UIApplication sharedApplication]setStatusBarHidden:NO withAnimation:UIStatusBarAnimationFade];
    [[UIApplication sharedApplication]setStatusBarStyle:UIStatusBarStyleLightContent];
    
    
    if ([CommonFunction reachabiltyCheck]) {
        [CommonFunction showActivityIndicatorWithText:@""];
        [self getUserProfile];
        [self getMyPost];
    }
    else
    {
        [CommonFunction alertTitle:@"" withMessage:@"Please check your network connection."];
    }
    [lblGifPostedCOunt setTextColor:[UIColor colorWithRed:228/255.0f green:15/255.0f blue:73/255.0f alpha:1.0f]];
    [_gifImageIcon setImage:[UIImage imageNamed:@"UP_roundIcon_Selected"]];

    [vwBg setFrame:self.view.frame];
    
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = YES;
        self.navigationController.interactivePopGestureRecognizer.delegate = self;
    }
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldBeRequiredToFailByGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    return YES;
}

-(void)setUpView
{
    imgProfileImg.clipsToBounds=YES;
    imgProfileImg.layer.borderColor=[UIColor colorWithRed:106.0f/255.0f green:106.0f/255.0f blue:106.0f/255.0f alpha:1.0f].CGColor;
    imgProfileImg.layer.borderWidth=1.0f;
    
    scaleX = [[UIScreen mainScreen]bounds].size.width/320;
    
    scaleY = [[UIScreen mainScreen]bounds].size.height/568;
    
    self.settingBtnTopConstraint.constant=32*scaleSixPlusY;
    
    imgProfileImg.layer.cornerRadius=imgProfileImg.frame.size.width/2 *scaleX;

    self.headerHeightConstraint.constant = self.headerHeightConstraint.constant * scaleX;
    self.settingBtnTopConstraint.constant=25*scaleY;
    self.settingBtnTrailingCnstraint.constant=10*scaleX;
    lblUsername.font = [UIFont fontWithName:lblUsername.font.fontName size:30*scaleX];
    
    if ([UIScreen mainScreen].bounds.size.width>375) {
    _replugLeadingConstraintFor6Plus.constant=20*scaleX;
    }   

    refreshControl = [[UIRefreshControl alloc] initWithFrame:CGRectMake(0, 150, 320.0f, 30.0f)];
    refreshControl.backgroundColor=[UIColor colorWithRed:33.0f/255.0f green:31.0f/255.0f blue:31.0f/255.0f alpha:1];
    refreshControl.tintColor=[UIColor whiteColor];
    
//    NSAttributedString *title = [[NSAttributedString alloc] initWithString:@"Please Wait"
//                                                                attributes: @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
//    refreshControl.attributedTitle = [[NSAttributedString alloc]initWithAttributedString:title];
    
    
    [refreshControl addTarget:self action:@selector(refresh:) forControlEvents:UIControlEventValueChanged];
    [self.tblVwData setAutoresizingMask:(UIViewAutoresizingFlexibleRightMargin|UIViewAutoresizingFlexibleLeftMargin)];

    [self.tblVwData addSubview:refreshControl];
    
    
    UITapGestureRecognizer *tapGesture=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(clickOnTapGesture)];
    tapGesture.numberOfTapsRequired=1;
    tapGesture.delegate=self;
    [vwBg addGestureRecognizer:tapGesture];
  
}

-(void)clickOnTapGesture
{
    [moreButton setImage:[UIImage imageNamed:@"PG_menu"] forState:UIControlStateNormal];
    [UIView animateWithDuration:0.3
                          delay:0.0
                        options: UIViewAnimationOptionTransitionFlipFromBottom
                     animations:^{
                         [vwCstmeActionSheet setFrame:CGRectMake(0.0f, [UIScreen mainScreen].bounds.size.height, [UIScreen mainScreen].bounds.size.width,160*scaleY)];
                         
                     }
                     completion:^(BOOL finished){
                         //                             [APPDELEGATE.window.rootViewController.view removeFromSuperview: vwCstmeActionSheet];
                         [vwBg removeFromSuperview];
                     }];
}
- (void)refresh:(UIRefreshControl *)refreshControls
{
    if (isReplugSelected)
    {
        currentPageReplugd=1;
        totalPageReplugd=0;
    }
    else
    {
        currentPage=1;
        totalPage=0;
    }
    
    if ([CommonFunction reachabiltyCheck]) {
        [CommonFunction showActivityIndicatorWithText:@""];
        //Kshitij Work
        
        [self getUserProfile];
       [self getMyPost];
    }
    else
    {
        [CommonFunction alertTitle:@"" withMessage:@"Please check your network connection."];
        [refreshControl endRefreshing];
    }

}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [timerGifCount invalidate],timerGifCount=nil;
    prevIndexPath = visibleIndexPath = nil;
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    
    if (isViewLoaded)
    {
        isViewLoaded = NO;
    }
    else
    {
        [self getUserProfile];
        [[self tblVwData] reloadData];
    }
    
//    [self.tblVwData reloadData];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - IBAction Button Methods
//Custome ActionSheet BtnClicked
- (IBAction)actionSheetBtnClicked:(UIButton *)sender {
    
    if (sender.tag==0) {

        
        UIAlertController *alertCntroller=[UIAlertController alertControllerWithTitle:@"GifPlug" message:@"Are you sure you want to delete this post?" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *calcelBtn=[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            
            [moreButton setImage:[UIImage imageNamed:@"PG_menu"] forState:UIControlStateNormal];
            [UIView animateWithDuration:0.3
                                  delay:0.0
                                options: UIViewAnimationOptionTransitionFlipFromBottom
                             animations:^{
                                 [vwCstmeActionSheet setFrame:CGRectMake(0.0f, [UIScreen mainScreen].bounds.size.height, [UIScreen mainScreen].bounds.size.width,160*scaleY)];
                                 
                             }
                             completion:^(BOOL finished){
                                 //                             [APPDELEGATE.window.rootViewController.view removeFromSuperview: vwCstmeActionSheet];
                                 [vwBg removeFromSuperview];
                                 
                                 
                             }];
            
            [self dismissViewControllerAnimated:YES completion:nil];
        }];
        UIAlertAction *okBtn=[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
            [moreButton setImage:[UIImage imageNamed:@"PG_menu"] forState:UIControlStateNormal];
            [UIView animateWithDuration:0.1
                                  delay:0.0
                                options: UIViewAnimationOptionTransitionFlipFromBottom
                             animations:^{
                                 [vwCstmeActionSheet setFrame:CGRectMake(0.0f, [UIScreen mainScreen].bounds.size.height, [UIScreen mainScreen].bounds.size.width,160*scaleY)];
                                 
                             }
                             completion:^(BOOL finished){
                                 //                             [APPDELEGATE.window.rootViewController.view removeFromSuperview: vwCstmeActionSheet];
                                 [vwBg removeFromSuperview];
                                 
                                 
                             }];
            
            [CommonFunction showActivityIndicatorWithWait];
            if (isReplugSelected)
            {
                [self deleteMyPost:[[dataManager.globalUserReplugArray objectAtIndex:selectedIndexpath] objectForKey:@"post_id"] Indexpath:selectedIndexpath];
            }
            else
            {
                [self deleteMyPost:[[dataManager.globalUserFeedArray objectAtIndex:selectedIndexpath] objectForKey:@"post_id"] Indexpath:selectedIndexpath];
            }
        }];
        [alertCntroller addAction:calcelBtn];
        [alertCntroller addAction:okBtn];
        [self presentViewController:alertCntroller animated:YES completion:nil];
    }
    else if (sender.tag==2) {
        

        
        [moreButton setImage:[UIImage imageNamed:@"PG_menu"] forState:UIControlStateNormal];
        [UIView animateWithDuration:0.3
                              delay:0.0
                            options: UIViewAnimationOptionTransitionFlipFromBottom
                         animations:^{
                             [vwCstmeActionSheet setFrame:CGRectMake(0.0f, [UIScreen mainScreen].bounds.size.height, [UIScreen mainScreen].bounds.size.width,160*scaleY)];
                             
                         }
                         completion:^(BOOL finished){
                             [vwBg removeFromSuperview];
//                             [APPDELEGATE.window.rootViewController.view removeFromSuperview: vwCstmeActionSheet];
                         }];
    }
    else
    {
        [moreButton setImage:[UIImage imageNamed:@"PG_menu"] forState:UIControlStateNormal];
        [UIView animateWithDuration:0.3
                              delay:0.0
                            options: UIViewAnimationOptionTransitionFlipFromBottom
                         animations:^{
                             [vwCstmeActionSheet setFrame:CGRectMake(0.0f, [UIScreen mainScreen].bounds.size.height, [UIScreen mainScreen].bounds.size.width,160*scaleY)];
                             
                         }
                         completion:^(BOOL finished){
                             [vwBg removeFromSuperview];
                         }];
        
        
        NSURL *URL = nil;
        
        if (isReplugSelected)
        {
            URL=[NSURL URLWithString:dataManager.globalUserReplugArray[moreButton.tag][@"watermark_image"]];
        }
        else
        {
            URL=[NSURL URLWithString:dataManager.globalUserFeedArray[moreButton.tag][@"watermark_image"]];
        }
        
        UIActivityViewController *activityViewController =
        [[UIActivityViewController alloc] initWithActivityItems:@[URL]
                                          applicationActivities:nil];
        
        NSArray *excludedActivities = @[
                                        UIActivityTypePostToWeibo,
                                        UIActivityTypePrint,
                                        UIActivityTypeAssignToContact,
                                        UIActivityTypeAirDrop
                                        ];
        activityViewController.excludedActivityTypes = excludedActivities;
        
        // Present the controller
        [self presentViewController:activityViewController animated:YES completion:nil];

    }
}


- (void)onDoubleTapUserLikeAction:(UITapGestureRecognizer *)likeTap
{
    NSInteger index = likeTap.view.tag;
    
    PostedGifsCustomeCell *cellView = (PostedGifsCustomeCell*) [self.tblVwData cellForRowAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0]];
    
    if (cellView.isHeartAnimationActive)
    {
        return;
    }
    
    if (isReplugSelected)
    {
        NSNumber *isLike=dataManager.globalUserReplugArray[index][@"is_like"];
        NSLog(@"%@",isLike);
        [cellView performHeartAnimation];
        
        if (![isLike boolValue])
        {
            NSInteger likeCount = [dataManager.globalUserReplugArray[index][@"likes_count"] integerValue];
            
            if (cellView.imgLikeFillUnfill.tag==0) {
                
                [cellView.imgLikeFillUnfill setImage:[UIImage imageNamed:@"PG_LikeBtn"]];
                cellView.imgLikeFillUnfill.tag=1;
                
                [CommonFunction showActivityIndicatorWithText:nil];
                likeCount += 1;
                
                cellView.lblLikeCount.text=[NSString stringWithFormat:@"%ld",(long)likeCount];
                
                [dataManager.globalUserReplugArray[index] setValue:[NSNumber numberWithInteger:likeCount] forKey:@"likes_count"];
                [dataManager.globalUserReplugArray[index] setValue:@1 forKey:@"is_like"];
                
                NSString *postID = dataManager.globalUserReplugArray[index][@"post_id"];
                
                for (NSInteger i=0; i<dataManager.globalHomeFeedArray.count; i++)
                {
                    NSString *userPostID = dataManager.globalHomeFeedArray[i][@"post_id"];
                    if ([userPostID isEqualToString:postID])
                    {
                        [dataManager.globalHomeFeedArray[i] setValue:[NSNumber numberWithInteger:likeCount] forKey:@"likes_count"];
                        [dataManager.globalHomeFeedArray[i] setValue:@1 forKey:@"is_like"];
                    }
                }
                [self likeAPost:postID cell:cellView indexpath:index replug:isReplugSelected];
            }
        }
    }
    else
    {
        NSNumber *isLike=dataManager.globalUserFeedArray[index][@"is_like"];
        NSLog(@"%@",isLike);
        [cellView performHeartAnimation];
        
        if (![isLike boolValue])
        {
            NSInteger likeCount = [dataManager.globalUserFeedArray[index][@"likes_count"] integerValue];
            
            if (cellView.imgLikeFillUnfill.tag==0) {
                
                [cellView.imgLikeFillUnfill setImage:[UIImage imageNamed:@"PG_LikeBtn"]];
                cellView.imgLikeFillUnfill.tag=1;
                
                [CommonFunction showActivityIndicatorWithText:nil];
                likeCount += 1;
                
                cellView.lblLikeCount.text=[NSString stringWithFormat:@"%ld",(long)likeCount];
                
                [dataManager.globalUserFeedArray[index] setValue:[NSNumber numberWithInteger:likeCount] forKey:@"likes_count"];
                [dataManager.globalUserFeedArray[index] setValue:@1 forKey:@"is_like"];
                
                NSString *postID = dataManager.globalUserFeedArray[index][@"post_id"];
                
                for (NSInteger i=0; i<dataManager.globalHomeFeedArray.count; i++)
                {
                    NSString *userPostID = dataManager.globalHomeFeedArray[i][@"post_id"];
                    if ([userPostID isEqualToString:postID])
                    {
                        [dataManager.globalHomeFeedArray[i] setValue:[NSNumber numberWithInteger:likeCount] forKey:@"likes_count"];
                        [dataManager.globalHomeFeedArray[i] setValue:@1 forKey:@"is_like"];
                    }
                }
                [self likeAPost:postID cell:cellView indexpath:index replug:isReplugSelected];
            }
        }
    }
}


-(void)likeAPost:(NSString *)postID cell:(PostedGifsCustomeCell *)cell indexpath:(NSInteger)arrindex replug:(BOOL)isReplugPost
{
    NSString *url = [NSString stringWithFormat:@"posts/like/%@",postID];
    //http://192.168.0.22:8353/v1/posts/like/{postId}
    
    //    for (AFHTTPRequestOperation *operation in [AFHTTPRequestOperationManager manager].operationQueue.operations)
    //    {
    //        NSString *path = operation.request.URL.path;
    //        if ([path containsString:url])
    //        {
    //            [operation cancel];
    //        }
    //    }
    
    NSMutableDictionary *header =[[NSMutableDictionary alloc]initWithObjectsAndKeys:@"application/json",@"Content-Type",[NSUSERDEFAULTS valueForKey:kUSERTOKEN],@"User-Token", nil];
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    
    __weak typeof(cell) weakCell = cell;
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypePost withHttpHeaders:header withServiceName:url withParameters:nil withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"--responce dict %@",responseDict);
        
        [CommonFunction removeActivityIndicator];
        [refreshControl endRefreshing];
        if(responseDict==Nil)        {
            [CommonFunction alertTitle:@"" withMessage:@"Server Error"];
            
            NSString *postidstr;
            NSInteger likeCount;
            if (isReplugPost)
            {
                likeCount = [dataManager.globalUserReplugArray[arrindex][@"likes_count"] integerValue];
                
                [weakCell.imgLikeFillUnfill setImage:[UIImage imageNamed:@"HF_likeUnfillIcon"]];
                weakCell.imgLikeFillUnfill.tag=0;
                
                likeCount -= 1;
                weakCell.lblLikeCount.text=[NSString stringWithFormat:@"%ld",(long)likeCount];
                [dataManager.globalUserReplugArray[arrindex] setValue:[NSNumber numberWithInteger:likeCount] forKey:@"likes_count"];
                [dataManager.globalUserReplugArray[arrindex] setValue:@0 forKey:@"is_like"];
                
                postidstr = dataManager.globalUserReplugArray[arrindex][@"post_id"];
            }
            else
            {
                likeCount = [dataManager.globalUserFeedArray[arrindex][@"likes_count"] integerValue];
                
                [weakCell.imgLikeFillUnfill setImage:[UIImage imageNamed:@"HF_likeUnfillIcon"]];
                weakCell.imgLikeFillUnfill.tag=0;
                
                likeCount -= 1;
                weakCell.lblLikeCount.text=[NSString stringWithFormat:@"%ld",(long)likeCount];
                [dataManager.globalUserFeedArray[arrindex] setValue:[NSNumber numberWithInteger:likeCount] forKey:@"likes_count"];
                [dataManager.globalUserFeedArray[arrindex] setValue:@0 forKey:@"is_like"];
                
                postidstr = dataManager.globalUserFeedArray[arrindex][@"post_id"];
            }
            
            for (NSInteger i=0; i<dataManager.globalHomeFeedArray.count; i++)
            {
                NSString *userPostID = dataManager.globalHomeFeedArray[i][@"post_id"];
                if ([userPostID isEqualToString:postidstr])
                {
                    [dataManager.globalHomeFeedArray[i] setValue:[NSNumber numberWithInteger:likeCount] forKey:@"likes_count"];
                    [dataManager.globalHomeFeedArray[i] setValue:@0 forKey:@"is_like"];
                }
            }
        }
        else if([operation.response statusCode]==200)
        {
            
        }
        else
        {
            [CommonFunction alertTitle:@"" withMessage:[responseDict objectForKey:@"msg"]];
            NSString *postidstr;
            NSInteger likeCount;
            if (isReplugPost)
            {
                likeCount = [dataManager.globalUserReplugArray[arrindex][@"likes_count"] integerValue];
                
                [weakCell.imgLikeFillUnfill setImage:[UIImage imageNamed:@"HF_likeUnfillIcon"]];
                weakCell.imgLikeFillUnfill.tag=0;
                
                likeCount -= 1;
                weakCell.lblLikeCount.text=[NSString stringWithFormat:@"%ld",(long)likeCount];
                [dataManager.globalUserReplugArray[arrindex] setValue:[NSNumber numberWithInteger:likeCount] forKey:@"likes_count"];
                [dataManager.globalUserReplugArray[arrindex] setValue:@0 forKey:@"is_like"];
                
                postidstr = dataManager.globalUserReplugArray[arrindex][@"post_id"];
            }
            else
            {
                likeCount = [dataManager.globalUserFeedArray[arrindex][@"likes_count"] integerValue];
                
                [weakCell.imgLikeFillUnfill setImage:[UIImage imageNamed:@"HF_likeUnfillIcon"]];
                weakCell.imgLikeFillUnfill.tag=0;
                
                likeCount -= 1;
                weakCell.lblLikeCount.text=[NSString stringWithFormat:@"%ld",(long)likeCount];
                [dataManager.globalUserFeedArray[arrindex] setValue:[NSNumber numberWithInteger:likeCount] forKey:@"likes_count"];
                [dataManager.globalUserFeedArray[arrindex] setValue:@0 forKey:@"is_like"];
                
                postidstr = dataManager.globalUserFeedArray[arrindex][@"post_id"];
            }
            
            for (NSInteger i=0; i<dataManager.globalHomeFeedArray.count; i++)
            {
                NSString *userPostID = dataManager.globalHomeFeedArray[i][@"post_id"];
                if ([userPostID isEqualToString:postidstr])
                {
                    [dataManager.globalHomeFeedArray[i] setValue:[NSNumber numberWithInteger:likeCount] forKey:@"likes_count"];
                    [dataManager.globalHomeFeedArray[i] setValue:@0 forKey:@"is_like"];
                }
            }
        }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          
                                          NSString *postidstr;
                                          NSInteger likeCount;
                                          if (isReplugPost)
                                          {
                                              likeCount = [dataManager.globalUserReplugArray[arrindex][@"likes_count"] integerValue];
                                              
                                              [weakCell.imgLikeFillUnfill setImage:[UIImage imageNamed:@"HF_likeUnfillIcon"]];
                                              weakCell.imgLikeFillUnfill.tag=0;
                                              
                                              likeCount -= 1;
                                              weakCell.lblLikeCount.text=[NSString stringWithFormat:@"%ld",(long)likeCount];
                                              [dataManager.globalUserReplugArray[arrindex] setValue:[NSNumber numberWithInteger:likeCount] forKey:@"likes_count"];
                                              [dataManager.globalUserReplugArray[arrindex] setValue:@0 forKey:@"is_like"];
                                              
                                              postidstr = dataManager.globalUserReplugArray[arrindex][@"post_id"];
                                          }
                                          else
                                          {
                                              likeCount = [dataManager.globalUserFeedArray[arrindex][@"likes_count"] integerValue];
                                              
                                              [weakCell.imgLikeFillUnfill setImage:[UIImage imageNamed:@"HF_likeUnfillIcon"]];
                                              weakCell.imgLikeFillUnfill.tag=0;
                                              
                                              likeCount -= 1;
                                              weakCell.lblLikeCount.text=[NSString stringWithFormat:@"%ld",(long)likeCount];
                                              [dataManager.globalUserFeedArray[arrindex] setValue:[NSNumber numberWithInteger:likeCount] forKey:@"likes_count"];
                                              [dataManager.globalUserFeedArray[arrindex] setValue:@0 forKey:@"is_like"];
                                              
                                              postidstr = dataManager.globalUserFeedArray[arrindex][@"post_id"];
                                          }
                                          
                                          for (NSInteger i=0; i<dataManager.globalHomeFeedArray.count; i++)
                                          {
                                              NSString *userPostID = dataManager.globalHomeFeedArray[i][@"post_id"];
                                              if ([userPostID isEqualToString:postidstr])
                                              {
                                                  [dataManager.globalHomeFeedArray[i] setValue:[NSNumber numberWithInteger:likeCount] forKey:@"likes_count"];
                                                  [dataManager.globalHomeFeedArray[i] setValue:@0 forKey:@"is_like"];
                                              }
                                          }
                                          
                                          if([operation.response statusCode]  == 400 ){
                                              NSLog(@"impo response %@",operation.response);
                                              [refreshControl endRefreshing];
                                              [CommonFunction removeActivityIndicator];
                                              APPDELEGATE.window.userInteractionEnabled=NO;
                                              [CommonFunction alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                          }
                                          else{
                                              
                                              if (wbServiceCount==1) {
                                                  wbServiceCount=0;
                                                  //  [CommonFunction showActivityIndicatorWithText:@""];
                                                  
                                              }
                                              else
                                              {
                                                  [CommonFunction removeActivityIndicator];
                                                  [CommonFunction alertTitle:@"" withMessage:@"Network Error..."];
                                              }
                                              
                                          }
                                          
                                      }];
}

-(void)unLikeAPost:(NSString *)postID cell:(PostedGifsCustomeCell *)cell indexpath:(NSInteger)arrindex replug:(BOOL)isReplugPost
{
    NSString *url = [NSString stringWithFormat:@"posts/like/%@",postID];
    //http://192.168.0.22:8353/v1/posts/like/{postId}
    
    //    for (AFHTTPRequestOperation *operation in [AFHTTPRequestOperationManager manager].operationQueue.operations)
    //    {
    //        NSString *path = operation.request.URL.path;
    //        if ([path containsString:url])
    //        {
    //            [operation cancel];
    //        }
    //    }
    
    NSMutableDictionary *header =[[NSMutableDictionary alloc]initWithObjectsAndKeys:@"application/json",@"Content-Type",[NSUSERDEFAULTS valueForKey:kUSERTOKEN],@"User-Token", nil];
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    
    __weak typeof(cell) weakCell = cell;
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypeDelete withHttpHeaders:header withServiceName:url withParameters:nil withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"--responce dict %@",responseDict);
        
        [CommonFunction removeActivityIndicator];
        [refreshControl endRefreshing];
        if(responseDict==Nil)        {
            [CommonFunction alertTitle:@"" withMessage:@"Server Error"];
            
            NSString *postidstr;
            NSInteger likeCount;
            if (isReplugPost)
            {
                likeCount = [dataManager.globalUserReplugArray[arrindex][@"likes_count"] integerValue];
                
                [weakCell.imgLikeFillUnfill setImage:[UIImage imageNamed:@"PG_LikeBtn"]];
                weakCell.imgLikeFillUnfill.tag=1;
                
                likeCount += 1;
                weakCell.lblLikeCount.text=[NSString stringWithFormat:@"%ld",(long)likeCount];
                [dataManager.globalUserReplugArray[arrindex] setValue:[NSNumber numberWithInteger:likeCount] forKey:@"likes_count"];
                [dataManager.globalUserReplugArray[arrindex] setValue:@1 forKey:@"is_like"];
                
                postidstr = dataManager.globalUserReplugArray[arrindex][@"post_id"];
            }
            else
            {
                likeCount = [dataManager.globalUserFeedArray[arrindex][@"likes_count"] integerValue];
                
                [weakCell.imgLikeFillUnfill setImage:[UIImage imageNamed:@"PG_LikeBtn"]];
                weakCell.imgLikeFillUnfill.tag=1;
                
                likeCount += 1;
                weakCell.lblLikeCount.text=[NSString stringWithFormat:@"%ld",(long)likeCount];
                [dataManager.globalUserFeedArray[arrindex] setValue:[NSNumber numberWithInteger:likeCount] forKey:@"likes_count"];
                [dataManager.globalUserFeedArray[arrindex] setValue:@1 forKey:@"is_like"];
                
                postidstr = dataManager.globalUserFeedArray[arrindex][@"post_id"];
            }
            
            for (NSInteger i=0; i<dataManager.globalHomeFeedArray.count; i++)
            {
                NSString *userPostID = dataManager.globalHomeFeedArray[i][@"post_id"];
                if ([userPostID isEqualToString:postidstr])
                {
                    [dataManager.globalHomeFeedArray[i] setValue:[NSNumber numberWithInteger:likeCount] forKey:@"likes_count"];
                    [dataManager.globalHomeFeedArray[i] setValue:@1 forKey:@"is_like"];
                }
            }
        }
        else if([operation.response statusCode]==200)
        {
            
        }
        else
        {
            [CommonFunction alertTitle:@"" withMessage:[responseDict objectForKey:@"msg"]];
            NSString *postidstr;
            NSInteger likeCount;
            if (isReplugPost)
            {
                likeCount = [dataManager.globalUserReplugArray[arrindex][@"likes_count"] integerValue];
                
                [weakCell.imgLikeFillUnfill setImage:[UIImage imageNamed:@"PG_LikeBtn"]];
                weakCell.imgLikeFillUnfill.tag=1;
                
                likeCount += 1;
                weakCell.lblLikeCount.text=[NSString stringWithFormat:@"%ld",(long)likeCount];
                [dataManager.globalUserReplugArray[arrindex] setValue:[NSNumber numberWithInteger:likeCount] forKey:@"likes_count"];
                [dataManager.globalUserReplugArray[arrindex] setValue:@1 forKey:@"is_like"];
                
                postidstr = dataManager.globalUserReplugArray[arrindex][@"post_id"];
            }
            else
            {
                likeCount = [dataManager.globalUserFeedArray[arrindex][@"likes_count"] integerValue];
                
                [weakCell.imgLikeFillUnfill setImage:[UIImage imageNamed:@"PG_LikeBtn"]];
                weakCell.imgLikeFillUnfill.tag=1;
                
                likeCount += 1;
                weakCell.lblLikeCount.text=[NSString stringWithFormat:@"%ld",(long)likeCount];
                [dataManager.globalUserFeedArray[arrindex] setValue:[NSNumber numberWithInteger:likeCount] forKey:@"likes_count"];
                [dataManager.globalUserFeedArray[arrindex] setValue:@1 forKey:@"is_like"];
                
                postidstr = dataManager.globalUserFeedArray[arrindex][@"post_id"];
            }
            
            for (NSInteger i=0; i<dataManager.globalHomeFeedArray.count; i++)
            {
                NSString *userPostID = dataManager.globalHomeFeedArray[i][@"post_id"];
                if ([userPostID isEqualToString:postidstr])
                {
                    [dataManager.globalHomeFeedArray[i] setValue:[NSNumber numberWithInteger:likeCount] forKey:@"likes_count"];
                    [dataManager.globalHomeFeedArray[i] setValue:@1 forKey:@"is_like"];
                }
            }
        }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          [CommonFunction removeActivityIndicator];
                                          
                                          NSString *postidstr;
                                          NSInteger likeCount;
                                          if (isReplugPost)
                                          {
                                              likeCount = [dataManager.globalUserReplugArray[arrindex][@"likes_count"] integerValue];
                                              
                                              [weakCell.imgLikeFillUnfill setImage:[UIImage imageNamed:@"PG_LikeBtn"]];
                                              weakCell.imgLikeFillUnfill.tag=1;
                                              
                                              likeCount += 1;
                                              weakCell.lblLikeCount.text=[NSString stringWithFormat:@"%ld",(long)likeCount];
                                              [dataManager.globalUserReplugArray[arrindex] setValue:[NSNumber numberWithInteger:likeCount] forKey:@"likes_count"];
                                              [dataManager.globalUserReplugArray[arrindex] setValue:@1 forKey:@"is_like"];
                                              
                                              postidstr = dataManager.globalUserReplugArray[arrindex][@"post_id"];
                                          }
                                          else
                                          {
                                              likeCount = [dataManager.globalUserFeedArray[arrindex][@"likes_count"] integerValue];
                                              
                                              [weakCell.imgLikeFillUnfill setImage:[UIImage imageNamed:@"PG_LikeBtn"]];
                                              weakCell.imgLikeFillUnfill.tag=1;
                                              
                                              likeCount += 1;
                                              weakCell.lblLikeCount.text=[NSString stringWithFormat:@"%ld",(long)likeCount];
                                              [dataManager.globalUserFeedArray[arrindex] setValue:[NSNumber numberWithInteger:likeCount] forKey:@"likes_count"];
                                              [dataManager.globalUserFeedArray[arrindex] setValue:@1 forKey:@"is_like"];
                                              
                                              postidstr = dataManager.globalUserFeedArray[arrindex][@"post_id"];
                                          }
                                          
                                          for (NSInteger i=0; i<dataManager.globalHomeFeedArray.count; i++)
                                          {
                                              NSString *userPostID = dataManager.globalHomeFeedArray[i][@"post_id"];
                                              if ([userPostID isEqualToString:postidstr])
                                              {
                                                  [dataManager.globalHomeFeedArray[i] setValue:[NSNumber numberWithInteger:likeCount] forKey:@"likes_count"];
                                                  [dataManager.globalHomeFeedArray[i] setValue:@1 forKey:@"is_like"];
                                              }
                                          }
                                          
                                          if([operation.response statusCode]  == 400 ){
                                              NSLog(@"impo response %@",operation.response);
                                              [refreshControl endRefreshing];
                                              [CommonFunction removeActivityIndicator];
                                              APPDELEGATE.window.userInteractionEnabled=NO;
                                              [CommonFunction alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                          }
                                          else{
                                              
                                              if (wbServiceCount==1) {
                                                  wbServiceCount=0;
                                              }
                                              else
                                              {
                                                  [CommonFunction alertTitle:@"" withMessage:@"Network Error..."];
                                              }
                                          }
                                      }];
}

//Posted Gif Cell IbAction Methods
-(IBAction)likeBtnClicked:(UIButton *)sender
{
    if (!isInternetAvailabel) {
        return;
    }
    NSLog(@"%ld",(long)sender.tag);
    PostedGifsCustomeCell *cellView = (PostedGifsCustomeCell*) [self.tblVwData cellForRowAtIndexPath:[NSIndexPath indexPathForRow:sender.tag inSection:0]];
    NSMutableArray *array = [NSMutableArray array];
    if (isReplugSelected)
    {
        [array addObjectsFromArray:dataManager.globalUserReplugArray];
    }
    else
    {
        [array addObjectsFromArray:dataManager.globalUserFeedArray];
    }
    NSInteger likeCount = [array[sender.tag][@"likes_count"] integerValue];
    
    if (cellView.imgLikeFillUnfill.tag==0) {
        
        [cellView.imgLikeFillUnfill setImage:[UIImage imageNamed:@"PG_LikeBtn"]];
        cellView.imgLikeFillUnfill.tag=1;
        
        [CommonFunction showActivityIndicatorWithText:nil];
        likeCount += 1;
        
        cellView.lblLikeCount.text=[NSString stringWithFormat:@"%ld",(long)likeCount];
        
        [array[sender.tag] setValue:[NSNumber numberWithInteger:likeCount] forKey:@"likes_count"];
        [array[sender.tag] setValue:@1 forKey:@"is_like"];
        
        NSString *postID = array[sender.tag][@"post_id"];
        
        if (isReplugSelected)
        {
            [dataManager.globalUserReplugArray removeAllObjects];
            [dataManager.globalUserReplugArray addObjectsFromArray:array];
        }
        else
        {
            [dataManager.globalUserFeedArray removeAllObjects];
            [dataManager.globalUserFeedArray addObjectsFromArray:array];
        }
        
        
        for (NSInteger i=0; i<dataManager.globalHomeFeedArray.count; i++)
        {
            NSString *userPostID = dataManager.globalHomeFeedArray[i][@"post_id"];
            if ([userPostID isEqualToString:postID])
            {
                [dataManager.globalHomeFeedArray[i] setValue:[NSNumber numberWithInteger:likeCount] forKey:@"likes_count"];
                [dataManager.globalHomeFeedArray[i] setValue:@1 forKey:@"is_like"];
            }
        }
        
        [self likeAPost:postID cell:cellView indexpath:sender.tag replug:isReplugSelected];
    }
    else
    {
        [CommonFunction showActivityIndicatorWithText:nil];
        
        [cellView.imgLikeFillUnfill setImage:[UIImage imageNamed:@"HF_likeUnfillIcon"]];
        cellView.imgLikeFillUnfill.tag=0;
        
        likeCount -= 1;
        cellView.lblLikeCount.text=[NSString stringWithFormat:@"%ld",(long)likeCount];
        [array[sender.tag] setValue:[NSNumber numberWithInteger:likeCount] forKey:@"likes_count"];
        [array[sender.tag] setValue:@0 forKey:@"is_like"];
        
        
        NSString *postID = array[sender.tag][@"post_id"];
        
        if (isReplugSelected)
        {
            [dataManager.globalUserReplugArray removeAllObjects];
            [dataManager.globalUserReplugArray addObjectsFromArray:array];
        }
        else
        {
            [dataManager.globalUserFeedArray removeAllObjects];
            [dataManager.globalUserFeedArray addObjectsFromArray:array];
        }
        
        for (NSInteger i=0; i<dataManager.globalHomeFeedArray.count; i++)
        {
            NSString *userPostID = dataManager.globalHomeFeedArray[i][@"post_id"];
            if ([userPostID isEqualToString:postID])
            {
                [dataManager.globalHomeFeedArray[i] setValue:[NSNumber numberWithInteger:likeCount] forKey:@"likes_count"];
                [dataManager.globalHomeFeedArray[i] setValue:@0 forKey:@"is_like"];
            }
        }
        [self unLikeAPost:postID cell:cellView indexpath:sender.tag replug:isReplugSelected];
    }

    
    
    
//    LikeVC *lvc=[[LikeVC alloc]init];
//    if (isReplugSelected)
//    {
//        lvc.strPostId=dataManager.globalUserReplugArray[sender.tag][@"post_id"];
//        lvc.strLikeCount=[NSString stringWithFormat:@"%@",dataManager.globalUserReplugArray[sender.tag][@"likes_count"]];
//    }
//    else
//    {
//        lvc.strPostId=dataManager.globalUserFeedArray[sender.tag][@"post_id"];
//        lvc.strLikeCount=[NSString stringWithFormat:@"%@",dataManager.globalUserFeedArray[sender.tag][@"likes_count"]];
//    }
//    lvc.hidesBottomBarWhenPushed=YES;
//    [self.navigationController pushViewController:lvc animated:YES];
}
-(IBAction)commentBtnClicked:(UIButton *)sender
{
    CommentVC *lvc=[[CommentVC alloc]init];
    lvc.commentDelegate=self;
    if (isReplugSelected)
    {
        lvc.strPostId=dataManager.globalUserReplugArray[sender.tag][@"post_id"];
        lvc.strLikeCount=[NSString stringWithFormat:@"%@",dataManager.globalUserReplugArray[sender.tag][@"comments_count"]];
    }
    else
    {
        lvc.strPostId=dataManager.globalUserFeedArray[sender.tag][@"post_id"];
        lvc.strLikeCount=[NSString stringWithFormat:@"%@",dataManager.globalUserFeedArray[sender.tag][@"comments_count"]];
    }
    lvc.hidesBottomBarWhenPushed=YES;
    [self.navigationController pushViewController:lvc animated:YES];
}
-(IBAction)rePlugBtnClicked:(UIButton *)sender
{
    ReplugVC *rpvc=[[ReplugVC alloc]init];
    if (isReplugSelected)
    {
        rpvc.strPostId=dataManager.globalUserReplugArray[sender.tag][@"post_id"];
        rpvc.strLikeCount=[NSString stringWithFormat:@"%@",dataManager.globalUserReplugArray[sender.tag][@"replugged_count"]];
    }
    else
    {
        rpvc.strPostId=dataManager.globalUserFeedArray[sender.tag][@"post_id"];
        rpvc.strLikeCount=[NSString stringWithFormat:@"%@",dataManager.globalUserFeedArray[sender.tag][@"replugged_count"]];
    }
    rpvc.hidesBottomBarWhenPushed=YES;
    [self.navigationController pushViewController:rpvc animated:YES];
}
-(IBAction)menuBtnClicked:(UIButton *)sender
{
    
    moreButton = sender;
    [sender setImage:[UIImage imageNamed:@"PG_menu_Red"] forState:UIControlStateNormal];
    
    [vwCstmeActionSheet setFrame:CGRectMake(0.0f, [UIScreen mainScreen].bounds.size.height, [UIScreen mainScreen].bounds.size.width,160*scaleY)];
    [APPDELEGATE.window.rootViewController.view addSubview:vwCstmeActionSheet];
    btnCancel.layer.cornerRadius=10.0f;
    btnCancel.layer.borderColor=[UIColor whiteColor].CGColor;
    btnCancel.layer.borderWidth=2.0f;
    btnDelete.layer.cornerRadius=10.0f;
    btnDelete.layer.borderColor=[UIColor whiteColor].CGColor;
    btnDelete.layer.borderWidth=2.0f;
    btnShare.layer.cornerRadius=10.0f;
    btnShare.layer.borderColor=[UIColor whiteColor].CGColor;
    btnShare.layer.borderWidth=2.0f;
    
//    [self.tblVwData scroll]

//    self.view.alpha=0.5;

    selectedIndexpath=(int)sender.tag;
    
    NSLog(@"%ld",(long)sender.tag);
    [self.tblVwData scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:sender.tag inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
    [vwBg setFrame:self.view.frame];
    __weak typeof(self) weakSelf = self;
    [UIView animateWithDuration:0.3
                          delay:0.0
                        options: UIViewAnimationOptionTransitionFlipFromBottom
                     animations:^{
                         [vwCstmeActionSheet setFrame:CGRectMake(0.0f, 408*scaleY, [UIScreen mainScreen].bounds.size.width,160*scaleY)];
                         [weakSelf.view addSubview:vwBg];
                     }
                     completion:^(BOOL finished){
                     //

                     }];

}


//User Profile View Buttons Methods
- (IBAction)editProfileBtnClicked:(id)sender {
    
    EditUserProfileVC *eupvc=[[EditUserProfileVC alloc]init];
    eupvc.dictData=dictData;
    [self presentViewController:eupvc animated:YES completion:nil];
}
- (IBAction)settingBtnClicked:(id)sender
{
    SettingsVC *settings = [[SettingsVC alloc]initWithNibName:NSStringFromClass([SettingsVC class]) bundle:nil];
    settings.hidesBottomBarWhenPushed=YES;
    [self.navigationController pushViewController:settings animated:YES];
}
- (IBAction)gifRepluggedBtnClicked:(id)sender {
    NSLog(@"Gif Replugged Btn Clicked");
    if (isReplugSelected || isPageRefreshingGif)
    {
        return;
    }
    
    [timerGifCount invalidate],timerGifCount=nil;
    prevIndexPath = visibleIndexPath = nil;
    
    [lblReplugCount setTextColor:[UIColor colorWithRed:228/255.0f green:15/255.0f blue:73/255.0f alpha:1.0f]];
    [_replugImageIcon setImage:[UIImage imageNamed:@"UP_refreshIcon_Selected"]];
    
    [lblGifPostedCOunt setTextColor:[UIColor colorWithRed:148/255.0f green:148/255.0f blue:148/255.0f alpha:1.0f]];
    [_gifImageIcon setImage:[UIImage imageNamed:@"UP_roundIcon"]];
    
    isReplugSelected = YES;
    [self.tblVwData reloadData];
    if (dataManager.globalUserReplugArray.count>0)
    {
        
    }
    else
    {
        [self getMyPost];
    }
}
- (IBAction)gifPostedBtnClicked:(id)sender {
        NSLog(@"Gif Posted Btn Clicked");
    if (!isReplugSelected || isPageRefreshingReplug)
    {
        return;
    }
    
    [timerGifCount invalidate],timerGifCount=nil;
    prevIndexPath = visibleIndexPath = nil;
    
    [lblGifPostedCOunt setTextColor:[UIColor colorWithRed:228/255.0f green:15/255.0f blue:73/255.0f alpha:1.0f]];
    [_gifImageIcon setImage:[UIImage imageNamed:@"UP_roundIcon_Selected"]];
    
    [lblReplugCount setTextColor:[UIColor colorWithRed:148/255.0f green:148/255.0f blue:148/255.0f alpha:1.0f]];
    [_replugImageIcon setImage:[UIImage imageNamed:@"UP_refreshIcon"]];
    
    isReplugSelected = NO;
    [self.tblVwData reloadData];
    if (dataManager.globalUserFeedArray.count>0)
    {
        
    }
    else
    {
        [self getMyPost];
    }
}
- (IBAction)followersBtnClicked:(id)sender {
    NSLog(@"followersBtnClicked Posted Btn Clicked");
    FollowerFollowingVC *followVC = [[FollowerFollowingVC alloc]initWithNibName:NSStringFromClass([FollowerFollowingVC class]) bundle:nil];
    followVC.userID = dictData[@"id"];
    followVC.type = 0;
    followVC.hidesBottomBarWhenPushed=YES;
    [self.navigationController pushViewController:followVC animated:YES];
}
- (IBAction)followingBtnClicked:(id)sender {
            NSLog(@"followingBtnClicked Posted Btn Clicked");
    FollowerFollowingVC *followVC = [[FollowerFollowingVC alloc]initWithNibName:NSStringFromClass([FollowerFollowingVC class]) bundle:nil];
    followVC.userID = dictData[@"id"];
    followVC.type = 1;
    followVC.hidesBottomBarWhenPushed=YES;
    [self.navigationController pushViewController:followVC animated:YES];
}

#pragma mark - UITableView Datasource Method

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
// set this according to that you want...
    if(IS_IPHONE6PLUS)
    {
            return 86;
    }
    else if (IS_IPHONE6) {
            return 56;
    }
    else
    {
            return 0.0f;
    }

}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *vw=[[UIView alloc]init];
    vw.backgroundColor=[UIColor clearColor];
    vw.userInteractionEnabled = NO;
    return vw;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
        NSLog(@"%f",461*SCREEN_YScale);
    return 461*SCREEN_YScale;

}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (isReplugSelected)
    {
        return dataManager.globalUserReplugArray.count;
    }
    return dataManager.globalUserFeedArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *cellIdentifier=@"PostedGifsCustomeCell";
    
    PostedGifsCustomeCell *cell=[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    
    cell.delegate=nil;
    cell.delegate=self;
    
    //    arrVisibleCell=[self.tblVwData indexPathsForVisibleRows];
    NSMutableArray *array = [NSMutableArray array];
    if (isReplugSelected)
    {
        [array addObjectsFromArray:dataManager.globalUserReplugArray];
    }
    else
    {
        [array addObjectsFromArray:dataManager.globalUserFeedArray];
    }
    if (array.count>0) {
        lblMsgError.text=@"";
        NSString *strImgUrl=array[indexPath.row][@"post_image"];
        NSString *mainURL=[NSString stringWithFormat:@"%@",strImgUrl];
        
        [cell.btnLike addTarget:self action:@selector(likeBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
        [cell.btnComment addTarget:self action:@selector(commentBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
        [cell.btnReplug addTarget:self action:@selector(rePlugBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
        [cell.btnMoreClicked addTarget:self action:@selector(menuBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
        
        for (NSInteger i=0; i<cell.imgVwGif.gestureRecognizers.count; i++)
        {
            UITapGestureRecognizer *imgLikeTap = cell.imgVwGif.gestureRecognizers[i];
            [cell.imgVwGif removeGestureRecognizer:imgLikeTap];
        }
        
        UILongPressGestureRecognizer *imgsavepress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(onSaveGifAction:)];
        [cell.imgVwGif addGestureRecognizer:imgsavepress];
        
        UITapGestureRecognizer *imgLikeTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onDoubleTapUserLikeAction:)];
        imgLikeTap.numberOfTapsRequired=2;
        [cell.imgVwGif addGestureRecognizer:imgLikeTap];
        cell.imgVwGif.userInteractionEnabled = YES;
        cell.imgVwGif.tag = indexPath.row;
        
        cell.btnLike.tag=indexPath.row;
        cell.btnComment.tag=indexPath.row;
        cell.btnReplug.tag=indexPath.row;
        cell.btnMoreClicked.tag=indexPath.row;
        if ([[array[indexPath.row][@"is_like"]stringValue] isEqualToString:@"0"]) {
            [cell.imgLikeFillUnfill setImage:[UIImage imageNamed:@"HF_likeUnfillIcon"]];
            cell.imgLikeFillUnfill.tag=0;
        }
        else
        {
            [cell.imgLikeFillUnfill setImage:[UIImage imageNamed:@"PG_LikeBtn"]];
            cell.imgLikeFillUnfill.tag=1;
        }
        
        [[array[indexPath.row][@"is_comment"]stringValue] isEqualToString:@"0"]
        ?[cell.imgCommentFillUnfill setImage:[UIImage imageNamed:@"PG_Comment"]]
        :[cell.imgCommentFillUnfill setImage:[UIImage imageNamed:@"PG_CommentSelected"]];
        
        cell.lblCommentCount.text=[NSString stringWithFormat:@"%@",array[indexPath.row][@"comments_count"]];
        cell.lblLikeCount.text=[NSString stringWithFormat:@"%@",array[indexPath.row][@"likes_count"]];
        cell.lblReplugCount.text=[NSString stringWithFormat:@"%@",array[indexPath.row][@"replugged_count"]];
        
        NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
        paragraphStyle.lineSpacing = 5.0f;
        paragraphStyle.lineBreakMode = NSLineBreakByTruncatingTail;
        
        NSString *string = [NSString stringWithFormat:@"%@",array[indexPath.row][@"caption"]];
        NSDictionary *attributtes = @{
                                      NSParagraphStyleAttributeName : paragraphStyle,
                                      NSForegroundColorAttributeName : [UIColor whiteColor],
                                      NSFontAttributeName : [UIFont fontWithName:cell.lblCaption.font.fontName size:14.0f]
                                      };
        cell.lblCaption.attributedText = [[NSAttributedString alloc] initWithString:string
                                                                         attributes:attributtes];
        
        
        cell.tag = indexPath.row;
        
        
        NSURL *url1 = [NSURL URLWithString:mainURL];
        NSNumber *progress = [[AppWebHandler sharedInstance].dictProgressTask objectForKey:[url1 absoluteString]];
        if (!progress) progress = @(0.0);
        
        [cell setProgress:[progress floatValue]];
        
        if ([progress intValue]==1) {
            
            [cell.progresView setHidden:true];
            [cell.imgVwGif setHidden:false];
        }
        else
        {
            [cell.progresView setHidden:false];
            [cell.imgVwGif setHidden:true];
        }
        
        if (![[AppWebHandler sharedInstance].dictProgressTask objectForKey:[url1 absoluteString]]) {
            
            // Download Giff with URL Item
            [self downloadGifFromUrl:url1];
        }
        else
        {
            
            // Update Progress Buffer
            
            NSURL *localURL = [self URLForGiffWithName:[mainURL lastPathComponent]];
            
            if ([[NSFileManager defaultManager] fileExistsAtPath:[localURL path]]) {
                
                [cell.progresView setHidden:true];
                [cell.imgVwGif setHidden:false];
                NSData *animatedImageData = [[NSFileManager defaultManager] contentsAtPath:localURL.path];
                
                FLAnimatedImage *animatedImage = [[FLAnimatedImage alloc] initWithAnimatedGIFData:animatedImageData generateThumbnail:NO];
                [cell.imgVwGif setAnimatedImage:nil];
                [cell.imgVwGif setAnimatedImage:animatedImage];
            }
        }
    }
    else
    {
        lblMsgError.text=@"No post found.";
    }
    [cell handleCaptionBar];
    return cell;
}


- (void)tableView:(UITableView *)tableView didEndDisplayingCell:(PostedGifsCustomeCell *)cell forRowAtIndexPath:(NSIndexPath*)indexPath
{
    if (cell) {
        [cell resetCaptionBar];
    }
}

//- (void)tableView:(UITableView *)tableView didEndDisplayingCell:(PostedGifsCustomeCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    NSString *strImgUrl=arrMyPost[indexPath.row][@"post_image"];
//    NSString *mainURL=[NSString stringWithFormat:@"%@%@",strMainImgURl,strImgUrl];
//    NSURL *localURL = [self URLForGiffWithName:[mainURL lastPathComponent]];
//    
//    if ([[NSFileManager defaultManager] fileExistsAtPath:[localURL path]]) {
//        
//        [cell.imgVwGif stopAnimating];
//    }
//
//}



/// Even though NSURLCache *may* cache the results for remote images, it doesn't guarantee it.
/// Cache control headers or internal parts of NSURLCache's implementation may cause these images to become uncache.
/// Here we enfore strict disk caching so we're sure the images stay around.
- (void)loadAnimatedImageWithURL:(NSURL *const)url completion:(void (^)(FLAnimatedImage *animatedImage))completion
{
    NSString *const filename = url.lastPathComponent;
    NSString *const diskPath = [NSHomeDirectory() stringByAppendingPathComponent:filename];
    
    NSData * __block animatedImageData = [[NSFileManager defaultManager] contentsAtPath:diskPath];
    FLAnimatedImage * __block animatedImage = [[FLAnimatedImage alloc] initWithAnimatedGIFData:animatedImageData generateThumbnail:NO];
    
    if (animatedImage) {
        if (completion) {
            completion(animatedImage);
        }
    } else {
        [[[NSURLSession sharedSession] dataTaskWithURL:url completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            animatedImageData = data;
            animatedImage = [[FLAnimatedImage alloc] initWithAnimatedGIFData:animatedImageData generateThumbnail:NO];
            if (animatedImage) {
                if (completion) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        completion(animatedImage);
                    });
                }
                [data writeToFile:diskPath atomically:YES];
            }
        }] resume];
    }
}


#pragma mark - WebService API
-(void)deleteMyPost:(NSString *)strPostID Indexpath:(int)cellIndexPath
{
    
    NSString *url = [NSString stringWithFormat:@"posts/%@",strPostID];
    //http://192.168.0.22:8353/v1/users/{username}
    
    NSMutableDictionary *header =[[NSMutableDictionary alloc]initWithObjectsAndKeys:@"application/json",@"Content-Type",[NSUSERDEFAULTS valueForKey:kUSERTOKEN],@"User-Token", nil];
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    __weak typeof(self) weakSelf = self;
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypeDelete withHttpHeaders:header withServiceName:url withParameters:nil withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"responce dict %@",responseDict);
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
        [CommonFunction removeActivityIndicator];
        if(responseDict==Nil)        {
            [CommonFunction alertTitle:@"" withMessage:@"Server Error"];
        }
        else if([operation.response statusCode]==200)
            
        {
            NSString *postID = nil;
            if (isReplugSelected)
            {
                postID = dataManager.globalUserReplugArray[cellIndexPath][@"post_id"];
                [dataManager.globalUserReplugArray removeObjectAtIndex:cellIndexPath];
                if (dataManager.globalUserReplugArray.count == 0)
                {
                    [self.tblVwData setContentOffset:CGPointZero];
                }
            }
            else
            {
                postID = dataManager.globalUserFeedArray[cellIndexPath][@"post_id"];
                [dataManager.globalUserFeedArray removeObjectAtIndex:cellIndexPath];
                if (dataManager.globalUserFeedArray.count == 0)
                {
                    [self.tblVwData setContentOffset:CGPointZero];
                }
            }
            
            
            for (NSInteger i=0; i<dataManager.globalHomeFeedArray.count; i++)
            {
                NSString *userPostID = dataManager.globalHomeFeedArray[i][@"post_id"];
                if ([userPostID isEqualToString:postID])
                {
                    [dataManager.globalHomeFeedArray removeObjectAtIndex:i];
                }
            }
            

            [self.tblVwData deleteRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:selectedIndexpath inSection:0]] withRowAnimation:UITableViewRowAnimationRight];
            [self.tblVwData reloadData];
            [CommonFunction showActivityIndicatorWithText:@""];
            [weakSelf getUserProfile];
//            [weakSelf getMyPost];
                        //[self.tblVwData reloadData];
        }
        else
        {
            [CommonFunction alertTitle:@"" withMessage:[responseDict objectForKey:@"msg"]];
        }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          if([operation.response statusCode]  == 400 ){
                                              NSLog(@"impo response %@",operation.response);
                                              
                                              [CommonFunction removeActivityIndicator];
                                              [CommonFunction alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                          }
                                          else{
                                              
                                              if (wbServiceCount==1) {
                                                  wbServiceCount=0;
                                                  [CommonFunction showActivityIndicatorWithText:@""];
                                                  [weakSelf getUserProfile];
                                              }
                                              else
                                              {
                                                  [CommonFunction removeActivityIndicator];
                                                  [CommonFunction alertTitle:@"" withMessage:@"Network Error..."];
                                              }
                                              
                                          }
                                          
                                      }];
}

-(void)getUserProfile
{
    NSString *url = [NSString stringWithFormat:@"users/%@",[NSUSERDEFAULTS valueForKey:kUSERNAME]];
    NSMutableDictionary *header =[[NSMutableDictionary alloc]initWithObjectsAndKeys:@"application/json",@"Content-Type",[NSUSERDEFAULTS valueForKey:kUSERTOKEN],@"User-Token", nil];

    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    
//    __weak typeof(self) weakSelf = self;
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypeGet withHttpHeaders:header withServiceName:url withParameters:nil withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"responce dict %@",responseDict);
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
        [CommonFunction removeActivityIndicator];
        if(responseDict==Nil)        {
            [CommonFunction alertTitle:@"" withMessage:@"Server Error"];
        }
        else if([operation.response statusCode]==200)
        {
            dictData=[responseDict[@"userInfo"]mutableCopy];
            
            lblFollowersCount.text=[NSString stringWithFormat:@"%@",[[responseDict valueForKey:@"userInfo"]valueForKey:@"followers_count"]];
            lblFollowingCount.text=[NSString stringWithFormat:@"%@",[[responseDict valueForKey:@"userInfo"]valueForKey:@"following_count"]];
            lblReplugCount.text=[NSString stringWithFormat:@"%@",[[responseDict valueForKey:@"userInfo"]valueForKey:@"replugged_count"]];
            lblGifPostedCOunt.text=[NSString stringWithFormat:@"%@",[[responseDict valueForKey:@"userInfo"]valueForKey:@"posted_gif_count"]];
            lblUserBio.text=[NSString stringWithFormat:@"%@",[[responseDict valueForKey:@"userInfo"]valueForKey:@"bio"]];
            lblLocation.text=[NSString stringWithFormat:@"%@",[[responseDict valueForKey:@"userInfo"]valueForKey:@"address"]];
            lblUsername.text=[NSString stringWithFormat:@"%@",[[responseDict valueForKey:@"userInfo"]valueForKey:@"username"]].capitalizedString;
            
            NSString *strAvtarImg=[[responseDict valueForKey:@"userInfo"]valueForKey:@"avatar_image"];
            if (strAvtarImg.length>1) {
                if ([strAvtarImg.pathExtension isEqualToString:@"gif"])
                {
                    imgProfileImg.contentMode = UIViewContentModeScaleAspectFill;
                }
                else
                {
                    imgProfileImg.contentMode = UIViewContentModeScaleToFill;
                }
                [imgProfileImg sd_setImageWithURL:[NSURL URLWithString:strAvtarImg] placeholderImage:[UIImage imageNamed:@"default.jpg"] options:SDWebImageRefreshCached completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                }];
             }
            else
            {
                imgProfileImg.contentMode = UIViewContentModeScaleToFill;
                [imgProfileImg setImage:[UIImage imageNamed:@"default.jpg"]];
            }
            [imgProfileBgImg setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[[responseDict valueForKey:@"userInfo"]valueForKey:@"background_image"]]] usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        }
        else
        {
            [CommonFunction alertTitle:@"" withMessage:[responseDict objectForKey:@"msg"]];
        }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          [CommonFunction removeActivityIndicator];
                                          if([operation.response statusCode]  == 400 ){
                                              NSLog(@"impo response %@",operation.response);
                                              [CommonFunction alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                          }
                                          else{
                                              
                                              if (wbServiceCount==1) {
                                                  wbServiceCount=0;
                                              }
                                              else
                                              {
                                                  [CommonFunction alertTitle:@"" withMessage:@"Network Error..."];
                                              }
                                          }
                                      }];
}

-(void)getMyPost
{
    NSString *url = [NSString stringWithFormat:@"posts/myPosts"];
    
    NSMutableDictionary *header =[[NSMutableDictionary alloc]initWithObjectsAndKeys:@"application/json",@"Content-Type",[NSUSERDEFAULTS valueForKey:kUSERTOKEN],@"User-Token", nil];
    
    NSString *type = isReplugSelected?@"replug":@"gif";
    NSInteger page = isReplugSelected?currentPageReplugd:currentPage;
        NSMutableDictionary *param=[[NSMutableDictionary alloc]initWithObjectsAndKeys:[NSString stringWithFormat:@"%ld",(long)page],@"page",type,@"post_type", nil];
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    __weak typeof(self) weakSelf = self;
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypePost withHttpHeaders:header withServiceName:url withParameters:param withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"responce dict %@",responseDict);
        [self.tblVwData setTableFooterView:nil];
        [CommonFunction removeActivityIndicator];

        if (refreshControl.isRefreshing)
        {
            AudioServicesPlaySystemSound(1109);
        }
       [refreshControl endRefreshing];
        if(responseDict==Nil){
            if (isReplugSelected)
            {
                if (currentPageReplugd>1)
                {
                    currentPageReplugd--;
                }
                isPageRefreshingReplug = NO;
            }else
            {
                if (currentPage>1)
                {
                    currentPage--;
                }
                isPageRefreshingGif = NO;
            }
            [CommonFunction alertTitle:@"" withMessage:@"Server Error"];
        }
        else if([operation.response statusCode]==200)
        {
            NSArray *arrListing = [responseDict objectForKey:@"posts"];
            
            if (arrListing.count > 0)
            {
                lblMsgError.text=@"";
                if (isReplugSelected)
                {
                    if (currentPageReplugd == 1)
                    {
                        if (dataManager.globalUserReplugArray.count > 0)
                        {
                            [dataManager.globalUserReplugArray removeAllObjects];
                        }
                        [dataManager.globalUserReplugArray addObjectsFromArray:arrListing];
                    }
                    else
                    {
                        [dataManager.globalUserReplugArray addObjectsFromArray:arrListing];
                    }
                    
                    for (NSInteger i = 0 ;i<dataManager.globalUserReplugArray.count;i++) {
                        NSDictionary *dict = dataManager.globalUserReplugArray[i];
                        NSString *strImgUrl=[dict valueForKey:@"post_image"];
                        NSURL *mainURL=[NSURL URLWithString:[NSString stringWithFormat:@"%@",strImgUrl]];
                        NSURL *localURL = [weakSelf URLForGiffWithName:[mainURL lastPathComponent]];
                        if ([[NSFileManager defaultManager] fileExistsAtPath:[localURL path]] && localURL) {
                            [[AppWebHandler sharedInstance].dictProgressTask setObject:@(1.0) forKey:[mainURL absoluteString]];
                        }
                    }
                    totalPageReplugd=[[responseDict objectForKey:@"total_pages"] integerValue];
                    totalPostReplugd=[[responseDict objectForKey:@"total_posts"] integerValue];
                    [self.tblVwData reloadData];
                    isPageRefreshingReplug = NO;
                }
                else
                {
                    if (currentPage == 1)
                    {
                        if (dataManager.globalUserFeedArray.count > 0)
                        {
                            [dataManager.globalUserFeedArray removeAllObjects];
                        }
                        [dataManager.globalUserFeedArray addObjectsFromArray:arrListing];
                    }
                    else
                    {
                        [dataManager.globalUserFeedArray addObjectsFromArray:arrListing];
                    }
                    
                    for (NSInteger i = 0 ;i<dataManager.globalUserFeedArray.count;i++) {
                        NSDictionary *dict = dataManager.globalUserFeedArray[i];
                        NSString *strImgUrl=[dict valueForKey:@"post_image"];
                        NSURL *mainURL=[NSURL URLWithString:[NSString stringWithFormat:@"%@",strImgUrl]];
                        NSURL *localURL = [weakSelf URLForGiffWithName:[mainURL lastPathComponent]];
                        if ([[NSFileManager defaultManager] fileExistsAtPath:[localURL path]] && localURL) {
                            [[AppWebHandler sharedInstance].dictProgressTask setObject:@(1.0) forKey:[mainURL absoluteString]];
                        }
                    }
                    
                    totalPage=[[responseDict objectForKey:@"total_pages"] integerValue];
                    totalPost=[[responseDict objectForKey:@"total_posts"] integerValue];
                    [self.tblVwData reloadData];
                    isPageRefreshingGif = NO;
                }
            }
            else
            {
                lblMsgError.text=@"No Post Found.";
            }
        }
        else
        {
            if (isReplugSelected)
            {
                if (currentPageReplugd>1)
                {
                    currentPageReplugd--;
                }
                isPageRefreshingReplug = NO;
            }else
            {
                if (currentPage>1)
                {
                    currentPage--;
                }
                isPageRefreshingGif = NO;
            }
            [CommonFunction alertTitle:@"" withMessage:[responseDict objectForKey:@"msg"]];
        }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          [self.tblVwData setTableFooterView:nil];
                                          [CommonFunction removeActivityIndicator];
                                          if (isReplugSelected)
                                          {
                                              if (currentPageReplugd>1)
                                              {
                                                  currentPageReplugd--;
                                              }
                                              isPageRefreshingReplug = NO;
                                          }else
                                          {
                                              if (currentPage>1)
                                              {
                                                  currentPage--;
                                              }
                                              isPageRefreshingGif = NO;
                                          }
                                          [refreshControl endRefreshing];
                                          if([operation.response statusCode]  == 400 ){
                                              [CommonFunction alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                          }
                                          else{
                                              
                                              if (wbServiceCount==1) {
                                                  wbServiceCount=0;
                                              }
                                              else
                                              {
                                                  [CommonFunction alertTitle:@"" withMessage:@"Network Error..."];
                                              }
                                          }
                                      }];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - FooterView Animating
-(void)animateFooterView
{
    
    [UIView animateWithDuration:1.0f delay:0.0f options:UIViewAnimationOptionRepeat|UIViewAnimationOptionCurveLinear
                     animations:^{
                         [replugCircle setTransform:CGAffineTransformRotate([replugCircle transform], M_PI-0.00001f)];
                     } completion:nil];

}

#pragma mark - ScrollView Delegate method


- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    BOOL flag = NO;
    if (isReplugSelected && dataManager.globalUserReplugArray.count>0)
    {
        flag = YES;
    }
    else if (!isReplugSelected && dataManager.globalUserFeedArray.count>0)
    {
        flag = YES;
    }
    if (flag)
    {
        CGRect visibleRect = (CGRect){.origin = self.tblVwData.contentOffset, .size = self.tblVwData.bounds.size};
        CGPoint visiblePoint = CGPointMake(CGRectGetMidX(visibleRect), CGRectGetMidY(visibleRect));
        visibleIndexPath = [self.tblVwData indexPathForRowAtPoint:visiblePoint];
        
        if (!prevIndexPath)
        {
            prevIndexPath = visibleIndexPath;
            timerGifCount = [NSTimer scheduledTimerWithTimeInterval:2.0f target:self selector:@selector(fireGifCountApi) userInfo:nil repeats:NO];
        }
        
        // For Gif Count......................................................
        if (prevIndexPath != visibleIndexPath)
        {
            if (timerGifCount.isValid || timerGifCount)
            {
                [timerGifCount invalidate],timerGifCount=nil;
            }
            prevIndexPath = visibleIndexPath;
            timerGifCount = [NSTimer scheduledTimerWithTimeInterval:2.0f target:self selector:@selector(fireGifCountApi) userInfo:nil repeats:NO];
        }
    }
    else
    {
        [timerGifCount invalidate],timerGifCount=nil;
        prevIndexPath = visibleIndexPath = nil;
    }
        lblUsername.lineBreakMode = NSLineBreakByTruncatingTail;
    if (scrollView.contentOffset.y<=0.0f)
    {
        NSLog(@"%f",scrollView.contentOffset.y);
    //[self.tableHeaderView setFrame:CGRectMake(0.0f, 0.0f, 320.0f*scaleX, 262.0f*scaleY)];
    self.lblErrorTpCnstraint.constant=128-scrollView.contentOffset.y;
    self.headerTopConstraint.constant=-scrollView.contentOffset.y;
    self.nameTopConstraint.constant=-scrollView.contentOffset.y;
    self.imgProfileVwTopConstraint.constant=-scrollView.contentOffset.y;
        
    self.tableHeaderView.alpha = 1;

    //  self.settingBtnTopConstraint.constant=-scrollView.contentOffset.y;
    self.settingBtnTopConstraint.constant=25*scaleY;
    self.settingBtnTrailingCnstraint.constant=10*scaleX;
    
    
    [imgProfileImg setFrame:CGRectMake(imgProfileImg.frame.origin.x, imgProfileImg.frame.origin.y, 86*scaleX, 86*scaleX)];
    //[imgProfileImg setFrame:CGRectMake(117*scaleX, 30.0f*scaleY, 86*scaleX, 86*scaleX)];
    //self.nameTopConstraint.constant=0/568*scaleX;
    self.imgProfileVwCenterContraint.constant = 0;
    self.imgProfileVwWidthContraint.constant=86/320*scaleX;
    
    imgProfileImg.layer.cornerRadius=imgProfileImg.frame.size.width/2 ;
    imgProfileImg.clipsToBounds=YES;
    
    lblUserBio.alpha=1 ;
    lblLocation.alpha=1;
    vwGifPosted.alpha=1 ;
    vwGifReplug.alpha=1 ;
    
    self.gifpostedCountTopConstrant.constant=0;
    self.gifRepluggedCountTopConstrant.constant=0;
    self.imgProfileVwCenterContraint.constant =0;
        
        btnSetting.hidden = NO;
        btnViewInfo.hidden = YES;
    
    }
    else
    {
        CGFloat heightRemained=self.tableHeaderView.frame.size.height - 58.0f*SCREEN_YScale;
        
        CGFloat offsetY = -+scrollView.contentOffset.y;
        
        if (offsetY <= -heightRemained)
        {
            lblUsername.lineBreakMode = NSLineBreakByTruncatingTail;
            self.headerTopConstraint.constant = - heightRemained;
            [UIView transitionWithView:self.tableHeaderView duration:.3 options:UIViewAnimationOptionCurveEaseIn animations:^{
                self.tableHeaderView.alpha=1.0f;
                
            } completion:nil];
//            self.nameTopConstraint.constant= MAX(-100*scaleX, offsetY/1.5);
                        self.nameTopConstraint.constant= MAX(-95*scaleX, offsetY/1.5);
            self.imgProfileVwCenterContraint.constant = MAX(-137*scaleX, offsetY);
            self.imgProfileVwTopConstraint.constant = MAX(-4*scaleX, offsetY);
            
            btnSetting.hidden = YES;
            btnViewInfo.hidden = NO;
            
            [UIView animateWithDuration:.1 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
                CGRect frame = imgProfileImg.frame;
                imgProfileImg.frame= frame;
                imgProfileImg.layer.cornerRadius=imgProfileImg.frame.size.width/2;
            } completion:nil];
        }
        else
        {
            if (offsetY <= -heightRemained+12*SCREEN_YScale)
            {
                btnSetting.hidden = YES;
                btnViewInfo.hidden = NO;
            }
            else
            {
                btnSetting.hidden = NO;
                btnViewInfo.hidden = YES;
            }
            
            
            lblUsername.lineBreakMode = NSLineBreakByTruncatingTail;
            self.headerTopConstraint.constant = offsetY;
            CGFloat origin = self.tableHeaderView.frame.origin.y;
            CGFloat delta = fabs(origin - (-offsetY));
            //self.settingBtnTopConstraint.constant=MAX(26*scaleY,offsetY );
            //self.tableHeaderView.alpha = 1 - delta/self.tableHeaderView.frame.size.height*0.4;
            lblUserBio.alpha=1 - delta/self.tableHeaderView.frame.size.height*2;
            lblLocation.alpha=1 - delta/self.tableHeaderView.frame.size.height*2;
            vwGifPosted.alpha=1 - delta/vwGifPosted.frame.size.height*0.9;
            vwGifReplug.alpha=1 - delta/vwGifPosted.frame.size.height*0.9;
            //self.nameTopConstraint.constant= MAX(-100*scaleX, offsetY/1.5);
            self.nameTopConstraint.constant= MAX(-95*scaleX, offsetY/1.5);
            self.imgProfileVwWidthContraint.constant = MAX(-60*scaleX, offsetY*1.2);
            self.imgProfileVwCenterContraint.constant = MAX(-137*scaleX, offsetY);
            self.imgProfileVwTopConstraint.constant = MAX(-4*scaleX, offsetY);
            self.gifpostedCountTopConstrant.constant=MAX(-99*scaleX, offsetY*2);
            self.gifRepluggedCountTopConstrant.constant=MAX(-99*scaleX, offsetY*2);
            imgProfileImg.layer.cornerRadius=imgProfileImg.frame.size.width/2;
        }
    }
    [self.view bringSubviewToFront:self.tableHeaderView];
    
    if (([self.tblVwData contentOffset].y + self.tblVwData.frame.size.height) >= [self.tblVwData contentSize].height)
    {
        NSLog(@"scrolled to bottom");
        if (isReplugSelected)
        {
            if (!isPageRefreshingReplug) {
                
                if(totalPageReplugd <= currentPageReplugd)
                {
                    return;
                }
                else
                {
                    currentPageReplugd++;
                    [self animateFooterView];
                    [self.tblVwData setTableFooterView:vwFoterVw];
                }
                
                isPageRefreshingReplug = YES;
                [self getMyPost];
            }
        }
        else
        {
            if (!isPageRefreshingGif) {
                
                if(totalPage <= currentPage)
                {
                    return;
                }
                else
                {
                    currentPage++;
                    [self animateFooterView];
                    [self.tblVwData setTableFooterView:vwFoterVw];
                }
                isPageRefreshingGif = YES;
                [self getMyPost];
            }
        }
    }
}

#pragma mark -
#pragma mark Gif Count Update
- (void)fireGifCountApi
{
    if (!prevIndexPath) {
        return;
    }
    NSIndexPath *indexPath = prevIndexPath;
    
    NSString *postid = nil;
    BOOL isViewed = NO;
    if (isReplugSelected)
    {
        if (dataManager.globalUserReplugArray.count>0) {
            postid = dataManager.globalUserReplugArray[indexPath.row][@"post_id"];
            isViewed = [dataManager.globalUserReplugArray[indexPath.row][@"is_view"]boolValue];
        }
        else
            return;
    }
    else
    {
        if (dataManager.globalUserFeedArray.count>0) {
            postid = dataManager.globalUserFeedArray[indexPath.row][@"post_id"];
            isViewed = [dataManager.globalUserFeedArray[indexPath.row][@"is_view"]boolValue];
        }
        else
            return;
    }
    
    if (isViewed) {
        return;
    }
    NSString *url = [NSString stringWithFormat:@"posts/views/%@",postid];
    
    NSMutableDictionary *header =[[NSMutableDictionary alloc]initWithObjectsAndKeys:@"application/json",@"Content-Type",[NSUSERDEFAULTS valueForKey:kUSERTOKEN],@"User-Token", nil];
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypePost withHttpHeaders:header withServiceName:url withParameters:nil withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"--responce dict %@",responseDict);
        if(responseDict==Nil){
        }
        else if([operation.response statusCode]==200)
        {
            if ([responseDict[@"success"] boolValue])
            {
                if (!prevIndexPath) {
                    return;
                }
                if (isReplugSelected)
                {
                    if (dataManager.globalUserReplugArray.count>0) {
                        NSInteger gifCount = [dataManager.globalUserReplugArray[indexPath.row][@"view_count"]integerValue];
                        gifCount +=1;
                        [dataManager.globalUserReplugArray[indexPath.row] setValue:[NSNumber numberWithInteger:gifCount] forKey:@"view_count"];
                        [dataManager.globalUserReplugArray[indexPath.row] setValue:[NSNumber numberWithBool:YES] forKey:@"is_view"];
                    }
                }
                else
                {
                    if (dataManager.globalUserFeedArray.count>0) {
                        NSInteger gifCount = [dataManager.globalUserFeedArray[indexPath.row][@"view_count"]integerValue];
                        gifCount +=1;
                        [dataManager.globalUserFeedArray[indexPath.row] setValue:[NSNumber numberWithInteger:gifCount] forKey:@"view_count"];
                        
                        [dataManager.globalUserFeedArray[indexPath.row] setValue:[NSNumber numberWithBool:YES] forKey:@"is_view"];
                        NSString *gifID = dataManager.globalUserFeedArray[indexPath.row][@"post_id"];
                        for (NSInteger i=0; i<dataManager.globalHomeFeedArray.count; i++)
                        {
                            NSString *postID = dataManager.globalHomeFeedArray[i][@"post_id"];
                            
                            if ([gifID isEqualToString:postID])
                            {
                                NSInteger gifCount = [dataManager.globalHomeFeedArray[i][@"view_count"]integerValue];
                                gifCount +=1;
                                [dataManager.globalHomeFeedArray[i] setValue:[NSNumber numberWithInteger:gifCount] forKey:@"view_count"];
                                [dataManager.globalHomeFeedArray[i] setValue:[NSNumber numberWithBool:YES] forKey:@"is_view"];
                            }
                        }
                    }
                }
            }
        }
        else
        {
        }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                      }];
}


#pragma mark - NSURLSession Delegate



- (NSURLSession *)backgroundSession {
    static NSURLSession *session = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        // Session Configuration
        NSURLSessionConfiguration *sessionConfiguration = [NSURLSessionConfiguration backgroundSessionConfigurationWithIdentifier:@"com.Singlecast.BackgroundSession"];
        
        // Initialize Session
        session = [NSURLSession sessionWithConfiguration:sessionConfiguration delegate:self delegateQueue:nil];
    });
    
    return session;
}

- (void)downloadGifFromUrl:(NSURL *)gifUrl {
    
    [[AppWebHandler sharedInstance]initializeBackgroundSession];
    
    if (gifUrl) {
        // Schedule Download Task
        
        [[AppWebHandler sharedInstance]downloadTaskForUrl:gifUrl];
//        [[self.session downloadTaskWithURL:gifUrl] resume];
        
        // Update Progress Buffer
        [[AppWebHandler sharedInstance].dictProgressTask setObject:@(0.0) forKey:[gifUrl absoluteString]];
    }
}


- (void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didResumeAtOffset:(int64_t)fileOffset expectedTotalBytes:(int64_t)expectedTotalBytes {
   // NSLog(@"%s", __PRETTY_FUNCTION__);
}


- (void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didWriteData:(int64_t)bytesWritten totalBytesWritten:(int64_t)totalBytesWritten totalBytesExpectedToWrite:(int64_t)totalBytesExpectedToWrite {
    // Calculate Progress
    double progress = (double)totalBytesWritten / (double)totalBytesExpectedToWrite;
    
    // Update Progress Buffer
    NSURL *URL = [[downloadTask originalRequest] URL];
    [[AppWebHandler sharedInstance].dictProgressTask setObject:@(progress) forKey:[URL absoluteString]];
//    [self.progressBuffer setObject:@(progress) forKey:[URL absoluteString]];
    
    // Update Table View Cell
    PostedGifsCustomeCell *cell = [self cellForForDownloadTask:downloadTask];
    __weak typeof(cell) weakCell = cell;
    dispatch_async(dispatch_get_main_queue(), ^{
        [weakCell setProgress:progress];
    });
    
}

- (void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didFinishDownloadingToURL:(NSURL *)location {
    // Write File to Disk
    
    // NSData *imageData = [NSData dataWithContentsOfURL:lo];
    NSData *imageData = [NSData dataWithContentsOfFile:[location path]];
   // NSLog(@"location is %@",location.absoluteString);
    
    [self moveFileWithURL:location downloadTask:downloadTask];
    
    // Update Table View Cell
    PostedGifsCustomeCell *cell = [self cellForForDownloadTask:downloadTask];
    
    // Update Progress Buffer
    NSURL *URL = [[downloadTask originalRequest] URL];
    [[AppWebHandler sharedInstance].dictProgressTask setObject:@(1.0) forKey:[URL absoluteString]];
//    [self.progressBuffer setObject:@(1.0) forKey:[URL absoluteString]];
    
    __weak typeof(cell) weakCell = cell;
    dispatch_async(dispatch_get_main_queue(), ^{
        
        FLAnimatedImage *image = [[FLAnimatedImage alloc] initWithAnimatedGIFData:imageData generateThumbnail:NO];
        [weakCell setGiffImage:image];
        [weakCell.progresView setHidden:true];
        [weakCell.imgVwGif setHidden:false];
        if (weakCell)
        {
            [self.tblVwData reloadData];
//            @try {
//                [self.tblVwData beginUpdates];
//                [self.tblVwData reloadRowsAtIndexPaths:@[[self.tblVwData indexPathForCell:weakCell]] withRowAnimation:UITableViewRowAnimationNone];
//                [self.tblVwData endUpdates];
//            }
//            @catch (NSException *exception) {
//                [self.tblVwData reloadData];
//            }
//            @finally {
//                
//            }
        }

    });
    // Invoke Background Completion Handler
    [self invokeBackgroundSessionCompletionHandler];
}


- (void)moveFileWithURL:(NSURL *)URL downloadTask:(NSURLSessionDownloadTask *)downloadTask {
    // Filename
    NSString *fileName = [[[downloadTask originalRequest] URL] lastPathComponent];
    
    // Local URL
    NSURL *localURL = [self URLForGiffWithName:fileName];
    
    NSFileManager *fm = [NSFileManager defaultManager];
    
    if ([fm fileExistsAtPath:[URL path]]) {
        NSError *error = nil;
        [fm moveItemAtURL:URL toURL:localURL error:&error];
//        [_tempDirectUrls addObject:[localURL path]];
        if (error) {
            NSLog(@"Unable to move temporary file to destination. %@, %@", error, error.userInfo);
        }
    }
}


- (NSURL *)URLForGiffWithName:(NSString *)name {
    if (!name) return nil;
    return [self.gifDirectory URLByAppendingPathComponent:name];
}

- (NSURL *)gifDirectory {
    return [AppWebHandler sharedInstance].tempDirectory;
}


- (PostedGifsCustomeCell *)cellForForDownloadTask:(NSURLSessionDownloadTask *)downloadTask {
    // Helpers
    PostedGifsCustomeCell *cell = nil;
    NSURL *URL = [[downloadTask originalRequest] URL];
    
    NSMutableArray *array = [NSMutableArray array];
    if (isReplugSelected)
    {
        [array addObjectsFromArray:dataManager.globalUserReplugArray];
    }
    else
    {
        [array addObjectsFromArray:dataManager.globalUserFeedArray];
    }
    
    for (NSInteger i =0; i<array.count; i++)
    {
        
        BOOL flag = NO;
        NSDictionary *dict = array[i];
        
        NSString *strImgUrl=[dict valueForKey:@"post_image"];
        NSURL *mainURL=[NSURL URLWithString:[NSString stringWithFormat:@"%@",strImgUrl]];
        //   NSURL *feedItemURL = [self urlForFeedItem:feedItem];
        
        
        @try {
            if ([URL isEqual:mainURL]) {
                NSUInteger index = [array indexOfObject:dict];
                NSIndexPath *indexpath = [NSIndexPath indexPathForRow:index inSection:0];
                if ([[self.tblVwData indexPathsForVisibleRows]containsObject:indexpath]) {
                    cell = (PostedGifsCustomeCell *)[self.tblVwData cellForRowAtIndexPath:indexpath];
                    flag = YES;
                }
            }
        } @catch (NSException *exception) {
            [self.tblVwData reloadData];
        } @finally {
            if (flag) {
                break;
            }
        }
    }
    return cell;
}

- (void)invokeBackgroundSessionCompletionHandler {
    
    [[AppWebHandler sharedInstance].httpSessionBackground getTasksWithCompletionHandler:^(NSArray *dataTasks, NSArray *uploadTasks, NSArray *downloadTasks) {
        NSUInteger count = [dataTasks count] + [uploadTasks count] + [downloadTasks count];
        
        if (!count) {
            AppDelegate *applicationDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
            void (^backgroundSessionCompletionHandler)() = [applicationDelegate backgroundSessionCompletionHandler];
            
            if (backgroundSessionCompletionHandler) {
                [applicationDelegate setBackgroundSessionCompletionHandler:nil];
                backgroundSessionCompletionHandler();
            }
        }
    }];
}


- (IBAction)onViewInfoSlider:(UIButton *)sender {
    
    BOOL flag = NO;
    if (isReplugSelected && dataManager.globalUserReplugArray.count>0)
    {
        flag = YES;
    }
    else if (!isReplugSelected && dataManager.globalUserFeedArray.count>0)
    {
        flag = YES;
    }
    if (flag)
    {
        CGRect visibleRect = (CGRect){.origin = self.tblVwData.contentOffset, .size = self.tblVwData.bounds.size};
        CGPoint visiblePoint = CGPointMake(CGRectGetMidX(visibleRect), CGRectGetMidY(visibleRect));
        visibleIndexPath = [self.tblVwData indexPathForRowAtPoint:visiblePoint];
        
        btnViewInfo.userInteractionEnabled=NO;
        [self.tblVwData setContentOffset:self.tblVwData.contentOffset animated:NO];
        SliderMenuVC *smvc=[[SliderMenuVC alloc]init];
        NSLog(@"%ld",(long)sender.tag);
        smvc.index = visibleIndexPath.row;
        if (isReplugSelected)
        {
            smvc.gifCount = [dataManager.globalUserReplugArray[visibleIndexPath.row][@"view_count"]integerValue];

            smvc.dictPostsData=dataManager.globalUserReplugArray[visibleIndexPath.row];
            smvc.strPostID=dataManager.globalUserReplugArray[visibleIndexPath.row][@"post_id"];
        }
        else
        {
            smvc.gifCount = [dataManager.globalUserFeedArray[visibleIndexPath.row][@"view_count"]integerValue];
            smvc.dictPostsData=dataManager.globalUserFeedArray[visibleIndexPath.row];
            smvc.strPostID=dataManager.globalUserFeedArray[visibleIndexPath.row][@"post_id"];
        }
        
        smvc.view.tag = 1001;
        
        smvc.view.frame=CGRectMake([UIScreen mainScreen].bounds.size.width, 0.0f, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height);
        CGRect basketTopFrame = smvc.view.frame;
        basketTopFrame.origin.x = 0;
        [APPDELEGATE.window.rootViewController.view addSubview:smvc.view];
        [APPDELEGATE.window.rootViewController addChildViewController:smvc];
        
        [UIView animateWithDuration:0.25 delay:0.0 usingSpringWithDamping:1.0 initialSpringVelocity:0.0 options:UIViewAnimationOptionCurveLinear animations:^{
            smvc.view.frame = basketTopFrame;
        } completion:^(BOOL finished) {
            btnViewInfo.userInteractionEnabled=YES;
        }];
    }
}


#pragma mark -
#pragma mark Get Post Details
- (void)getGifLatestDetailsWithID:(NSString *)postId
{
    NSString *url = [NSString stringWithFormat:@"posts/%@",postId];
    
    NSMutableDictionary *header =[[NSMutableDictionary alloc]initWithObjectsAndKeys:@"application/json",@"Content-Type",[NSUSERDEFAULTS valueForKey:kUSERTOKEN],@"User-Token", nil];
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypeGet withHttpHeaders:header withServiceName:url withParameters:nil withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"--responce dict %@",responseDict);
        
        if(responseDict==Nil)        {
            //            [CommonFunction alertTitle:@"" withMessage:@"Server Error"];
        }
        else if([operation.response statusCode]==200)
        {
            BOOL flag = NO;
            for (NSInteger i=0; i<dataManager.globalUserFeedArray.count; i++)
            {
                NSMutableDictionary *dict = dataManager.globalUserFeedArray[i];
                if ([dict[@"post_id"] isEqualToString:postId])
                {
                    NSInteger count = [responseDict[@"post"][@"comments_count"] integerValue];
                    BOOL isComent = [responseDict[@"post"][@"is_comment"] boolValue];
                    [dataManager.globalUserFeedArray[i] setObject:[NSNumber numberWithInteger:count] forKey:@"comments_count"];
                    [dataManager.globalUserFeedArray[i] setObject:[NSNumber numberWithBool:isComent] forKey:@"is_comment"];
                    flag = YES;
                }
                if (flag) {
                    break;
                }
            }
            
            flag = NO;
            for (NSInteger i=0; i<dataManager.globalUserReplugArray.count; i++)
            {
                NSMutableDictionary *dict = dataManager.globalUserReplugArray[i];
                if ([dict[@"post_id"] isEqualToString:postId])
                {
                    NSInteger count = [responseDict[@"post"][@"comments_count"] integerValue];
                    BOOL isComent = [responseDict[@"post"][@"is_comment"] boolValue];
                    [dataManager.globalUserReplugArray[i] setObject:[NSNumber numberWithInteger:count] forKey:@"comments_count"];
                    [dataManager.globalUserReplugArray[i] setObject:[NSNumber numberWithBool:isComent] forKey:@"is_comment"];
                    flag = YES;
                }
                if (flag) {
                    break;
                }
            }
            
            flag = NO;
            for (NSInteger i=0; i<dataManager.globalHomeFeedArray.count; i++)
            {
                NSMutableDictionary *dict = dataManager.globalHomeFeedArray[i];
                if ([dict[@"post_id"] isEqualToString:postId])
                {
                    NSInteger count = [responseDict[@"post"][@"comments_count"] integerValue];
                    BOOL isComent = [responseDict[@"post"][@"is_comment"] boolValue];
                    [dataManager.globalHomeFeedArray[i] setObject:[NSNumber numberWithInteger:count] forKey:@"comments_count"];
                    [dataManager.globalHomeFeedArray[i] setObject:[NSNumber numberWithBool:isComent] forKey:@"is_comment"];
                    flag = YES;
                }
                if (flag) {
                    break;
                }
            }

            [self.tblVwData reloadData];
        }
        else
        {
            //            [CommonFunction alertTitle:@"" withMessage:[responseDict objectForKey:@"msg"]];
        }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          if([operation.response statusCode]  == 400 ){
                                              NSLog(@"impo response %@",operation.response);
                                              
                                              //                                              [CommonFunction alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                          }
                                          else{
                                              
                                              if (wbServiceCount==1) {
                                                  wbServiceCount=0;
                                              }
                                              else
                                              {
                                                  //                                                  [CommonFunction alertTitle:@"" withMessage:@"Network Error..."];
                                              }
                                          }
                                      }];
}

#pragma mark -
#pragma mark Handle UserName Tap
- (void)customTableViewCellDidTapOnUserHandle:(NSString *)userHandle
{
    NSString *loggedInUser = [NSUSERDEFAULTS objectForKey:kUSERID];
    if ([loggedInUser isEqualToString:userHandle])
    {
        
    }
    else
    {
        OtherUserProfileVC *ouVC=[[OtherUserProfileVC alloc]init];
        ouVC.hidesBottomBarWhenPushed=YES;
        ouVC.strUserName=userHandle;
        [self.navigationController pushViewController:ouVC animated:YES];
    }
}

#pragma mark -
#pragma mark Handle HashTag Tap
- (void)customTableViewCellDidTapHashTagHandle:(NSString *)hashtagHandle
{
    NSString *categoryName = [NSString stringWithFormat:@"#%@",hashtagHandle];
    NSString *categoryTag = hashtagHandle;
    CategoryDetailVC *categorydetailVC = [[CategoryDetailVC alloc]initWithNibName:NSStringFromClass([CategoryDetailVC class]) bundle:nil];
    categorydetailVC.hidesBottomBarWhenPushed=YES;
    categorydetailVC.categoryTag = categoryTag;
    categorydetailVC.categoryName = categoryName;
    [self.navigationController pushViewController:categorydetailVC animated:YES];
}


#pragma mark -
#pragma mark Save Gif
- (void)saveGIFToGallery:(NSNumber *)indexNumber
{
    UIAlertController *controller = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *actionCancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
    
    UIAlertAction *action = [UIAlertAction actionWithTitle:@"Save GIF" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        NSString *imgGifUrl=isReplugSelected?dataManager.globalUserReplugArray[[indexNumber integerValue]][@"post_image"]:dataManager.globalUserFeedArray[[indexNumber integerValue]][@"post_image"];
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
            NSURL *localURL = [self URLForGiffWithName:[imgGifUrl lastPathComponent]];
            
            if ([[NSFileManager defaultManager] fileExistsAtPath:[localURL path]]) {
                
                NSData *animatedImageData = [[NSFileManager defaultManager] contentsAtPath:localURL.path];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    
                    if ([[[UIDevice currentDevice]systemVersion]floatValue]<9)
                    {
                        ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
                        [library writeImageDataToSavedPhotosAlbum:animatedImageData metadata:nil completionBlock:^(NSURL *assetURL, NSError *error) {
                            dispatch_async(dispatch_get_main_queue(), ^{
                                [CommonFunction removeActivityIndicator];
                                if (!error)
                                {
                                    [CommonFunction showAlertWithTitle:@"Gif Saved" message:@"Gif has been saved to Gallery" onViewController:self useAsDelegate:NO dismissBlock:nil];
                                }
                                else
                                {
                                    [CommonFunction showAlertWithTitle:@"Error" message:error.localizedDescription onViewController:self useAsDelegate:NO dismissBlock:nil];
                                }
                            });
                        }];
                    }
                    else
                    {
                        [[PHPhotoLibrary sharedPhotoLibrary] performChanges:^{
                            [[PHAssetCreationRequest creationRequestForAsset]addResourceWithType:PHAssetResourceTypePhoto data:animatedImageData options:nil];
                        } completionHandler:^(BOOL success, NSError *error) {
                            
                            dispatch_async(dispatch_get_main_queue(), ^{
                                [CommonFunction removeActivityIndicator];
                                if (success) {
                                    [CommonFunction showAlertWithTitle:@"Gif Saved" message:@"Your Gif has been saved to Gallery" onViewController:self useAsDelegate:NO dismissBlock:nil];
                                }
                                else {
                                    [CommonFunction showAlertWithTitle:@"Error" message:error.localizedDescription onViewController:self useAsDelegate:NO dismissBlock:nil];
                                }
                            });
                        }];
                    }
                });
            }
        });
    }];
    
    [controller addAction:actionCancel];
    [controller addAction:action];
    
    [self presentViewController:controller animated:YES completion:nil];
}

- (void)onSaveGifAction:(UILongPressGestureRecognizer *)likeTap
{
    if (likeTap.state == UIGestureRecognizerStateBegan)
    {
        if ([PHPhotoLibrary authorizationStatus] == PHAuthorizationStatusDenied || [PHPhotoLibrary authorizationStatus] == PHAuthorizationStatusRestricted)
        {
            //            [self showPhotoAccessAlert];
            PhotoPickerController *photoPicker = [[PhotoPickerController alloc]initWithNibName:NSStringFromClass([PhotoPickerController class]) bundle:nil];
            [self.navigationController presentViewController:photoPicker animated:YES completion:nil];
        }
        else
        {
            NSInteger index = likeTap.view.tag;
            
            [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status) {
                switch (status) {
                    case PHAuthorizationStatusAuthorized:
                    {
                        [self performSelectorOnMainThread:@selector(saveGIFToGallery:) withObject:[NSNumber numberWithInteger:index] waitUntilDone:NO];
                    }
                        break;
                        
                    default:
                        break;
                }
            }];
            
        }
    }
}


@end





//@interface UIScrollView (touch)
//
//- (BOOL)cat_touchesShouldCancelInContentView:(UIView *)view;
//
//@end
//
//@implementation UIScrollView (touch)
//
//+ (void)load
//{
//    static dispatch_once_t once_token;
//    dispatch_once(&once_token,  ^{
//        SEL oldThumbRect = @selector(touchesShouldCancelInContentView:);
//        SEL newThumbRect = @selector(cat_touchesShouldCancelInContentView:);
//        Method originalMethod = class_getInstanceMethod(self, oldThumbRect);
//        Method extendedMethod = class_getInstanceMethod(self, newThumbRect);
//        method_exchangeImplementations(originalMethod, extendedMethod);
//    });
//}
//
//- (BOOL)cat_touchesShouldCancelInContentView:(UIView *)view
//{
//    return NO;
//}
//
//@end


