//
//  UIImage+Thumbnail.h
//  GiffPlugApp
//
//  Created by Kshitij Godara on 12/02/16.
//  Copyright © 2016 Kshitij Godara. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Thumbnail)
-(UIImage *)squareAndSmallOfSize:(CGSize)finalSize;
@end
