//
//  UIImage+Thumbnail.m
//  GiffPlugApp
//
//  Created by Kshitij Godara on 12/02/16.
//  Copyright © 2016 Kshitij Godara. All rights reserved.
//

#import "UIImage+Thumbnail.h"

@implementation UIImage (Thumbnail)

-(UIImage *)squareAndSmallOfSize:(CGSize)finalSize // as a category (so, 'self' is the input image)
{
    // fromCleverError's original
    // http://stackoverflow.com/questions/17884555
  //  CGSize finalsize = CGSizeMake(128,128);
    
    CGFloat scale = MAX(
                        finalSize.width/self.size.width,
                        finalSize.height/self.size.height);
    CGFloat width = self.size.width * scale;
    CGFloat height = self.size.height * scale;
    
    CGRect rr = CGRectMake( 0, 0, width, height);
    
    UIGraphicsBeginImageContextWithOptions(finalSize, NO, 0);
    [self drawInRect:rr];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

@end
