//
//  AppWebHandler.m
//  Tabco App
//
//  Created by Santosh on 21/12/15.
//  Copyright © 2015 Vaibhav Khatri. All rights reserved.
//

#import "AppWebHandler.h"
#import "Reachability.h"

#pragma mark -
#pragma mark Internet Check Bool
BOOL isInternetAvailabel;

#pragma mark -
#pragma mark CLASS EXTENSION
@interface AppWebHandler ()<NSURLSessionDelegate>

#pragma mark -
@property(nonatomic)Reachability *internetRechability;
@property(nonatomic)NSHashTable *delegates;
//@property(nonatomic)dispatch_queue_t delegateQueue;

#pragma mark -
- (NSMutableURLRequest *)createGetRequestWithUrl:(NSURL *)url;
- (NSMutableURLRequest *)createPostRequestWithUrl:(NSURL *)url andParmaters:(NSDictionary *)dictionary;

- (void)createTempDirectory;
- (void)removeDirectory;
- (void)initializeBackgroundSession;

@end
#pragma mark -

#pragma mark -
#pragma mark CLASS IMPLEMENTATION
@implementation AppWebHandler

#pragma mark -
#pragma mark Shared Handler
+(AppWebHandler *)sharedInstance
{
    static AppWebHandler *_webHandler;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _webHandler = [[self alloc]init];
    });
    return _webHandler;
}

#pragma mark -
#pragma mark Init
-(instancetype)init
{
    if (self = [super init])
    {
        // Default Session
        NSURLSessionConfiguration *_configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
        _httpSessionDefault = [NSURLSession sessionWithConfiguration:_configuration];
        
        [self initializeBackgroundSession];
        
        _arrayDataTask = [[NSMutableArray alloc]init];
        _dictProgressTask = [[NSMutableDictionary alloc]init];
        
        // Observer for network status
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(checkNetworkStatus:) name:kReachabilityChangedNotification object:nil];
        
        self.internetRechability = [Reachability reachabilityForInternetConnection];
        [self.internetRechability startNotifier];
        [self checkNetworkStatus:nil];
        
        [self createTempDirectory];
        
        _delegates = [NSHashTable weakObjectsHashTable];
    }
    return self;
}

- (void)initializeBackgroundSession
{
    if (!_httpSessionBackground) {
        // Background Session
        NSURLSessionConfiguration *_configurationBackground = [NSURLSessionConfiguration backgroundSessionConfigurationWithIdentifier:BACKGROUNG_SESSION_IDENTIFIER];
        _httpSessionBackground = [NSURLSession sessionWithConfiguration:_configurationBackground delegate:self delegateQueue:nil];
    }
}

- (void)addDelegate:(id)delegate
{
    if (![_delegates containsObject:delegate])
    {
        [_delegates addObject:delegate];
    }
}

- (void)removeDelegate:(id)delegate
{
    if ([_delegates containsObject:delegate])
    {
        [_delegates removeObject:delegate];
    }
}

- (void)removeDataLogout
{
    if (_delegates  && _delegates.count>0)
    {
        [self.delegates removeAllObjects];
    }
    if (_arrayDataTask && _arrayDataTask.count>0)
    {
        [self.arrayDataTask removeAllObjects];
    }
    if (_dictProgressTask && _dictProgressTask.count>0)
    {
        [self.dictProgressTask removeAllObjects];
    }
    if (_httpSessionBackground)
    {
        [self.httpSessionBackground invalidateAndCancel], self.httpSessionBackground = nil;
    }
    [self removeDirectory];
    [[SDImageCache sharedImageCache]clearMemory];
    [[SDImageCache sharedImageCache]clearDisk];
}


#pragma mark -
#pragma mark Data Fetch Method
- (void)fetchDataFromUrl:(NSURL *)url httpMethod:(HttpMethodType)httpMethodType parameters:(NSDictionary *)parameters shouldDeSerialize:(BOOL)deSerialize success:(DataTaskSuccessfullHandler)successHandler failure:(DataTaskFailureHandler)failureHandler
{
    // Created initial task with nil value
    NSURLSessionDataTask *dataTask = nil;
    // Created initial request with nil value
    NSMutableURLRequest *urlRequest = nil;
    
    // Case for Http Method used for data fetch
    switch (httpMethodType)
    {
        case HttpMethodTypeGet:// Get http method
        {
            // Calling method "createGetRequestWithUrl:" which will return new instance of 'NSMutableURLRequest' which will be used for Get functionality.
            urlRequest = [self createGetRequestWithUrl:url];
        }
            break;
            
        case HttpMethodTypePost:// Post http method
        {
            // Calling method "createPostRequestWithUrl: andParmaters:" which will return new instance of 'NSMutableURLRequest' which will be used for Post functionality.
            urlRequest = [self createPostRequestWithUrl:url andParmaters:parameters];
        }
            break;
            
        default:
            break;
    }
    
    NSCachedURLResponse *cachedResponse = [[NSURLCache sharedURLCache] cachedResponseForRequest:urlRequest];
    if (cachedResponse.data)
    {
        successHandler(cachedResponse.data,nil);
        return;
    }
    
    // Using the single session created, to make and start a new task of type 'NSURLSessionDataTask'. Request created above is passed as a paramater to perform task, along with completion handler block which return data(if no error is found) and error(if some error encountered).
    dataTask = [_httpSessionDefault dataTaskWithRequest:urlRequest completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            
            
            if (error.domain == NSURLErrorDomain && error.code == NSURLErrorNotConnectedToInternet ){
            }
            // Checking for error, if there is any
            if (error)
            {
                // If there is error, then checking the condition if its error handling block is nil.
                if (!failureHandler)
                {
                    // Nil, so returning from the method, user does not posted with anything.
                    return;
                }
                else
                {
                    // User is posted with the error.
                    failureHandler(error);
                    return;
                }
            }
            // If no completion handler, then return.
            if (!successHandler) {
                return;
            }
            
            NSLog(@"%@",[[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding]);
            
            // Bool to check whether develoepr wants result in deserialized format ,i.e., in foundation object
            if (deSerialize)
            {
                // Converting data into NSDictionary object.
                NSError *deSerializeError;
                NSDictionary *deSerializedDictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error:&deSerializeError];
                
                // If error
                if (deSerializeError)
                {
                    // If there is error, then checking the condition if its error handling block is nil.
                    if (!failureHandler)
                    {
                        return;
                    }
                    else
                    {
                        // User is posted with the error.
                        failureHandler(deSerializeError);
                        return;
                    }
                }
                else
                {
                    // Sending data as nil and dictionary object because 'deSerialize is TRUE'.
                    successHandler(nil,deSerializedDictionary);
                }
            }
            else
            {
                // Sending data and dictionary as nil object because 'deSerialize is FALSE'.
                successHandler(data,nil);
            }
        });
    }];
    
    // Start the data fetch process.
    [dataTask resume];
}

- (void)downloadTaskForUrl:(NSURL *)url
{
//    if (!_httpSessionBackground)
//    {
//        [self initializeBackgroundSession];
//    }
    // Created initial task with nil value
    NSURLSessionDownloadTask *dataTask = nil;
    // Created initial request with nil value
    NSMutableURLRequest *urlRequest = nil;
    
    // Calling method "createGetRequestWithUrl:" which will return new instance of 'NSMutableURLRequest' which will be used for Get functionality.
    urlRequest = [self createGetRequestWithUrl:url];
    
    NSCachedURLResponse *cachedResponse = [[NSURLCache sharedURLCache] cachedResponseForRequest:urlRequest];
    if (cachedResponse.data)
    {
        return;
    }
    dataTask = [_httpSessionBackground downloadTaskWithRequest:urlRequest];
    [dataTask setTaskDescription:[url absoluteString]];
    
    if (![self.dictProgressTask objectForKey:[url absoluteString]])
    {
        [dataTask resume];
    }
    else
    {
        [dataTask cancel];
    }
}

#pragma mark -
#pragma mark Get Request Generation Method
- (NSMutableURLRequest *)createGetRequestWithUrl:(NSURL *)url
{
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestReturnCacheDataElseLoad timeoutInterval:120];
    [urlRequest addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [urlRequest addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [urlRequest setHTTPMethod:@"GET"];
    
    return urlRequest;
}

#pragma mark -
#pragma mark Post Request Generation Method
- (NSMutableURLRequest *)createPostRequestWithUrl:(NSURL *)url andParmaters:(NSDictionary *)dictionary
{
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:url];
    [urlRequest addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [urlRequest addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [urlRequest setHTTPMethod:@"POST"];
    
    NSError *error;
    NSData *postData = [NSJSONSerialization dataWithJSONObject:dictionary options:0 error:&error];
    NSString *str = [[NSString alloc]initWithData:postData encoding:NSUTF8StringEncoding];
    str = [str stringByReplacingOccurrencesOfString:@"\\/" withString:@"/"];
    postData = [str dataUsingEncoding:NSUTF8StringEncoding];
    NSLog(@"%@",[[NSString alloc]initWithData:postData encoding:NSUTF8StringEncoding]);
    if (error)
    {
        NSString *postBody = [NSString string];
        
        for (NSString *key in dictionary.allKeys)
        {
            id anyObject = dictionary[key];
            postBody = [postBody stringByAppendingString:[NSString stringWithFormat:@"%@=%@ ",key,anyObject]];
        }
        postBody = [postBody stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        [urlRequest setHTTPBody:[postBody dataUsingEncoding:NSUTF8StringEncoding]];
    }
    else
    {
        [urlRequest setHTTPBody:postData];
    }
    return urlRequest;
}

#pragma mark -
#pragma mark Internet Handling
-(void) checkNetworkStatus:(NSNotification *)notice
{
    // called after network status changes
    NetworkStatus internetStatus = [self.internetRechability currentReachabilityStatus];
    switch (internetStatus)
    {
        case NotReachable:
        {
            NSLog(@"The internet is down.");
            isInternetAvailabel = NO;
            
            break;
        }
        case ReachableViaWiFi:
        {
            NSLog(@"The internet is working via WIFI.");
            isInternetAvailabel = YES;
            
            break;
        }
        case ReachableViaWWAN:
        {
            NSLog(@"The internet is working via WWAN.");
            isInternetAvailabel = YES;
            
            break;
        }
    }
}


//#pragma mark -
//#pragma mark NSURLSessionDownloadDelegate
//- (void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask
//didFinishDownloadingToURL:(NSURL *)location
//{
////    dispatch_sync(_delegateQueue, ^{
////        [self.delegates enumerateObjectsUsingBlock:^(id  _Nonnull obj, BOOL * _Nonnull stop) {
////            if ([obj respondsToSelector:@selector(URLSession:downloadTask:didFinishDownloadingToURL:)])
////            {
////                [obj URLSession:session downloadTask:downloadTask didFinishDownloadingToURL:location];
////            }
////        }];
////    });
////    NSURL *fileURL = [_tempDirectory URLByAppendingPathComponent:downloadTask.taskDescription];
//}
//
//
//- (void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask
//      didWriteData:(int64_t)bytesWritten
// totalBytesWritten:(int64_t)totalBytesWritten
//totalBytesExpectedToWrite:(int64_t)totalBytesExpectedToWrite
//{
////    dispatch_sync(_delegateQueue, ^{
////        [self.delegates enumerateObjectsUsingBlock:^(id  _Nonnull obj, BOOL * _Nonnull stop) {
////            if ([obj respondsToSelector:@selector(URLSession:downloadTask:didWriteData:totalBytesWritten:totalBytesExpectedToWrite:)])
////            {
////                [obj URLSession:session downloadTask:downloadTask didWriteData:bytesWritten totalBytesWritten:totalBytesWritten totalBytesExpectedToWrite:totalBytesExpectedToWrite];
////            }
////        }];
////    });
////    CGFloat percentDone = (double)totalBytesWritten/(double)totalBytesExpectedToWrite;
////    [_dictProgressTask setObject:[NSNumber numberWithFloat:percentDone] forKey:downloadTask.taskDescription];
//}

- (void)createTempDirectory
{
    NSURL *directoryURL = [NSURL fileURLWithPath:[NSTemporaryDirectory() stringByAppendingPathComponent:@"Feeds"] isDirectory:YES];
    [[NSFileManager defaultManager] createDirectoryAtURL:directoryURL withIntermediateDirectories:YES attributes:nil error:nil];
    _tempDirectory = directoryURL;
}

- (void)removeDirectory
{
    NSURL *directoryURL = [NSURL fileURLWithPath:[NSTemporaryDirectory() stringByAppendingPathComponent:@"Feeds"] isDirectory:YES];
    
//    if ([[NSFileManager defaultManager]fileExistsAtPath:directoryURL.absoluteString isDirectory:nil])
//    {
        [[NSFileManager defaultManager]removeItemAtURL:directoryURL error:NULL];
//    }
    
    [self createTempDirectory];
}

- (BOOL)respondsToSelector:(SEL)aSelector
{
    if ([super respondsToSelector:aSelector])
        return YES;
    
    // if any of the delegates respond to this selector, return YES
    for(id delegate in _delegates)
    {
        if ([delegate respondsToSelector:aSelector])
        {
            return YES;
        }
    }
    return NO;
}

- (NSMethodSignature *)methodSignatureForSelector:(SEL)aSelector
{
    // can this class create the signature?
    NSMethodSignature* signature = [super methodSignatureForSelector:aSelector];
    
    // if not, try our delegates
    if (!signature)
    {
        for(id delegate in _delegates)
        {
            if ([delegate respondsToSelector:aSelector])
            {
                return [delegate methodSignatureForSelector:aSelector];
            }
        }
    }
    return signature;
}

- (void)forwardInvocation:(NSInvocation *)anInvocation
{
    // forward the invocation to every delegate
    for(id delegate in [_delegates copy])
    {
        if ([delegate respondsToSelector:[anInvocation selector]])
        {
            [anInvocation invokeWithTarget:delegate];
        }
    }
}


@end
