//
//  AppWebHandler.h
//  Tabco App
//
//  Created by Santosh on 21/12/15.
//  Copyright © 2015 Vaibhav Khatri. All rights reserved.
//


#pragma mark -
#pragma mark Internet Check Bool
FOUNDATION_EXPORT BOOL isInternetAvailabel;

#pragma mark -
#pragma mark Background Session Identifier
static NSString *const BACKGROUNG_SESSION_IDENTIFIER = @"gilplug.background.download";


#pragma mark -
#pragma mark HttpMethodType Enum
typedef NS_ENUM(NSInteger,HttpMethodType)
{
    HttpMethodTypeGet = 0,// GET
    HttpMethodTypePost // POST
};

#pragma mark -
#pragma mark SuccessHandler Block
typedef void(^DataTaskSuccessfullHandler)(NSData *data, NSDictionary *dictionary);
#pragma mark -
#pragma mark ErrorHandler Block
typedef void(^DataTaskFailureHandler)(NSError *error);

#pragma mark -
#pragma mark -
#pragma mark CLASS INTERFACE
@interface AppWebHandler : NSObject

#pragma mark -
#pragma mark Properties
@property(nonatomic,strong)NSURLSession *httpSessionDefault;
@property(nonatomic,strong)NSURLSession *httpSessionBackground;
@property(nonatomic,strong)NSMutableArray *arrayDataTask;
@property(nonatomic,strong)NSURL *tempDirectory;
@property(nonatomic,strong)NSMutableDictionary *dictProgressTask;

#pragma mark -
#pragma mark Class Functions
+ (AppWebHandler *)sharedInstance;

#pragma mark -
#pragma mark Instance Functions
- (void)fetchDataFromUrl:(NSURL *)url httpMethod:(HttpMethodType)httpMethodType parameters:(NSDictionary *)parameters shouldDeSerialize:(BOOL)deSerialize success:(DataTaskSuccessfullHandler)successHandler failure:(DataTaskFailureHandler)failureHandler;

- (void)initializeBackgroundSession;

- (void)downloadTaskForUrl:(NSURL *)url;

- (void)addDelegate:(id)delegate;

- (void)removeDelegate:(id)delegate;

- (void)removeDataLogout;


@end
