//
//  CommonFunction.h
//  GiffPlugApp
//
//  Created by Kshitij Godara on 08/01/16.
//  Copyright © 2016 Kshitij Godara. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MBProgressHUD.h"
#import "Reachability.h"

@interface CommonFunction : NSObject<UIGestureRecognizerDelegate>

#pragma mark - hide/unhide navigation bar
+(void)hideNavigationBarFromController:(UIViewController *)controller;
+(void)unHideNavigationBarFromController:(UIViewController *)controller;


#pragma mark - Set RootView Controller
+(void)changeRootViewController:(BOOL)isTabBar;

+ (void)removeAllUserDefaults;


#pragma mark - Textfield Methods
+ (BOOL)isString:(NSString *)aString containsCuserWord:(NSArray *)curseArray;

+ (BOOL)isValueNotEmpty:(NSString*)aString;

+(NSString *)trimSpaceInString:(NSString *)mainstr;

+(BOOL)IsValidEmail:(NSString *)checkString;

#pragma mark - Shows UIAlerts
+ (void)showAlertWithTitle:(NSString *)title message:(NSString *)message onViewController:(UIViewController *)viewController useAsDelegate:(BOOL)isDelegate dismissBlock:(void(^)())dismissBlock;


+ (void)alertTitle:(NSString*)aTitle withMessage:(NSString*)aMsg withDelegate:(id)delegate;

+ (void)alertTitle:(NSString*)aTitle withMessage:(NSString*)aMsg BtnTitle:(NSString*)btnName withDelegate:(id)delegate;

+ (void)alertTitle:(NSString*)aTitle withMessage:(NSString*)aMsg;

+ (void)alertTitle:(NSString*)aTitle withMessage:(NSString*)aMsg withDelegate:(id)delegate withTag:(int)tag;

+ (void)showAlertWithTitle:(NSString *)title message:(NSString *)message onViewController:(UIViewController *)viewController withButtonsArray:(NSArray *)buttonsArray dismissBlock:(void(^)(NSInteger buttonIndex))dismissBlock;


#pragma mark - Reachablity Check 
+(BOOL) reachabiltyCheck;
+(BOOL)reachabilityChanged:(NSNotification*)note;

+ (void)removeDirectories;
+(void)createFolder;


#pragma mark - Show n Hide Progress HUD
+ (void)removeActivityIndicator;

+ (void)showActivityIndicatorWithText:(NSString *)text;
+ (void)showActivityIndicatorWithWait;
+ (void)removeActivityIndicatorWithWait;
//+(void)navigateToTabBarController;


+(NSString *) convertTimeStampToDate :(NSDate *) date;
+(UIImage *)imageResize :(UIImage*)img andResizeTo:(CGSize)newSize;

+(void)setRoundedView:(UIView *)roundedView toDiameter:(float)newSize;

+(NSURL *)createGiffFromImageArray:(NSArray *)imageArray andDelayTime:(CGFloat)delayTime isProfileUpload:(BOOL)isProfileUpload;

+ (NSURL *)createVideoFromImageArray:(NSArray *)imageArray andDelayTime:(CGFloat)delayTime;


+ (NSString *)abbreviateNumber:(NSInteger)num;

+ (NSDictionary *)animatedGIFWithData:(NSData *)data;

+(UIImage *)watermarkImage:(UIImage *)normalImage;
+ (NSData *)getWatermarkImageData;
@end
