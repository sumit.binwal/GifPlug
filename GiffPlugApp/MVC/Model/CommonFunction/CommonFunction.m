//
//  CommonFunction.m
//  GiffPlugApp
//
//  Created by Kshitij Godara on 08/01/16.
//  Copyright © 2016 Kshitij Godara. All rights reserved.
//

#import "CommonFunction.h"
#import "CEMovieMaker.h"
#import "SliderMenuVC.h"
#import "HomeFeedVC.h"
#import "SearchVC.h"
#import "ActivitiesVC.h"
#import "UserProfileVC.h"
#import "LoginVC.h"

@implementation CommonFunction

#pragma mark - Hide/Unhide Navigation Bar

+(void)hideNavigationBarFromController:(UIViewController *)controller
{
    
    if (controller!=nil) {
        
        [controller.navigationController setNavigationBarHidden:YES];
    }
    else
    {
        //This is going to hide navigation of Screen those are not in tabbarcontroller
        [APPDELEGATE.navigationController setNavigationBarHidden:YES];
    }
}

+(void)unHideNavigationBarFromController:(UIViewController *)controller
{
    if (controller!=nil) {
        
        [controller.navigationController setNavigationBarHidden:NO];
    }
    else
    {
        //This is going to unhide navigation of Screen those are not in tabbarcontroller
        [APPDELEGATE.navigationController setNavigationBarHidden:NO];
    }
}

+(void)changeRootViewController:(BOOL)isTabBar
{
    if (isTabBar)
    {
        
        if (APPDELEGATE.window.rootViewController)
        {
            for (NSInteger i=0; i<APPDELEGATE.navigationController.viewControllers.count; i++)
            {
                UIViewController *controller = APPDELEGATE.navigationController.viewControllers[i];
                [[NSNotificationCenter defaultCenter]removeObserver:controller];
                [controller.navigationController popViewControllerAnimated:NO];
                controller = nil;
            }
            [APPDELEGATE.navigationController popToRootViewControllerAnimated:NO];
            APPDELEGATE.navigationController = nil;
        }
        
        HomeFeedVC *homeVC = [[HomeFeedVC alloc]initWithNibName:NSStringFromClass([HomeFeedVC class]) bundle:nil];
        homeVC.tabBarItem.image = [UIImage imageNamed:@"TB_Home"];
        homeVC.tabBarItem.selectedImage = [UIImage imageNamed:@"TB_HomeActive"];
        homeVC.tabBarItem.imageInsets = UIEdgeInsetsMake(6, 0, -6, 0);
        
        SearchVC *searchVC = [[SearchVC alloc]initWithNibName:NSStringFromClass([SearchVC class]) bundle:nil];
        searchVC.tabBarItem.image = [UIImage imageNamed:@"TB_Search"];
        searchVC.tabBarItem.selectedImage = [UIImage imageNamed:@"TB_SearchActive"];
        searchVC.tabBarItem.imageInsets = UIEdgeInsetsMake(6, 0, -6, 0);
        
        UIViewController *controller = [[UIViewController alloc]init];
        controller.tabBarItem.image = [UIImage imageNamed:@"TB_Round"];
        controller.tabBarItem.selectedImage = [UIImage imageNamed:@"TB_RoundActive"];
        controller.view.backgroundColor = [UIColor blackColor];
        controller.tabBarItem.imageInsets = UIEdgeInsetsMake(6, 0, -6, 0);
        
        ActivitiesVC *activityVC = [[ActivitiesVC alloc]initWithNibName:NSStringFromClass([ActivitiesVC class]) bundle:nil];
        activityVC.tabBarItem.image = [UIImage imageNamed:@"TB_Activity"];
        activityVC.tabBarItem.selectedImage = [UIImage imageNamed:@"TB_ActivityActive"];
        activityVC.tabBarItem.imageInsets = UIEdgeInsetsMake(6, 0, -6, 0);
        
        UserProfileVC *userVC = [[UserProfileVC alloc]initWithNibName:NSStringFromClass([UserProfileVC class]) bundle:nil];
        userVC.tabBarItem.image = [UIImage imageNamed:@"TB_User"];
        userVC.tabBarItem.selectedImage = [UIImage imageNamed:@"TB_UserActive"];
        userVC.tabBarItem.imageInsets = UIEdgeInsetsMake(6, 0, -6, 0);
        
        UINavigationController *tabVC1 = [[UINavigationController alloc]initWithRootViewController:homeVC];
        UINavigationController *tabVC2 = [[UINavigationController alloc]initWithRootViewController:searchVC];
        UINavigationController *tabVC3 = [[UINavigationController alloc]initWithRootViewController:controller];
        UINavigationController *tabVC4 = [[UINavigationController alloc]initWithRootViewController:activityVC];
        UINavigationController *tabVC5 = [[UINavigationController alloc]initWithRootViewController:userVC];
        tabVC1.navigationBarHidden = YES;
        tabVC2.navigationBarHidden = YES;
        tabVC3.navigationBarHidden = YES;
        tabVC4.navigationBarHidden = YES;
        tabVC5.navigationBarHidden = YES;
        
        tabVC1.tabBarItem.tag = 1;
        tabVC2.tabBarItem.tag = 2;
        tabVC3.tabBarItem.tag = 3;
        tabVC4.tabBarItem.tag = 4;
        tabVC5.tabBarItem.tag = 5;
        
        APPDELEGATE.appTabBarController = [[UITabBarController alloc]init];
        APPDELEGATE.appTabBarController.tabBar.barStyle=UIBarStyleBlack;
        APPDELEGATE.appTabBarController.tabBar.translucent = NO;
        [APPDELEGATE.appTabBarController.tabBar setValue:@(YES) forKeyPath:@"_hidesShadow"];
        
        [APPDELEGATE.appTabBarController setViewControllers:@[tabVC1,tabVC2,tabVC3,tabVC4,tabVC5]];
        
        
        if (APPDELEGATE.window)
        {
            NSLog(@"window alive");
        }
        
        if (APPDELEGATE.appTabBarController)
        {
            NSLog(@"tabbar alive");
        }
        
        [APPDELEGATE.window setRootViewController:APPDELEGATE.appTabBarController];
        APPDELEGATE.appTabBarController.view.backgroundColor = [UIColor blackColor];
        [APPDELEGATE.appTabBarController setSelectedIndex:0];
        APPDELEGATE.appTabBarController.delegate=APPDELEGATE;
        
        [APPDELEGATE showNotificationBadge:unReadCount==0?NO:YES];
    }
    else
    {
        if (APPDELEGATE.window.rootViewController)
        {
            for (NSInteger i=0; i<APPDELEGATE.appTabBarController.viewControllers.count; i++)
            {
                UIViewController *controller = APPDELEGATE.appTabBarController.viewControllers[i];
                [[NSNotificationCenter defaultCenter]removeObserver:controller];
                controller = nil;
            }
            [APPDELEGATE.appTabBarController setViewControllers:nil];
            APPDELEGATE.appTabBarController = nil;
        }
        
        LoginVC *loginVC = [[LoginVC alloc]initWithNibName:NSStringFromClass([LoginVC class]) bundle:nil];
        
        APPDELEGATE.navigationController = [[UINavigationController alloc]initWithRootViewController:loginVC];
        APPDELEGATE.navigationController.navigationBarHidden = YES;
        
        [APPDELEGATE.navigationController.view setBackgroundColor:[UIColor blackColor]];
        
        [APPDELEGATE.window setRootViewController:APPDELEGATE.navigationController];
    }
    [APPDELEGATE.window setBackgroundColor:[UIColor blackColor]];
    [APPDELEGATE.window makeKeyAndVisible];
    
}

+(BOOL) reachabiltyCheck
{
    
    BOOL status =YES;
    
//    [[NSNotificationCenter defaultCenter] addObserver:self
//                                             selector:@selector(reachabilityChanged:)
//                                                 name:kReachabilityChangedNotification
//                                               object:nil];
    
    Reachability * reach = [Reachability reachabilityForInternetConnection];
    
    // //NSLog(@"reachabiltyCheck status  : %d",[reach currentReachabilityStatus]);
    
    if([reach currentReachabilityStatus]==0)
    {
        status = NO;
        //NSLog(@"network not connected");
    }
    
    reach.reachableBlock = ^(Reachability * reachability)
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            // blockLabel.text = @"Block Says Reachable";
        });
    };
    
    reach.unreachableBlock = ^(Reachability * reachability)
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            //  blockLabel.text = @"Block Says Unreachable";
        });
    };
    
    [reach startNotifier];
    return status;
}

+(BOOL)reachabilityChanged:(NSNotification*)note
{
    BOOL status =YES;
    //NSLog(@"reachabilityChanged");
    
    Reachability * reach = [note object];
    
    if([reach isReachable])
    {
        //notificationLabel.text = @"Notification Says Reachable"
        status = YES;
        //NSLog(@"NetWork is Available");
    }
    else
    {
        status = NO;
        /*
         CustomAlert *alert=[[CustomAlert alloc]initWithTitle:@"There was a small problem" message:@"The network doesn't seem to be responding, please try again." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
         [alert show];
         */
    }
    return status;
}

+ (void)removeAllUserDefaults
{
    [NSUSERDEFAULTS removeObjectForKey:@"previousSelectedVC"];
    [NSUSERDEFAULTS removeObjectForKey:@"settings"];
    [NSUSERDEFAULTS removeObjectForKey:kUSERTOKEN];
    [NSUSERDEFAULTS removeObjectForKey:@"recentPostArray"];
    [NSUSERDEFAULTS removeObjectForKey:kUSERID];
}

#pragma mark - MBProgressHud Methods
+ (void)removeActivityIndicator {
        [UIApplication sharedApplication].networkActivityIndicatorVisible=NO;
        APPDELEGATE.window.userInteractionEnabled=YES;
}

+ (void)showActivityIndicatorWithText:(NSString *)text {
    [UIApplication sharedApplication].networkActivityIndicatorVisible=YES;
}
+ (void)showActivityIndicatorWithWait {
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible=YES;
    APPDELEGATE.window.userInteractionEnabled=NO;
    
}
+ (void)removeActivityIndicatorWithWait {
    [UIApplication sharedApplication].networkActivityIndicatorVisible=NO;
        APPDELEGATE.window.userInteractionEnabled=YES;
}

+(NSString *) convertTimeStampToDate :(NSDate *) date {
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.locale = [NSLocale currentLocale];
    [formatter setDateFormat:@"MMM dd, yyyy"];
    
    NSString *stringFromDate = [formatter stringFromDate:date];
    return stringFromDate;
}


+ (BOOL)isValueNotEmpty:(NSString*)aString{
    NSCharacterSet *whitespace = [NSCharacterSet whitespaceAndNewlineCharacterSet];
    aString = [aString stringByTrimmingCharactersInSet:whitespace];
    
    if (aString == nil || [aString length] == 0){
        
        return NO;
    }
    return YES;
}

+ (BOOL)isString:(NSString *)aString containsCuserWord:(NSArray *)curseArray{
    NSCharacterSet *whitespace = [NSCharacterSet whitespaceAndNewlineCharacterSet];
    aString = [aString stringByTrimmingCharactersInSet:whitespace];
    BOOL flag = NO;
    for (NSInteger i=0; i<curseArray.count; i++)
    {
        NSString *curseWord = curseArray[i];
        if ([aString rangeOfString:curseWord].location != NSNotFound)
        {
            flag = YES;
        }
        if (flag) {
            break;
        }
    }
    return flag;
}

+(NSString *)trimSpaceInString:(NSString *)mainstr
{
    NSCharacterSet *whitespace=[NSCharacterSet whitespaceAndNewlineCharacterSet];
    mainstr=[mainstr stringByTrimmingCharactersInSet:whitespace];
    mainstr=[mainstr stringByReplacingOccurrencesOfString:@"  " withString:@""];
    return mainstr;
}

+(BOOL)IsValidEmail:(NSString *)checkString
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}

+ (void)alertTitle:(NSString*)aTitle withMessage:(NSString*)aMsg withDelegate:(id)delegate{
    [[[UIAlertView alloc] initWithTitle:@"GifPlug"
                                message:aMsg
                               delegate:delegate
                      cancelButtonTitle:@"OK"
                      otherButtonTitles:nil, nil] show];
}

#pragma mark UIAlertView
+ (void)showAlertWithTitle:(NSString *)title message:(NSString *)message onViewController:(UIViewController *)viewController useAsDelegate:(BOOL)isDelegate dismissBlock:(void(^)())dismissBlock
{
    if (NSClassFromString(@"UIAlertController"))
    {
        UIAlertController *alertController = [UIAlertController
                                              alertControllerWithTitle:title
                                              message:message
                                              preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                   handler:^(UIAlertAction * action) {
                                                       
                                                       if (dismissBlock)
                                                       {
                                                           dismissBlock();
                                                       }
                                                       [alertController dismissViewControllerAnimated:YES completion:^{
                                                       }];
                                                       
                                                   }];
        
        [alertController addAction:ok];
        [viewController presentViewController:alertController animated:YES completion:nil];
    }
    else
    {
        if (isDelegate)
        {
            [[[UIAlertView alloc]initWithTitle:title message:message delegate:viewController cancelButtonTitle:@"OK" otherButtonTitles:nil]show];
        }
        else
        {
            [[[UIAlertView alloc]initWithTitle:title message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil]show];
        }
    }
}



+ (void)alertTitle:(NSString*)aTitle withMessage:(NSString*)aMsg BtnTitle:(NSString*)btnName withDelegate:(id)delegate{
    [[[UIAlertView alloc] initWithTitle:@"GifPlug"
                                message:aMsg
                               delegate:delegate
                      cancelButtonTitle:btnName
                      otherButtonTitles:nil, nil] show];
}
+ (void)alertTitle:(NSString*)aTitle withMessage:(NSString*)aMsg{
    [self alertTitle:@"GifPlug" withMessage:aMsg withDelegate:self];
}

+ (void)alertTitle:(NSString*)aTitle withMessage:(NSString*)aMsg withDelegate:(id)delegate withTag:(int)tag{
    UIAlertView *alrtVw=[[UIAlertView alloc] initWithTitle:@"GifPlug"
                                                   message:aMsg
                                                  delegate:delegate
                                         cancelButtonTitle:@"OK"
                                         otherButtonTitles:nil, nil];
    
    alrtVw.tag=tag;
    [alrtVw show];
    
    
}

+ (void)showAlertWithTitle:(NSString *)title message:(NSString *)message onViewController:(UIViewController *)viewController withButtonsArray:(NSArray *)buttonsArray dismissBlock:(void(^)(NSInteger buttonIndex))dismissBlock
{
    if (NSClassFromString(@"UIAlertController"))
    {
        UIAlertController *alertController = [UIAlertController
                                              alertControllerWithTitle:title
                                              message:message
                                              preferredStyle:UIAlertControllerStyleAlert];
        
        for (NSInteger i=0; i<buttonsArray.count; i++)
        {
            UIAlertAction* ok = [UIAlertAction actionWithTitle:buttonsArray[i] style:UIAlertActionStyleDefault
                                                       handler:^(UIAlertAction * action) {
                                                           
                                                           if (dismissBlock)
                                                           {
                                                               dismissBlock(i);
                                                           }
                                                           [alertController dismissViewControllerAnimated:YES completion:^{
                                                           }];
                                                           
                                                       }];
            
            [alertController addAction:ok];
        }
        [viewController presentViewController:alertController animated:YES completion:nil];
    }
}

#pragma mark - Make UIView Rounded

+(void)setRoundedView:(UIView *)roundedView toDiameter:(float)newSize;
{
    CGPoint saveCenter = roundedView.center;
    CGRect newFrame = CGRectMake(roundedView.frame.origin.x, roundedView.frame.origin.y, newSize, newSize);
    roundedView.frame = newFrame;
    roundedView.layer.cornerRadius = newSize / 2.0;
    roundedView.center = saveCenter;
}


#pragma mark -
#pragma mark Resize Image
+(UIImage *)imageResize :(UIImage*)img andResizeTo:(CGSize)newSize
{
//    CGFloat scale = [[UIScreen mainScreen]scale];
    /*You can remove the below comment if you dont want to scale the image in retina   device .Dont forget to comment UIGraphicsBeginImageContextWithOptions*/
    //UIGraphicsBeginImageContext(newSize);
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 1.0);
    [img drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

#pragma mark - Create Giff


+(NSURL *)createGiffFromImageArray:(NSArray *)imageArray andDelayTime:(CGFloat)delayTime isProfileUpload:(BOOL)isProfileUpload
{
    NSDictionary *fileProperties = @{
                                     (__bridge id)kCGImagePropertyGIFDictionary: @{
                                             (__bridge id)kCGImagePropertyGIFLoopCount: @0, // 0 means loop forever
                                             }
                                     };
    
    NSDictionary *frameProperties = @{
                                      (__bridge id)kCGImagePropertyGIFDictionary: @{
                                              (__bridge id)kCGImagePropertyGIFDelayTime: [NSNumber numberWithFloat:delayTime], // a float (not double!) in seconds, rounded to centiseconds in the GIF data
                                              }
                                      };
    
    
    // NSFileManager *fileManager = [NSFileManager defaultManager];//create instance of NSFileManager
    
    [self removeDirectories];
    [self createFolder];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES); //create an array and store result of our search for the documents directory in it
    
    NSString *documentsDirectory = [paths objectAtIndex:0]; //create NSString object, that holds our exact path to the documents directory
    
    NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"/%@",kAPPFOLDERNAME]];
    
    NSString *fullPath = [dataPath stringByAppendingPathComponent:[NSString stringWithFormat:@"giffplug.gif"]]; //add our image to the path
    
    
    
    //  NSURL  *documentsDirectoryURL = [[NSFileManager defaultManager] URLForDirectory:NSDocumentDirectory inDomain:NSUserDomainMask appropriateForURL:nil create:YES error:nil];
    NSURL   *fileURL = [NSURL fileURLWithPath:fullPath isDirectory:NO];
    
    CGImageDestinationRef destination = CGImageDestinationCreateWithURL((__bridge CFURLRef)fileURL, kUTTypeGIF, imageArray.count, NULL);
    
    if (isProfileUpload)
    {
        for (UIImage *image in imageArray) {
            UIImage *img = image;
            
//            CFDictionaryRef options = (__bridge CFDictionaryRef) @{
//                                                                   (id) kCGImageSourceCreateThumbnailWithTransform : @YES,
//                                                                   (id) kCGImageSourceCreateThumbnailFromImageAlways : @YES,
////                                                                   (id) kCGImageSourceThumbnailMaxPixelSize : @(1269),
//                                                                   (id) kCGImageSourceShouldCache : [NSNumber numberWithBool:false]
//                                                                   };
//            
//            CGImageSourceRef src = CGImageSourceCreateWithData((CFDataRef)UIImageJPEGRepresentation(img, 0.8), NULL);
//            
//            CGImageRef ref = CGImageSourceCreateThumbnailAtIndex(src, 0, options);
//            
//            CFRelease(src);
            CGImageDestinationAddImage(destination, img.CGImage, (CFDictionaryRef)frameProperties);
//            CGImageRelease(ref);
        }
    }
    else
    {
        for (NSInteger i=0; i<imageArray.count; i++)
        {
            UIImage *img = nil;
            
            if ([imageArray[i] isKindOfClass:[NSDictionary class]])
            {
                img = imageArray[i][@"image"];
            }
            else
            {
                img = imageArray[i];
            }
            
            CFDictionaryRef options = (__bridge CFDictionaryRef) @{
                                                                   (id) kCGImageSourceCreateThumbnailWithTransform : @YES,
                                                                   (id) kCGImageSourceCreateThumbnailFromImageAlways : @YES,
                                                                   (id) kCGImageSourceThumbnailMaxPixelSize : @(1264),
                                                                   (id) kCGImageSourceShouldCache : [NSNumber numberWithBool:false]
                                                                   };
            
            CGImageSourceRef src = CGImageSourceCreateWithData((CFDataRef)UIImageJPEGRepresentation(img, 0.8), NULL);
            
            CGImageRef ref = CGImageSourceCreateThumbnailAtIndex(src, 0, options);
            
            CFRelease(src);
            CGImageDestinationAddImage(destination, ref, (CFDictionaryRef)frameProperties);
            CGImageRelease(ref);
            
        }
    }
    
    CGImageDestinationSetProperties(destination, (__bridge CFDictionaryRef)fileProperties);    
    
    if (!CGImageDestinationFinalize(destination)) {
        CFRelease(destination);
        NSLog(@"failed to finalize image destination");
        return nil;
    }
    CFRelease(destination);
    
    
    
    NSLog(@"url=%@", fileURL);
    return fileURL;
}


+ (NSURL *)createVideoFromImageArray:(NSArray *)imageArray andDelayTime:(CGFloat)delayTime
{
    NSURL *videoUrl = nil;
    
    NSDictionary *settings = [CEMovieMaker videoSettingsWithCodec:AVVideoCodecH264 withWidth:640 andHeight:960];
    CEMovieMaker *movieMaker = [[CEMovieMaker alloc] initWithSettings:settings andDelayTime:delayTime];
    NSMutableArray *imgArray = [NSMutableArray array];
    for (NSDictionary *dict in imageArray)
    {
        UIImage *image = dict[@"image"];
        [imgArray addObject:image];
    }
    [movieMaker createMovieFromImages:imgArray withCompletion:^(NSURL *fileURL){
        NSLog(@"video urk: %@",fileURL);
    }];
    
    return videoUrl;
}

+ (void)removeDirectories
{
    NSError *error;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0]; // Get documents folder
    NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"/%@",kAPPFOLDERNAME]];
    
    BOOL isDir;
    if ([[NSFileManager defaultManager]fileExistsAtPath:dataPath isDirectory:&isDir])
    {
        [[NSFileManager defaultManager]removeItemAtPath:dataPath error:&error];
    }
}

+(void)createFolder
{
    NSError *error;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0]; // Get documents folder
    NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"/%@",kAPPFOLDERNAME]];
    
    [[NSFileManager defaultManager] createDirectoryAtPath:dataPath withIntermediateDirectories:NO attributes:nil error:&error]; //Create folder
    
    // if (![[NSFileManager defaultManager] fileExistsAtPath:dataPath])
    
}


+ (NSString *)abbreviateNumber:(NSInteger)num {
    
    NSString *abbrevNum;
    float number = (float)num;
    
    //Prevent numbers smaller than 1000 to return NULL
    if (num >= 1000) {
        NSArray *abbrev = @[@"K", @"M", @"B"];
        
        for (NSInteger i = abbrev.count - 1; i >= 0; i--) {
            
            // Convert array index to "1000", "1000000", etc
            int size = pow(10,(i+1)*3);
            
            if(size <= number) {
                // Removed the round and dec to make sure small numbers are included like: 1.1K instead of 1K
                number = number/size;
                NSString *numberString = [self floatToString:number];
                
                // Add the letter for the abbreviation
                abbrevNum = [NSString stringWithFormat:@"%@%@", numberString, [abbrev objectAtIndex:i]];
            }
            
        }
    } else {
        
        // Numbers like: 999 returns 999 instead of NULL
        abbrevNum = [NSString stringWithFormat:@"%d", (int)number];
    }
    
    return abbrevNum;
}

+ (NSString *) floatToString:(float) val {
    NSString *ret = [NSString stringWithFormat:@"%.1f", val];
    unichar c = [ret characterAtIndex:[ret length] - 1];
    
    while (c == 48) { // 0
        ret = [ret substringToIndex:[ret length] - 1];
        c = [ret characterAtIndex:[ret length] - 1];
        
        //After finding the "." we know that everything left is the decimal number, so get a substring excluding the "."
        if(c == 46) { // .
            ret = [ret substringToIndex:[ret length] - 1];
        }
    }
    
    return ret;
}

+ (NSDictionary *)animatedGIFWithData:(NSData *)data {
    if (!data) {
        return nil;
    }
    
    CGImageSourceRef source = CGImageSourceCreateWithData((__bridge CFDataRef)data, NULL);
    
    size_t count = CGImageSourceGetCount(source);
    
    NSMutableArray *images = [NSMutableArray array];
    
    NSTimeInterval duration = 0.0f;
    
    for (size_t i = 0; i < count; i++) {
        CGImageRef image = CGImageSourceCreateImageAtIndex(source, i, NULL);
        
        duration += [self frameDurationAtIndex:i source:source];
        
        UIImage *normalImage = [UIImage imageWithCGImage:image scale:[UIScreen mainScreen].scale orientation:UIImageOrientationUp];
        
        UIImage *waterMarkedImage = [CommonFunction watermarkImage:normalImage];        
        
        [images addObject:waterMarkedImage];
        
        CGImageRelease(image);
    }
    
    if (!duration) {
        duration = (1.0f / 10.0f) * count;
    }
    
    NSDictionary *dict = @{@"images":images, @"duration":[NSNumber numberWithFloat:duration]};
    
    CFRelease(source);
    
    return dict;
}

+ (float)frameDurationAtIndex:(NSUInteger)index source:(CGImageSourceRef)source {
    float frameDuration = 0.1f;
    CFDictionaryRef cfFrameProperties = CGImageSourceCopyPropertiesAtIndex(source, index, nil);
    NSDictionary *frameProperties = (__bridge NSDictionary *)cfFrameProperties;
    NSDictionary *gifProperties = frameProperties[(NSString *)kCGImagePropertyGIFDictionary];
    
    NSNumber *delayTimeUnclampedProp = gifProperties[(NSString *)kCGImagePropertyGIFUnclampedDelayTime];
    if (delayTimeUnclampedProp) {
        frameDuration = [delayTimeUnclampedProp floatValue];
    }
    else {
        
        NSNumber *delayTimeProp = gifProperties[(NSString *)kCGImagePropertyGIFDelayTime];
        if (delayTimeProp) {
            frameDuration = [delayTimeProp floatValue];
        }
    }
    
    // Many annoying ads specify a 0 duration to make an image flash as quickly as possible.
    // We follow Firefox's behavior and use a duration of 100 ms for any frames that specify
    // a duration of <= 10 ms. See <rdar://problem/7689300> and <http://webkit.org/b/36082>
    // for more information.
    
    if (frameDuration < 0.011f) {
        frameDuration = 0.100f;
    }
    
    CFRelease(cfFrameProperties);
    return frameDuration;
}

+(UIImage *)watermarkImage:(UIImage *)normalImage
{
    CGFloat imageScale = normalImage.scale;
    
    UIImage *watermarkImage = nil;
    
    UILabel *label = [waterMarkView viewWithTag:100];
    label.text = [NSUSERDEFAULTS objectForKey:kUSERID];
    label.text = [label.text uppercaseString];
    UIGraphicsBeginImageContextWithOptions(normalImage.size, NO, 0.0f);
    
    [normalImage drawInRect:CGRectMake(0, 0, normalImage.size.width, normalImage.size.height)];

    waterMarkView.frame = CGRectMake(0, 0, 8+24+8+label.intrinsicContentSize.width+8, 40);

    CGRect drawRect = CGRectMake(0, 0, (waterMarkView.frame.size.width * SCREEN_XScale)/imageScale, (waterMarkView.frame.size.height*SCREEN_XScale)/imageScale);
    CGPoint drawOrigin = drawRect.origin;
    drawOrigin.x = normalImage.size.width - drawRect.size.width - 8/imageScale;
    drawOrigin.y = 8/imageScale;
    drawRect.origin = drawOrigin;
    
    [waterMarkView drawViewHierarchyInRect:drawRect afterScreenUpdates:YES];
    
    watermarkImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return watermarkImage;
}

+ (NSData *)getWatermarkImageData
{
    CGFloat imageScale = [UIScreen mainScreen].scale;
    
    UIImage *watermarkImage = nil;
    
    UILabel *label = [waterMarkView viewWithTag:100];
    label.text = [NSUSERDEFAULTS objectForKey:kUSERID];
    label.text = [label.text uppercaseString];
    
    waterMarkView.frame = CGRectMake(0, 0, 8+24+8+label.intrinsicContentSize.width+8, 40);
    
    CGRect drawRect = CGRectMake(0, 0, (waterMarkView.frame.size.width * SCREEN_XScale)/imageScale, (waterMarkView.frame.size.height*SCREEN_XScale)/imageScale);
    
    UIGraphicsBeginImageContextWithOptions(drawRect.size, NO, 0.0f);
    
    [waterMarkView drawViewHierarchyInRect:drawRect afterScreenUpdates:YES];
    
    watermarkImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return UIImageJPEGRepresentation(watermarkImage, 1.0);
}

@end
