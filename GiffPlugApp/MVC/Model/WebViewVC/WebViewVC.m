//
//  WebViewVC.m
//  GiffPlugApp
//
//  Created by Sumit Sharma on 08/03/16.
//  Copyright © 2016 Kshitij Godara. All rights reserved.
//

#import "WebViewVC.h"

@interface WebViewVC ()<UIWebViewDelegate>
{
    
    IBOutlet UIWebView *wbView;
}
@end

@implementation WebViewVC
@synthesize strWbUrl;
- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (_webURL)
    {
        //Create a URL object.
        
        [wbView loadRequest:[NSURLRequest requestWithURL:_webURL]];
    }
    else
    {
        //Create a URL object.
        NSURL *url = [NSURL URLWithString:strWbUrl];
        
        [wbView loadRequest:[NSURLRequest requestWithURL:url]];
    }
    
    

    // Do any additional setup after loading the view from its nib.
}

     
     
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)backBtnClicked:(id)sender {
    [CommonFunction removeActivityIndicator];
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)webViewDidStartLoad:(UIWebView *)webView
{
    [CommonFunction showActivityIndicatorWithText:@""];
}
- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [CommonFunction removeActivityIndicator];
}

@end
