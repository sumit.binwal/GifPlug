//
//  KIHTTPRequestOperationManager.m
//  GiffPlugApp
//
//  Created by Santosh on 12/05/16.
//  Copyright © 2016 Kshitij Godara. All rights reserved.
//

#import "KIHTTPRequestOperationManager.h"

@implementation KIHTTPRequestOperationManager

+ (instancetype)manager {
    static id sharedInstance = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    
    return sharedInstance;
}

- (void)cancelAllHTTPOperationsWithPath:(NSString *)path {
    NSArray *operations = [KIHTTPRequestOperationManager manager].operationQueue.operations;
    for (AFHTTPRequestOperation *operation in operations) {
        NSString *url = [[operation.request.URL baseURL] absoluteString];
        NSRange range = [url rangeOfString:path];
        if (range.location != NSNotFound) {
            NSLog(@"Cancelled request");
            [operation cancel];
        }
    }
}

@end