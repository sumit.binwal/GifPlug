//
//  KIHTTPRequestOperationManager.h
//  GiffPlugApp
//
//  Created by Santosh on 12/05/16.
//  Copyright © 2016 Kshitij Godara. All rights reserved.
//

#import "AFHTTPRequestOperationManager.h"

@interface KIHTTPRequestOperationManager : AFHTTPRequestOperationManager

+ (instancetype)manager;
- (void)cancelAllHTTPOperationsWithPath:(NSString *)path;

@end