//
//  ShareSheetView.h
//  GiffPlugApp
//
//  Created by Santosh on 03/05/16.
//  Copyright © 2016 Kshitij Godara. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShareSheetView : UIView

- (void)updateFrameAndAnimation;
- (void)removeAnimationsAndFrame;

@end
