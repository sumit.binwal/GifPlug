//
//  AfterSearchView.m
//  GiffPlugApp
//
//  Created by Santosh on 02/04/16.
//  Copyright © 2016 Kshitij Godara. All rights reserved.
//

#import "AfterSearchView.h"
#import "AfterSearchTableCell.h"
#import "UserCollectionCell.h"
#import "GifCollectionCell.h"
#import "AppWebHandler.h"
#import "OtherUserProfileVC.h"
#import "SearchVC.h"
#import "KIHTTPRequestOperationManager.h"
@interface AfterSearchView ()<UITableViewDelegate,UITableViewDataSource,UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout>
{
    NSMutableArray *topGifsArray;
    NSMutableArray *recentGifsArray;
    NSMutableArray *usersArray;
}
@property (weak, nonatomic) IBOutlet UITableView *tableSearchView;

@end
@implementation AfterSearchView


- (void)dealloc
{
    NSLog(@"dealloced called for After search View...................");
}
- (void)awakeFromNib
{
    topGifsArray = [[NSMutableArray alloc]init];
    recentGifsArray = [[NSMutableArray alloc]init];
    usersArray = [[NSMutableArray alloc]init];
    self.tableSearchView.estimatedRowHeight = 44.0f;
    
    self.currentPageRecent = 1;
    self.totalPageRecent = 0;
    self.isPageRefreshingRecent = YES;
}

#pragma mark -
#pragma mark UITableView Methods
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 6;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.row)
    {
        case 0:
        {
            return 54.0f * SCREEN_XScale;// Text cell
        }
            break;
        case 1:
        {
            if (usersArray.count==0)
            {
                return 0;
            }
            return 110.0f * SCREEN_XScale;// User list cell
        }
            break;
        case 2:
        {
            return 64.0f * SCREEN_XScale;// Text cell
        }
            break;
        case 4:
        {
            return 64.0f * SCREEN_XScale;// Text cell
        }
            break;
        case 3:// Gif cell
        {
            
        }
        case 5:
        {
            return UITableViewAutomaticDimension;// Gif cell
        }
            break;
            
        default:
            break;
    }
    return 110.0f * SCREEN_XScale;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    AfterSearchTableCell *cell = nil;
    switch (indexPath.row)
    {
        case 0:
        {

        }
        case 2:
        {

        }
        case 4:
        {
            cell = [tableView dequeueReusableCellWithIdentifier:kCellAfterSearchIdentifierLabel];
            if (!cell)
            {
                cell = [[NSBundle mainBundle]loadNibNamed:NSStringFromClass([AfterSearchTableCell class]) owner:self options:nil][1];
                cell.cellType = CELL_TYPE_LABEL;
                [cell adjustViewAccordingToCellType];
            }
            cell.cellType = CELL_TYPE_LABEL;
        }
            break;
        case 1:
        {
            cell = [tableView dequeueReusableCellWithIdentifier:kCellAfterSearchIdentifier];
            if (!cell)
            {
                cell = [[NSBundle mainBundle]loadNibNamed:NSStringFromClass([AfterSearchTableCell class]) owner:self options:nil][0];
                cell.cellType = CELL_TYPE_COLLECTION_USERS;
                [cell adjustViewAccordingToCellType];
            }
            cell.cellType = CELL_TYPE_COLLECTION_USERS;
        }
            break;
            
        case 3:
        {
            cell = [tableView dequeueReusableCellWithIdentifier:kCellAfterSearchIdentifierGif];
            if (!cell)
            {
                cell = [[NSBundle mainBundle]loadNibNamed:NSStringFromClass([AfterSearchTableCell class]) owner:self options:nil][2];
                cell.cellType = CELL_TYPE_COLLECTION_GIF;
                [cell adjustViewAccordingToCellType];
            }
            cell.cellType = CELL_TYPE_COLLECTION_GIF;
        }
            break;
        case 5:
        {
            cell = [tableView dequeueReusableCellWithIdentifier:kCellAfterSearchIdentifierGifRecent];
            if (!cell)
            {
                cell = [[NSBundle mainBundle]loadNibNamed:NSStringFromClass([AfterSearchTableCell class]) owner:self options:nil][3];
                cell.cellType = CELL_TYPE_COLLECTION_GIF_RECENT;
                [cell adjustViewAccordingToCellType];
            }
            cell.cellType = CELL_TYPE_COLLECTION_GIF_RECENT;
        }
            break;
            
        default:
            break;
    }
    
    switch (indexPath.row)
    {
        case 0:
        {
            cell.labelHeader.text = usersArray.count>0?@"USERS":@"USERS (0 Result)";
        }
            break;
        case 2:
        {
            cell.labelHeader.text = topGifsArray.count>0?@"TOP GIFS":@"TOP GIFS (0 Result)";
        }
            break;
        case 4:
        {
            cell.labelHeader.text = recentGifsArray.count>0?@"RECENT":@"RECENT (0 Result)";
        }
            break;
        case 1:
        {
            
        }
        case 3:
        {
            
        }
        case 5:
        {
            if (!cell.collectionView.delegate)
            {
                cell.collectionView.delegate = self;
                cell.collectionView.dataSource = self;
            }
            [cell.collectionView reloadData];
        }
            break;
            
        default:
            break;
    }
    
    return cell;
}


- (void)tableView:(UITableView *)tableView willDisplayCell:(AfterSearchTableCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.row)
    {
        case 0:
        {
            cell.labelHeader.text = usersArray.count>0?@"USERS":@"USERS (0 Result)";
        }
            break;
        case 2:
        {
            cell.labelHeader.text = topGifsArray.count>0?@"TOP GIFS":@"TOP GIFS (0 Result)";
        }
            break;
        case 4:
        {
            cell.labelHeader.text = recentGifsArray.count>0?@"RECENT":@"RECENT (0 Result)";
        }
            break;
        case 1:
        {

        }
        case 3:
        {

        }
        case 5:
        {
            if (!cell.collectionView.delegate)
            {
                cell.collectionView.delegate = self;
                cell.collectionView.dataSource = self;
            }
            [cell.collectionView reloadData];
        }
            break;
            
        default:
            break;
    }
}

#pragma mark -
#pragma mark UICollectionView Methods

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (collectionView.tag == 102 || collectionView.tag == 103)
    {
        return CGSizeMake(104*SCREEN_XScale, 104*SCREEN_XScale);
    }
    return CGSizeMake(106.0f *SCREEN_XScale, collectionView.frame.size.height);
}


- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 1.0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 1.0;
}

// Layout: Set Edges
- (UIEdgeInsets)collectionView:
(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    if (collectionView.tag == 102 || collectionView.tag == 103)
    {
        return UIEdgeInsetsMake(1,3,1,3);
    }
    return UIEdgeInsetsMake(0,4,0,4);  // top, left, bottom, right
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if (collectionView.tag == 102)
    {
        return topGifsArray.count;
    }
    else if (collectionView.tag == 103)
    {
        return recentGifsArray.count;
    }
    return usersArray.count;
}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (collectionView.tag == 102)
    {
        GifCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:kCellGifCollectionIdentifier forIndexPath:indexPath];
        
        __weak typeof(cell) weakCell = cell;
        
        
        cell.gifImageView.contentMode = UIViewContentModeScaleAspectFill;
        
        NSString *imgGifUrl=topGifsArray[indexPath.row][@"post_image"];
        
        NSURL *url1 = [NSURL URLWithString:imgGifUrl];
        
        [cell.loader startAnimating];
        
        [cell.gifImageView sd_setImageWithURL:url1 completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            weakCell.gifImageView.image=image;
            weakCell.animatingImage = image;
            weakCell.singleImage = [UIImage imageWithData:UIImageJPEGRepresentation(image, 1)];
            [weakCell.loader stopAnimating];
            weakCell.loader.hidden = YES;
        }];
        
        return cell;
    }
    else if (collectionView.tag == 103)
    {
        GifCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:kCellGifCollectionIdentifier forIndexPath:indexPath];
        
        __weak typeof(cell) weakCell = cell;
        
        cell.gifImageView.contentMode = UIViewContentModeScaleAspectFill;
        
        NSString *imgGifUrl=recentGifsArray[indexPath.row][@"post_image"];
        
        NSURL *url1 = [NSURL URLWithString:imgGifUrl];
        
        [cell.loader startAnimating];
        
        [cell.gifImageView sd_setImageWithURL:url1 completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            weakCell.gifImageView.image=image;
            weakCell.animatingImage = image;
            weakCell.singleImage = [UIImage imageWithData:UIImageJPEGRepresentation(image, 1)];
            [weakCell.loader stopAnimating];
            weakCell.loader.hidden = YES;
        }];
        
        return cell;
    }
    UserCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:kCellUserCollectionIdentifier forIndexPath:indexPath];
    NSString *urlString = usersArray[indexPath.row][@"avatar_image"];
    if (urlString)
    {
        if ([urlString.pathExtension isEqualToString:@"gif"])
        {
            cell.userProfileImage.contentMode = UIViewContentModeScaleAspectFill;
        }
        else
        {
            cell.userProfileImage.contentMode = UIViewContentModeScaleToFill;
        }
        
        __weak typeof(cell) weakCell = cell;
        [cell.userProfileImage sd_setImageWithURL:[NSURL URLWithString:urlString] placeholderImage:nil options:0 completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            if (error)
            {
                cell.userProfileImage.contentMode = UIViewContentModeScaleToFill;
                [cell.userProfileImage setImage:[UIImage imageNamed:@"default.jpg"]];
            }
            [(UIActivityIndicatorView*)[weakCell.contentView viewWithTag:200] stopAnimating];
        }];
    }
    cell.userName.text = usersArray[indexPath.row][@"username"];
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewLayoutAttributes *attributes = [collectionView layoutAttributesForItemAtIndexPath:indexPath];
    
    CGRect cellRect = attributes.frame;
    
    CGRect cellFrameInSuperview = [collectionView convertRect:cellRect toView:[collectionView superview]];
    
    NSLog(@"*********location y : %f",cellFrameInSuperview.origin.y);
    
    if (collectionView.tag == 101)
    {
        if (self.delegate && [self.delegate respondsToSelector:@selector(afterSearchViewDidSelectedUserWithName:)])
        {
            [self.delegate afterSearchViewDidSelectedUserWithName:usersArray[indexPath.row][@"username"]];
        }
    }
    else if (collectionView.tag == 102)
    {
        if (self.delegate && [self.delegate respondsToSelector:@selector(afterSearchViewDidSelectedGifID:)])
        {
            [self.delegate afterSearchViewDidSelectedGifID:topGifsArray[indexPath.row][@"post_id"]];
        }
    }
    else
    {
        if (self.delegate && [self.delegate respondsToSelector:@selector(afterSearchViewDidSelectedGifID:)])
        {
            [self.delegate afterSearchViewDidSelectedGifID:recentGifsArray[indexPath.row][@"post_id"]];
        }
    }
}

- (void)setSearchDataDictionary:(NSDictionary *)searchDictionary
{
    if (searchDictionary && searchDictionary.allKeys.count>0)
    {
        [usersArray removeAllObjects];
        if (searchDictionary[@"users"])
        {
            [usersArray addObjectsFromArray:searchDictionary[@"users"]];
        }
        
        [topGifsArray removeAllObjects];
        if (searchDictionary[@"posts"])
        {
            [topGifsArray addObjectsFromArray:searchDictionary[@"posts"]];
        }
        
        [recentGifsArray removeAllObjects];
        if (searchDictionary[@"recent_posts"])
        {
            [recentGifsArray addObjectsFromArray:searchDictionary[@"recent_posts"]];
        }
        self.currentPageRecent = [searchDictionary[@"recent_current_page"] integerValue];
        self.totalPageRecent = [searchDictionary[@"recent_post_total_pages"] integerValue];
    }
    [self.tableSearchView reloadData];
}

- (void)resetView
{
    self.currentPageRecent=1;
    self.totalPageRecent=0;
    self.isPageRefreshingRecent = YES;
    [usersArray removeAllObjects];
    [topGifsArray removeAllObjects];
    [recentGifsArray removeAllObjects];
    [self.tableSearchView reloadData];
}


#pragma mark - FooterView Animating
-(void)animateFooterView
{
    
    [UIView animateWithDuration:1.0f delay:0.0f options:UIViewAnimationOptionRepeat|UIViewAnimationOptionCurveLinear
                     animations:^{
                         [_loaderImageView setTransform:CGAffineTransformRotate([_loaderImageView transform], M_PI-0.00001f)];
                     } completion:nil];
    
}

#pragma mark - UIScrollViewDelegate Method

-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (([self.tableSearchView contentOffset].y + self.tableSearchView.frame.size.height) >= [self.tableSearchView contentSize].height)
    {
        NSLog(@"scrolled to bottom");
        if (!_isPageRefreshingRecent) {
            
            if(_totalPageRecent <= _currentPageRecent)
            {
//                return;
            }
            else
            {
                _currentPageRecent++;
                _isPageRefreshingRecent = YES;
                [self animateFooterView];
                [self.tableSearchView setTableFooterView:_loaderFooter];
                NSDictionary *paramDict = @{@"page":[NSNumber numberWithInteger:_currentPageRecent], @"keyword":self.searchText};
                [self getResultsFromServerUsingParameters:paramDict];
            }
//            _isPageRefreshingRecent = YES;
//            [self animateFooterView];
//            [self.tableSearchView setTableFooterView:_loaderFooter];
//            NSDictionary *paramDict = @{@"page":[NSNumber numberWithInteger:_currentPageRecent], @"keyword":self.searchText};
//            [self getResultsFromServerUsingParameters:paramDict];
        }
    }
    
    [self updateCellsOnScroll];
}

- (void)updateCellsOnScroll
{
    NSArray* cells = self.tableSearchView.visibleCells;
    
    NSMutableArray *collectionArray = [NSMutableArray array];
    for (NSInteger i=0; i<cells.count; i++)
    {
        AfterSearchTableCell *cell = cells[i];
        if (cell.cellType == CELL_TYPE_COLLECTION_GIF_RECENT || cell.cellType == CELL_TYPE_COLLECTION_GIF)
        {
            [collectionArray addObject:cell.collectionView];
        }
    }
    
    for (NSInteger i=0; i<collectionArray.count; i++)
    {
        UICollectionView *collectionView = collectionArray[i];
        
        NSArray *visibleCells = collectionView.visibleCells;
        NSArray* indexPaths = collectionView.indexPathsForVisibleItems;
        
        NSUInteger cellCount = [visibleCells count];
        
        if (cellCount == 0) return;

        for (NSUInteger j = 0; j < cellCount; j++)
            [self checkVisibilityOfCell:[visibleCells objectAtIndex:j] forIndexPath:[indexPaths objectAtIndex:j]inCollection:collectionView];
    }
}

- (void)checkVisibilityOfCell:(GifCollectionCell *)cell forIndexPath:(NSIndexPath *)indexPath inCollection:(UICollectionView *)collectionView {
    UICollectionViewLayoutAttributes *attr = [collectionView layoutAttributesForItemAtIndexPath:indexPath];
    CGRect cellRect = attr.frame;
//    NSLog(@"*********** y position :%f",cellRect.origin.y);
    cellRect = [collectionView convertRect:cellRect toView:_tableSearchView.superview];
//    NSLog(@"*********** new y position :%f",cellRect.origin.y);
    BOOL completelyVisible = CGRectContainsRect(_tableSearchView.frame, cellRect);
    
    [cell notifyCellVisibleWithIsCompletelyVisible:completelyVisible];
}

- (void)getResultsFromServerUsingParameters:(NSDictionary *)paramDict
{
    NSString *url = [NSString stringWithFormat:@"posts/findByKeywords"];
    
    [[KIHTTPRequestOperationManager manager]cancelAllHTTPOperationsWithPath:url];
    
    NSMutableDictionary *header =[[NSMutableDictionary alloc]initWithObjectsAndKeys:@"application/json",@"Content-Type",[NSUSERDEFAULTS valueForKey:kUSERTOKEN],@"User-Token", nil];
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    
    [CommonFunction showActivityIndicatorWithText:@""];
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypePost withHttpHeaders:header withServiceName:url withParameters:[paramDict mutableCopy] withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"--responce dict %@",responseDict);
        
        self.isPageRefreshingRecent = NO;
        [CommonFunction removeActivityIndicator];
        if(responseDict==Nil){
            if (self.currentPageRecent>1)
            {
                self.currentPageRecent -=1;
            }
            [CommonFunction alertTitle:@"" withMessage:@"Server Error"];
        }
        else if([operation.response statusCode]==200)
        {
            if (responseDict[@"recent_posts"] && [responseDict[@"recent_posts"] isKindOfClass:[NSArray class]])
            {
                NSArray *array = responseDict[@"recent_posts"];
                if (array.count>0 && self.currentPageRecent>1)
                {
                    [recentGifsArray addObjectsFromArray:[array mutableCopy]];
                    [self.tableSearchView reloadData];
                }
            }
        }
        else
        {
            if (self.currentPageRecent>1)
            {
                self.currentPageRecent -=1;
            }
            [CommonFunction alertTitle:@"" withMessage:[responseDict objectForKey:@"msg"]];
        }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          if (self.currentPageRecent>1)
                                          {
                                              self.currentPageRecent -=1;
                                          }
                                          self.isPageRefreshingRecent = NO;
                                          [CommonFunction removeActivityIndicator];
                                          if (error.code == -999) {
                                              
                                          }
                                          else
                                          {
                                              [CommonFunction alertTitle:@"" withMessage:error.localizedDescription];
                                          }
                                      }];
}

@end
