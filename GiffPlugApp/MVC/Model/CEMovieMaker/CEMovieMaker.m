//
//  CEMovieMaker.m
//  CEMovieMaker
//
//  Created by Cameron Ehrlich on 9/17/14.
//  Copyright (c) 2014 Cameron Ehrlich. All rights reserved.
//

#import "CEMovieMaker.h"

typedef UIImage*(^CEMovieMakerUIImageExtractor)(NSObject* inputObject);

@implementation CEMovieMaker

- (instancetype)initWithSettings:(NSDictionary *)videoSettings andDelayTime:(CGFloat)delayTime;
{
    self = [self init];
    if (self) {
        NSError *error;
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths firstObject];
        NSString *tempPath = [documentsDirectory stringByAppendingFormat:@"/insta.mp4"];
        
        if ([[NSFileManager defaultManager] fileExistsAtPath:tempPath]) {
            [[NSFileManager defaultManager] removeItemAtPath:tempPath error:&error];
            if (error) {
                NSLog(@"Error: %@", error.debugDescription);
            }
        }
        
        _frameDelayTime = delayTime;
        _fileURL = [NSURL fileURLWithPath:tempPath];
        _assetWriter = [[AVAssetWriter alloc] initWithURL:self.fileURL
                                                 fileType:AVFileTypeMPEG4 error:&error];
        if (error) {
            NSLog(@"Error: %@", error.debugDescription);
        }
        NSParameterAssert(self.assetWriter);
        
        _videoSettings = videoSettings;
        _writerInput = [AVAssetWriterInput assetWriterInputWithMediaType:AVMediaTypeVideo
                                                          outputSettings:videoSettings];
        NSParameterAssert(self.writerInput);
        NSParameterAssert([self.assetWriter canAddInput:self.writerInput]);
        
        [self.assetWriter addInput:self.writerInput];
        
//        NSDictionary *bufferAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
//                                          [NSNumber numberWithInt:kCVPixelFormatType_32ARGB], kCVPixelBufferPixelFormatTypeKey, nil];
        
        _bufferAdapter = [[AVAssetWriterInputPixelBufferAdaptor alloc] initWithAssetWriterInput:self.writerInput sourcePixelBufferAttributes:nil];
        _frameTime = CMTimeMake(delayTime, 1);
    }
    return self;
}

- (void) createMovieFromImageURLs:(NSArray CE_GENERIC_URL*)urls withCompletion:(CEMovieMakerCompletion)completion;
{
    [self createMovieFromSource:urls extractor:^UIImage *(NSObject *inputObject) {
        return [UIImage imageWithData: [NSData dataWithContentsOfURL:((NSURL*)inputObject)]];
    } withCompletion:completion];
}

- (void) createMovieFromImages:(NSArray CE_GENERIC_IMAGE *)images withCompletion:(CEMovieMakerCompletion)completion;
{
    [self createMovieFromSource:images extractor:^UIImage *(NSObject *inputObject) {
        return (UIImage*)inputObject;
    } withCompletion:completion];
}

- (void) createMovieFromSource:(NSArray *)images extractor:(CEMovieMakerUIImageExtractor)extractor withCompletion:(CEMovieMakerCompletion)completion;
{
    self.completionBlock = completion;
    
    [self.assetWriter startWriting];
    [self.assetWriter startSessionAtSourceTime:kCMTimeZero];
    
    NSInteger multiplyFactor = 1;
    CGFloat prevTime = _frameDelayTime;
    while (prevTime<3.0f)
    {
        multiplyFactor++;
        prevTime = _frameDelayTime * multiplyFactor;
    }
    
    _frameDelayTime = prevTime;
    
    CGFloat perFrameDelay = _frameDelayTime/(images.count * multiplyFactor);
    
    CVPixelBufferRef sampleBuffer ;//= [self newPixelBufferFromCGImage:[img CGImage]];
    
    CMTime timeOffset = CMTimeMake(0,1000);
    
    while (multiplyFactor>0)
    {
        multiplyFactor--;
        for(int i = 0; i< images.count; i++)
        {
            while (self.bufferAdapter.assetWriterInput.readyForMoreMediaData == FALSE) {
                NSLog(@"Waiting inside a loop");
                NSDate *maxDate = [NSDate dateWithTimeIntervalSinceNow:0.1];
                [[NSRunLoop currentRunLoop] runUntilDate:maxDate];
            }
            
            UIImage* img = images[i];
            //Write samples:
            sampleBuffer = [self newPixelBufferFromImage:img];
            
            [self.bufferAdapter appendPixelBuffer:sampleBuffer withPresentationTime:timeOffset];
            
            CMTime addOffset = CMTimeMake(perFrameDelay * 1000, 1000);
            timeOffset = CMTimeAdd(timeOffset, addOffset);
        }
    }
    
    
    while (self.bufferAdapter.assetWriterInput.readyForMoreMediaData == FALSE) {
        NSLog(@"Waiting outside a loop");
        NSDate *maxDate = [NSDate dateWithTimeIntervalSinceNow:0.1];
        [[NSRunLoop currentRunLoop] runUntilDate:maxDate];
    }
    
//    sampleBuffer = pixelBufferFromCGImage(gAnalysisFrames[gAnalysisFrames.size()-1].frameImage, gWidth, gHeight);
//    [adaptor appendPixelBuffer:buffer withPresentationTime:timeOffset];
    
    [self.writerInput markAsFinished];
    [self.assetWriter endSessionAtSourceTime:timeOffset];
    [self.assetWriter finishWritingWithCompletionHandler:^{
        dispatch_async(dispatch_get_main_queue(), ^{
            self.completionBlock(self.fileURL);
        });
    }];
    
//    [self.writerInput requestMediaDataWhenReadyOnQueue:mediaInputQueue usingBlock:^{
//        while (YES){
//            if (i >= frameNumber) {
//                break;
//            }
//            if ([self.writerInput isReadyForMoreMediaData]) {
//                UIImage* img = extractor([images objectAtIndex:i]);
//                if (img == nil) {
//                    i++;
//                    NSLog(@"Warning: could not extract one of the frames");
//                    continue;
//                }
//                CVPixelBufferRef sampleBuffer = [self newPixelBufferFromCGImage:[img CGImage]];
//                
//                if (sampleBuffer) {
//                    if (i == 0) {
//                        [self.bufferAdapter appendPixelBuffer:sampleBuffer withPresentationTime:kCMTimeZero];
//                    }else{
//                        CMTime lastTime = CMTimeMake(i-1, self.frameTime.timescale);
//                        CMTime presentTime = CMTimeAdd(lastTime, self.frameTime);
//                        [self.bufferAdapter appendPixelBuffer:sampleBuffer withPresentationTime:presentTime];
//                    }
//                    CFRelease(sampleBuffer);
//                    i++;
//                }
//            }
//        }
//        
//        [self.writerInput markAsFinished];
//        [self.assetWriter finishWritingWithCompletionHandler:^{
//            dispatch_async(dispatch_get_main_queue(), ^{
//                self.completionBlock(self.fileURL);
//            });
//        }];
//        
//        CVPixelBufferPoolRelease(self.bufferAdapter.pixelBufferPool);
//    }];
}


- (CVPixelBufferRef)newPixelBufferFromImage:(UIImage *)image
{
    NSDictionary *options = [NSDictionary dictionaryWithObjectsAndKeys:
                             [NSNumber numberWithBool:YES], kCVPixelBufferCGImageCompatibilityKey,
                             [NSNumber numberWithBool:YES], kCVPixelBufferCGBitmapContextCompatibilityKey,
                             nil];
    
    CVPixelBufferRef pxbuffer = NULL;
    
    CGFloat frameWidth = [[self.videoSettings objectForKey:AVVideoWidthKey] floatValue];
    CGFloat frameHeight = [[self.videoSettings objectForKey:AVVideoHeightKey] floatValue];
    
    
    CVReturn status = CVPixelBufferCreate(kCFAllocatorDefault,
                                          frameWidth,
                                          frameHeight,
                                          kCVPixelFormatType_32ARGB,
                                          (__bridge CFDictionaryRef) options,
                                          &pxbuffer);
    
    NSParameterAssert(status == kCVReturnSuccess && pxbuffer != NULL);
    
    CVPixelBufferLockBaseAddress(pxbuffer, 0);
    void *pxdata = CVPixelBufferGetBaseAddress(pxbuffer);
    NSParameterAssert(pxdata != NULL);
    
    CGColorSpaceRef rgbColorSpace = CGColorSpaceCreateDeviceRGB();
    
    CGContextRef context = CGBitmapContextCreate(pxdata,
                                                 frameWidth,
                                                 frameHeight,
                                                 8,
                                                 4 * frameWidth,
                                                 rgbColorSpace,
                                                 (CGBitmapInfo)kCGImageAlphaNoneSkipFirst);
    NSParameterAssert(context);
    CGContextConcatCTM(context, CGAffineTransformIdentity);
    CGContextDrawImage(context, CGRectMake(0,
                                           0,
                                           CGImageGetWidth(image.CGImage),
                                           CGImageGetHeight(image.CGImage)),
                       image.CGImage);
    CGColorSpaceRelease(rgbColorSpace);
    CGContextRelease(context);
    
    CVPixelBufferUnlockBaseAddress(pxbuffer, 0);
    
    return pxbuffer;
}

+ (NSDictionary *)videoSettingsWithCodec:(NSString *)codec withWidth:(CGFloat)width andHeight:(CGFloat)height
{
    
    if ((int)width % 16 != 0 ) {
        NSLog(@"Warning: video settings width must be divisible by 16.");
    }
    
    NSDictionary *videoSettings = @{AVVideoCodecKey : AVVideoCodecH264,
                                    AVVideoWidthKey : [NSNumber numberWithInt:(int)width],
                                    AVVideoHeightKey : [NSNumber numberWithInt:(int)height]};
    
    return videoSettings;
}

@end
