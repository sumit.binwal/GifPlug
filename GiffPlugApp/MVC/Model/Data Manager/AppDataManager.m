//
//  AppDataManager.m
//  GiffPlugApp
//
//  Created by Santosh on 18/04/16.
//  Copyright © 2016 Kshitij Godara. All rights reserved.
//

#import "AppDataManager.h"

@implementation AppDataManager


#pragma mark -
#pragma mark Shared Handler
+(AppDataManager *)sharedInstance
{
    static AppDataManager *_dataHandler;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _dataHandler = [[self alloc]init];
    });
    return _dataHandler;
}


#pragma mark -
#pragma mark Init
-(instancetype)init
{
    if (self = [super init])
    {
        _globalHomeFeedArray = [[NSMutableArray alloc]init];
        _globalUserFeedArray = [[NSMutableArray alloc]init];
        _globalUserReplugArray = [[NSMutableArray alloc]init];
        _followersDataArray = [[NSMutableArray alloc]init];
    }
    return self;
}


- (void)onLogoutDataRemove
{
    if (_globalHomeFeedArray && _globalHomeFeedArray.count>0)
    {
        [self.globalHomeFeedArray removeAllObjects];
    }
    if (_globalUserFeedArray && _globalUserFeedArray.count>0)
    {
        [self.globalUserFeedArray removeAllObjects];
    }
    if (_globalUserReplugArray && _globalUserReplugArray.count>0)
    {
        [self.globalUserReplugArray removeAllObjects];
    }
    if (_followersDataArray && _followersDataArray.count>0)
    {
        [self.followersDataArray removeAllObjects];
    }
    if (_otherUserArrayGif && _otherUserArrayGif.count>0)
    {
        [self.otherUserArrayGif removeAllObjects];
    }
    if (_otherUserArrayReplug && _otherUserArrayReplug.count>0)
    {
        [self.otherUserArrayReplug removeAllObjects];
    }
}

@end
