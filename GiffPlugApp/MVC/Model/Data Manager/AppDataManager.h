//
//  AppDataManager.h
//  GiffPlugApp
//
//  Created by Santosh on 18/04/16.
//  Copyright © 2016 Kshitij Godara. All rights reserved.
//

#import <Foundation/Foundation.h>


#pragma mark -
#pragma mark -
#pragma mark CLASS INTERFACE
@interface AppDataManager : NSObject

#pragma mark -
#pragma mark Class Functions
+ (AppDataManager *)sharedInstance;

#pragma mark -
#pragma mark Properties
@property(nonatomic)NSMutableArray *globalHomeFeedArray;
@property(nonatomic)NSMutableArray *globalUserFeedArray;
@property(nonatomic)NSMutableArray *globalUserReplugArray;
@property(nonatomic)NSMutableArray *followersDataArray;

@property(nonatomic)NSMutableArray *otherUserArrayGif;
@property(nonatomic)NSMutableArray *otherUserArrayReplug;

#pragma mark -
#pragma mark Member Functions
- (void)onLogoutDataRemove;

@end
