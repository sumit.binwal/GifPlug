//
//  GuidedContentCollectionCell.h
//  GiffPlugApp
//
//  Created by Sumit Sharma on 21/01/16.
//  Copyright © 2016 Kshitij Godara. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GuidedContentCollectionCell : UICollectionViewCell
@property (strong, nonatomic) IBOutlet UIButton *cellBtnClicked;
@property (strong, nonatomic) IBOutlet UILabel *lblCateLbl;
@property (strong, nonatomic) IBOutlet UIImageView *imgVwCatImg;
@property (strong, nonatomic) IBOutlet UIView *vwBgVw;
@property (strong, nonatomic) IBOutlet UIView *vwBorderVw;

@end
