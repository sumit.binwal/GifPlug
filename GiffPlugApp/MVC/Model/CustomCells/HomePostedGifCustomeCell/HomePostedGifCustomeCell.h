//
//  PostedGifsCustomeCell.h
//  GiffPlugApp
//
//  Created by Sumit Sharma on 04/03/16.
//  Copyright © 2016 Kshitij Godara. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FLAnimatedImage.h"
#import "ResponsiveLabel.h"
#import "KILabel.h"

@protocol UserHandleDelegate;

@interface HomePostedGifCustomeCell : UITableViewCell

@property (nonatomic, weak) id<UserHandleDelegate>delegate;

@property (assign, nonatomic) float progress;
@property (strong,nonatomic)FLAnimatedImage *giffImage;
@property (strong, nonatomic) IBOutlet FLAnimatedImageView *imgVwGif;

@property (strong, nonatomic) IBOutlet UIImageView *imgCommentFillUnFill;
@property (strong, atomic) IBOutlet UIImageView *imgLikeUnlikeImg;
@property (strong, atomic) IBOutlet UIImageView *imgReplug;

@property (weak, nonatomic) IBOutlet KILabel *lblCaption;

@property (weak, nonatomic) IBOutlet UIProgressView *progresView;
@property (weak, atomic) IBOutlet UILabel *lblLikeCount;
@property (weak, atomic) IBOutlet UILabel *lblCommentCount;
@property (weak, atomic) IBOutlet UILabel *lblReplugCount;
@property (weak, nonatomic) IBOutlet UIButton *btnReplug;
@property (weak, nonatomic) IBOutlet UIButton *btnComment;
@property (weak, nonatomic) IBOutlet UIButton *btnLike;
@property (strong, nonatomic) IBOutlet UIImageView *imgLike;
@property (strong, nonatomic) IBOutlet UIButton *btnMoreClicked;
@property(strong,nonatomic)SDWebImageDownloader *imageDownloader;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *captionHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *captionContainerHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *captionTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *captionBottomtConstraint;



@property BOOL isHeartAnimationActive;
@property BOOL isCaptionOpened;
- (void)performHeartAnimation;
- (void)resetCaptionBar;
- (void)handleCaptionBar;

@end

@protocol UserHandleDelegate<NSObject>

@optional
- (void)customTableViewCellDidTapOnUserHandle:(NSString *)userHandle;
- (void)customTableViewCellDidTapHashTagHandle:(NSString *)userHandle;

@end

