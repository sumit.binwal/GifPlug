//
//  FilteredCollectionCell.m
//  GiffPlugApp
//
//  Created by Kshitij Godara on 12/01/16.
//  Copyright © 2016 Kshitij Godara. All rights reserved.
//

#import "FilteredCollectionCell.h"

@implementation FilteredCollectionCell
- (id)initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    
    if (self) {
        // Initialization code
        NSArray *arrayOfViews = [[NSBundle mainBundle] loadNibNamed:@"FilteredCollectionCell" owner:self options:nil];
        
        if ([arrayOfViews count] < 1) {
            return nil;
        }
        
        if (![[arrayOfViews objectAtIndex:0] isKindOfClass:[UICollectionViewCell class]]) {
            return nil;
        }
        
        self = [arrayOfViews objectAtIndex:0];
        
    }
    
    return self;
    
}
@end
