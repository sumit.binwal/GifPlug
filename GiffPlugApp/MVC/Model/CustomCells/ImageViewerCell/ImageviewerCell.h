//
//  ImageviewerCell.h
//  GiffPlugApp
//
//  Created by Santosh on 07/03/16.
//  Copyright © 2016 Kshitij Godara. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ImageviewerCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *gifImageView;
@property (strong, nonatomic) NSData *imageData;

@end
