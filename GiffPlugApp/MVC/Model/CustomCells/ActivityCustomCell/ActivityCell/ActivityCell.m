//
//  ActivityCell.m
//  GiffPlugApp
//
//  Created by Santosh on 31/03/16.
//  Copyright © 2016 Kshitij Godara. All rights reserved.
//

#import "ActivityCell.h"

@implementation ActivityCell

- (void)awakeFromNib {
    // Initialization code
    
    self.loader.hidden = YES;
    self.activityGifView.layer.cornerRadius = 5.0f * SCREEN_XScale;
    self.activityProfileView.layer.cornerRadius = self.activityProfileView.frame.size.width/2 *SCREEN_XScale;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
