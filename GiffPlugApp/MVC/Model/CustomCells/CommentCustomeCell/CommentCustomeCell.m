//
//  FollowUnfollowCustomeCell.m
//  GiffPlugApp
//
//  Created by Sumit Sharma on 29/01/16.
//  Copyright © 2016 Kshitij Godara. All rights reserved.
//

#import "CommentCustomeCell.h"

@implementation CommentCustomeCell


//-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
//{
//    self = [super initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"CommentCustomeCell"];
//    if (self) {
//    self=[[[NSBundle mainBundle] loadNibNamed:@"CommentCustomeCell" owner:self options:nil] objectAtIndex:0];
//    }
//
//    
//    return self;
//}


- (void)awakeFromNib
{
    [self configureCellForFirstUse];
}


- (void)configureCellForFirstUse
{
    self.loader.hidden=YES;
//    self.imgProfileImg.layer.cornerRadius=self.imgProfileImg.frame.size.width*SCREEN_XScale/2;
//    self.imgProfileImg.layer.borderColor=[UIColor colorWithRed:167.0f/255.0f green:167.0f/255.0f blue:167.0f/255.0f alpha:1].CGColor;
//    self.imgProfileImg.layer.borderWidth=1.0f;
//    self.imgProfileImg.clipsToBounds=YES;
    
    self.imageViewProfile.layer.cornerRadius=self.imageViewProfile.frame.size.width*SCREEN_XScale/2;
    self.imageViewProfile.layer.borderColor=[UIColor colorWithRed:167.0f/255.0f green:167.0f/255.0f blue:167.0f/255.0f alpha:1].CGColor;
    self.imageViewProfile.layer.borderWidth=1.0f;
    self.imageViewProfile.clipsToBounds=YES;
    
    CGFloat fontSizeBio = self.lblUserBio.font.pointSize;
    CGFloat fontSizeUser = self.lblUsername.font.pointSize;
    
    [self.lblUsername setFont:[UIFont fontWithName:self.lblUsername.font.fontName size:fontSizeUser*SCREEN_XScale]];
    [self.lblUserBio setFont:[UIFont fontWithName:self.lblUserBio.font.fontName size:fontSizeBio*SCREEN_XScale]];

}



- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
#pragma mark -

@end
