//
//  FollowUnfollowCustomeCell.h
//  GiffPlugApp
//
//  Created by Sumit Sharma on 29/01/16.
//  Copyright © 2016 Kshitij Godara. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ResponsiveLabel.h"

@protocol CustomTableViewCellDelegate;
@interface CommentCustomeCell : UITableViewCell
@property (nonatomic, weak) id<CustomTableViewCellDelegate>delegate;

@property (strong, nonatomic) IBOutlet UILabel *lblCommentTime;
@property (strong, nonatomic) IBOutlet UIButton *btnFollowUnfollow;
@property (strong, nonatomic) IBOutlet UILabel *lblUsername;
@property (strong, nonatomic) IBOutlet UILabel *lblComment;
@property (weak, nonatomic) IBOutlet UIButton *imgProfileImg;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewProfile;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loader;
@property (weak, nonatomic) IBOutlet UILabel *labelRequested;
@property (strong, nonatomic) IBOutlet ResponsiveLabel *lblUserBio;
//- (void)configureText:(NSString*)str forExpandedState:(BOOL)isExpanded;
@end
@protocol CustomTableViewCellDelegate<NSObject>

@optional

@end