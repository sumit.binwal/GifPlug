//
//  CategoryCell.h
//  GiffPlugApp
//
//  Created by Santosh on 28/03/16.
//  Copyright © 2016 Kshitij Godara. All rights reserved.
//

#import <UIKit/UIKit.h>

static NSString *const kCategoryCellIdentifier = @"kCategoryIdentifier";
@interface CategoryCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *categoryImageView;
@property (weak, nonatomic) IBOutlet UILabel *categoryName;
@end
