//
//  UserCollectionCell.h
//  GiffPlugApp
//
//  Created by Santosh on 02/04/16.
//  Copyright © 2016 Kshitij Godara. All rights reserved.
//

#import <UIKit/UIKit.h>

static NSString *const kCellUserCollectionIdentifier = @"kCellUserCollectionIdentifier";
@interface UserCollectionCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *userProfileImage;
@property (weak, nonatomic) IBOutlet UILabel *userName;

@end
