//
//  UserCollectionCell.m
//  GiffPlugApp
//
//  Created by Santosh on 02/04/16.
//  Copyright © 2016 Kshitij Godara. All rights reserved.
//

#import "UserCollectionCell.h"

@implementation UserCollectionCell

- (void)awakeFromNib {
    // Initialization code
    
    self.userProfileImage.layer.cornerRadius = (self.userProfileImage.frame.size.width/2) * SCREEN_XScale;
    [self viewWithTag:200].center = self.userProfileImage.center;
}

@end
