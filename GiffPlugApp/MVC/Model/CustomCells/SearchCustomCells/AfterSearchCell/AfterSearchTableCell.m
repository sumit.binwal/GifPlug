//
//  AfterSearchTableCell.m
//  GiffPlugApp
//
//  Created by Santosh on 02/04/16.
//  Copyright © 2016 Kshitij Godara. All rights reserved.
//

#import "AfterSearchTableCell.h"
#import "UserCollectionCell.h"
#import "GifCollectionCell.h"

@implementation AfterSearchTableCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)adjustViewAccordingToCellType
{
    switch (self.cellType)
    {
        case CELL_TYPE_COLLECTION_GIF:
        {
            [self.collectionView registerNib:[UINib nibWithNibName:NSStringFromClass([GifCollectionCell class]) bundle:nil] forCellWithReuseIdentifier:kCellGifCollectionIdentifier];
        }
            break;
            
        case CELL_TYPE_COLLECTION_GIF_RECENT:
        {
            [self.collectionView registerNib:[UINib nibWithNibName:NSStringFromClass([GifCollectionCell class]) bundle:nil] forCellWithReuseIdentifier:kCellGifCollectionIdentifier];
        }
            break;
            
        case CELL_TYPE_COLLECTION_USERS:
        {
            [self.collectionView registerNib:[UINib nibWithNibName:NSStringFromClass([UserCollectionCell class]) bundle:nil] forCellWithReuseIdentifier:kCellUserCollectionIdentifier];
        }
            break;
            
        case CELL_TYPE_LABEL:
        {
            
        }
            break;
            
        default:
            break;
    }
}

// THIS IS THE MOST IMPORTANT METHOD
//
// This method tells the auto layout
// You cannot calculate the collectionView content size in any other place,
// because you run into race condition issues.
// NOTE: Works for iOS 8 or later
- (CGSize)systemLayoutSizeFittingSize:(CGSize)targetSize withHorizontalFittingPriority:(UILayoutPriority)horizontalFittingPriority verticalFittingPriority:(UILayoutPriority)verticalFittingPriority {
    
    if (self.collectionView.tag == 102 || self.collectionView.tag == 103)
    {
        [self.collectionView reloadData];
        [self.collectionView layoutIfNeeded];

        return [self.collectionView.collectionViewLayout collectionViewContentSize];
    }
    
    return CGSizeZero;
}



@end
