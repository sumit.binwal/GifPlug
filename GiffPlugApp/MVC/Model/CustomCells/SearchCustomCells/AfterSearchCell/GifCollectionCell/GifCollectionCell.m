//
//  GifCollectionCell.m
//  GiffPlugApp
//
//  Created by Santosh on 02/04/16.
//  Copyright © 2016 Kshitij Godara. All rights reserved.
//

#import "GifCollectionCell.h"

@implementation GifCollectionCell

- (void)awakeFromNib {
    // Initialization code
    self.isViewAnimating = YES;
}
- (void)notifyCellVisibleWithIsCompletelyVisible:(BOOL)isVisible
{
    if (isVisible) {
        if (!_isViewAnimating)
        {
            _isViewAnimating = YES;
            [self.gifImageView setImage:_animatingImage];
        }
    }
    else
    {
        if (_isViewAnimating)
        {
            _isViewAnimating = NO;
            [self.gifImageView setImage:_singleImage];
        }
    }
}
@end
