//
//  RecentSearchCell.h
//  GiffPlugApp
//
//  Created by Santosh on 28/03/16.
//  Copyright © 2016 Kshitij Godara. All rights reserved.
//

#import <UIKit/UIKit.h>

static NSString *const kCellIdentifier = @"recentSearchCell";
@interface RecentSearchCell : UITableViewCell

- (void)updateNameWithText:(NSString *)usernameText;
- (NSString *)getRecentSearchText;
@end
