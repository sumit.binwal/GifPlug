//
//  TrendingCell.h
//  GiffPlugApp
//
//  Created by Santosh on 29/03/16.
//  Copyright © 2016 Kshitij Godara. All rights reserved.
//

#import <UIKit/UIKit.h>

static NSString *const kCellTrendingIdentifier = @"kCellTrendingIdentifier";
@interface TrendingCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UILabel *hashtagName;
@end
