//
//  UserSuggestCell.m
//  GiffPlugApp
//
//  Created by Santosh on 24/06/16.
//  Copyright © 2016 Kshitij Godara. All rights reserved.
//

#import "UserSuggestCell.h"

@implementation UserSuggestCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    _userImageView.layer.cornerRadius = (_userImageView.frame.size.width/2) * SCREEN_XScale;
    _userImageView.clipsToBounds=YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
