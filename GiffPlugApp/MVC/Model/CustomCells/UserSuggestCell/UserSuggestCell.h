//
//  UserSuggestCell.h
//  GiffPlugApp
//
//  Created by Santosh on 24/06/16.
//  Copyright © 2016 Kshitij Godara. All rights reserved.
//

#import <UIKit/UIKit.h>

static NSString *const kCellIdentifierSuggestUser = @"suggestUserCell";

@interface UserSuggestCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *userImageView;
@property (weak, nonatomic) IBOutlet UILabel *labelUsername;
@property (weak, nonatomic) IBOutlet UILabel *labelGeneralName;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintCenterUsername;

@end
