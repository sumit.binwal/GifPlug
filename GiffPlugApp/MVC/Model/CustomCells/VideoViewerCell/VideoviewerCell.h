//
//  ImageviewerCell.h
//  GiffPlugApp
//
//  Created by Santosh on 07/03/16.
//  Copyright © 2016 Kshitij Godara. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VideoviewerCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *gifImageView;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (strong, nonatomic) NSData *imageData;
@property (strong,nonatomic)NSString *fileUrl;

@end
