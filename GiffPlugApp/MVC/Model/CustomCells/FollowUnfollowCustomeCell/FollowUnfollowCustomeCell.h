//
//  FollowUnfollowCustomeCell.h
//  GiffPlugApp
//
//  Created by Sumit Sharma on 29/01/16.
//  Copyright © 2016 Kshitij Godara. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ResponsiveLabel.h"

@protocol CustomTableViewCellDelegate;
@interface FollowUnfollowCustomeCell : UITableViewCell
@property (nonatomic, weak) id<CustomTableViewCellDelegate>delegate;
@property (strong, nonatomic) IBOutlet UILabel *lblComment;

@property (strong, nonatomic) IBOutlet UIButton *btnFollowUnfollow;
@property (strong, nonatomic) IBOutlet UILabel *lblUsername;
@property (weak, nonatomic) IBOutlet UIButton *imgProfileImg;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewProfile;
@property (strong, nonatomic) IBOutlet ResponsiveLabel *lblUserBio;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loader;
@property (weak, nonatomic) IBOutlet UILabel *labelRequested;
@property (strong, nonatomic) IBOutlet UIButton *btnCancelRequest;
- (void)reset;
- (void)configureText:(NSString*)str forExpandedState:(BOOL)isExpanded;
@end
@protocol CustomTableViewCellDelegate<NSObject>

@optional
- (void)didTapOnMoreButton:(FollowUnfollowCustomeCell *)cell;
- (void)customTableViewCell:(FollowUnfollowCustomeCell *)cell didTapOnHashTag:(NSString *)hashTag;
- (void)customTableViewCell:(FollowUnfollowCustomeCell *)cell didTapOnUserHandle:(NSString *)userHandle;
- (void)customTableViewCell:(FollowUnfollowCustomeCell *)cell didTapOnURL:(NSString *)urlString;

@end