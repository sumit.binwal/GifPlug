//
//  FollowUnfollowCustomeCell.m
//  GiffPlugApp
//
//  Created by Sumit Sharma on 29/01/16.
//  Copyright © 2016 Kshitij Godara. All rights reserved.
//

#import "FollowUnfollowCustomeCell.h"
static NSString *kExpansionToken = @"...Read More";
static NSString *kCollapseToken = @"Read Less";
@implementation FollowUnfollowCustomeCell


//-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
//{
//    self = [super initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"FollowUnfollowCustomeCell"];
//    if (self) {
//    self=[[[NSBundle mainBundle] loadNibNamed:@"FollowUnfollowCustomeCell" owner:self options:nil] objectAtIndex:0];        
//    }
//
//    
//    return self;
//}

- (void)awakeFromNib {
    self.lblUserBio.userInteractionEnabled = YES;
    
    
    __weak typeof(self) weakSelf = self;
    PatternTapResponder urlTapAction = ^(NSString *tappedString) {
        if ([weakSelf.delegate respondsToSelector:@selector(customTableViewCell:didTapOnURL:)]) {
            [weakSelf.delegate customTableViewCell:weakSelf didTapOnURL:tappedString];
        }
    };
    [self.lblUserBio enableURLDetectionWithAttributes:@{NSForegroundColorAttributeName:[UIColor colorWithRed:43.0f/255.0f green:86.0f/255.0f blue:165.0f/255.0f alpha:1],
                                                         NSUnderlineStyleAttributeName:[NSNumber numberWithInt:1],
                                                         RLTapResponderAttributeName:urlTapAction}];
    
    [self configureCellForFirstUse];


}
- (void)configureText:(NSString*)str forExpandedState:(BOOL)isExpanded {
    NSMutableAttributedString *finalString;
    if (isExpanded) {
        NSString *expandedString = [NSString stringWithFormat:@"%@%@",str,kCollapseToken];
        finalString = [[NSMutableAttributedString alloc]initWithString:expandedString attributes:@{NSForegroundColorAttributeName:[UIColor blackColor]}];
        
        __weak typeof(self) weakSelf = self;
        PatternTapResponder tap = ^(NSString *string) {
            if ([weakSelf.delegate respondsToSelector:@selector(didTapOnMoreButton:)]) {
                [weakSelf.delegate didTapOnMoreButton:weakSelf];
            }
        };
        [finalString addAttributes:@{NSForegroundColorAttributeName:[UIColor blueColor],RLTapResponderAttributeName:tap}
                             range:[expandedString rangeOfString:kCollapseToken]];
        [finalString addAttributes:@{NSFontAttributeName:self.lblUserBio.font} range:NSMakeRange(0, finalString.length)];
        self.lblUserBio.numberOfLines = 0;
        [self.lblUserBio setAttributedText:finalString withTruncation:NO];
        
    }else {
        self.lblUserBio.numberOfLines = 3;
        [self.lblUserBio setText:str withTruncation:YES];
    }
}




- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)configureCellForFirstUse
{
    self.loader.hidden=YES;
    self.selectionStyle=UITableViewCellSelectionStyleNone;
    
//    self.imgProfileImg.layer.cornerRadius=(self.imgProfileImg.frame.size.width/2)*SCREEN_XScale;
//    self.imgProfileImg.layer.borderColor=[UIColor colorWithRed:167.0f/255.0f green:167.0f/255.0f blue:167.0f/255.0f alpha:1].CGColor;
//    self.imgProfileImg.layer.borderWidth=1.0f;
//    self.imgProfileImg.clipsToBounds=YES;
    
    self.imageViewProfile.layer.cornerRadius=(self.imageViewProfile.frame.size.width/2)*SCREEN_XScale;
    self.imageViewProfile.layer.borderColor=[UIColor colorWithRed:167.0f/255.0f green:167.0f/255.0f blue:167.0f/255.0f alpha:1].CGColor;
    self.imageViewProfile.layer.borderWidth=1.0f;
    self.imageViewProfile.clipsToBounds=YES;
    
    CGFloat fontSizeBio = 9;
    CGFloat fontSizeUser = 14;
    
    [self.lblUsername setFont:[UIFont fontWithName:self.lblUsername.font.fontName size:fontSizeUser*SCREEN_XScale]];
    [self.lblUserBio setFont:[UIFont fontWithName:self.lblUserBio.font.fontName size:fontSizeBio*SCREEN_XScale]];
}

- (void)reset
{
    self.loader.hidden=YES;
    self.btnCancelRequest.hidden = NO;
    self.btnFollowUnfollow.hidden = NO;
}

#pragma mark -

@end
