//
//  AppDelegate.m
//  GiffPlugApp
//
//  Created by Kshitij Godara on 08/01/16.
//  Copyright © 2016 Kshitij Godara. All rights reserved.
//

#import "AppDelegate.h"
#import "CreateGiffFirstVC.h"
#import "GuidedContentScreen.h"
#import "FollowUnFollowScreen.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import "SliderMenuVC.h"
#import "HomeFeedVC.h"
#import "SearchVC.h"
#import "AppWebHandler.h"
#import "CustomBadge.h"
#import "CEMovieMaker.h"

#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>
#import "PHAsset+Utility.h"

CIContext *globalCIContext;
SystemSoundID myAlertSound;
BOOL isSocketConnected;
NSInteger unReadCount;
NSString *appDeviceToken;
BOOL isInBackground;
UIView *waterMarkView;

@interface AppDelegate ()<TCPDelegate>

@end

@implementation AppDelegate

#pragma mark - Application Life Cycle


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    // Profile Gif Work.....
    
    [[UIApplication sharedApplication]setApplicationIconBadgeNumber:0];

    self.communication = [[TCPCommunication alloc] initWithBackgroundQueue:dispatch_queue_create([@"TCPCommunication" UTF8String], NULL)];
    _communication.delegate = self;
    
    [application setStatusBarOrientation:UIInterfaceOrientationPortrait animated:NO];
    self.window = [[UIWindow alloc]initWithFrame:[UIScreen mainScreen].bounds];

    [AppWebHandler sharedInstance];

    //This is for tab bar tint color
    
    [[UITabBar appearance] setTintColor:[UIColor colorWithRed:255.0f/255.0f green:33.0f/255.0f blue:87.0f/255.0f alpha:1]];
    
    NSLog(@"%@",[NSUSERDEFAULTS valueForKey:kISFOLLOWINGSUBMITTED]);
        NSLog(@"%@",[NSUSERDEFAULTS valueForKey:kISCATEGORYSUBMITTED]);
    
   

//    [Crittercism enableWithAppID:@"f83959d3308b4ee7983f3dd8c27f739600555300"];
    
    if ([NSUSERDEFAULTS valueForKey:kUSERTOKEN])
    {
        [APPDELEGATE.communication setUpCommunicationStream];
        [CommonFunction changeRootViewController:YES];
    }
    else
    {
        [CommonFunction changeRootViewController:NO];
//        UIApplicationShortcutItem *item1 = [[UIApplicationShortcutItem alloc]initWithType:@"Log In" localizedTitle:@"Log In"];
//        [[UIApplication sharedApplication]setShortcutItems:@[item1]];
    }
    _appTabBarController.delegate=self;
    
    if ([[[UIDevice currentDevice]systemVersion]floatValue]>=8.0f)
    {
        UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeAlert|UIUserNotificationTypeSound|UIUserNotificationTypeBadge) categories:nil];
        [[UIApplication sharedApplication]registerUserNotificationSettings:settings];
    }
    else
    {
        [[UIApplication sharedApplication] registerForRemoteNotifications];
    }
    
#if TARGET_OS_SIMULATOR
    
    appDeviceToken = @"fail";
#endif
    
    waterMarkView = [[NSBundle mainBundle]loadNibNamed:@"WatermarkView" owner:self options:nil][0];
    waterMarkView.layer.borderWidth = 3.0f;
    waterMarkView.layer.borderColor = [[UIColor whiteColor]colorWithAlphaComponent:0.5].CGColor;
    
//    [CommonFunction getWatermarkImageData];
//    [CommonFunction watermarkImage:[UIImage imageNamed:@"def"]];
    
    [[FBSDKApplicationDelegate sharedInstance] application:application
                             didFinishLaunchingWithOptions:launchOptions];
    
     [Fabric with:@[[Crashlytics class]]];
    
//    NSData *watermarkData = [CommonFunction getWatermarkImageData];
//    UIImage *image = [UIImage imageWithData:watermarkData];
    
//    NSDictionary *settings = [CEMovieMaker videoSettingsWithCodec:AVVideoCodecH264 withWidth:320 andHeight:320];
//    CEMovieMaker *maker = [[CEMovieMaker alloc]initWithSettings:settings andDelayTime:2.5];
//    NSMutableArray *array = [NSMutableArray array];
//    //            for (NSDictionary *dict in self.giffImagePartsArray) {
//    //                UIImage *image = dict[@"image"];
//    //                [array addObject:image];
//    //            }
//    [array addObject:[UIImage imageNamed:@"backBtn"]];
//    [array addObject:[UIImage imageNamed:@"brightnessIcon"]];
//    [array addObject:[UIImage imageNamed:@"brightnessIconActive"]];
//    [array addObject:[UIImage imageNamed:@"caegoryIconActive"]];
//    [array addObject:[UIImage imageNamed:@"Camera Action"]];
//    [maker createMovieFromImages:array withCompletion:^(NSURL *fileURL) {
//        NSLog(@"%@",fileURL);
//        
//        [PHAsset saveVideoAtURL:fileURL location:nil completionBlock:^(PHAsset *asset, BOOL success) {
//            [[PHImageManager defaultManager]requestAVAssetForVideo:asset options:nil resultHandler:^(AVAsset * _Nullable asset, AVAudioMix * _Nullable audioMix, NSDictionary * _Nullable info) {
//                NSLog(@"****** %@",asset);
//                NSLog(@"****** %@",info);
//            }];
//        }];
//        
//        
////        NSString *assetPath = fileURL.absoluteString;
//////        NSURL *instaURL = [NSURL URLWithString:[NSString stringWithFormat:@"instagram://camera"]];
////
////        NSURL *instaURL = [NSURL URLWithString:[NSString stringWithFormat:@"instagram://library?AssetPath=%@&InstagramCaption=hi",assetPath]];
////        if ([[UIApplication sharedApplication]canOpenURL:instaURL])
////        {
////            [[UIApplication sharedApplication]openURL:instaURL];
////        }
//    }];
    
    return YES;
}

- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings
{
    //register to receive notifications
    [application registerForRemoteNotifications];
}

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error
{
    
}

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    NSString *_deviceToken = @"";
    _deviceToken = [[[[deviceToken description]
                      stringByReplacingOccurrencesOfString: @"<" withString: @""]
                     stringByReplacingOccurrencesOfString: @">" withString: @""]
                    stringByReplacingOccurrencesOfString: @" " withString: @""];
    NSLog(@"deviceToken = %@",_deviceToken);
    
    appDeviceToken = _deviceToken;
    [NSUSERDEFAULTS setObject:appDeviceToken forKey:@"device_token"];
    [NSUSERDEFAULTS synchronize];
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    if ([NSUSERDEFAULTS valueForKey:kUSERTOKEN])
    {
        [_appTabBarController setSelectedIndex:3];
        [NSUSERDEFAULTS setInteger:_appTabBarController.selectedIndex forKey:@"previousSelectedVC"];
        [NSUSERDEFAULTS synchronize];
    }
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    isInBackground = YES;
    if ([NSUSERDEFAULTS valueForKey:kUSERTOKEN])
    {
        [APPDELEGATE.communication closeStream];
    }
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    isInBackground = NO;
    if ([NSUSERDEFAULTS valueForKey:kUSERTOKEN])
    {
        [APPDELEGATE.communication setUpCommunicationStream];
    }
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    [FBSDKAppEvents activateApp];
    [[UIApplication sharedApplication]setApplicationIconBadgeNumber:0];

}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    // Saves changes in the application's managed object context before the application terminates.
    isInBackground = YES;
    if ([NSUSERDEFAULTS valueForKey:kUSERTOKEN])
    {
        [APPDELEGATE.communication closeStream];
    }
    [self saveContext];
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    return [[FBSDKApplicationDelegate sharedInstance] application:application
                                                          openURL:url
                                                sourceApplication:sourceApplication
                                                       annotation:annotation
            ];
}


- (void)goToHomeFeed
{
    [[[_appTabBarController presentedViewController]navigationController]popToRootViewControllerAnimated:NO];
    [[_appTabBarController presentedViewController] dismissViewControllerAnimated:YES completion:nil];
    [_appTabBarController setSelectedIndex:0];
    [NSUSERDEFAULTS setInteger:_appTabBarController.selectedIndex forKey:@"previousSelectedVC"];
    [NSUSERDEFAULTS synchronize];
}

- (void)goToUserProfile
{
    [_appTabBarController setSelectedIndex:4];
    [NSUSERDEFAULTS setInteger:_appTabBarController.selectedIndex forKey:@"previousSelectedVC"];
    [NSUSERDEFAULTS synchronize];
}

#pragma mark - Background Event

- (void)application:(UIApplication *)application handleEventsForBackgroundURLSession:(NSString *)identifier completionHandler:(void (^)())completionHandler {
    [self setBackgroundSessionCompletionHandler:completionHandler];
}

#pragma mark - UITabbar Contorller

- (BOOL)tabBarController:(UITabBarController *)tabBarController shouldSelectViewController:(UIViewController *)viewController
{
    if (viewController.tabBarItem.tag==3) {
        if (isGifUploadedInProcess) {
            [CommonFunction alertTitle:@"GifPlug" withMessage:@"Please wait while uploading..."];
            return NO;
        }
    }
//    else if (viewController.tabBarItem.tag == 2 || viewController.tabBarItem.tag == 4)
//    {
//        [CommonFunction alertTitle:@"GifPlug" withMessage:@"next release..."];
//        return NO;
//    }
    return YES;
}

-(void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController
{
    
//    if (_appTabBarController.selectedIndex==0) {
//        
//        self.deckController =[[IIViewDeckController alloc]initWithNibName:@"HomeFeedVC" bundle:nil];
//        
//        SliderMenuVC *rightViewCntroller=[[SliderMenuVC alloc]init];
//        HomeFeedVC *mainViewCntroller=[[HomeFeedVC alloc]init];
//        self.deckController.rightController=rightViewCntroller;
//        self.deckController.centerController=mainViewCntroller;
////        [self.window setRootViewController:self.deckController];
//    }
   if (_appTabBarController.selectedIndex == 2) {

      // [CommonFunction alertTitle:@"" withMessage:@"This will be in next release."];

         _cgfv = [[CreateGiffFirstVC alloc] initWithNibName:@"CreateGiffFirstVC" bundle:nil];
       _cgfv.isOpenForProfilePurpose = NO;
        UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:_cgfv];
    
        [_appTabBarController presentViewController:navController animated:NO completion:nil];
        
        _cgfv = nil;
       
    }
    else
    {
        [NSUSERDEFAULTS setInteger:_appTabBarController.selectedIndex forKey:@"previousSelectedVC"];
        [NSUSERDEFAULTS synchronize];
    }

   

}
#pragma mark - Core Data stack

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

- (NSURL *)applicationDocumentsDirectory {
    // The directory the application uses to store the Core Data store file. This code uses a directory named "com.kipl.GiffPlugApp" in the application's documents directory.
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

- (NSManagedObjectModel *)managedObjectModel {
    // The managed object model for the application. It is a fatal error for the application not to be able to find and load its model.
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"GiffPlugApp" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
    // The persistent store coordinator for the application. This implementation creates and returns a coordinator, having added the store for the application to it.
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    // Create the coordinator and store
    
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"GiffPlugApp.sqlite"];
    NSError *error = nil;
    NSString *failureReason = @"There was an error creating or loading the application's saved data.";
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        // Report any error we got.
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        dict[NSLocalizedDescriptionKey] = @"Failed to initialize the application's saved data";
        dict[NSLocalizedFailureReasonErrorKey] = failureReason;
        dict[NSUnderlyingErrorKey] = error;
        error = [NSError errorWithDomain:@"YOUR_ERROR_DOMAIN" code:9999 userInfo:dict];
        // Replace this with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _persistentStoreCoordinator;
}


- (NSManagedObjectContext *)managedObjectContext {
    // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.)
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (!coordinator) {
        return nil;
    }
    _managedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
    [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    return _managedObjectContext;
}

-(NSString*) bv_jsonStringWithPrettyPrint:(BOOL) prettyPrint andDict:(NSDictionary *)dict
{
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dict
                                                       options:(NSJSONWritingOptions)    (prettyPrint ? NSJSONWritingPrettyPrinted : 0)
                                                         error:&error];
    
    if (! jsonData) {
        NSLog(@"bv_jsonStringWithPrettyPrint: error: %@", error.localizedDescription);
        return @"{}";
    } else {
        return [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
}

- (void)connectToSocket
{
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    NSString *userid = [NSUSERDEFAULTS objectForKey:kUSER_SID];
    [dict setObject:userid forKey:@"userId"];
    
    NSDictionary *mainDict = [NSDictionary dictionaryWithObjectsAndKeys:@"joinServer",@"command",dict,@"data", nil];
    NSString *myString = (NSString *) [APPDELEGATE bv_jsonStringWithPrettyPrint:NO andDict:mainDict];
    [_communication sendRequest:myString];
}


#pragma mark - Core Data Saving support

- (void)saveContext {
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        NSError *error = nil;
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}

#pragma mark - TCPCommunication Methods
-(void)dataReceived:(NSData *)response
{
    NSLog(@"dataReceived");
    NSError *error;
    NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:response options:kNilOptions error:&error];
    NSLog(@"dictResponse : %@", dictResponse);
    
    if (dictResponse && dictResponse.allKeys.count>0)
    {
        if ([dictResponse[@"replyCode"]integerValue] == 200)
        {
            BOOL flag = YES;
            NSString *command = dictResponse[@"command"];
            if ([command isEqualToString:@"joinServer"])
            {
                unReadCount = [dictResponse[@"unread_count"] integerValue];
                if (APPDELEGATE.appTabBarController.selectedIndex == 3)
                {
                    flag = NO;
                    unReadCount = 0;
                }
            }
            else
            {
                unReadCount ++;
                if (APPDELEGATE.appTabBarController.selectedIndex == 3)
                {
                    flag = NO;
                    unReadCount = 0;
                }
            }
            if (unReadCount == 0)
            {
                flag = NO;
            }
            [self showNotificationBadge:flag];
        }
    }
}

- (void)showNotificationBadge:(BOOL)showBadge
{
    if (APPDELEGATE.appTabBarController)
    {
        if (showBadge)
        {
            if ([APPDELEGATE.appTabBarController.tabBar viewWithTag:1000])
            {
                [[APPDELEGATE.appTabBarController.tabBar viewWithTag:1000]removeFromSuperview];
            }
            CustomBadge *badge = [CustomBadge customBadgeWithString:[NSString stringWithFormat:@"%ld",(long)unReadCount]];
            badge.center = CGPointMake([[UIScreen mainScreen]bounds].size.width/1.37, 0);
            badge.tag = 1000;
            [APPDELEGATE.appTabBarController.tabBar addSubview:badge];
        }
        else
        {
            if ([APPDELEGATE.appTabBarController.tabBar viewWithTag:1000])
            {
                [[APPDELEGATE.appTabBarController.tabBar viewWithTag:1000]removeFromSuperview];
            }
        }
    }
}

-(void)connectionClosed
{
    NSLog(@"connectionClosed");
    if (!isInBackground)
    {
        if ([NSUSERDEFAULTS valueForKey:kUSERTOKEN])
        {
            [APPDELEGATE.communication setUpCommunicationStream];
        }
    }
}

-(void)requestSent
{
    NSLog(@"requestSent");
}


@end
