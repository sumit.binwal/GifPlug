//
//  AppDelegate.h
//  GiffPlugApp
//
//  Created by Kshitij Godara on 08/01/16.
//  Copyright © 2016 Kshitij Godara. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "CreateGiffFirstVC.h"
#import "IIViewDeckController.h"
#import "TCPCommunication.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate,UITabBarControllerDelegate,UITabBarDelegate>

@property (copy, nonatomic) void (^backgroundSessionCompletionHandler)();

@property(strong,nonatomic)CreateGiffFirstVC *cgfv;

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic)  UITabBarController *appTabBarController;
@property (strong, nonatomic)  UINavigationController *navigationController;

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;
@property(strong,nonatomic)IIViewDeckController *deckController;
@property (nonatomic,strong)NSOperationQueue *socialQueue;
@property (nonatomic,strong) TCPCommunication *communication;

- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;

- (void)goToHomeFeed;
- (void)goToUserProfile;
-(NSString*) bv_jsonStringWithPrettyPrint:(BOOL) prettyPrint andDict:(NSDictionary *)dict;
- (void)connectToSocket;
- (void)showNotificationBadge:(BOOL)showBadge;

@end

FOUNDATION_EXPORT CIContext *globalCIContext;
FOUNDATION_EXPORT SystemSoundID myAlertSound;
FOUNDATION_EXPORT BOOL isSocketConnected;
FOUNDATION_EXPORT NSInteger unReadCount;
FOUNDATION_EXPORT NSString *appDeviceToken;
FOUNDATION_EXPORT BOOL isInBackground;
FOUNDATION_EXPORT UIView *waterMarkView;
