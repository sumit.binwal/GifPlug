//
//  Config.h
//  Metal Calculator
//
//  Created by Sumit Sharma on 26/11/14.
//  Copyright (c) 2014 Konstant Infosolutions Pvt Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Config : NSObject
extern  NSString		*serverURL;
#define IS_HEIGHT_GTE_568 [[UIScreen mainScreen ] bounds].size.height >= 568.0f
extern NSString		*serverHelpURL;
extern  NSString        *imageAgent;
//extern NSString *pushDeviceToken;
extern  float           CurrentLatitude;
extern  float           CurrentLongitude;
extern int wbServiceCount;
extern BOOL isGifUploadedInProcess;
extern NSString *const TCP_SERVER_IP;
extern int const TCP_SERVER_PORT;
/**
 Device tocken for push notification
 */


@end
