//
//  TCPCommunication.h
//  TCPWebserviceConsumption
//
//  Created by Ghanshyam on 08/07/14.
//  Copyright (c) 2014 Ghanshyam. All rights reserved.
//

#import <Foundation/Foundation.h>


@protocol TCPDelegate <NSObject>

@optional
-(void)dataReceived:(NSData *)response;
-(void)connectionClosed;
-(void)requestSent;

@end

@interface TCPCommunication : NSObject<NSStreamDelegate>{
    NSMutableData      *data;
    NSMutableData      *containerData;
    BOOL               processing;
}

@property (nonatomic,strong)  NSString           *lastRequest;
@property (nonatomic,strong)  NSString           *lastRequestImage;
@property (nonatomic,strong)  dispatch_queue_t   backgroundQueue;
@property (nonatomic,weak)    id<TCPDelegate>    delegate;
@property (nonatomic,strong)  NSInputStream      *inputStream;
@property (nonatomic,strong)  NSOutputStream     *outputStream;
@property(assign)int checkIndex;

-(id)initWithBackgroundQueue:(dispatch_queue_t)queue;
-(void)setUpCommunicationStream;
-(void)sendRequest:(NSString *)request;
-(void)closeStream;
-(void)releaseData;
-(void)sendRequestImageData:(NSString *)request;
-(void)closeStreamWithZero;
@end
